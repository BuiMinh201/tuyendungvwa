import styled from "styled-components";
import Image from "next/image";
const SubBanner = () => {
  return (
    <SubBannerWrapper>
      <div className={"bg-banner py-[40px]"}>
        <div className="container mx-auto px-[20px] xl:px-0 ">
          <div className=" grid lg:grid-cols-3 grid-cols-1 gap-[30px] wow fadeInUp">
            <div className="item p-[32px]">
              <Image
                src="/images/Banner/program.svg"
                alt={"icon"}
                width={60}
                height={60}
              />
              <div className="title mt-[18px]">Chương trình</div>
              <div className="description mt-[8px]">
                Chương trình giảng dạy phù hợp với xu thế công nghệ 4.0 hiện giờ
              </div>
            </div>
            <div className="item p-[32px]">
              <Image
                src="/images/Banner/safely.svg"
                alt={"icon"}
                width={60}
                height={60}
              />
              <div className="title mt-[18px]">An toàn</div>
              <div className="description mt-[8px]">
                Đảm bảo 100% về chất lượng giảng dạy và đầu ra khóa học
              </div>
            </div>
            <div className="item p-[32px]">
              <Image
                src="/images/Banner/award.svg"
                alt={"icon"}
                width={60}
                height={60}
              />
              <div className="title mt-[18px]">Chuyên nghiệp</div>
              <div className="description mt-[8px]">
                Sự chuyên nghiệp trong giảng dạy và cách xử lý của nhà trường
              </div>
            </div>
          </div>
        </div>
      </div>
    </SubBannerWrapper>
  );
};
const SubBannerWrapper = styled.div`
  .bg-banner {
    background-image: url("/images/Banner/bg-banner.png");
  }
  .item {
    background-color: #ffffff;
    .title {
      color: #1d1d1f;
      font-family: "Exo 2", sans-serif;
      font-size: 20px;
      font-style: normal;
      font-weight: 700;
      line-height: normal;
      text-transform: capitalize;
    }
    .description {
      color: rgba(29, 29, 31, 0.8);
      font-family: "Exo 2", sans-serif;
      font-size: 16px;
      font-style: normal;
      font-weight: 400;
      line-height: normal;
    }
  }
`;
export default SubBanner;
