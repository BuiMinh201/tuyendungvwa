import { DatePicker } from "antd";
import moment, { Moment } from "moment";

interface IProps {
  placeholder?: string;
  onChange?: (arg: any) => any;
  value?: any;
  allowClear?: boolean;
  disabled?: boolean;
  /**
   * Format lưu lại, mặc định: ISOString
   */
  saveFormat?: string;
  format?: string;
}
const MyDatePicker = (props: IProps) => {
  const { placeholder, onChange, value, allowClear, disabled, saveFormat } =
    props;
  const format = props?.format ?? "DD/MM/YYYY";
  let objMoment: any = undefined;
  if (props.value && typeof props.value == "string")
    objMoment = moment(props.value, saveFormat);
  else objMoment = props?.value;

  const handleChange = (value: Moment | null) => {
    if (props.onChange)
      if (value)
        props.onChange(
          saveFormat ? value?.format(props?.saveFormat) : value.toISOString()
        );
      else props.onChange(null);
  };

  return (
    <>
      <DatePicker
        format={format}
        disabled={disabled}
        allowClear={allowClear}
        value={objMoment}
        style={{ width: "100%" }}
        placeholder={placeholder}
        //@ts-ignore
        onChange={handleChange}
      />
    </>
  );
};
export default MyDatePicker;
