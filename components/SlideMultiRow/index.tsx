// @ts-ignore
import Slider from "react-slick";
import React, { ReactNode, useImperativeHandle, useRef } from "react";
import styled from "styled-components";

interface IProps {
  showArrow?: boolean;
  children: ReactNode;
}
const SlideMultiRow = React.forwardRef((props: IProps, ref) => {
  const SliderRef = useRef(null);
  const settings = {
    // className: "center",
    centerMode: false,
    infinite: true,
    // centerPadding: "60px",
    slidesToShow: 1,
    speed: 500,
    // rows: 2,
    // slidesPerRow: 4
  };
  const handleNext = () => {
    if (SliderRef && SliderRef.current) {
      //@ts-ignore
      SliderRef?.current?.slickNext();
    }
  };
  const handlePrev = () => {
    if (SliderRef && SliderRef.current) {
      //@ts-ignore
      SliderRef?.current?.slickPrev();
    }
  };
  useImperativeHandle(ref, () => ({
    handleNext: () => handleNext(),
    handlePrev: () => handlePrev(),
  }));
  return (
    <SlideMultiRowWrapper>
      <Slider {...settings} ref={SliderRef}>
        {props?.children}
      </Slider>
      {props.showArrow && (
        <div className="flex justify-center mt-[30px]">
          <button
            className="hidden lg:flex next-btn mr-[16px]  items-center justify-center  z-50"
            onClick={handlePrev}
          >
            <img
              className={"w-[36px]"}
              src="/images/Banner/arrow-left.svg"
              alt={"image"}
            />
          </button>
          <button
            className="hidden lg:flex prev-btn  items-center justify-center  z-50"
            onClick={handleNext}
          >
            <img
              className={"w-[36px]"}
              src="/images/Banner/arrow-right.svg"
              alt={"image"}
            />
          </button>
        </div>
      )}
    </SlideMultiRowWrapper>
  );
});

const SlideMultiRowWrapper = styled.div`
  .slick-slide > div {
    margin: 0 10px;
  }
  .slick-list {
    margin: 0 -10px;
  }
`;
export default SlideMultiRow;
