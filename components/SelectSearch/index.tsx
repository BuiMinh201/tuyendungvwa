import styled from "styled-components";
import SelectSearch from "react-select-search";
import {Select} from "antd";

interface IProps {
  option: SelectSearchOption[];
  onChange: (val: any) => void;
  value: any;
  style?:React.CSSProperties;
  placeholder: string;
  emptyMessage?: string;
  id?: string;
  search?: boolean;
}
type SelectSearchOption = {
  name?: string;
  label: string;

  value?: string | number;
  type?: string;
  items?: SelectSearchOption[];
  disabled?: boolean;
  [key: string]: any;
};
const SelectSearchFake = (props: IProps) => {
  return (
    <DropdownWrapper className={'relative w-full'}>
      {/*<SelectSearch*/}
      {/*  id={props?.id}*/}
      {/*  // className={"w-full"}*/}
      {/*  onChange={props.onChange}*/}
      {/*  value={props.value}*/}
      {/*  placeholder={props.placeholder}*/}
      {/*  options={props.option}*/}
      {/*  search={props.search}*/}
      {/*  emptyMessage={props?.emptyMessage??'Không có dữ liệu'}*/}
      {/*/>*/}
      {/*<div className={'absolute right-[10px] top-[10px] w-[20px] h-[20px]'}>*/}
      {/*  <div className='arrow w-[12px] h-[12px]'>*/}
      {/*    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-[16px] h-[16px]">*/}
      {/*      <path strokeLinecap="round" strokeLinejoin="round" d="M19.5 8.25l-7.5 7.5-7.5-7.5" />*/}
      {/*    </svg>*/}

      {/*  </div>*/}
      {/*</div>*/}
      <Select
        showSearch
        style={props?.style??{ width: '100%' }}
        placeholder={props?.placeholder}
        optionFilterProp="children"
        onChange={props?.onChange}
        allowClear={true}
        filterOption={(input, option) => (option?.label ?? '')?.toLowerCase()?.includes(input?.toLowerCase())}
        filterSort={(optionA, optionB) =>
          (optionA?.label ?? '')?.toLowerCase().localeCompare((optionB?.label ?? '')?.toLowerCase())
        }
        options={props?.option}
        dropdownRender={(menu) => (
          <div>
            {menu}
          </div>
        )}
      />
    </DropdownWrapper>
  );
};
const DropdownWrapper = styled.div`
  .arrow{
    content: "\\e902";
  }
  .Dropdown-control {
    background: #ffffff;
    //border-radius: 8px;
    border: 0.5px solid rgba(0, 0, 0, 0.4);
    width: 100%;
    height: 100%;
    //border: none;
    padding: 0.75rem 0.5rem;
    .Dropdown-arrow {
      top: 25px;
    }
  }
  .select-search-row{
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
  }
  .ant-select-selector{
    height: 36px!important;
    border-radius: 2px;
    border: 0.5px solid rgba(0,0,0,0.4)!important;
    
  }
  .ant-select-selection-search-input{
    height: 36px!important;
  
  }
  input{
    border: none;
    &:focus{
    border-color:transparent ;
      outline: none!important;
    }
  }
  input[type='search']:focus{
    border-color:transparent ;
    outline: none!important;
    --tw-ring-color: transparent!important;
    box-shadow: none!important;
  }
 
`;
export default SelectSearchFake;
