import styled from "styled-components";
import moment from "moment";
interface IProp{
  time:string
  title:string
}
const CardDotTuyenDung = (props:IProp) => {
  const {time,title}=props
  return (
    <CardDotTuyenDungWrapper className="cursor-pointer">
      <div className="image overflow-hidden">
        <img className={'hover:scale-125 transform transition duration-y'} src="/images/vi-tri.png" alt="image" />
      </div>
      <div className="date-time mt-[16px]">{moment(time).format('DD/MM/YYYY')}</div>
      <div className="content-card mt-[8px] hover:text-[#2387e3] text-[#0e0e0e]">
        {title}
      </div>
    </CardDotTuyenDungWrapper>
  );
};

const CardDotTuyenDungWrapper = styled.div`
  .image {
    border-radius: 8px;
  }
  .date-time {
    color: var(--Normal-gray, #b2b5c1);
    font-family: Arial;
    font-size: 15px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
  }
  .content-card {
  
    text-align: justify;
    font-family: Arial;
    font-size: 16px;
    font-style: normal;
    font-weight: 700;
    line-height: 125%; /* 20px */
  }
`;
export default CardDotTuyenDung;
