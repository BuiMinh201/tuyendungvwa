import styled from "styled-components";
import Title from "../Title";
import CardDotTuyenDung from "./components/CardDotTuyenDung";
import { useEffect, useRef, useState } from "react";
import { ip3 } from "../../service/ip";
import { useRouter } from "next/router";
import SlideComponents from "../Slide";
import NoData from "../NoData";
import axios from "../../service/axios";
const DotTuyenDung = () => {
  const router = useRouter();
  const [dataDot, setDataDot] = useState<DotTuyenDung.IRecord[]>([]);
  const [keyword, setKeyWord] = useState<string>();
  const slideRef = useRef(null);
  const getDotTuyenDung = async (key?: string) => {
    try {
      const res = await axios.get(`${ip3}/dot-tuyen-dung-public/page`, {
        params: {
          page: 1,
          limit: 8,
          "filters[]": JSON.stringify({
            field: "tenDotTuyenDung",
            operator: "contain",
            values: [key ? key : ""],
          }),
        },
      });
      if (res) {
        setDataDot(res?.data?.data?.result ?? []);
      }
    } catch (e) {
      console.log(e);
    }
  };
  useEffect(() => {
    getDotTuyenDung(keyword);
  }, [keyword]);
  return (
    <DotTuyenDungWrapper>
      <div
        className="container mx-auto py-[50px] px-[20px] xl:px-0"
        id={"dot-tuyen-dung"}
      >
        <Title
          type={"primary"}
          title={"Đợt tuyển dụng"}
          handleSearch={(keyword) => {
            setKeyWord(keyword);
          }}
          hideArrows={dataDot?.length<3}
          handleNext={() => {
            if (slideRef.current) {
              // @ts-ignore
              slideRef?.current?.handleNext();
            }
          }}
          handlePrev={() => {
            if (slideRef.current) {
              // @ts-ignore
              slideRef?.current?.handlePrev();
            }
          }}
        />
        {dataDot?.length > 0 ? (
          <>
            {" "}
            <div className="mt-[28px]">
              {dataDot?.length >= 3 ? (
                <>
                  {" "}
                  <SlideComponents slidesToShow={3} auto ref={slideRef}>
                    {dataDot?.map((val) => {
                      return (
                        <div
                          className="col-span-1"
                          onClick={() => {
                            router.push(`/dot-tuyen-dung/${val?._id}`);
                          }}
                        >
                          <CardDotTuyenDung
                            title={val?.tenDotTuyenDung}
                            time={val?.thoiGianBDNhanHoSo}
                          />
                        </div>
                      );
                    })}
                  </SlideComponents>
                </>
              ) : (
                <>
                  {" "}
                  <div className="grid xl:grid-cols-3 md:grid-cols-2 grid-cols-1 gap-[20px] mt-[28px]">
                    {dataDot?.map((val) => {
                      return (
                        <div
                          className="col-span-1"
                          onClick={() => {
                            router.push(`/dot-tuyen-dung/${val?._id}`);
                          }}
                        >
                          <CardDotTuyenDung
                            title={val?.tenDotTuyenDung}
                            time={val?.thoiGianBDNhanHoSo}
                          />
                        </div>
                      );
                    })}
                  </div>
                </>
              )}
            </div>
          </>
        ) : (
          <>
            <NoData title={"Đợt tuyển dụng hiện tại không có dữ liệu"} />
          </>
        )}
      </div>
    </DotTuyenDungWrapper>
  );
};

const DotTuyenDungWrapper = styled.div`
  background: #f7f7f7;
`;
export default DotTuyenDung;
