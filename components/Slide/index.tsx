// @ts-ignore
import Slider from "react-slick";

import React, {ReactNode, useImperativeHandle, useRef} from "react";
import styled from "styled-components";

interface IProps {
  showArrow?: boolean;
  showTitle?: boolean;
  infinite?: boolean;
  auto?: boolean;
  title?: string;
  children: ReactNode;
  slidesToShow?: number;
}
const SlideComponents = React.forwardRef((props: IProps, ref) => {
  const SliderRef = useRef(null);
  const settings = {
    centerMode: false,
    infinite: props?.infinite ?? false,
    slidesToShow: props?.slidesToShow ?? 3,
    speed: 500,
    dot: false,
    arrows: false,
    autoplay: props?.auto ?? false,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          // centerMode: true,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 600,
        settings: {
          autoplay: true,
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 480,
        settings: {
          autoplay: true,
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  const handleNext = () => {
    if (SliderRef && SliderRef.current) {
      //@ts-ignore
      SliderRef?.current?.slickNext();
    }
  };
  const handlePrev = () => {
    if (SliderRef && SliderRef.current) {
      //@ts-ignore
      SliderRef?.current?.slickPrev();
    }
  };
  useImperativeHandle(ref, () => ({
    handleNext: () => handleNext(),
    handlePrev: () => handlePrev(),
  }));
  return (
    <SlideMultiRowWrapper>
      {props.showArrow && (
        <div className="flex justify-between items-center mb-[30px]">
          <div>
            <div className="flex justify-center mt-[30px]">
              <button
                className="hidden lg:flex next-btn mr-[16px]  items-center justify-center  z-50"
                onClick={handlePrev}
              >
                <img
                  className={"w-[36px]"}
                  src="/images/banner/arrow-left.svg"
                  alt={"image"}
                />
              </button>
              <button
                className="hidden lg:flex prev-btn  items-center justify-center  z-50"
                onClick={handleNext}
              >
                <img
                  className={"w-[36px]"}
                  src="/images/banner/arrow-right.svg"
                  alt={"image"}
                />
              </button>
            </div>
          </div>
        </div>
      )}
      <div>
        <Slider {...settings} ref={SliderRef}>
          {props?.children}
        </Slider>
      </div>
    </SlideMultiRowWrapper>
  );
});

const SlideMultiRowWrapper = styled.div`
  .title-detail {
    font-family: "Exo 2", sans-serif;
    font-style: normal;
    font-weight: 700;
    font-size: 24px;
    line-height: 32px;

    color: #212529;
  }
  .slick-slide > div {
    margin: 0 10px;
  }
  .slick-list {
    margin: 0 -10px;
  }
`;
export default SlideComponents;
