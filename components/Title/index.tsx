import styled from "styled-components";
import { useEffect, useRef, useState } from "react";
import { Input } from "antd";

interface IProps {
  title: string;
  title2?: string;
  align?: "center" | "left" | "right";
  descriptionAlign?: "center" | "left" | "right";
  description?: string;
  uppercase?: boolean;
  type?: "primary" | "default";
  titleColor?: string;
  title2Color?: string;
  descriptionColor?: string;
  hideSearch?: boolean;
  hideArrows?: boolean;
  handleNext?: () => void;
  handlePrev?: () => void;
  handleSearch?: (keyword: string) => void;
}
const Title = (props: IProps) => {
  const {
    title,
    uppercase=true,
    hideSearch,
    align,
    handlePrev,
    handleNext,
    handleSearch,
    hideArrows,
    type,
  } = props;
  const [toggle, setToggle] = useState<boolean>(false);
  const inputRef = useRef<HTMLDivElement>(null);
  const onSearch = (val: any) => {
    handleSearch && handleSearch(val);
  };

  const handleClickOutsideUInput = (e: any) => {
    const node = inputRef?.current;
    const { target } = e;
    if (node) {
      if (!node.contains(target)) {
        setToggle(false);
      }
    }
  };

  useEffect(() => {
    window.addEventListener("click", handleClickOutsideUInput);
    return () => {
      window.removeEventListener("click", handleClickOutsideUInput);
    };
  }, []);

  return (
    <TitleWrapper>
      <div
        ref={inputRef}
        className={`title-title flex ${
          align === "center" ? "justify-center" : "justify-between"
        } wow fadeInUp`}
      >
        <div>
          <div
            className={`title xl:text-[24px] lg:text-[24px]  text-[16px] ${
              uppercase ? "uppercase" : ""
            }`}
          >
            {title}
          </div>
          <div
            className={`line mt-[12px] h-[2px] flex ${
              align === "center" ? "justify-center" : ""
            }`}
          >
            <div className="line-1 h-full w-[80px]"></div>
            {/*<div*/}
            {/*  className={`line-2 h-full w-[40px] ${*/}
            {/*    type === "primary" ? "bg-white" : "bg-[#F4F4F4]"*/}
            {/*  }`}*/}
            {/*></div>*/}
          </div>
        </div>
        <div className="flex">
          {!hideSearch && (
            <div className="hidden lg:block xl:block">
              <div className={`${toggle ? "" : "hidden"}`}>
                <Input.Search
                  placeholder="Tìm kiếm việc làm"
                  onSearch={onSearch}
                  style={{ width: 300 }}
                />
              </div>

              <div
                className={`search w-[36px] h-[36px] mr-[24px] flex justify-center items-center cursor-pointer ${
                  toggle ? "hidden" : ""
                } ${type === "primary" ? "bg-white" : "bg-[#F4F4F4]"}`}
                onClick={() => setToggle(true)}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="24"
                  height="25"
                  viewBox="0 0 24 25"
                  fill="none"
                >
                  <circle
                    cx="11"
                    cy="11"
                    r="7"
                    stroke="#2D2D2D"
                    stroke-width="1.5"
                  />
                  <path d="M16 16L21 21" stroke="#2D2D2D" stroke-width="1.5" />
                </svg>
              </div>
            </div>
          )}
          {!hideArrows && (
            <div className="flex">
              <div
                className="mr-[20px] cursor-pointer"
                onClick={() => {
                  if (handlePrev) handlePrev();
                }}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="36"
                  height="37"
                  viewBox="0 0 36 37"
                  fill="none"
                >
                  <g filter="url(#filter0_b_7520_27504)">
                    <path
                      d="M21 10.9946L13.5 17.9946L21 25.4946"
                      stroke="#0584E9"
                      stroke-width="2"
                    />
                  </g>
                  <defs>
                    <filter
                      id="filter0_b_7520_27504"
                      x="-50"
                      y="-49.3218"
                      width="136"
                      height="136"
                      filterUnits="userSpaceOnUse"
                      color-interpolation-filters="sRGB"
                    >
                      <feFlood flood-opacity="0" result="BackgroundImageFix" />
                      <feGaussianBlur
                        in="BackgroundImageFix"
                        stdDeviation="25"
                      />
                      <feComposite
                        in2="SourceAlpha"
                        operator="in"
                        result="effect1_backgroundBlur_7520_27504"
                      />
                      <feBlend
                        mode="normal"
                        in="SourceGraphic"
                        in2="effect1_backgroundBlur_7520_27504"
                        result="shape"
                      />
                    </filter>
                  </defs>
                </svg>
              </div>
              <div
                className="cursor-pointer"
                onClick={() => {
                  if (handleNext) handleNext();
                }}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="36"
                  height="37"
                  viewBox="0 0 36 37"
                  fill="none"
                >
                  <g filter="url(#filter0_b_7520_27506)">
                    <path
                      d="M14 10.9946L21.5 17.9946L14 25.4946"
                      stroke="#0584E9"
                      stroke-width="2"
                    />
                  </g>
                  <defs>
                    <filter
                      id="filter0_b_7520_27506"
                      x="-50"
                      y="-49.3218"
                      width="136"
                      height="136"
                      filterUnits="userSpaceOnUse"
                      color-interpolation-filters="sRGB"
                    >
                      <feFlood flood-opacity="0" result="BackgroundImageFix" />
                      <feGaussianBlur
                        in="BackgroundImageFix"
                        stdDeviation="25"
                      />
                      <feComposite
                        in2="SourceAlpha"
                        operator="in"
                        result="effect1_backgroundBlur_7520_27506"
                      />
                      <feBlend
                        mode="normal"
                        in="SourceGraphic"
                        in2="effect1_backgroundBlur_7520_27506"
                        result="shape"
                      />
                    </filter>
                  </defs>
                </svg>
              </div>
            </div>
          )}
        </div>
      </div>
    </TitleWrapper>
  );
};

const TitleWrapper = styled.div`
  .title-title{
    visibility: visible!important;
  }
  .search {
    border-radius: 8px;
  }
  .title {
    color: var(--TEXT-01, #212529);
    text-align: center;
    font-family: Arial;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
    //text-transform: uppercase;
  }
  .line {
    border-radius: 8px;
    overflow: hidden;
    .line-1 {
      border-radius: 8px;
      background: linear-gradient(90deg, #FFC20E -0.39%, #FF9300 100.13%);
      //background: var(--Main-2, #2387e3);
    }
    .line-2 {
    }
  }
`;
export default Title;
