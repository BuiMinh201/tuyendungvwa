import React, { useEffect, useState } from "react";

import BannerHome from "../BannerHome";
import ViTriViecLam from "../ViTriViecLam";
import CardDotTuyenDung from "../DotTuyenDung/components/CardDotTuyenDung";
import DotTuyenDung from "../DotTuyenDung";
import EmailBanner from "../EmailBanner";
import QuyTrinhXuLy from "../QuyTrinhXuLy";

const HomePage = (props: { dataKhoaHoc?: any; dataNhomChuongTrinh?: any }) => {
  return (
    <div className="overflow-hidden bg-white">
      <BannerHome />
      <ViTriViecLam/>
      <DotTuyenDung/>
      <EmailBanner/>
      <QuyTrinhXuLy/>
    </div>
  );
};
export default HomePage;
