import styled from "styled-components";
import moment from "moment";
import { da } from "date-fns/locale";
import Skeleton from "react-loading-skeleton";


const CardTinTucSkeleton = () => {
  return (
    <CardTinTucWrapper>
      <div className="lg:w-[380px] xl:w-full h-[285px]">
        <Skeleton width={"100%"} height={"100%"} />
      </div>
      <div className="date-time mt-[20px]">
        <Skeleton />
      </div>
      <div className="title mt-[8px]">
        <Skeleton />
      </div>
      <div className="description mt-[8px]">
        <Skeleton />
      </div>
    </CardTinTucWrapper>
  );
};

const CardTinTucWrapper = styled.div`
  .date-time {
    color: var(--1, #278bff);
    font-family: "Exo 2", sans-serif;
    font-size: 14px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
  }
  .title {
    color: #1d1d1f;
    font-family: "Exo 2", sans-serif;
    font-size: 20px;
    font-style: normal;
    font-weight: 600;
    line-height: normal;

    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
  }
  .description {
    color: #1d1d1f;
    font-family: "Exo 2", sans-serif;
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;

    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
  }
`;
export default CardTinTucSkeleton;
