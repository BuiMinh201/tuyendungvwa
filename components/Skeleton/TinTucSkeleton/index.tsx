import BreadcrumbPage from "../../Breadcrumb";
import {EmailShareButton, FacebookShareButton} from "react-share";
import ReactToPrint from "react-to-print";
import TableBase from "../../tableBase";
import React from "react";
import styled from "styled-components";
import Skeleton from "react-loading-skeleton";
import Tag from "../../Tag";

const TinTucSkeleton = () => {
  return(
    <>
      <KhoaHocWrapper>
      <Skeleton count={15}/>
      </KhoaHocWrapper>
    </>
  )
}
const KhoaHocWrapper = styled.div`
  h2 {
    font-family: "Inter";
    font-style: normal;
    font-weight: 700;
    font-size: 24px;
    line-height: 32px;
    color: #212529;
  }
  .share {
    span {
      font-family: "Inter";
      font-style: normal;
      font-weight: 500;
      font-size: 16px;
      line-height: 24px;
      color: #000000;
    }
    .share-item {
      cursor: pointer;
      margin-right: 16px;
      background: #f1f3f5;
      border-radius: 100px;
      width: 40px;
      height: 40px;
      display: flex;
      align-items: center;
      justify-content: center;
      &:last-of-type {
        margin-right: 0;
      }
    }
  }

  .table-content {
    margin-bottom: 16px;
    font-family: "Inter";
    font-style: normal;
    font-weight: 700;
    font-size: 20px;
    line-height: 32px;
    display: flex;
    align-items: center;
    color: #212529;
  }
  .register {
    padding: 12px 64px;
    //border-radius: 8px;
    font-family: "Inter";
    font-style: normal;
    font-weight: 600;
    font-size: 16px;
    line-height: 24px;
    /* identical to box height, or 150% */

    display: flex;
    align-items: center;
    justify-content: center;

    /* White */

    color: #ffffff;
  }
  .modal-khoa-hoc {
    .title-modal {
      color: #0353aa;
      font-family: "Exo 2", sans-serif;
      font-size: 24px;
      font-style: normal;
      font-weight: 700;
      line-height: normal;
    }
    .title-2 {
      color: #000;
      font-family: "Exo 2", sans-serif;
      font-size: 16px;
      font-style: normal;
      font-weight: 400;
      line-height: normal;
    }
  }
  .title-content{
    color: #000;
    font-family: 'Exo 2',sans-serif;
    font-size: 16px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
  }
`;
export default TinTucSkeleton;