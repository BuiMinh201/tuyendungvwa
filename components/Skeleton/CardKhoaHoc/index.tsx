import styled from "styled-components";
import Skeleton from "react-loading-skeleton";

const CardKhoaHocSkeleton = () => {
  return(
    <CardKhoaHocWrapper>
      <div className="image relative cursor-pointer">
        <div className="xl:w-[380px] xl:w-full xl:h-[380px] w-full h-[280px]">
         <Skeleton width={'100%'} height={'100%'}/>
        </div>
      </div>
      <div className="title-1 mt-[20px]"> <Skeleton/></div>
      <Skeleton/>

      <div className="show-more flex mt-[12px]">
        <Skeleton/>
      </div>
    </CardKhoaHocWrapper>
  )
}
const CardKhoaHocWrapper = styled.div`
  .time {
    background: #26b9db;
    .title {
      color: #fff;
      text-align: center;
      font-family: "Exo 2", sans-serif;
      font-size: 14px;
      font-style: normal;
      font-weight: 400;
      line-height: normal;
      text-transform: capitalize;
    }
    .number {
      color: #fff;
      font-family: "Exo 2", sans-serif;
      font-size: 24px;
      font-style: normal;
      font-weight: 600;
      line-height: normal;
    }
  }
  .title-1 {
    color: #1d1d1f;
    font-family: "Exo 2", sans-serif;
    font-size: 20px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
    text-transform: capitalize;
  }
  .description {
    color: rgba(29, 29, 31, 0.8);
    font-family: "Exo 2", sans-serif;
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;

    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 3;
    -webkit-box-orient: vertical;
  }
  .show-more {
    color: #0353aa;
    font-family: "Exo 2", sans-serif;
    font-size: 16px;
    font-style: normal;
    font-weight: 500;
    line-height: normal;
  }
`;
export default CardKhoaHocSkeleton;