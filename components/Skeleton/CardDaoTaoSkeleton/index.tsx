import styled from "styled-components";
import React from "react";
import Skeleton from "react-loading-skeleton";

const CardDaoTaoSkeleton = () => {
  return(
    <CardDaoTaoSkeletonWrapper>
      <div>
        <div className="item-modal">
          <Skeleton/>
          <div className="item-modal-title  mt-[16px]">
          <Skeleton/>
          </div>
          <div
            className="link-modal mt-[8px] cursor-pointer"
          >
           <Skeleton/>
          </div>
        </div>
      </div>
    </CardDaoTaoSkeletonWrapper>
  )
}
const CardDaoTaoSkeletonWrapper=styled.div`

`
export default CardDaoTaoSkeleton;