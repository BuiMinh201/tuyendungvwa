import styled from "styled-components";
import moment from "moment";
import { da } from "date-fns/locale";
import Skeleton from "react-loading-skeleton";

const CardChuongTrinhDaoTaoSkeleton = () => {
  return (
    <CardChuongTrinhDaoTaoWrapper>
      <div className="flex ">
        <div className="image mr-[20px] lg:w-[220px] w-full h-[220px] shrink-0">
          <Skeleton width={"100%"} height={"100%"} />
        </div>
        <div className="infor py-[20px]">
          <Skeleton width={"100%"} />

          <Skeleton />

          <Skeleton />
        </div>
      </div>
    </CardChuongTrinhDaoTaoWrapper>
  );
};
const CardChuongTrinhDaoTaoWrapper = styled.div`
  .title {
    color: #1d1d1f;
    font-family: "Exo 2", sans-serif;
    font-size: 20px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
    text-transform: capitalize;

    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
  }
  .show-more {
    color: #0353aa;
    font-family: "Exo 2", sans-serif;
    font-size: 16px;
    font-style: normal;
    font-weight: 500;
    line-height: normal;
  }
  .description {
    color: rgba(29, 29, 31, 0.8);
    font-family: "Exo 2", sans-serif;
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;

    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 3;
    -webkit-box-orient: vertical;
  }
`;
export default CardChuongTrinhDaoTaoSkeleton;
