import styled from "styled-components";
import {useRouter} from "next/router";
export interface Data{
  title:string,
  linkTo:string,
  children:Data[]
}
interface IProps{
  data:Data[]
}
const Tree = (props:IProps) => {
  const router=useRouter();
  const {data}=props;
  return(
    <TreeWrapper>
      <ul className="tree">
        {data?.map((value,i)=>{
          return(
            <li>
              <div className={'parent-1 py-[8px]'} style={{border:"none",color:'#FFFFFF'}} onClick={()=>{
                if (value?.children?.length>0){

                }else {
                  router.push(value?.linkTo)
                }
              }}>{value?.title}</div>
              {value?.children?.length>0&&
              <ul>
                {value?.children?.map((val2,i2)=>{
                  return(
                    <li>
                      <div onClick={()=>router.push(value?.children?.length>0?'#':val2?.linkTo)}>{val2?.title}</div>
                      {val2?.children?.length>0&&
                          <ul>
                            {val2?.children?.map((val3,i3)=>{
                              return(
                                <li>
                                  <div onClick={()=>router.push(val3?.linkTo)}>{val3?.title}</div>
                                  {val3?.children?.length>0&&
                                      <ul>
                                        {val3?.children?.map((val3,i3)=>{
                                          return(
                                            <li>

                                            </li>
                                          )
                                        })}
                                      </ul>}
                                </li>
                              )
                            })}
                          </ul>}
                    </li>
                  )
                })}
              </ul>}
            </li>
          )
        })}
        {/*<li><div>Home</div>*/}
        {/*  <ul>*/}
        {/*    <li><div>About us</div>*/}
        {/*      <ul>*/}
        {/*        <li><div>Our history</div>*/}
        {/*          <ul>*/}
        {/*            <li><div>Founder</div></li>*/}
        {/*          </ul>*/}
        {/*        </li>*/}
        {/*        <li><div>Our board</div>*/}
        {/*          <ul>*/}
        {/*            <li><div>Brad Whiteman</div></li>*/}
        {/*            <li><div>Cynthia Tolken</div></li>*/}
        {/*            <li><div>Bobby Founderson</div></li>*/}
        {/*          </ul>*/}
        {/*        </li>*/}
        {/*      </ul>*/}
        {/*    </li>*/}
        {/*    <li><div>Our products</div>*/}
        {/*      <ul>*/}
        {/*        <li><div>The Widget 2000™</div>*/}
        {/*          <ul>*/}
        {/*            <li><div>Order form</div></li>*/}
        {/*          </ul>*/}
        {/*        </li>*/}
        {/*        <li><div>The McGuffin V2</div>*/}
        {/*          <ul>*/}
        {/*            <li><div>Order form</div></li>*/}
        {/*          </ul>*/}
        {/*        </li>*/}
        {/*      </ul>*/}
        {/*    </li>*/}
        {/*    <li><div>Contact us</div>*/}
        {/*      <ul>*/}
        {/*        <li><div>Social media</div>*/}
        {/*          <ul>*/}
        {/*            <li><div>Facebook</div></li>*/}
        {/*          </ul>*/}
        {/*        </li>*/}
        {/*      </ul>*/}
        {/*    </li>*/}
        {/*  </ul>*/}
        {/*</li>*/}
      </ul>

    </TreeWrapper>
  )
}

const TreeWrapper=styled.div`
  .parent-1{
    color: var(--white, #FFF);
    text-align: center;
    font-family: 'Exo 2',sans-serif;
    font-size: 18px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
    background: #0353AA;
  }
  .tree, .tree ul, .tree li {
    list-style: none;
    margin: 0;
    padding: 0;
    position: relative;
  }

  .tree {
    margin: 0 0 1em;
    text-align: center;
  }
  .tree, .tree ul {
    display: table;
  }
  .tree ul {
    width: 100%;
  }
  .tree li {
    display: table-cell;
    padding: .5em 0;
    vertical-align: top;
  }
  /* _________ */
  .tree li:before {
    outline: solid 1px #666;
    content: "";
    left: 0;
    position: absolute;
    right: 0;
    top: 0;
  }
  .tree li:first-child:before {left: 50%;}
  .tree li:last-child:before {right: 50%;}

  .tree code, .tree div {
    color: var(--darkgray, #3D3E40);
    font-family: 'Exo 2',sans-serif;
    font-size: 16px;
    font-style: normal;
    font-weight: 700;
    line-height: 20px; /* 125% */
     
    border: solid .1em #666;
    border-radius: .2em;
    display: inline-block;
    margin: 0 .2em .5em;
    padding: .5em 1em;
    position: relative;
    
    &:hover{
      color: #0353AA;
    }
  }
  /* If the tree represents DOM structure */
  .tree code {
    font-family: monaco, Consolas, 'Lucida Console', monospace;
  }

  /* | */
  .tree ul:before,
  .tree code:before,
  .tree div:before {
    outline: solid 1px #666;
    content: "";
    height: 0.5em;
    left: 50%;
    position: absolute;
  }
  .tree ul:before {
    top: -.5em;
  }
  .tree code:before,
  .tree div:before {
    top: -.55em;
  }

  /* The root node doesn't connect upwards */
  .tree > li {margin-top: 0;}
  .tree > li:before,
  .tree > li:after,
  .tree > li > code:before,
  .tree > li > div:before {
    outline: none;
  }

`
export default Tree;
