import { Table } from "antd";
import styled from "styled-components";
import { useRouter } from "next/router";
import moment from "moment";

interface IProps {
  dataDot: DotTuyenDung.IRecord;
}
const DanhSachViecLam = (props: IProps) => {
  const { dataDot } = props;
  const router = useRouter();
  const onCell = (record: any) => ({
    onClick: () => {
      router.push(`/vi-tri-tuyen-dung/${record?._id}`);
    },
    style: { cursor: "pointer" },
  });

  const columns: any[] = [
    {
      title: "STT",
      dataIndex: "index",
      align: "center",
      width: 40,
      onCell,
    },
    {
      title: "Vị trí việc làm",
      dataIndex: "tenViTri",
      width: 120,
      onCell,
    },
    {
      title: "Đơn vị/Phòng ban",
      dataIndex: ["donVi", "ten"],
      width: 120,
      onCell,
    },
    {
      title: "Số lượng",
      dataIndex: "soLuong",
      align: "center",
      width: 80,
      onCell,
    },
    {
      title: "Trình độ chuyên môn",
      dataIndex: "trinhDoChuyenMon",
      align: "center",
      width: 120,
      onCell,
    },
    {
      title: "Yêu cầu",
      dataIndex: "yeuCau",
      width: 120,
      onCell,
      render: (val: any) => {
        return (
          <div className="line-clamp-3">
            <div dangerouslySetInnerHTML={{ __html: val }} />
          </div>
        );
      },
    },
    {
      title: "Thao tác",
      width: 80,
      fixed: "right",
      align: "center",
      render: (recordVal: any) => {
        return (
          <>
            <button
              disabled={moment().isBefore(
                moment(dataDot?.thoiGianBDNhanHoSo) ||
                  moment().isAfter(moment(dataDot?.thoiGianKTThiTuyen))
              )}
              className="btn-ung-tuyen"
              onClick={() => router.push(`/ung-tuyen/${recordVal?._id}`)}
            >
              Ứng tuyển
            </button>
          </>
        );
      },
    },
  ];
  return (
    <DanhSachViecLamWrapper>
      <Table
        pagination={false}
        dataSource={dataDot?.soLuongViTriTuyenDung?.map((val, index) => ({
          ...val,
          index: index + 1,
        }))}
        columns={columns}
        scroll={{ x: 1000 }}
      ></Table>
    </DanhSachViecLamWrapper>
  );
};

const DanhSachViecLamWrapper = styled.div`
  .btn-ung-tuyen {
    color: var(--Text, #0084ff);
    text-align: center;
    font-family: Arial;
    font-size: 14px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
    text-decoration-line: underline;
  }
`;
export default DanhSachViecLam;
