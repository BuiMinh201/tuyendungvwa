import styled from "styled-components";

interface IProps {
  dataDot: DotTuyenDung.IRecord;
}
const ThongTinChung = (props: IProps) => {
  const { dataDot } = props;
  return (
    <ThongTinChungWrapper className="mt-[12px]">
      <div className="title-thong-tin mb-[8px]">
        Tiêu chuẩn, điều kiện chung
      </div>
      <div
        dangerouslySetInnerHTML={{ __html: dataDot?.tieuChuanDieuKienChung }}
      ></div>

      <div className="title-thong-tin mb-[8px]">
        Hình thức và nội dung tuyển dụng
      </div>
      <div
        dangerouslySetInnerHTML={{
          __html: dataDot?.hinhThucVaNoiDungTuyenDung,
        }}
      ></div>

      <div className="title-thong-tin mb-[8px]">Hồ sơ dự tuyển</div>
      <div>
        {dataDot?.hoSoDuTuyen?.map((val) => {
          return <div>-</div>;
        })}
      </div>

      <div className="title-thong-tin mb-[8px]">
        Địa điểm nộp hồ sơ trực tuyến
      </div>
      <div
        dangerouslySetInnerHTML={{ __html: dataDot?.diaDiemNopHoSo ?? "" }}
      ></div>
      <div className="flex mt-[12px]">
        {dataDot?.urlQuyetDinhCongVan && (
          <div>
            <a className={"mr-[12px] underline text-[#2387e3] text-[16px]"} href={dataDot?.urlQuyetDinhCongVan}>
              Quyết định công văn
            </a>
          </div>
        )}
        {dataDot?.urlVanBanDaDongDau && (
          <div>
            <a className="underline text-[#2387e3] text-[16px]" href={dataDot?.urlVanBanDaDongDau}>Văn bản đã đóng dấu</a>
          </div>
        )}
      </div>
    </ThongTinChungWrapper>
  );
};

const ThongTinChungWrapper = styled.div`
  .title-thong-tin {
    color: var(--TEXT-01, #212529);
    font-family: Arial;
    font-size: 16px;
    font-style: normal;
    font-weight: 700;
    line-height: 125%; /* 20px */
  }
`;
export default ThongTinChung;
