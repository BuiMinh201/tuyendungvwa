import { Select } from "antd";
import { useEffect, useState } from "react";
import { getDanToc } from "../../service/core/dantoc";

/**
 * Secect Căn cứ pháp lý để cho vào FormItem
 */
const SelectDanToc = (props: {
  value?: string;
  onChange?: any;
  multiple?: boolean;
  loadData?: boolean;
  disabled?: boolean;
  allowClear?: boolean;
  isGetId?: boolean;
  hienThiMaDanToc?: boolean;
}) => {
  const {
    value,
    onChange,
    multiple,
    loadData,
    allowClear,
    isGetId,
    disabled,
    hienThiMaDanToc = true,
  } = props;
  const [danhSach, setDanhSach] = useState<DanToc.IRecord[]>([]);
  const getData = async () => {
    try {
      const res = await getDanToc();
      setDanhSach(res?.data?.data);
    } catch (e) {}
  };

  useEffect(() => {
    if (loadData !== false) getData();
  }, []);

  return (
    <Select
      mode={multiple ? "multiple" : undefined}
      value={value}
      disabled={disabled}
      allowClear={allowClear}
      onChange={onChange}
      options={danhSach.map((item) => ({
        key: item._id,
        value: isGetId ? item?._id : item.tenDanToc,
        label: hienThiMaDanToc
          ? `${item.tenDanToc} - ${item.ma}`
          : item.tenDanToc,
      }))}
      showSearch
      optionFilterProp="label"
      placeholder="Chọn dân tộc"
    />
  );
};

export default SelectDanToc;
