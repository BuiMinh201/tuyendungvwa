import { Select } from "antd";
import { useEffect, useState } from "react";
import { getDanToc } from "../../service/core/dantoc";
import { getTinhTrangHonNhan } from "../../service/core/tinhtranghonnhan";

/**
 * Secect Căn cứ pháp lý để cho vào FormItem
 */
const SelectTinhTrangHonNhan = (props: {
  value?: string;
  onChange?: any;
  multiple?: boolean;
  loadData?: boolean;
  disabled?: boolean;
  allowClear?: boolean;
}) => {
  const {
    value,
    onChange,
    multiple,
    loadData,
    allowClear,
    disabled,
  } = props;
  const [danhSach, setDanhSach] = useState<TinhTrangHonNhan.IRecord[]>([]);
  const getData = async () => {
    try {
      const res = await getTinhTrangHonNhan();
      setDanhSach(res?.data?.data);
    } catch (e) {}
  };

  useEffect(() => {
    if (loadData !== false) getData();
  }, []);

  return (
    <Select
      allowClear={allowClear}
      disabled={disabled}
      mode={multiple ? "multiple" : undefined}
      value={value}
      onChange={onChange}
      options={danhSach.map((item) => ({
        key: item._id,
        value: item._id,
        label: item.ten,
      }))}
      showSearch
      optionFilterProp="label"
      placeholder="Tình trạng hôn nhân"
    />
  );
};

export default SelectTinhTrangHonNhan;
