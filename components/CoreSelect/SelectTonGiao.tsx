import { Select } from "antd";
import { useEffect, useState } from "react";
import { getDanToc } from "../../service/core/dantoc";
import {getTonGiao} from "../../service/core/tongiao";

/**
 * Secect Căn cứ pháp lý để cho vào FormItem
 */
const SelectTonGiao = (props: {
  value?: string;
  onChange?: any;
  multiple?: boolean;
  loadData?: boolean;
  disabled?: boolean;
  allowClear?: boolean;
  isGetId?: boolean;
  hienThiMaTonGiao?: boolean;
}) => {
  const {
    value,
    onChange,
    multiple,
    loadData,
    allowClear,
    isGetId,
    disabled,
    hienThiMaTonGiao = true,
  } = props;
  const [danhSach, setDanhSach] = useState<TonGiao.IRecord[]>([]);
  const getData = async () => {
    try {
      const res = await getTonGiao();
      setDanhSach(res?.data?.data);
    } catch (e) {}
  };

  useEffect(() => {
    if (loadData !== false) getData();
  }, []);

  return (
    <Select
      mode={multiple ? 'multiple' : undefined}
      value={value}
      allowClear={allowClear}
      onChange={onChange}
      disabled={disabled}
      options={danhSach.map((item) => ({
        key: item._id,
        value: isGetId ? item?._id : item.tenTonGiao,
        label: hienThiMaTonGiao ? `${item.tenTonGiao} - ${item.ma}` : item.tenTonGiao,
      }))}
      showSearch
      optionFilterProp='label'
      placeholder='Chọn tôn giáo'
    />
  );
};

export default SelectTonGiao;
