import { Select } from "antd";
import { useEffect, useState } from "react";
import { getQuocTich } from "../../service/core/quoctich";

/**
 * Secect Căn cứ pháp lý để cho vào FormItem
 */
const SelectQuocTich = (props: {
  value?: string;
  onChange?: any;
  multiple?: boolean;
  disabled?: boolean;
  loadData?: boolean;
  allowClear?: boolean;
  placeholder?: string;
  hienThiMaQuocTich?: boolean;
}) => {
  const {
    value,
    onChange,
    multiple,
    loadData,
    allowClear,
    placeholder,
    disabled,
    hienThiMaQuocTich = true,
  } = props;
  const [danhSach, setDanhSach] = useState<QuocTich.IRecord[]>([]);
  const getData = async () => {
    try {
      const res = await getQuocTich();
      setDanhSach(res?.data?.data);
    } catch (e) {}
  };

  useEffect(() => {
    if (loadData !== false) getData();
  }, []);

  return (
    <Select
      mode={multiple ? "multiple" : undefined}
      value={value}
      disabled={disabled}
      allowClear={allowClear}
      onChange={onChange}
      options={danhSach.map((item) => ({
        key: item._id,
        value: item._id,
        label: hienThiMaQuocTich
          ? `${item.tenQuocTich} - ${item.ma}`
          : item.tenQuocTich,
      }))}
      showSearch
      optionFilterProp="label"
      placeholder={placeholder ?? "Chọn quốc tịch"}
    />
  );
};

export default SelectQuocTich;
