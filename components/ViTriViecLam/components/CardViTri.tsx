import styled from "styled-components";
import moment from "moment";
interface IProps {
  handleUngTuyen: (id: string) => void;
  handleViewDetail: (id: string) => void;
  id: string;
  disabled?: boolean;
  data: {
    timeStart: string;
    timeEnd: string;
    title: string;
    content: string;
    dotTuyenDung?: string;
    tags: {
      name: string;
    }[];
  };
}
const CardViTri = (props: IProps) => {
  const { handleUngTuyen, data, id, disabled, handleViewDetail } = props;
  return (
    <CardViTriWrapper className="xl:p-[20px] p-[12px]">
      <div className={`${disabled ? "disabled" : ""}`}>
        <div className="header-card flex justify-between">
          <div className="">
            <div className="title-header-card xl:text-[16px] text-[12px] mr-[8px] hidden xl:block">
              Ngày bắt đầu nhận hồ sơ:
            </div>
            <div className="title-header-card xl:text-[16px] text-[12px]">
              {data.timeStart
                ? moment(data?.timeStart).format("DD/MM/YYYY")
                : ""}
            </div>
          </div>
          <div className="">
            <div className="title-header-card xl:text-[16px] text-[12px] mr-[8px] hidden xl:block">
              Ngày kết thúc nhận hồ sơ:
            </div>
            <div className="title-header-card xl:text-[16px] text-[12px]">
              {data.timeEnd ? moment(data?.timeEnd).format("DD/MM/YYYY") : ""}
            </div>
          </div>
        </div>
        <div
          className="content-card mt-[20px]"
          onClick={() => handleViewDetail(id)}
        >
          <div className="content-1 cursor-pointer xl:text-[20px] text-[14px]">
            {data.title}
          </div>
          {data?.dotTuyenDung&&
          <div className="content-2 mt-[4px] xl:text-[14px] text-[12px]">
            ({data?.dotTuyenDung})
          </div>}
          <div className="content-2 mt-[10px] cursor-pointer hidden xl:block line-clamp-1 xl:h-[40px] overflow-y-scroll text-[16px]">
            <div dangerouslySetInnerHTML={{ __html: data.content }}></div>
          </div>
        </div>
        <div className="list-tab flex flex-wrap gap-[8px] mt-[24px]">
          {data?.tags?.map((val, index) => {
            return (
              <div
                className="tab-item py-[6px] xl:px-[12px] px-[6px] xl:text-[14px] text-[11px]"
                key={index}
              >
                {val?.name}
              </div>
            );
          })}
        </div>
        <div className="footer-card flex justify-end mt-[8px]">
          <button
            disabled={disabled}
            className="btn-card xl:w-[174px] w-[100px] xl:h-[38px] h-[24px] xl:text-[16px] text-[12px]"
            onClick={() => {
              handleUngTuyen(id);
            }}
          >
            Ứng tuyển vị trí
          </button>
        </div>
      </div>
    </CardViTriWrapper>
  );
};
const CardViTriWrapper = styled.div`
  border-radius: 8px;
  border: 1.5px solid #dfdfdf;
  background: #fff;
  .title-header-card {
    color: var(--02, #6dc2ff);
    font-family: Arial;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
  }
  .content-card {
    .content-1 {
      color: #000;
      font-family: Arial;
      font-style: normal;
      font-weight: 700;
      line-height: normal;

      overflow: hidden;
      text-overflow: ellipsis;
      display: -webkit-box;
      -webkit-line-clamp: 1;
      -webkit-box-orient: vertical;

      &:hover {
        color: #2387e3;
      }
    }
    .content-2 {
      color: var(--Dark-Gray, #2d2d2d);
      font-family: Arial;
      font-style: normal;
      font-weight: 400;
      line-height: normal;
    }
  }
  .list-tab {
    .tab-item {
      color: var(--TEXT-01, #212529);
      font-family: Arial;
      font-style: normal;
      font-weight: 400;
      line-height: normal;

      border-radius: 8px;
      background: var(--Gray, #f4f4f4);
    }
  }
  .btn-card {
    color: #fff;
    text-align: center;
    font-family: Arial;
    font-style: normal;
    font-weight: 400;
    line-height: normal;

    border-radius: 4px;
    background: var(--Main-2, #2387e3);
  }
  .disabled {
    button {
      cursor: not-allowed;
      border-radius: 4px;
      opacity: 0.6;
      background: var(--Normal-gray, #b2b5c1);
    }
  }
  .content-2 {
    /* width */
    &::-webkit-scrollbar {
      width: 0px;
    }

    /* Track */
    &::-webkit-scrollbar-track {
      box-shadow: inset 0 0 5px grey;
      border-radius: 4px;
    }

    ///* Handle */
    //::-webkit-scrollbar-thumb {
    //  background: red;
    //  border-radius: 10px;
    //}
    //
    ///* Handle on hover */
    //::-webkit-scrollbar-thumb:hover {
    //  background: #b30000;
    //}
  }
`;
export default CardViTri;
