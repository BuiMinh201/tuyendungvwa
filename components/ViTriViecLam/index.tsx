import styled from "styled-components";
import Title from "../Title";
import CardViTri from "./components/CardViTri";
import { useRouter } from "next/router";
import { useEffect, useRef, useState } from "react";
import { ip3 } from "../../service/ip";
import SlideMultiRow from "../SlideMultiRow";
import moment from "moment";
import NoData from "../NoData";
import axios from "../../service/axios";

const ViTriViecLam = () => {
  const router = useRouter();
  const [dataViTri, setDataViTri] = useState<ViTriViecLam.IRecord[]>([]);
  const [keyword, setKeyWord] = useState<string>();
  const slideRef = useRef(null);
  const [dataViTriSub, setDataViTriSub] = useState<ViTriViecLam.IRecord[][]>(
    []
  );
  const getData = async (key?: string) => {
    try {
      const res = await axios.get(`${ip3}/vi-tri-tuyen-dung-public/many`, {
        params: {
          // page: 1,
          // limit: 100,
          "filters[]":key? JSON.stringify({
            field: "tenViTri",
            operator: "contain",
            values: [key ? key : []],
          }):undefined,
          // condition: JSON.stringify({
          //   filters: [{ field: "tenViTri", operator: "contain", values: [key] }],
          // }),
        },
      });
      if (res) {
        setDataViTri(res?.data?.data ?? []);
      }
    } catch (e) {
      console.log(e);
    }
  };
  useEffect(() => {
    getData(keyword);
  }, [keyword]);
  useEffect(() => {
    const arr = [...dataViTri];
    const subArrays = [];
    for (let i = 0; i < arr.length; i += 4) {
      const subArray = arr.slice(i, i + 4);
      subArrays.push(subArray);
    }
    setDataViTriSub(subArrays);
  }, [dataViTri]);
  return (
    <ViTriViecLamWrapper>
      <div
        className="container mx-auto py-[50px] px-[20px] xl:px-0"
        id={"vi-tri-viec-lam"}
      >
        <Title
          type={"default"}
          title={"Vị trí việc làm cần tuyển"}
          uppercase={true}
          handleSearch={(keyword) => {
            setKeyWord(keyword);
          }}
          hideArrows={dataViTri?.length < 5}
          handleNext={() => {
            if (slideRef.current) {
              // @ts-ignore
              slideRef?.current?.handleNext();
            }
          }}
          handlePrev={() => {
            if (slideRef.current) {
              // @ts-ignore
              slideRef?.current?.handlePrev();
            }
          }}
        />
        {dataViTri?.length > 0 ? (
          <>
            {" "}
            <div className="  mt-[28px] ">
              <div className="hidden xl:block">
                <SlideMultiRow showArrow={false} ref={slideRef}>
                  {dataViTriSub?.map((item) => {
                    return (
                      <>
                        <div className={"grid grid-cols-2 gap-[20px]"}>
                          {item?.map((val) => {
                            return (
                              <div className={"col-span-1"}>
                                <CardViTri
                                  data={{
                                    dotTuyenDung:
                                      val?.dotTuyenDung?.tenDotTuyenDung,
                                    title: val?.tenViTri ?? "",
                                    content: val?.yeuCau,
                                    tags: [
                                      { name: `${val?.soLuong} vị trí` },
                                      {
                                        name: `${val?.trinhDoChuyenMon}`,
                                      },
                                      { name: val?.donVi?.ten },
                                    ],
                                    timeEnd:
                                      val?.dotTuyenDung?.thoiGianKTNhanHoSo,
                                    timeStart:
                                      val?.dotTuyenDung?.thoiGianBDNhanHoSo,
                                  }}
                                  handleUngTuyen={(id) => {
                                    router.push(`/ung-tuyen/${id}`);
                                  }}
                                  handleViewDetail={(id) => {
                                    router.push(`/vi-tri-tuyen-dung/${id}`);
                                  }}
                                  id={val?._id}
                                  disabled={
                                    moment().isBefore(
                                      moment(
                                        val?.dotTuyenDung?.thoiGianBDNhanHoSo
                                      )
                                    ) ||
                                    moment().isAfter(
                                      moment(
                                        val?.dotTuyenDung?.thoiGianKTNhanHoSo
                                      )
                                    )
                                  }
                                />
                              </div>
                            );
                          })}
                        </div>
                      </>
                    );
                  })}
                </SlideMultiRow>
              </div>
              <div className="xl:hidden grid lg:grid-cols-2 grid-cols-1 gap-[20px]">
                {dataViTri?.map((val) => {
                  return (
                    <div className="col-span-1">
                      <CardViTri
                        data={{
                          title: val?.tenViTri ?? "",
                          content: val?.yeuCau,
                          tags: [
                            { name: `${val?.soLuong} vị trí` },
                            {
                              name: `${val?.trinhDoChuyenMon}`,
                            },
                            { name: val?.donVi?.ten },
                          ],
                          timeEnd: val?.dotTuyenDung?.thoiGianKTNhanHoSo,
                          timeStart: val?.dotTuyenDung?.thoiGianBDNhanHoSo,
                        }}
                        handleUngTuyen={(id) => {
                          router.push(`/ung-tuyen/${id}`);
                        }}
                        handleViewDetail={(id) => {
                          router.push(`/vi-tri-tuyen-dung/${id}`);
                        }}
                        id={val?._id}
                        disabled={
                          moment().isBefore(
                            moment(val?.dotTuyenDung?.thoiGianBDNhanHoSo)
                          ) ||
                          moment().isAfter(
                            moment(val?.dotTuyenDung?.thoiGianKTNhanHoSo)
                          )
                        }
                      />
                    </div>
                  );
                })}
              </div>
            </div>
          </>
        ) : (
          <>
            <NoData title={"Việc làm hiện tại không có dữ liệu"} />
          </>
        )}
      </div>
    </ViTriViecLamWrapper>
  );
};

const ViTriViecLamWrapper = styled.div``;
export default ViTriViecLam;
