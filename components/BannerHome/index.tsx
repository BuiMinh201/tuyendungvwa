import React, { useContext, useEffect, useRef, useState } from "react";
import Button from "../Button";
import {
  Banner,
  DataCTDaoTao,
  DataNhomDaoTao,
  IDataBanner,
  TuyenSinh,
} from "../../utils/interface";
import axios from "axios";
import { gateWay, ip, ip3 } from "../../service/ip";
import { decodeHtmlEntities, renderImage } from "../../utils/util";
// @ts-ignore
import Slider from "react-slick";
import Modal from "../Common/Modal";
import { useRouter } from "next/router";
import styled from "styled-components";
import { AuthContext } from "../../context/AuthContext";

const BannerHome = () => {
  const router = useRouter();
  const [dataNhom, setDataNhom] = useState<DataNhomDaoTao[]>([]);
  const [recordNhom, setRecordNhom] = useState<DataNhomDaoTao>();
  const [dataBanner, setDataBanner] = useState<IDataBanner[]>([]);
  const [dataKhoaHocTheoChuDe, setDataKhoaHocTheoChuDe] = useState<
    DataCTDaoTao[]
  >([]);
  const { dataHome, dataConfig, dataDaoTao } = useContext(AuthContext);
  const [showModal, setShowModal] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);
  const SliderRef = useRef(null);
  useEffect(() => {
    // getData();
    // getDataBanner()
    if (dataDaoTao) {
      setDataNhom(dataDaoTao?.nhomChuongTrinhDaoTaoItems);
    }
  }, [dataHome, dataDaoTao]);
  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    arrows: false,
  };
  const handleNext = () => {
    if (SliderRef && SliderRef.current) {
      //@ts-ignore
      SliderRef?.current?.slickNext();
    }
  };
  const handlePrev = () => {
    if (SliderRef && SliderRef.current) {
      //@ts-ignore
      SliderRef?.current?.slickPrev();
    }
  };
  const handleGetDataDetail = async (id: string) => {
    setShowModal(true);
  };
  return (
    <BannerHomeWrapper>
      <div className="relative xl:h-[619px] h-[300px] wow fadeInLeft">
        <img
          className="banner-image w-full h-full"
          src="/banner/banner-1.png"
          alt={"banner"}
        />
        <div className="absolute xl:top-0 xl:left-[120px] top-0 left-0 xl:w-[587px] xl:h-[619px] w-full h-full flex flex-col justify-center xl:items-start lg:items-center items-center px-[20px] xl:px-0">
          <div className="text-title xl:text-[36px] text-[24px] text-center xl:text-left">
            Website Tuyển dụng chính thức của Học viện Phụ nữ Việt Nam
          </div>
          <div className="content xl:text-[20px] text-[14px] xl:mt-[24px] mt-[12px] text-center xl:text-left">
            Các đợt tuyển dụng sẽ được cập nhật trên trang web để thuận lợi cho
            việc dự tuyển của các ứng viên.
          </div>
          {/*transition duration-0 hover:duration-700 bg-gradient-to-r from-[#2387e3] to-[#2387e3] hover:from-[#ffffff] from-1% hover:to-[#2387e3] to-99%*/}
          <a
            className="btn-them xl:w-[210px] w-[120px] xl:h-[47px] h-[32px] flex justify-center items-center xl:mt-[48px] mt-[20px] bg-[#2387e3] xl:text-[20px] text-[14px]"
            href={"https://hvpnvn.edu.vn/"}
            target={"_blank"}
          >
            Tìm hiểu chung
          </a>
        </div>
      </div>
    </BannerHomeWrapper>
  );
};
const BannerHomeWrapper = styled.div`
  .banner-image {
  }

  .content {
    color: #000;
    font-family: Arial;
    font-style: normal;
    font-weight: 400;
    line-height: 125%; /* 25px */
  }

  .text-title {
    color: var(--Main-2, #2387e3);
    font-family: Arial;

    font-style: normal;
    font-weight: 700;
  }

  .btn-them {
    color: #fff;
    text-align: center;
    font-family: Arial;
    font-style: normal;
    font-weight: 400;
    line-height: normal;

    border-radius: 4px;
    //background: var(--Main-2, #2387e3);
  }
`;
export default BannerHome;
