import {
  ArrowLeftOutlined,
  ArrowRightOutlined,
  DownloadOutlined,
} from "@ant-design/icons";
import { Button } from "antd";
import { useRef, useState } from "react";
// @ts-ignore
import { Document, Page, pdfjs } from "react-pdf";
import ReactToPrint from "react-to-print";
import axios from "axios";
import { gateWay, ip3 } from "../../service/ip";
import fileDownload from "js-file-download";
import styled from "styled-components";
// import "react-pdf/dist/esm/Page/TextLayer.css";
pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

const PDFViewer = (props: {
  data: any;
  pagination?: boolean;
  scale?: number;
  idDoc?: string;
}) => {
  const [numPages, setNumPages] = useState(0);
  const [pageNumber, setPageNumber] = useState(1);
  // const [scale, setScale] = useState<number>(1);
  const docRef = useRef<any>();
  const [loadingText] = useState(
    "Đang tải tài liệu. Vui lòng chờ trong giây lát..."
  );

  const onDocumentLoadSuccess = (data: { numPages: number }) => {
    setPageNumber(1);
    setNumPages(data.numPages);
  };
  const handleDownloadFile = async (id: string) => {
    try {

    } catch (e) {
      console.log(e);
    }
  };
  return (
    <PdfWrapper>
      {" "}
      <div style={{ textAlign: "center" }}>
        <div>
          <ReactToPrint
            trigger={() => {
              return (
                <button style={{ display: "none" }} id="printButton">
                  Print
                </button>
              );
            }}
            content={() => docRef.current}
          />
        </div>
        {/*<div style={{ marginBottom: 16 }}>*/}
        {/*  {props.pagination && (*/}
        {/*    <Button*/}
        {/*      style={{ marginRight: 8 }}*/}
        {/*      disabled={pageNumber === 1}*/}
        {/*      onClick={() => setPageNumber(pageNumber - 1)}*/}
        {/*      className={""}*/}
        {/*      icon={<ArrowLeftOutlined rev={undefined} />}*/}
        {/*    >*/}
        {/*      Trước*/}
        {/*    </Button>*/}
        {/*  )}*/}
        {/*  {props?.idDoc && (*/}
        {/*    <Button*/}
        {/*      style={{ marginRight: 8 }}*/}
        {/*      type={"default"}*/}
        {/*      onClick={() => {*/}
        {/*        handleDownloadFile(props?.idDoc ?? "");*/}
        {/*      }}*/}
        {/*      icon={<DownloadOutlined rev={undefined} />}*/}
        {/*    >*/}
        {/*      Tải tài liệu*/}
        {/*    </Button>*/}
        {/*  )}*/}
        {/*  {props.pagination && (*/}
        {/*    <Button*/}
        {/*      disabled={numPages <= pageNumber}*/}
        {/*      onClick={() => setPageNumber(pageNumber + 1)}*/}
        {/*      className={""}*/}
        {/*      icon={<ArrowRightOutlined rev={undefined} />}*/}
        {/*    >*/}
        {/*      Sau*/}
        {/*    </Button>*/}
        {/*  )}*/}
        {/*</div>*/}
        <div ref={docRef} style={{ textAlign: "center", marginBottom: 12,minHeight:'100vh' }}>
          <Document
            onLoadSuccess={onDocumentLoadSuccess}
            onLoadError={() => setNumPages(0)}
            file={props.data}
            noData={<div style={{ padding: "32px 0" }}>{loadingText}</div>}
            loading={<div style={{ padding: "32px 0" }}>{loadingText}</div>}
            error={loadingText}
          >
            <Page
              pageNumber={pageNumber}
              scale={props.scale ?? 1.8}
              loading="Đang tải..."
            />
          </Document>
        </div>

        <div className="flex justify-center flex-col items-center">
          <div className="flex">
            {props.pagination && (
              <Button
                style={{ marginRight: 8 }}
                disabled={pageNumber === 1}
                onClick={() => setPageNumber(pageNumber - 1)}
                className={""}
                icon={<ArrowLeftOutlined rev={undefined} />}
              >
                Trước
              </Button>
            )}

            {/*{props.pagination && (*/}
            {/*	<Button disabled={pageNumber === 1} onClick={() => setPageNumber(pageNumber - 1)} className={'s'}>*/}
            {/*		<ArrowLeftOutlined rev={undefined} /> Trước*/}
            {/*	</Button>*/}
            {/*)}*/}
            <div>
              <span style={{ marginRight: 8 }}>
                Trang {pageNumber} trên tổng số {numPages}
              </span>
            </div>

            {/* <div>
              <span>Phóng to: {scale}</span>
              <Slider
                min={1}
                max={2}
                value={scale}
                step={0.1}
                onChange={(page) => setScale(page)}
              />
            </div> */}
            {/*{props.pagination && (*/}
            {/*	<Button*/}
            {/*		disabled={numPages <= pageNumber}*/}
            {/*		onClick={() => setPageNumber(pageNumber + 1)}*/}
            {/*		className={''}*/}
            {/*	>*/}
            {/*		Sau <ArrowRightOutlined rev={undefined} />*/}
            {/*	</Button>*/}
            {/*)}*/}

            {props.pagination && (
              <Button
                disabled={numPages <= pageNumber}
                onClick={() => setPageNumber(pageNumber + 1)}
                className={""}
                icon={<ArrowRightOutlined rev={undefined} />}
              >
                Sau
              </Button>
            )}
          </div>

          {props?.idDoc && (
            <Button
              style={{ marginRight: 8 }}
              type={"default"}
              onClick={() => {
                handleDownloadFile(props?.idDoc ?? "");
              }}
              icon={<DownloadOutlined rev={undefined} />}
            >
              Tải tài liệu
            </Button>
          )}
        </div>
      </div>
    </PdfWrapper>
  );
};

export default PDFViewer;

const PdfWrapper = styled.div`
  //.react-pdf__Page__textContent {
  //  display: none;
  //}
  .react-pdf__Page__annotations {
    position: absolute;
    display: none;
  }
  .react-pdf__Document {
    overflow: hidden;
  }
  .react-pdf__Page {
    display: flex;
    justify-content: center;

    overflow: hidden;
  }
`;
