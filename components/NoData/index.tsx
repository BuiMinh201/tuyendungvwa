import styled from "styled-components";

const NoData = (props: { title?: string; hideTitleButton?: boolean }) => {
  return (
    <NoDataWrapper>
      <div className="flex justify-center flex-col items-center wow fadeInUp mt-[20px] mb-[20px] min-h-[300px]">
        <img src="/icon/logo-result.svg" alt="no-data" />
        <div className="title-no-data mt-[20px]">Xin lỗi!</div>
        <div className="ly-do mt-[16px]">
          {props?.title ?? " Khóa học hiện tại đang không có dữ liệu."}
        </div>
        {!props?.hideTitleButton && (
          <div className="reply mt-[8px]">Chúng tôi sẽ cập nhật lại sau.</div>
        )}
      </div>
    </NoDataWrapper>
  );
};
const NoDataWrapper = styled.div`
  .title-no-data {
    color: #0353aa;
    font-size: 23px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
    text-transform: uppercase;
  }
  .ly-do {
    color: var(--darkgray, #3d3e40);
    text-align: center;
    font-size: 16px;
    font-style: normal;
    font-weight: 500;
    line-height: normal;
  }
  .rep-ly {
    color: var(--darkgray, #3d3e40);
    text-align: center;
    font-size: 16px;
    font-style: normal;
    font-weight: 500;
    line-height: normal;
  }
`;
export default NoData;
