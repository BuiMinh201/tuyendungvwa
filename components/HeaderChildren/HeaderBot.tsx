import styled from "styled-components";
import React, { useEffect, useState } from "react";
import { Button, Form, Input, message, Modal } from "antd";
import { useRouter } from "next/router";
import Title from "../Title";
import { traCuuHoSo } from "../../service/hoso/hoso";

const HeaderTop = () => {
  const router = useRouter();
  const [visible, setVisible] = useState<boolean>(false);
  const [activeTab, setActiveTab] = useState<string>("");
  const [form] = Form.useForm();
  const [showMenu, setShowMenu] = useState<boolean>(false);
  const handleChangeTabs = (tab: string) => {
    setActiveTab(tab);
    setShowMenu(false);
  };
  const onFinish = async (values: any) => {
    try {
      if (values?.cccd || values?.maHoSo) {
        router?.push({
          pathname: "/tra-cuu",
          query:
            values?.cccd && values?.maHoSo
              ? { cccd: values?.cccd?.trim(), maHoSo: values?.maHoSo?.trim() }
              : values?.cccd
              ? { cccd: values?.cccd?.trim() }
              : { maHoSo: values?.maHoSo?.trim() },
        });
        setVisible(false);
        form.resetFields();
        setShowMenu(false);
      } else {
        message.warning(
          "Vui lòng nhập thông tin CCCD hoặc Mã hồ sơ để tra cứu"
        );
      }
    } catch (e) {
      console.log(e);
    }
  };
  useEffect(() => {
    const handleScroll = () => {
      const scrollPosition = window.scrollY;

      // Adjust the threshold and element IDs according to your needs
      const element1 = document.getElementById("vi-tri-viec-lam");
      const element2 = document.getElementById("dot-tuyen-dung");
      const element3 = document.getElementById("quy-trinh");
      const offset = 100; // Adjust this offset as needed
      if (element1 || element2 || element3) {
        if (
          scrollPosition >= (element1?.offsetTop ?? 0) - offset &&
          scrollPosition < (element2?.offsetTop ?? 0) - offset
        ) {
          setActiveTab("tab-1");
        } else if (
          scrollPosition >= (element2?.offsetTop ?? 0) - offset &&
          scrollPosition < (element3?.offsetTop ?? 0) - offset
        ) {
          setActiveTab("tab-2");
        } else if (scrollPosition >= (element3?.offsetTop ?? 0) - offset) {
          setActiveTab("tab-3");
        } else {
          setActiveTab("");
        }
      }
    };

    window.addEventListener("scroll", handleScroll);

    // Cleanup the event listener on component unmount
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);
  useEffect(() => {
    if (router) {
      if (router?.pathname !== "/") {
        setActiveTab("");
      }
    }
  }, [router]);
  return (
    <HeaderBotWrapper className="pt-[4px] pb-[4px] px-[20px] xl:px-0 bg-white">
      <div className="container mx-auto">
        <div className="flex items-center justify-between">
          <div
            className="logo wow fadeInDown cursor-pointer"
            onClick={() => {
              router.push(`/`);
            }}
          >
            <img src="/header/logo.svg" alt={"logo"} />
          </div>
          <div className="menu hidden xl:grid grid-cols-3 gap-[40px]">
            <div
              className={`menu-item cursor-pointer py-[12px] ${
                activeTab === "tab-1" ? "active" : ""
              }`}
              onClick={() => {
                handleChangeTabs("tab-1");
                router.push(`/#vi-tri-viec-lam`);
              }}
            >
              Vị trí việc làm
            </div>
            <div
              className={`menu-item cursor-pointer py-[12px] ${
                activeTab === "tab-2" ? "active" : ""
              }`}
              onClick={() => {
                handleChangeTabs("tab-2");
                router.push(`/#dot-tuyen-dung`);
              }}
            >
              Đợt tuyển dụng
            </div>
            <div
              className={`menu-item cursor-pointer py-[12px] ${
                activeTab === "tab-3" ? "active" : ""
              }`}
              onClick={() => {
                handleChangeTabs("tab-3");
                router.push(`/#quy-trinh`);
              }}
            >
              Quy trình xử lý
            </div>
          </div>
          <div className="wow fadeInDown hidden xl:block">
            <button
              className="btn-tra-cuu w-[179px] h-[36px] flex justify-center items-center"
              onClick={() => setVisible(true)}
            >
              Tra cứu hồ sơ
            </button>
          </div>
          <div className="xl:hidden " onClick={() => setShowMenu(true)}>
            <img src={"/icon/menu.svg"} alt={"image"} />
          </div>
          {showMenu && (
            <>
              <div
                className="cover-ham"
                onClick={() => {
                  setShowMenu(false);
                }}
              ></div>
              <div className="ham-menu z-50 flex flex-col">
                <div className="pl-[16px] h-full w-full flex items-center">
                  <div className="">
                    <div
                      className={`menu-item cursor-pointer py-[12px] ${
                        activeTab === "tab-1" ? "active" : ""
                      }`}
                      onClick={() => {
                        handleChangeTabs("tab-1");
                        router.push(`#vi-tri-viec-lam`);
                      }}
                    >
                      Vị trí việc làm
                    </div>
                    <div
                      className={`menu-item cursor-pointer py-[12px] ${
                        activeTab === "tab-2" ? "active" : ""
                      }`}
                      onClick={() => {
                        handleChangeTabs("tab-2");
                        router.push(`#dot-tuyen-dung`);
                      }}
                    >
                      Đợt tuyển dụng
                    </div>
                    <div
                      className={`menu-item cursor-pointer py-[12px] ${
                        activeTab === "tab-3" ? "active" : ""
                      }`}
                      onClick={() => {
                        handleChangeTabs("tab-3");
                        router.push(`#quy-trinh`);
                      }}
                    >
                      Quy trình xử lý
                    </div>
                  </div>
                </div>
                <div className="flex pb-[20px] justify-center">
                  <button
                    className="btn-tra-cuu w-[179px] h-[36px] flex justify-center items-center"
                    onClick={() => setVisible(true)}
                  >
                    Tra cứu hồ sơ
                  </button>
                </div>
              </div>
            </>
          )}
        </div>
      </div>
      <Modal
        open={visible}
        onCancel={() => setVisible(false)}
        footer={null}
        destroyOnClose
      >
        <TraCuuHoSoWrapper>
          <Title
            title={"Tra cứu hồ sơ"}
            align={"center"}
            uppercase={false}
            hideSearch
            hideArrows
          />
          <div>
            <Form layout={"vertical"} onFinish={onFinish} form={form}>
              <Form.Item name={"maHoSo"} label={"Mã hồ sơ"}>
                <Input placeholder={"Nhập thông tin"} />
              </Form.Item>
              <Form.Item name={"cccd"} label={"CCCD/CMND"}>
                <Input placeholder={"Nhập thông tin"} />
              </Form.Item>
              <div className="flex justify-center mt-[32px]">
                <button
                  type={'submit'}
                  // onClick={() => form.submit()}
                  className="btn-tra-cuu w-[370px] h-[40px] flx justify-center items-center"
                >
                  Tra cứu
                </button>
              </div>
            </Form>
          </div>

        </TraCuuHoSoWrapper>
      </Modal>
    </HeaderBotWrapper>
  );
};
const TraCuuHoSoWrapper = styled.div`
  .btn-tra-cuu {
    color: var(--White, #fff);
    text-align: center;
    font-family: Arial;
    font-size: 18px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;

    border-radius: 4px;
    background: var(--Main-2, #2387e3);
  }
`;
const HeaderBotWrapper = styled.div`
  .menu-item {
    color: var(--TEXT-01, #212529);
    font-family: Arial;
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
  }
  .active {
    color: var(--Main-2, #2387e3);
    text-align: center;
    font-family: Arial;
    font-size: 16px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;

    border-bottom: 2px solid #2387e3;
  }
  .btn-tra-cuu {
    color: var(--white, #fff);
    text-align: center;
    font-family: Arial;
    font-size: 16px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
    text-transform: uppercase;

    border-radius: 4px;
    background: var(--Main-2, #2387e3);
  }

  .cover-ham {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: #ffffffa1;
    z-index: 30;
    opacity: 1;
    -webkit-transition: 500ms;
    transition: 500ms;
  }
  .ham-menu {
    width: 50%;
    height: 100%;
    position: fixed;
    top: 0;
    right: 0;
    //visibility: hidden;
    //transform: translate(110%);
    //z-index: 998;
    background-color: #ffffff;
    animation: slide 0.5s forwards;
    //transition: 1s;
    //display: flex;
    //justify-content: center;
    //padding-left: 16px;
    //align-items: center;
    background-image: url("/images/bg-quy-trinh.png");
    background-size: cover;
    background-repeat: no-repeat;
  }
  @keyframes slide {
    from {
      right: -50%;
    }
    to {
      right: 0;
    }
  }
  @keyframes appear {
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }
`;
export default HeaderTop;
