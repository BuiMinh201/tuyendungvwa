import { Col, Divider, Form, Input, Row, Select } from "antd";
import { useContext, useEffect, useState } from "react";
import { getphuongXa, getQuanHuyen } from "../../service/core/donvihanhchinh";
import rules from "../../utils/rules";
import { AuthContext } from "../../context/AuthContext";
import { ThongTinNhanSu } from "../../Interface/ThongTinNhanSu/typing";
import { useWatch } from "antd/lib/form/Form";

const DonViHanhChinhBlock = (props: {
  type: string;
  title: string;
  dataTinh: any;
  addSoNha?: boolean;
  form: any;
  disabled?: boolean;
  required?: boolean;
}) => {
  const { type, title, addSoNha, form, dataTinh, disabled, required } = props;
  const [idTinh, setIdTinh] = useState<string>();
  const [idHuyen, setIdHuyen] = useState<string>();
  const [listHuyen, setListHuyen] = useState<any>([]);
  const [listXa, setListXa] = useState<any>([]);
  const maTinh = useWatch(["noiSinhThanhPhoMa"], form);
  const { dataDaoTao } = useContext(AuthContext);
  // const isCreate = !edit && !isView && !isXemDanhSachCapNhat && !isViewHistory;
  // const disabledForm = isCreate ? false : !isDraff;
  const disabledForm = disabled;

  const onchangeTinhThanhPho = (e: string) => {
    setIdTinh(e);

    form.setFieldsValue({
      [type + "XaMa"]: null,
      [type + "QuanMa"]: null,
      [`${type}ThanhPhoTen`]: dataTinh.data.find((i: any) => i.ma === e)
        ?.tenDonVi,
    });

    setListHuyen([]);
    setListXa([]);
  };
  const onchangeQuanHuyen = (e: string) => {
    setIdHuyen(e);
    form.setFieldsValue({
      [type + "XaMa"]: null,
      [type + "QuanTen"]: listHuyen.data.find((i: any) => i.ma === e)?.tenDonVi,
    });

    setListXa([]);
  };
  const onChangeXa = (e: string) => {
    form.setFieldsValue({
      [type + "XaTen"]: listXa.data.find((i: any) => i.ma === e)?.tenDonVi,
    });
  };
  useEffect(() => {
    if (maTinh && type === "noiSinh") {
      // const idTinhForm = dataTinh?.data?.find((item:any) => item?.ma === maTinh);
      setIdTinh(maTinh);
    }
  }, [maTinh]);
  useEffect(() => {
    if (idTinh) {
      getQuanHuyen(idTinh).then((data) => {
        setListHuyen(data.data);
      });
    } else {
      setListHuyen([]);
      setListXa([]);
    }
  }, [idTinh]);

  useEffect(() => {
    if (idHuyen) {
      getphuongXa(idHuyen).then((data) => {
        setListXa(data.data);
      });
    } else setListXa([]);
  }, [idHuyen]);

  useEffect(() => {
    if (dataDaoTao) {
      setIdTinh(
        dataDaoTao?.[
          `${type}ThanhPhoMa` as keyof ThongTinNhanSu.IRecord
        ] as string
      );
      setIdHuyen(
        dataDaoTao?.[`${type}QuanMa` as keyof ThongTinNhanSu.IRecord] as string
      );
    }
  }, [type, dataDaoTao]);

  return (
    <>
      <Divider orientation="center">{title}</Divider>
      <Row gutter={[12, 0]}>
        <Col span={24} md={addSoNha ? 6 : 8}>
          <Form.Item
            name={`${type}ThanhPhoMa`}
            label="Tỉnh/Thành phố"
            rules={required ? [...rules.required] : []}
          >
            <Select
              disabled={disabledForm}
              placeholder="Chọn tỉnh/thành phố"
              options={dataTinh.data?.map((item: any) => ({
                key: item.ma,
                value: item.ma,
                label: item.tenDonVi,
              }))}
              onChange={onchangeTinhThanhPho}
              allowClear
              showSearch
              optionFilterProp='label'
              filterOption={(input, option) => ((option?.label as string)?.toLowerCase() ?? '')?.includes(input?.toLowerCase())}
            />
          </Form.Item>
          <Form.Item name={`${type}ThanhPhoTen`} style={{ display: "none" }}>
            <Input />
          </Form.Item>
        </Col>
        <Col span={24} md={addSoNha ? 6 : 8}>
          <Form.Item
            name={`${type}QuanMa`}
            label="Quận/Huyện"
            rules={required ? [...rules.required] : []}
          >
            <Select
              disabled={disabledForm}
              placeholder="Chọn quận/huyện"
              options={(listHuyen.data ?? []).map((item: any) => ({
                key: item.ma,
                value: item.ma,
                label: item.tenDonVi,
              }))}
              onChange={onchangeQuanHuyen}
              allowClear
              showSearch
              optionFilterProp='label'
              filterOption={(input, option) => ((option?.label as string)?.toLowerCase() ?? '')?.includes(input?.toLowerCase())}
            />
          </Form.Item>
          <Form.Item name={`${type}QuanTen`} style={{ display: "none" }}>
            <Input />
          </Form.Item>
        </Col>
        <Col span={24} md={addSoNha ? 6 : 8}>
          <Form.Item
            name={`${type}XaMa`}
            label="Phường/Xã"
            rules={required ? [...rules.required] : []}
          >
            <Select
              disabled={disabledForm}
              placeholder="Chọn phường/xã"
              options={(listXa.data ?? []).map((item: any) => ({
                key: item.ma,
                value: item.ma,
                label: item.tenDonVi,
              }))}
              allowClear
              onChange={onChangeXa}
              showSearch
              optionFilterProp='label'
              filterOption={(input, option) => ((option?.label as string)?.toLowerCase() ?? '')?.includes(input?.toLowerCase())}
            />
          </Form.Item>
          <Form.Item name={`${type}XaTen`} style={{ display: "none" }}>
            <Input />
          </Form.Item>
        </Col>

        {props.addSoNha && (
          <Col span={24} md={addSoNha ? 6 : 8}>
            <Form.Item
              name={`${type}SoNha`}
              label="Địa chỉ cụ thể"
              rules={
                required
                  ? [...rules.text, ...rules.length(250), ...rules.required]
                  : [...rules.text, ...rules.length(250)]
              }
            >
              <Input
                disabled={disabledForm}
                placeholder="Ví dụ: 05 đường Nguyễn Khuyến"
              />
            </Form.Item>
          </Col>
        )}
      </Row>
    </>
  );
};
export default DonViHanhChinhBlock;
