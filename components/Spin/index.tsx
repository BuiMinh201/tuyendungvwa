import styled from "styled-components";
import {Spinner} from "flowbite-react";

interface IProps {
  spinning: boolean;
  children: any;
  style?:React.CSSProperties
  className?:string
}
const Spin = (props: IProps) => {
  if (props.spinning) {
    return (
      <SpinningWrapper>
       <div className={`flex items-center justify-center  w-full ${props?.className??'h-full'}`}>
         <div className={`w-full h-full flex items-center justify-center `} style={props?.style}>
           <div className="text-center">
             <Spinner   size="xl" aria-label="Center-aligned spinner example" />
             <div>
               Đang lấy thông tin....
             </div>
           </div>
         </div>
       </div>
      </SpinningWrapper>
    );
  } else {
    return (
      <>
        <div>{props?.children}</div>
      </>
    );
  }
};

const SpinningWrapper = styled.div``;
export default Spin;
