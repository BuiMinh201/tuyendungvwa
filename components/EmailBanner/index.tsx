import styled from "styled-components";
import { Form, Input, message } from "antd";
import { useState } from "react";
import _ from "lodash";
import { luuEmail } from "../../service/core/nhanemail";
import rules from "../../utils/rules";

const EmailBanner = () => {
  const [email, setEmail] = useState<string>();
  const [form] = Form.useForm();
  const debouncedInput = _.debounce((value) => {
    setEmail(value?.target?.value);
  }, 800);
  function isValidEmail(email: string) {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  }

  const handleSendEmail = async (val: any) => {
    try {
      if (isValidEmail(val?.email ?? "")) {
        const res = await luuEmail(val?.email ?? "");
        if (res) {
          message.success("Hệ thống đã ghi nhận email của bạn");
          form.resetFields();
          setEmail("");
        }
      } else {
        message.error("Email không đúng định dạng VD:email@gmail.com");
      }
    } catch (e) {
      console.log(e);
    }
  };
  return (
    <EmailBannerWrapper>
      <div className="container mx-auto py-[60px] px-[20px] xl:px-0">
        <div className="title-email-1 text-center">
          Đăng ký email để nhận thông báo về các đợt tuyển dụng của Học viện
        </div>
        <div className="title-email-2 text-center mt-[16px]">
          Các đợt tuyển dụng sẽ được thông báo tới tài khoản email của ứng viên
          và không bị bỏ lỡ.
        </div>
        <div className="send-email flex justify-center mt-[32px]">
          <Form form={form} onFinish={handleSendEmail}>
            <Form.Item
              name={"email"}
              rules={[...rules.required, ...rules.email]}
            >
              <Input
                value={email}
                placeholder="Nhập email"
                className="xl:w-[436px] w-[250px] h-[50px]"
                // onChange={(val) => setEmail(val?.target?.value)}
              />
            </Form.Item>
          </Form>
          <button
            className="xl:w-[117px] w-[80px] flex justify-center items-center xl:text-[16px] text-[12px]"
            onClick={() => {
              form.submit();
            }}
          >
            Gửi email
          </button>
        </div>
      </div>
    </EmailBannerWrapper>
  );
};

const EmailBannerWrapper = styled.div`
  background-image: url("/images/email-bg.png");
  background-repeat: no-repeat;
  background-size: cover;
  .title-email-1 {
    color: var(--white, #fff);
    text-align: center;
    font-family: Arial;
    font-size: 28px;
    font-style: normal;
    font-weight: 700;
    line-height: 125%; /* 35px */
  }
  .title-email-2 {
    color: var(--white, #fff);
    font-family: Arial;
    font-size: 18px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
  }
  .send-email {
    .ant-form-item {
      margin: 0 !important;
    }
    input {
      height: 50px;

      color: var(--Normal-gray, #000000);
      font-family: Arial;
      font-size: 16px;
      font-style: normal;
      font-weight: 400;
      line-height: normal;

      border-radius: 4px 0px 0px 4px;
      padding: 10px 16px;

      outline: none;
    }
    button {
      height: 50px;
      color: var(--Main-2, #2387e3);
      text-align: center;
      font-family: Arial;
      font-style: normal;
      font-weight: 700;
      line-height: normal;

      border-radius: 0px 4px 4px 0px;
      border-left: 1px solid var(--Main-2, #2387e3);
      background: #9de0ff;
    }
  }
`;
export default EmailBanner;
