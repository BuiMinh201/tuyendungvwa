import styled from "styled-components";
import { message, Modal } from "antd";
import { ReactNode } from "react";
import { CopyOutlined } from "@ant-design/icons";
import moment from "moment";
import { ThongTinNhanSu } from "../../Interface/ThongTinNhanSu/typing";
interface IProps {
  visible?: boolean;
  setVisible?: (val: boolean) => void;
  title?: string;
  footer?: ReactNode;
  data?: ThongTinNhanSu.IRecord;
  dataVitri?: any;
  isModal?: boolean;
  type?: "ho-so" | "lich-thi" | "ket-qua" | "trung-tuyen";
}
const KetQuaUngTuyen = (props: IProps) => {
  const {
    visible,
    setVisible,
    title,
    footer = null,
    data,
    isModal,
    type = "ho-so",
    dataVitri,
  } = props;
  const handleCopyClipboard = (value: any) => {
    navigator.clipboard.writeText(value);
    message.success("Sao chép thành công");
  };
  const renderStatus = (
    type: "ho-so" | "lich-thi" | "ket-qua" | "trung-tuyen"
  ) => {
    switch (type) {
      case "ket-qua":
        return (
          <div className="status w-[412px] h-[30px] flex justify-center items-center">
            Thời gian cập nhật điểm thi{" "}
            {data?.updatedAt
              ? moment(data?.updatedAt).format("HH:mm DD/MM/YYYY")
              : "--"}
          </div>
        );
        break;
      case "trung-tuyen":
        return (
          <div className={`${data?.diemTuyenDung?.trungTuyen==='Không trúng tuyển'?'status-red':'status-green'} w-[193px] h-[30px] flex justify-center items-center`}>
            {data?.diemTuyenDung?.trungTuyen??'Đã trúng tuyển'}
          </div>
        );
        break;
      case "ho-so":
        return (
          <div className="status w-[177px] h-[30px] flex justify-center items-center">
            {data?.trangThaiHoSoUngTuyen}
          </div>
        );
        break;
      case "lich-thi":
        return (
          <div className="status w-[291px] h-[30px] flex justify-center items-center">
            Hồ sơ đủ điều kiện ứng tuyển
          </div>
        );
        break;
      default:
        return (
          <div className="status w-[177px] h-[30px] flex justify-center items-center">
            Đã nộp hồ sơ
          </div>
        );
        break;
    }
  };
  if (isModal && setVisible) {
    return (
      <>
        <Modal
          title={title}
          open={visible}
          onCancel={() => setVisible(false)}
          destroyOnClose
          footer={footer}
          width={1000}
        >
          <KetQuaUngTuyenWrapper>
            <div>
              <div className="flex justify-center">
                <img src="/icon/logo-result.svg" />
              </div>
              <div className="title-result uppercase mt-[28px]">
                hồ sơ ứng tuyển
              </div>
              <div className="flex justify-center mt-[16px]">
                <div className="status w-[177px] h-[30px] flex justify-center items-center">
                  Đã nộp hồ sơ
                </div>
              </div>
            </div>
            <div className="information mt-[40px]">
              <div className="grid grid-cols-2 gap-[20px]">
                <div className="col-span-1 flex items-center ">
                  <div className="title-info lg:w-[105px] mr-[8px]">
                    Mã hồ sơ
                  </div>
                  <div className="content-info">
                    <b>{data?.maHoSo}</b>{" "}
                    <CopyOutlined
                      rev={undefined}
                      style={{ cursor: "pointer" }}
                      onClick={() => {
                        handleCopyClipboard(data?.maHoSo);
                      }}
                    />
                  </div>
                </div>
                <div className="col-span-1 flex items-center ">
                  <div className="title-info lg:w-[105px] mr-[8px]">
                    Họ và tên
                  </div>
                  <div className="content-info">{data?.hoTen}</div>
                </div>
                <div className="col-span-1 flex items-center ">
                  <div className="title-info lg:w-[105px] mr-[8px]">
                    CCCD/CMND
                  </div>
                  <div className="content-info">{data?.cccdCMND}</div>
                </div>
                <div className="col-span-1 flex items-center ">
                  <div className="title-info lg:w-[105px] mr-[8px]">Email</div>
                  <div className="content-info">{data?.email}</div>
                </div>
                <div className="col-span-1 flex items-center">
                  <div className="title-info lg:w-[105px] mr-[8px]">
                    Vị trí ứng tuyển
                  </div>
                  <div className="content-info">
                    {data?.tenDonViViTriTuyenDung ?? data?.viTriUngTuyen}
                  </div>
                </div>
                <div className="col-span-1 flex items-center">
                  <div className="title-info lg:w-[105px] mr-[8px]">Đơn vị</div>
                  <div className="content-info">
                    {data?.tenDonViTuyenDung ?? data?.tenDonViTuyenDung}
                  </div>
                </div>
              </div>
            </div>
            <div className={"title-footer mt-[25px]"}>
              <div>
                Học viện đã tiếp nhận hồ sơ ứng tuyển và đang trong quá trình
                đánh giá hồ sơ. Kết quả vòng hồ sơ sẽ được phản hồi lại trong
                vòng 5 - 7 ngày qua email ứng tuyển.
              </div>
              <div className="mt-[12px]">
                Xem lại hồ sơ ứng tuyển{" "}
                <a href={`/ho-so/${data?.maHoSo}`}>tại đây</a> hoặc in hồ sơ{" "}
                <a href={`/ho-so/${data?.maHoSo}`}>tại đây</a>.
              </div>
              <div className="mt-[12px]">
                Tra cứu tình trạng hồ sơ ứng tuyển bằng Mã hồ sơ và CCCD/CMND{" "}
                <a href={`/tra-cuu?cccd=&maHoSo=${data?.maHoSo}`}>tại đây</a>.
              </div>
            </div>
          </KetQuaUngTuyenWrapper>
        </Modal>
      </>
    );
  } else {
    return (
      <KetQuaUngTuyenWrapper>
        <div className="box-ket-qua p-[40px]">
          <div>
            <div className="flex justify-center">
              <img src="/icon/logo-result.svg" />
            </div>
            <div className="title-result uppercase mt-[28px]">
              hồ sơ ứng tuyển
            </div>
            <div className="flex justify-center mt-[16px]">
              {renderStatus(type)}
            </div>
          </div>
          <div className="information mt-[40px]">
            <div className="grid xl:grid-cols-2 grid-cols-1 gap-[20px]">
              <div className="col-span-1 flex items-center ">
                <div className="title-info lg:w-[105px] mr-[8px]">Mã hồ sơ</div>
                <div className="content-info">
                  <b>{data?.maHoSo}</b>{" "}
                  <CopyOutlined
                    rev={undefined}
                    style={{ cursor: "pointer" }}
                    onClick={() => {
                      handleCopyClipboard(data?.maHoSo);
                    }}
                  />
                </div>
              </div>
              <div className="col-span-1 flex items-center ">
                <div className="title-info lg:w-[105px] mr-[8px]">
                  Họ và tên
                </div>
                <div className="content-info">{data?.hoTen}</div>
              </div>
              <div className="col-span-1 flex items-center ">
                <div className="title-info lg:w-[105px] mr-[8px]">
                  CCCD/CMND
                </div>
                <div className="content-info">{data?.cccdCMND}</div>
              </div>
              <div className="col-span-1 flex items-center ">
                <div className="title-info lg:w-[105px] mr-[8px]">Email</div>
                <div className="content-info">{data?.email}</div>
              </div>
              <div className="col-span-1 flex items-center">
                <div className="title-info lg:w-[105px] mr-[8px]">
                  Vị trí ứng tuyển
                </div>
                <div className="content-info">
                  {dataVitri?.tenViTri ?? data?.viTriUngTuyen}
                </div>
              </div>
              <div className="col-span-1 flex items-center">
                <div className="title-info lg:w-[105px] mr-[8px]">Đơn vị</div>
                <div className="content-info">
                  {dataVitri?.donVi?.ten ?? data?.tenDonViTuyenDung}
                </div>
              </div>
              {type === "lich-thi" && (
                <>
                  <div className="col-span-1 flex items-center">
                    <div className="title-info-lich lg:w-[105px] mr-[8px]">
                      Lịch thi viết
                    </div>
                    <div className="content-info-lich">
                      {/*{data?.diemTuyenDung?.lichThiViet*/}
                      {/*  ? moment(data?.diemTuyenDung?.lichThiViet)?.format(*/}
                      {/*      "HH:mm DD/MM/YYYY"*/}
                      {/*    )*/}
                      {/*  : "--"}*/}
                      {data?.diemTuyenDung?.lichThiVietFormat}
                    </div>
                  </div>
                  <div className="col-span-1 flex items-center ">
                    <div className="title-info-lich lg:w-[105px] mr-[8px]">
                      Lịch phỏng vấn
                    </div>
                    <div className="content-info-lich">
                      {/*{data?.diemTuyenDung?.lichThiPhongVan*/}
                      {/*  ? moment(data?.diemTuyenDung?.lichThiViet)?.format(*/}
                      {/*      "HH:mm DD/MM/YYYY"*/}
                      {/*    )*/}
                      {/*  : "--"}*/}
                      {data?.diemTuyenDung?.lichThiPhongVanFormat}
                    </div>
                  </div>
                  <div className="col-span-1 flex items-center">
                    <div className="title-info-lich lg:w-[105px] mr-[8px]">
                      Lịch thi giảng
                    </div>
                    <div className="content-info-lich">
                      {/*{data?.diemTuyenDung?.lichThiGiang*/}
                      {/*  ? moment(data?.diemTuyenDung?.lichThiViet)?.format(*/}
                      {/*      "HH:mm DD/MM/YYYY"*/}
                      {/*    )*/}
                      {/*  : "--"}*/}
                      {data?.diemTuyenDung?.lichThiGiangFormat}
                    </div>
                  </div>
                  <div className="col-span-1 flex items-center">
                    <div className="title-info-lich lg:w-[105px] mr-[8px] flex-shrink-0">
                      Địa điểm thi
                    </div>
                    <div className="content-info-lich">
                      {data?.diemTuyenDung?.diaDiemThi ?? "--"}
                    </div>
                  </div>
                </>
              )}
              {type === "ket-qua" && (
                <>
                  <div className="col-span-1 flex items-center ">
                    <div className="title-info-lich lg:w-[105px] mr-[8px]">
                      Thi viết
                    </div>
                    <div className="content-info-lich">
                      {data?.diemTuyenDung?.diemThiViet
                        ? data?.diemTuyenDung?.diemThiViet
                        : "--"}
                      /100
                    </div>
                  </div>
                  <div className="col-span-1 flex items-center ">
                    <div className="title-info-lich lg:w-[105px] mr-[8px]">
                      Phỏng vấn
                    </div>
                    <div className="content-info-lich">
                      {data?.diemTuyenDung?.diemPhongVan
                        ? data?.diemTuyenDung?.diemPhongVan
                        : "--"}
                      /50
                    </div>
                  </div>
                  <div className="col-span-1 flex items-center">
                    <div className="title-info-lich lg:w-[105px] mr-[8px]">
                      Giảng
                    </div>
                    <div className="content-info-lich">
                      {data?.diemTuyenDung?.diemThiGiang
                        ? data?.diemTuyenDung?.diemThiGiang
                        : "--"}
                      /100
                    </div>
                  </div>
                  <div className="col-span-1 flex items-center">
                    <div className="title-info-lich lg:w-[105px] mr-[8px]">
                      Điểm ưu tiên
                    </div>
                    <div className="content-info-lich">
                      {" "}
                      {data?.diemTuyenDung?.diemUuTien
                        ? data?.diemTuyenDung?.diemUuTien
                        : "--"}
                    </div>
                  </div>
                </>
              )}
            </div>
          </div>
          {type === "ho-so" && (
            <div className={"title-footer mt-[25px]"}>
              <div>
                Học viện đã tiếp nhận hồ sơ ứng tuyển và đang trong quá trình
                đánh giá hồ sơ. Kết quả vòng hồ sơ sẽ được phản hồi lại trong
                vòng 5 - 7 ngày qua email ứng tuyển.
              </div>
            </div>
          )}
          {type === "lich-thi" && (
            <div className={"title-footer mt-[25px]"}>
              <div>
                Kính gửi các ứng viên,<br/>
                Phòng TCHC Học viện Phụ nữ Việt Nam xin gửi các ứng viên lịch thi tuyển dụng nhân sự. Đề nghị các ứng viên có mặt đầy đủ theo đúng lịch trong thông báo.<br/>
                Trân trọng cảm ơn.
              </div>
            </div>
          )}
          {type === "ket-qua" && (
            <div className={"title-footer mt-[25px]"}>
              <div className="point uppercase text-center">
                TỔNG ĐIỂM {data?.diemTuyenDung?.tongDiem}
              </div>
              <div className="mt-[24px]">
                Kính gửi các ứng viên,<br/>

                Học viện Phụ nữ Việt Nam xin gửi thông báo điểm thi tuyển dụng nhân sự và chuyển ngạch năm 2024. Thông báo cũng sẽ được đăng tải trên <a href={'https://canbo.hvpnvn.edu.vn/dashboard'} target={"_blank"}>website</a> của Học viện.<br/>

                Trân trọng cảm ơn
                {/*Điểm thi đã được cập nhật. Ứng viên có thể phúc khảo{" "}*/}
                {/*<a*/}
                {/*  href={`https://canbo.hvpnvn.edu.vn/dashboard`}*/}
                {/*  target={"_blank"}*/}
                {/*>*/}
                {/*  tại đây*/}
                {/*</a>{' '}*/}
                {/*trong thời gian 10 ngày kể từ ngày cập nhật điểm.*/}
              </div>
            </div>
          )}
          {type === "trung-tuyen" && (
            <div className={"title-footer mt-[25px]"}>
              <div>
                Anh/chị đã trúng tuyển trong kỳ thi tuyển dụng nhân sự năm 2024
                của Học viện Phụ nữ Việt Nam. Đề nghị anh/chị trực tiếp đến Học
                viện Phụ nữ Việt Nam để được hướng dẫn và thực hiện các thủ tục
                tuyển dụng theo đúng quy định hiện hành; mọi sự chậm trễ, Học
                viện không chịu trách nhiệm giải quyết.
              </div>
            </div>
          )}
        </div>
      </KetQuaUngTuyenWrapper>
    );
  }
};
const KetQuaUngTuyenWrapper = styled.div`
  .box-ket-qua {
    border-radius: 8px;
    background: #fff;

    /* 1 */
    box-shadow: 0px 0px 4px 0px rgba(26, 31, 109, 0.25);
  }

  .point {
    color: var(--Main-2, #2387e3);
    font-family: Arial;
    font-size: 20px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
  }

  .title-footer {
    color: var(--TEXT-01, #212529);
    font-family: Arial;
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    line-height: 125%; /* 20px */
  }

  .title-info {
    color: var(--Normal-gray, #b2b5c1);
    font-family: Arial;
    font-size: 15px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
  }

  .title-info-lich {
    color: var(--Normal-gray, #2387e3);
    font-family: Arial;
    font-size: 15px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
  }

  .content-info {
    color: var(--TEXT-01, #212529);
    font-family: Arial;
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
  }

  .content-info-lich {
    color: var(--TEXT-01, #2387e3);
    font-family: Arial;
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
  }

  .status {
    color: var(--Text, #0084ff);
    font-family: Arial;
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;

    border-radius: 4px;
    background: rgba(0, 132, 255, 0.2);
  }

  .status-green {
    color: #00a61b;
    font-family: Arial;
    font-size: 16px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
    border-radius: 4px;
    background: rgba(0, 166, 27, 0.2);
  }

  .status-red {
    color: #b70000;
    font-family: Arial;
    font-size: 16px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;

    border-radius: 4px;
    background: rgba(142, 0, 0, 0.2);
  }

  .title-result {
    color: #000;
    text-align: center;
    font-family: Arial;
    font-size: 24px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
    text-transform: uppercase;
  }

  .title-footer {
    a {
      color: #0353aa;
      cursor: pointer;
      text-decoration: underline;
    }
  }
`;
export default KetQuaUngTuyen;
