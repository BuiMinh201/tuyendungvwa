import styled from "styled-components";
interface IProps{
  title:string,
  description?:string
}
const TitlePage = (props:IProps) => {
  const {title,description}=props
  return(
    <TitlePageWrapper className=" flex flex-col items-center mt-[40px] mb-[24px]">
     <div className="title-mid lg:text-[32px] text-[24px]">
       {title?.toUpperCase()}
     </div>
      {description&&
      <p>{description}</p>}
    </TitlePageWrapper>
  )
}
const TitlePageWrapper=styled.div`
  .title-mid {
    color: #0353aa;
    text-align: center;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
  }
`
export default TitlePage;