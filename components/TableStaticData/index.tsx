import { MenuOutlined, PlusOutlined, SearchOutlined } from "@ant-design/icons";
import { Button, Drawer, Input, Modal, Table, Tooltip } from "antd";
import _ from "lodash";
import { useEffect, useState } from "react";
import { type TDataOption, type TableStaticProps } from "./typing";
import styled from "styled-components";

const TableStaticData = (props: TableStaticProps) => {
  const {
    Form,
    showEdit,
    setShowEdit,
    addStt,
    data,
    children,
    hasCreate,
    hasTotal,
    rowSortable,
    pagination=false
  } = props;
  const [searchText, setSearchText] = useState<string>("");
  const [searchedColumn, setSearchedColumn] = useState();
  const [total, setTotal] = useState<number>();

  useEffect(() => {
    setTotal(data?.length);
    setSearchText("");
    setSearchedColumn(undefined);
  }, [data?.length]);

  const handleSearch = (selectedKeys: any, confirm: any, dataIndex: any) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = (clearFilters: any) => {
    clearFilters();
    setSearchText("");
    setSearchedColumn(undefined);
  };

  const getColumnSearchProps = (dataIndex: any) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }: any) => (
      <div className="column-search-box" onKeyDown={(e) => e.stopPropagation()}>
        <Input
          placeholder="Nhập từ khóa"
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
        />
        <Button
          type="primary"
          onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
          icon={<SearchOutlined rev={undefined} />}
          style={{ width: 90 }}
        >
          Tìm
        </Button>
        <Button
          onClick={() => {
            handleReset(clearFilters);
            handleSearch(selectedKeys, confirm, dataIndex);
            setSearchText("");
          }}
          style={{ width: 90 }}
        >
          Xóa
        </Button>
      </div>
    ),
    filterIcon: (filtered: boolean) => (
      <SearchOutlined
        className={filtered ? "text-primary" : undefined}
        rev={undefined}
      />
    ),
    onFilter: (value: any, record: any) =>
      typeof dataIndex === "string"
        ? record[dataIndex]
            ?.toString()
            ?.toLowerCase()
            ?.includes(value.toLowerCase())
        : typeof dataIndex === "object"
        ? record[dataIndex[0]][dataIndex?.[1]]
            ?.toString()
            ?.toLowerCase()
            ?.includes(value.toLowerCase())
        : "",

    render: (text: any) =>
      searchedColumn === dataIndex ? (
        <></>
      ) : (
        // <Highlighter
        //   highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
        //   searchWords={[searchText]}
        //   autoEscape
        //   textToHighlight={text ? text.toString() : ''}
        // />
        text
      ),
  });

  const getFilterColumnProps = (dataIndex: any, filterData?: any[]) => {
    return {
      filters: filterData?.map((item: string | TDataOption) =>
        typeof item === "string"
          ? { key: item, value: item, text: item }
          : { key: item.value, value: item.value, text: item.label }
      ),
      onFilter: (value: string, record: any) =>
        record[dataIndex]?.indexOf(value) === 0,
    };
  };

  const columns = props.columns
    ?.filter((item) => !item.hide)
    ?.map((item) => ({
      ...item,
      ...(item?.filterType === "string"
        ? getColumnSearchProps(item.dataIndex)
        : item?.filterType === "select"
        ? getFilterColumnProps(item.dataIndex, item.filterData)
        : undefined),
      ...(item?.sortable && {
        sorter: (a: any, b: any) =>
          a[item.dataIndex as string] > b[item.dataIndex as string] ? 1 : -1,
      }),
      children: item.children?.map((child) => ({
        ...child,
        ...(child?.filterType === "string"
          ? getColumnSearchProps(child.dataIndex)
          : child?.filterType === "select"
          ? getFilterColumnProps(child.dataIndex, child.filterData)
          : undefined),
        ...(child?.sortable && {
          sorter: (a: any, b: any) =>
            a[child.dataIndex as string] > b[child.dataIndex as string]
              ? 1
              : -1,
        }),
      })),
    }));

  if (addStt)
    columns.unshift({
      title: "TT",
      dataIndex: "index",
      align: "center",
      width: 40,
      children: undefined,
    });

  const onSortEnd = ({ oldIndex, newIndex }: any) => {
    if (oldIndex !== newIndex) {
      const record = props.data?.[oldIndex];
      if (props.onSortEnd) props.onSortEnd(record, newIndex);
    }
  };

  const DraggableBodyRow: React.FC<any> = ({
    className,
    style,
    ...restProps
  }) => {
    // function findIndex base on Table rowKey props and should always be a right array index
    const index = restProps["data-row-key"];
    return <></>;
  };
  //#endregion

  return (
    <TableStaticDataWrapper>
      <div className="table-base">
        <div className="header">
          {children}
          <div className="action">
            {hasCreate && (
              <Button
                onClick={() => {
                  if (setShowEdit) setShowEdit(true);
                }}
                icon={<PlusOutlined rev={undefined} />}
                type="primary"
                style={{ marginBottom: 8 }}
                size={props?.size ?? "middle"}
              >
                Thêm mới
              </Button>
            )}
          </div>

          <div className="extra">
            {hasTotal ? (
              <Tooltip title="Tổng số dữ liệu">
                <div className="total">
                  Tổng số:
                  <span>{total || props.data?.length || 0}</span>
                </div>
              </Tooltip>
            ) : null}
          </div>
        </div>

        <Table
          pagination={pagination}
          title={props?.title ? () => props.title : false}
          columns={columns}
          dataSource={(props?.data ?? []).map((item, index) => ({
            ...item,
            index: index + 1,
            key: index,
          }))}
          onChange={(pagination, filters, sorter, extra) => {
            setTotal(extra.currentDataSource.length ?? pagination.total);
          }}
          loading={props?.loading}
          size={props.size}
          scroll={{ x: _.sum(columns.map((item) => item.width ?? 80)) }}
          bordered
          components={
            rowSortable
              ? {
                  body: {
                    wrapper: null,
                    row: DraggableBodyRow,
                  },
                }
              : undefined
          }
          {...props?.otherProps}
        />
        {Form && (
          <>
            {props?.formType === "Drawer" ? (
              <Drawer
                width={props?.widthDrawer}
                onClose={() => {
                  if (setShowEdit) setShowEdit(false);
                }}
                destroyOnClose
                footer={false}
                open={showEdit}
              >
                <Form
                  onCancel={() => {
                    if (setShowEdit) setShowEdit(false);
                  }}
                  {...props.formProps}
                />
              </Drawer>
            ) : (
              <Modal
                width={props?.widthDrawer}
                onCancel={() => {
                  if (setShowEdit) setShowEdit(false);
                }}
                destroyOnClose
                footer={false}
                bodyStyle={{padding:0}}
                styles={{body:{padding:0},header:{padding:0}}}
                open={showEdit}
              >
                <Form
                  onCancel={() => {
                    if (setShowEdit) setShowEdit(false);
                  }}
                  {...props.formProps}
                />
              </Modal>
            )}
          </>
        )}
      </div>
    </TableStaticDataWrapper>
  );
};
const TableStaticDataWrapper = styled.div`
  .table-base {
    .header {
      display: flex;
      flex-wrap: wrap;
      gap: 8px;
      justify-content: space-between;
      margin-bottom: 8px;

      .action {
        display: flex;
        flex-wrap: wrap;
        gap: 8px;
      }

      .extra {
        display: flex;
        flex-wrap: wrap;
        gap: 8px;
        justify-content: flex-end;

        .total {
          height: 32px;
          padding: 0 15px;
          line-height: 1.5715;
          white-space: nowrap;
          border: 1px solid #d9d9d9;
          border-radius: 2px;
          transition: 0.3s;

          &:hover {
            border-color: #2387e3;
            transition: 0.3s;
          }

          span {
            margin-left: 8px;
            color: #2387e3;
            font-weight: 700;
            font-size: 1.1rem;
          }
        }
      }

      @media (max-width: 480px) {
        .extend {
          display: none;
        }

        .extra {
          width: 100%;
        }
      }
    }

    /* Track */
    ::-webkit-scrollbar-track {
      background: #f0f0f0;
    }
  }

  .filter-item-space {
    width: 100%;
    margin-bottom: 12px;
    border-bottom: 1px solid #f1f1f1;

    .ant-space-item:last-child {
      width: 100%;
    }
  }

  .ant-modal.modal-ho-so-nhan-su {
    top: 0;
    width: 100vw !important;
    height: 100vh;
    margin: 0;
    .ant-modal-content {
      width: 100vw;
      height: 100vh;
      border-radius: 0px;
    }
  }

  .column-search-box {
    display: flex;
    gap: 5px;
    padding: 8px;
  }

  .ant-modal.modal-ho-so-nhan-su {
    @media (min-width: 480px) {
      top: 0;
      width: 100vw !important;
      height: 100vh;
      margin: 0;
      .ant-modal-content {
        width: 100vw;
        height: 100vh;
        border-radius: 0px;
      }
    }
  }

  .row-dragging {
    z-index: 1001;
    background: #fafafa;
    border: 1px solid #ccc;
  }

  .row-dragging td {
    padding: 3px 12px !important;
  }

  .row-dragging .drag-visible {
    visibility: visible;
  }
`;
export default TableStaticData;
