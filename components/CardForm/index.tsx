import styled from "styled-components";
import Title from "../Title";
interface IProps {
  children?: any;
  title?: string;
  bordered?: boolean;
}
const CardForm = (props: IProps) => {
  const { children, title } = props;
  return (
    <CardFormWrapper>
     <div className={'mb-[12px] mt-[12px]'}>
       <Title uppercase={false} title={title??''} align={'center'} hideArrows={true} hideSearch={true}/>
     </div>
      {children}
    </CardFormWrapper>
  );
};
const CardFormWrapper = styled.div``;
export default CardForm;
