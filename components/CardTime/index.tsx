import styled from "styled-components";
import moment from "moment/moment";
interface IProps {
  timeStart?: string;
  timeEnd?: string;
  align: "center" | "left" | "right";
}
const CardTime = (props: IProps) => {
  const { timeEnd, timeStart, align } = props;
  return (
    <CardTimeWrapper>
      <div
        className={`time flex  mt-[20px] ${
          align === "center" ? "justify-center" : "justify-start"
        }`}
      >
        <div className="card-time xl:text-[14px] text-[12px] pt-[6px] px-[12px]">
          Ngày bắt đầu{" "}
          {timeStart ? moment(timeStart).format("DD/MM/YYYY") : "--"}
        </div>{" "}
        <div className="mx-[12px]">-</div>
        <div className="card-time xl:text-[14px] text-[12px] pt-[6px] px-[12px]">
          Ngày kết thúc {timeEnd ? moment(timeEnd).format("DD/MM/YYYY") : "--"}
        </div>
      </div>
    </CardTimeWrapper>
  );
};
const CardTimeWrapper = styled.div`
  .time {
    .card-time {
      color: var(--TEXT-01, #212529);
      font-family: Arial;
      font-style: normal;
      font-weight: 400;
      line-height: normal;
      height: 28px;
      border-radius: 8px;
      background: var(--Gray, #f4f4f4);
    }
  }
`;
export default CardTime;
