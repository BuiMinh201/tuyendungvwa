import DatePicker, {registerLocale} from "react-datepicker";
import React, { useState } from "react";
import styled from "styled-components";
import {subDays} from "date-fns";
import vi from "date-fns/locale/vi"; // the locale you want
registerLocale("vi", vi); // register it with the name you want
interface IProps {
	selected: any;
	onChange: (date: any) => void;
	placeholder?:string
	className?:string
	dateFormat?:string
	excludeDates?:any
	maxDate?:any
}
const DatePickerFake = (props: IProps) => {
	return (
		<DatePickerWraper className={"w-full"}>
			<div className={`date-picker flex ${props?.className}`}>
				<DatePicker  showMonthDropdown showYearDropdown locale={'vi'} dateFormat={props.dateFormat} maxDate={props.maxDate} excludeDates={props.excludeDates} className={'w-full'} selected={props.selected} onChange={(date) => props.onChange(date)} placeholderText={props.placeholder} />
				{/*<div className='icon-picker'>*/}
				{/*	<img src='/images/icons/date-picker.svg'  alt={'image'}/>*/}
				{/*</div>*/}
			</div>
		</DatePickerWraper>
	);
};
const DatePickerWraper = styled.div`
  .icon-picker{
    display: flex;
    align-items: center;
    justify-content: center;
    background: #495057;
    //border-radius: 0px 8px 8px 0px;
    width: 48px;
    height: 36px;
  }
	.react-datepicker-wrapper{
		width: 100%;
	}
input{
	color: rgba(0, 0, 0, 1);
	font-family: 'Exo 2',sans-serif;
	font-size: 14px;
	font-style: normal;
	font-weight: 400;
	line-height: normal;
	&::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
		color: rgba(0, 0, 0, 0.4);
		opacity: 1; /* Firefox */
	}

	&:-ms-input-placeholder { /* Internet Explorer 10-11 */
		color: rgba(0, 0, 0, 0.4);
	}

	&::-ms-input-placeholder { /* Microsoft Edge */
		color: rgba(0, 0, 0, 0.4);
	}
}`;
export default DatePickerFake;
