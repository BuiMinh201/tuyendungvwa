import {Col, DatePicker, Divider, Form, Input, Row} from "antd";
import { FormInstance } from "antd/es/form/Form";
import rules from "../../../utils/rules";
import MyDatePicker from "../../MyDatePicker";

const DoanView = (props: { form: FormInstance,disabled?:boolean }) => {
  const { form ,disabled} = props;
  // const isCreate = !edit && !isView && !isXemDanhSachCapNhat && !isViewHistory;
  // ;
  const ngaySinh = Form.useWatch("ngaySinh", form);
  // const isCreate = !edit && !isView && !isXemDanhSachCapNhat && !isViewHistory;
  // const disabledForm = isCreate ? false : !isDraff;
  const disabledForm = disabled;

  return (
    <>
      <Divider orientation="center">Đoàn</Divider>
      <Row gutter={[12, 0]} style={{ marginBottom: 12 }}>
        <Col span={24} md={12}>
          <Form.Item name="noiVaoDoan" label="Nơi kết nạp">
            <Input  placeholder="Nơi kết nạp" disabled={disabledForm} />
          </Form.Item>
        </Col>
        <Col span={24} md={12}>
          <Form.Item
            rules={[...(ngaySinh ? rules.sauNgay(ngaySinh, "ngày sinh") : [])]}
            name="ngayVaoDoan"
            label="Ngày vào Đoàn"
          >
            <MyDatePicker
              disabled={disabledForm}
              placeholder="Ngày vào Đoàn"
              allowClear
            />
          </Form.Item>
        </Col>
      </Row>
    </>
  );
};

export default DoanView;
