import { type FormInstance } from "antd/es/form/Form";
import { Col, Form, Input, Row } from "antd";
import rules from "../../../utils/rules";
import { FormValues } from "../KhenThuong";

interface Props {
  form: FormInstance<FormValues>;
  disabled: boolean;
  isXemChiTietHoSo: boolean;
}

export const DacDiemLichSuBanThan = ({
  disabled,
  isXemChiTietHoSo = false,
}: Props) => {
  return (
    <Row gutter={[12, 0]}>
      <Col span={24}>
        <Form.Item
          name="lichSuBanThanKhaiRo"
          label={
            "Khai rõ: bị bắt, bị tù, đã khai báo cho ai, những vấn đề gì, bản thân có làm việc trong chế độ cũ"
          }
          rules={[...rules.text, ...rules.length(550)]}
        >
          <Input.TextArea
            placeholder="Nhập thông tin"
            rows={2}
            disabled={disabled}
          />
        </Form.Item>
      </Col>
      <Col span={24}>
        <Form.Item
          name="lichSuBanThanThamGia"
          label={
            "Tham gia hoặc có quan hệ các tổ chức chính trị, kinh tế, xã hội nào ở nước ngoài"
          }
          rules={[...rules.text, ...rules.length(550)]}
        >
          <Input.TextArea
            placeholder="Nhập thông tin"
            rows={2}
            disabled={disabled}
          />
        </Form.Item>
      </Col>
      <Col span={24}>
        <Form.Item
          name="lichSuBanThanCoThanNhan"
          label={"Có thân nhân ở nước ngoài"}
          rules={[...rules.text, ...rules.length(550)]}
        >
          <Input.TextArea
            placeholder="Nhập thông tin"
            rows={2}
            disabled={disabled}
          />
        </Form.Item>
      </Col>
    </Row>
  );
};
