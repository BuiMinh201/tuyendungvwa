import { Col, Form, Row } from "antd";
import rules from "../../../../utils/rules";
import SelectDanToc from "../../../CoreSelect/SelectDanToc";
import SelectQuocTich from "../../../CoreSelect/SelectQuocTich";
import SelectTinhTrangHonNhan from "../../../CoreSelect/SelectTinhTrangHonNhan";
import SelectTonGiao from "../../../CoreSelect/SelectTonGiao";

const ThanhPhanBanThan = (props: {
  disabled?: boolean;
  required?: boolean;
}) => {
  const disabledForm = props?.disabled;
  const { required } = props;

  return (
    <Row gutter={[12, 0]}>
      <Col span={24} md={6}>
        <Form.Item
          name="quocTichId"
          label={"Quốc tịch"}
          rules={required ? [...rules.required] : []}
        >
          <SelectQuocTich hienThiMaQuocTich={false} disabled={disabledForm} />
        </Form.Item>
      </Col>
      <Col span={24} md={6}>
        <Form.Item name="danTocId" label={"Dân tộc"}  rules={required ? [...rules.required] : []}>
          <SelectDanToc
            hienThiMaDanToc={false}
            isGetId
            disabled={disabledForm}
          />
        </Form.Item>
      </Col>
      <Col span={24} md={6}>
        <Form.Item
          name="tonGiaoId"
          label="Tôn giáo"
          rules={required ? [...rules.required] : []}
        >
          <SelectTonGiao allowClear isGetId disabled={disabledForm} />
        </Form.Item>
      </Col>

      <Col span={24} md={6}>
        <Form.Item
          name="tinhTrangHonNhanId"
          label="Tình trạng hôn nhân"
          rules={
            required
              ? [...rules.text, ...rules.length(250), ...rules.required]
              : [...rules.text, ...rules.length(250)]
          }
        >
          <SelectTinhTrangHonNhan allowClear disabled={disabledForm} />
        </Form.Item>
      </Col>
    </Row>
  );
};
export default ThanhPhanBanThan;
