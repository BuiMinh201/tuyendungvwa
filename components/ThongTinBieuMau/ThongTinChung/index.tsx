import { Divider } from "antd";
import { type FormInstance } from "antd/es/form/Form";
import { useEffect, useState } from "react";
import { getTinhThanhPho } from "../../../service/core/donvihanhchinh";
import DonViHanhChinhBlock from "../../DonViHanhChinhBlock";
import ThanhPhanBanThan from "./components/ThanhPhanBanThan";
import SucKhoe from "../SucKhoe";
import DoanView from "../Doan";
import DangView from "../Dang";
import QuanDoiView from "../QuanDoi";
import DanQuanTuVeView from "../QuanDanTuVe";

interface Props {
  form: FormInstance<any>;
  disabled?: boolean;
}

const ThongTinChung = ({ form, disabled }: Props) => {
  const [dataTinh, setDataTinh] = useState<any[]>([]);

  useEffect(() => {
    getTinhThanhPho().then((data) => {
      setDataTinh(data.data);
    });
  }, []);

  return (
    <>
      {/* <Divider orientation='center'>Danh hiệu được phong tặng</Divider> */}
      {/* <DanhHieuDuocPhongTang /> */}

      <DonViHanhChinhBlock
        title="Nơi sinh"
        type="noiSinh"
        dataTinh={dataTinh}
        form={form}
        disabled={disabled}
        required
      />

      <DonViHanhChinhBlock
        title="Quê quán"
        type="queQuan"
        dataTinh={dataTinh}
        form={form}
        disabled={disabled}
        required
      />

      <DonViHanhChinhBlock
        title="Hộ khẩu thường trú"
        type="hoKhau"
        dataTinh={dataTinh}
        addSoNha
        form={form}
        disabled={disabled}
        required
      />

      <DonViHanhChinhBlock
        title="Nơi ở hiện nay"
        type="noiO"
        dataTinh={dataTinh}
        addSoNha
        form={form}
        disabled={disabled}
        required
      />

      <Divider orientation="center">Thành phần bản thân</Divider>
      <ThanhPhanBanThan disabled={disabled} required/>

      {/*<Divider orientation="center">Sức khỏe</Divider>*/}
      {/*<SucKhoe disabled={disabled} />*/}
      {/*<Divider orientation="center">Đoàn</Divider>*/}
      {/*<DoanView form={form} disabled={disabled} />*/}
      {/*<Divider orientation="center">Đảng</Divider>*/}
      {/*<DangView form={form} disabled={disabled} />*/}
      {/*<Divider orientation="center">Quân đội</Divider>*/}
      {/*<QuanDoiView form={form} disabled={disabled} />*/}
      {/*<Divider orientation="center">Nghĩa vụ dân quân tự vệ</Divider>*/}
      {/*<DanQuanTuVeView form={form} disabled={disabled} />*/}
    </>
  );
};
export default ThongTinChung;
