import { Button, Card, Col, Form, Input, Row, Select } from "antd";
import { useEffect, useState } from "react";
import { ELoaiQuyetDinh } from "../../../../utils/constant";
import rules from "../../../../utils/rules";
import CardForm from "../../../CardForm";
import MyDatePicker from "../../../MyDatePicker";
import { FormValues } from "../../KhenThuong";
import {resetFieldsForm} from "../../../../utils/util";

export type FormQuaTrinhCongTacValues =
  Required<FormValues>["dsQuaTrinhCongTac"][number];

export interface FormQuaTrinhCongTacProps {
  onFinish: (values: FormQuaTrinhCongTacValues) => void;
  onCancel: () => void;
  isEdit: boolean;
  isView: boolean;
  record?: FormQuaTrinhCongTacValues;
}

export const FormQuaTrinhCongTac = ({
  isEdit,
  isView,
  onCancel,
  onFinish,
  record,
}: FormQuaTrinhCongTacProps) => {
  const [form] = Form.useForm();

  const [tuThagNam, setTuThangNam] = useState<any>();

  useEffect(() => {
    if (record) {
      form.setFieldsValue(record);
      setTuThangNam(record.tuThagNam);
    } else {
      form.resetFields();
    }
  }, [record]);

  return (
    <CardForm
      bordered={false}
      title={
        (isView ? "Xem chi tiết " : isEdit ? "Chỉnh sửa " : "Thêm mới ") +
        "quá trình công tác"
      }
    >
      <Form id='FormQuaTrinhCongTac' onFinish={onFinish} form={form} layout="vertical" disabled={isView}>
        <Row gutter={[12, 0]} style={{ marginBottom: 12 }}>
          <Col span={24}>
            <Form.Item
              name="loaiQuyetDinh"
              label="Loại quyết định"
              rules={[...rules.required]}
            >
              <Select
                options={Object.values(ELoaiQuyetDinh).map((item) => ({
                  label: item,
                  value: item,
                }))}
                placeholder="Loại quyết định"
                disabled={isView}
              />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="tuThagNam"
              label={"Từ tháng năm"}
              rules={[
                ...rules.required,
                ...rules.nhoHonBangHomNay,
                // ...rules.sauNgay(recordThongTinNhanSu?.ngaySinh, 'ngày sinh'),
              ]}
            >
              <MyDatePicker
                disabled={isView}
                placeholder="Từ tháng năm"
                onChange={(e) => {
                  setTuThangNam(e);
                  form.validateFields(["denThangNam"]);
                }}
              />
            </Form.Item>
          </Col>

          <Col span={24} md={12}>
            <Form.Item
              name="denThangNam"
              label={"Đến tháng năm"}
              rules={[
                // ...rules.sauNgay(recordThongTinNhanSu?.ngaySinh, 'ngày sinh'),
                ...(tuThagNam
                  ? rules.sauNgay(new Date(tuThagNam), "từ tháng năm")
                  : []),
              ]}
            >
              <MyDatePicker
                placeholder="Đến tháng năm"
                allowClear
                disabled={isView}
              />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="donViCongTac"
              label={"Đơn vị công tác"}
              rules={[...rules.required, ...rules.text, ...rules.length(250)]}
            >
              <Input placeholder="Đơn vị công tác" disabled={isView} />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="chucDanh"
              label={"Chức danh"}
              rules={[...rules.text, ...rules.length(250)]}
            >
              <Input placeholder="Chức danh" disabled={isView} />
            </Form.Item>
          </Col>
          <Col span={24}>
            <Form.Item
              name="chucVu"
              label={"Chức vụ công tác"}
              rules={[...rules.text, ...rules.length(250)]}
            >
              <Input placeholder="Chức vụ công tác" disabled={isView} />
            </Form.Item>
          </Col>

          <Col span={24} md={24}>
            <Form.Item
              name="noiDung"
              label={"Nội dung"}
              rules={[...rules.text, ...rules.length(550)]}
            >
              <Input.TextArea placeholder="Nội dung" disabled={isView} />
            </Form.Item>
          </Col>
        </Row>

        {/*{!isView && (*/}
        {/*  <Form.Item style={{ textAlign: "center", marginTop: 24 }}>*/}
        {/*    <Button style={{ marginRight: 8 }} htmlType="submit" type="primary">*/}
        {/*      {!isEdit ? "Thêm mới " : "Lưu lại"}*/}
        {/*    </Button>*/}

        {/*    <Button onClick={onCancel}>Đóng</Button>*/}
        {/*  </Form.Item>*/}
        {/*)}*/}
        <div style={{ textAlign: 'center', marginTop: 24 }}>
          {!isView && (
            <Button form='FormQuaTrinhCongTac' style={{ marginRight: 8 }} htmlType='submit' type='primary'>
              {!isEdit ? 'Thêm mới ' : 'Lưu lại'}
            </Button>
          )}
          <Button
            onClick={() => {
              onCancel();
              resetFieldsForm(form);
            }}
          >
            Đóng
          </Button>
        </div>
      </Form>
    </CardForm>
  );
};
