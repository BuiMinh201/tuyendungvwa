import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import {
  Button,
  Popconfirm,
  Tooltip,
  type FormInstance,
  Divider,
  Form,
} from "antd";
import { useWatch } from "antd/lib/form/Form";
import { useState } from "react";
import moment from "moment";
import { FormValues } from "../KhenThuong";
import {
  FormQuaTrinhCongTac,
  FormQuaTrinhCongTacProps,
  FormQuaTrinhCongTacValues,
} from "./components/FormQuaTrinhCongTac";
import { IColumn } from "../../TableStaticData/typing";
import TableStaticData from "../../TableStaticData";
import { ThongTinNhanSu } from "../../../Interface/ThongTinNhanSu/typing";

interface Props {
  form: FormInstance<FormValues>;
  isViewMode?: boolean;
  dataHoSo?: ThongTinNhanSu.IRecord;
}

type TableRecord = FormQuaTrinhCongTacValues;
export const QuaTrinhCongTac = ({ form, isViewMode, dataHoSo }: Props) => {
  const dataDanhSach = useWatch(["dsQuaTrinhCongTac"], form);
  const dsQuaTrinhCongTac = dataHoSo?.danhSachQuaTrinhCongTac ?? dataDanhSach;

  const [visibleForm, setVisibleForm] = useState(false);
  const [record, setRecord] = useState<FormQuaTrinhCongTacValues>();
  const [isEdit, setIsEdit] = useState(false);
  const [isView, setIsView] = useState(false);
  const [indexRecordDangThaoTac, setIndexRecordDangThaoTac] = useState<
    number | undefined
  >(undefined);

  const handleCreate = (vi: boolean) => {
    setVisibleForm(vi);
    setIsEdit(false);
    setIsView(false);
    setRecord(undefined);
  };

  const handleEdit = (tableRecord: TableRecord, index: number) => {
    setVisibleForm(true);
    setIsEdit(true);
    setIsView(false);
    setRecord(tableRecord);
    setIndexRecordDangThaoTac(index);
  };

  const handleView = (tableRecord: TableRecord) => {
    setVisibleForm(true);
    setIsView(true);
    setIsEdit(false);
    setRecord(tableRecord);
  };

  const handleCloseModal = () => {
    setRecord(undefined);
    setVisibleForm(false);
    setIsEdit(false);
    setIsView(false);
  };

  const handleDelete = (index: number) => {
    const prevDsQuaTrinhCongTac: Required<FormValues>["dsQuaTrinhCongTac"] =
      form.getFieldValue("dsQuaTrinhCongTac") ?? [];
    form.setFieldsValue({
      dsQuaTrinhCongTac: prevDsQuaTrinhCongTac.filter(
        (_, QuaTrinhCongTacIndex) => QuaTrinhCongTacIndex !== index
      ),
    });
  };

  const handleSubmitForm = (values: FormQuaTrinhCongTacValues) => {
    const prevDsQuaTrinhCongTac: Required<FormValues>["dsQuaTrinhCongTac"] =
      form.getFieldValue("dsQuaTrinhCongTac") ?? [];

    if (!isEdit) {
      form.setFieldsValue({
        dsQuaTrinhCongTac: prevDsQuaTrinhCongTac.prepend(values),
      });
    } else if (isEdit) {
      form.setFieldsValue({
        dsQuaTrinhCongTac: prevDsQuaTrinhCongTac.map((thanhVien, index) => {
          if (indexRecordDangThaoTac === index) {
            return {
              ...thanhVien,
              ...values,
            };
          }
          return thanhVien;
        }),
      });
    }

    setIndexRecordDangThaoTac(undefined);
    setVisibleForm(false);
    setIsEdit(false);
    setIsView(false);
  };

  const onCell = (rec: TableRecord) => ({
    onClick: () => handleView(rec),
    style: { cursor: "pointer" },
  });
  const columns: IColumn<TableRecord>[] = [
    {
      title: "Từ tháng năm",
      dataIndex: "tuThagNam",
      // align: 'center',
      width: 120,
      filterType: "date",
      onCell: onCell,
      sortable: true,
      render: (value) =>
        value ? moment(value).format("DD/MM/YYYY HH:mm") : null,
    },
    {
      title: "Đến tháng năm",
      dataIndex: "denThangNam",
      width: 120,
      filterType: "date",
      onCell: onCell,
      sortable: true,
      render: (value) =>
        value ? moment(value).format("DD/MM/YYYY HH:mm") : null,
    },
    {
      title: "Đơn vị công tác",
      dataIndex: "donViCongTac",
      width: 120,
      filterType: "string",
      onCell: onCell,
      sortable: true,
    },
    {
      title: "Chức vụ",
      dataIndex: "chucVu",
      width: 120,
      filterType: "string",
      onCell: onCell,
      sortable: true,
    },
    {
      title: "Nội dung",
      dataIndex: "noiDung",
      width: 220,
      filterType: "string",
    },
  ];
  if (!isViewMode) {
    columns.push({
      title: "Thao tác",
      align: "center",
      width: 90,
      fixed: "right",
      render: (_, rec, index) => {
        return (
          <>
            <Tooltip title="Chỉnh sửa">
              <Button
                type="link"
                icon={<EditOutlined rev={undefined} />}
                onClick={() => handleEdit(rec, index)}
              />
            </Tooltip>

            <Tooltip title="Xóa">
              <Popconfirm
                onConfirm={() => handleDelete(index)}
                title="Bạn có chắc chắn muốn xóa quá trình công tác này?"
                placement="topLeft"
              >
                <Button
                  danger
                  type="link"
                  icon={<DeleteOutlined rev={undefined} />}
                />
              </Popconfirm>
            </Tooltip>
          </>
        );
      },
    });
  }

  return (
    <Form.Item name="dsQuaTrinhCongTac">
      <div id={"qua-trinh-cong-tac"}>
        <Divider orientation="center">Quá trình công tác</Divider>
      </div>
      <TableStaticData
        pagination={false}
        hasCreate={!isViewMode}
        hasTotal
        showEdit={visibleForm}
        setShowEdit={handleCreate}
        data={dsQuaTrinhCongTac ?? []}
        columns={columns}
        addStt
        widthDrawer={800}
        Form={FormQuaTrinhCongTac}
        formProps={
          {
            record,
            isEdit,
            isView,
            onCancel: handleCloseModal,
            onFinish: handleSubmitForm,
          } as FormQuaTrinhCongTacProps
        }
      />
    </Form.Item>
  );
};
