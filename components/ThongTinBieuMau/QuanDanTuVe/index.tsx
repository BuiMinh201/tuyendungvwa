import { Col, DatePicker, Divider, Form, Input, Row } from "antd";
import { FormInstance } from "antd/es/form/Form";
import rules from "../../../utils/rules";
import MyDatePicker from "../../MyDatePicker";

const DanQuanTuVeView = (props: { form: FormInstance; disabled?: boolean }) => {
  const { form, disabled } = props;
  // const isCreate = !edit && !isView && !isXemDanhSachCapNhat && !isViewHistory;
  // ;
  const ngaySinh = Form.useWatch("ngaySinh", form);
  // const isCreate = !edit && !isView && !isXemDanhSachCapNhat && !isViewHistory;
  // const disabledForm = isCreate ? false : !isDraff;
  const disabledForm = disabled;

  return (
    <>
      <Divider orientation="center">Nghĩa vụ dân quân tự vệ</Divider>
      <Row gutter={[12, 0]} style={{ marginBottom: 12 }}>
        {/* <Col span={24} md={8}>
          <Form.Item
            name="tenNghiaVuDanQuanTuVe"
            label={`Tên "Nghĩa vụ dân quân tự vệ"`}
            rules={[...rules.text, ...rules.length(250)]}
          >
            <Input  readOnly placeholder={`Tên "Nghĩa vụ dân quân tự vệ"`} />
          </Form.Item>
        </Col> */}
        <Col span={24} md={12}>
          <Form.Item
            name="ngayKetNapDanQuanTuVe"
            label={"Ngày kết nạp"}
            rules={[
              ...rules.nhoHonBangHomNay,
              ...(ngaySinh ? rules.sauNgay(ngaySinh, "ngày sinh") : []),
            ]}
          >
            <MyDatePicker
              placeholder="Ngày kết nạp"
              allowClear
              disabled={disabledForm}
            />
          </Form.Item>
        </Col>
        <Col span={24} md={12}>
          <Form.Item
            name="ngayHoanThanhNghiaVu"
            label={"Ngày hoàn thành nghĩa vụ"}
            rules={[
              ...rules.nhoHonBangHomNay,
              ...(ngaySinh ? rules.sauNgay(ngaySinh, "ngày sinh") : []),
            ]}
          >
            <MyDatePicker
              placeholder="Ngày hoàn thành nghĩa vụ"
              allowClear
              disabled={disabledForm}
            />
          </Form.Item>
        </Col>
        <Col span={24} md={8}>
          <Form.Item
            name="chucDanhDanQuanTuVe"
            label={"Chức danh"}
            rules={[...rules.text, ...rules.length(250)]}
          >
            <Input placeholder="Chức danh" disabled={disabledForm} />
          </Form.Item>
        </Col>
        <Col span={24} md={8}>
          <Form.Item
            name="soQuyetDinhHoanThanh"
            label={"Số quyết định hoàn thành"}
            rules={[...rules.text, ...rules.length(20)]}
          >
            <Input
              placeholder="Số quyết định hoàn thành"
              disabled={disabledForm}
            />
          </Form.Item>
        </Col>
        <Col span={24} md={8}>
          <Form.Item
            name="nguoiKyQuyetDinh"
            label={"Người ký quyết định"}
            rules={[...rules.text, ...rules.length(250)]}
          >
            <Input placeholder="Người ký quyết định" disabled={disabledForm} />
          </Form.Item>
        </Col>
      </Row>
    </>
  );
};

export default DanQuanTuVeView;
