import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { Button, Popconfirm, Tooltip, type FormInstance } from "antd";
import { useWatch } from "antd/lib/form/Form";
import { useState } from "react";
import moment from "moment";
import { FormValues } from "../KhenThuong";
import {
  FormQuaTrinhCuDiDaoTaoBoiDuong,
  FormQuaTrinhCuDiDaoTaoBoiDuongProps,
  FormQuaTrinhCuDiDaoTaoBoiDuongValues,
} from "./components/FormQuaTrinhDaoTaoBoiDuong";
import { IColumn } from "../../TableStaticData/typing";
import TableStaticData from "../../TableStaticData";
import { ThongTinNhanSu } from "../../../Interface/ThongTinNhanSu/typing";

interface Props {
  form: FormInstance<FormValues>;
  isViewMode?: boolean;
  dataHoSo?: ThongTinNhanSu.IRecord;
}

type TableRecord = FormQuaTrinhCuDiDaoTaoBoiDuongValues;
export const QuaTrinhCuDiDaoTaoBoiDuong = ({
  form,
  isViewMode,
  dataHoSo,
}: Props) => {
  const dataDanhSach = useWatch(["dsQuaTrinhCuDiDaoTaoBoiDuong"], form);
  const dsQuaTrinhCuDiDaoTaoBoiDuong =
    dataHoSo?.danhSachQuaTrinhCuDiDaoTaoBoiDuong ?? dataDanhSach;

  const [visibleForm, setVisibleForm] = useState(false);
  const [record, setRecord] = useState<FormQuaTrinhCuDiDaoTaoBoiDuongValues>();
  const [isEdit, setIsEdit] = useState(false);
  const [isView, setIsView] = useState(false);
  const [indexRecordDangThaoTac, setIndexRecordDangThaoTac] = useState<
    number | undefined
  >(undefined);

  const handleCreate = (vi: boolean) => {
    setVisibleForm(vi);
    setIsEdit(false);
    setIsView(false);
    setRecord(undefined);
  };

  const handleEdit = (tableRecord: TableRecord, index: number) => {
    setVisibleForm(true);
    setIsEdit(true);
    setIsView(false);
    setRecord(tableRecord);
    setIndexRecordDangThaoTac(index);
  };

  const handleView = (tableRecord: TableRecord) => {
    setVisibleForm(true);
    setIsView(true);
    setIsEdit(false);
    setRecord(tableRecord);
  };

  const handleCloseModal = () => {
    setRecord(undefined);
    setVisibleForm(false);
    setIsEdit(false);
    setIsView(false);
  };

  const handleDelete = (index: number) => {
    const prevDsQuaTrinhCuDiDaoTaoBoiDuong: Required<FormValues>["dsQuaTrinhCuDiDaoTaoBoiDuong"] =
      form.getFieldValue("dsQuaTrinhCuDiDaoTaoBoiDuong") ?? [];
    form.setFieldsValue({
      dsQuaTrinhCuDiDaoTaoBoiDuong: prevDsQuaTrinhCuDiDaoTaoBoiDuong.filter(
        (_, QuaTrinhCuDiDaoTaoBoiDuongIndex) =>
          QuaTrinhCuDiDaoTaoBoiDuongIndex !== index
      ),
    });
  };

  const handleSubmitForm = (values: FormQuaTrinhCuDiDaoTaoBoiDuongValues) => {
    const prevDsQuaTrinhCuDiDaoTaoBoiDuong: Required<FormValues>["dsQuaTrinhCuDiDaoTaoBoiDuong"] =
      form.getFieldValue("dsQuaTrinhCuDiDaoTaoBoiDuong") ?? [];

    if (!isEdit) {
      form.setFieldsValue({
        dsQuaTrinhCuDiDaoTaoBoiDuong:
          prevDsQuaTrinhCuDiDaoTaoBoiDuong.concat(values),
      });
    } else if (isEdit) {
      form.setFieldsValue({
        dsQuaTrinhCuDiDaoTaoBoiDuong: prevDsQuaTrinhCuDiDaoTaoBoiDuong.map(
          (thanhVien, index) => {
            if (indexRecordDangThaoTac === index) {
              return {
                ...thanhVien,
                ...values,
              };
            }
            return thanhVien;
          }
        ),
      });
    }

    setIndexRecordDangThaoTac(undefined);
    setVisibleForm(false);
    setIsEdit(false);
    setIsView(false);
  };

  const onCell = (rec: TableRecord) => ({
    onClick: () => handleView(rec),
    style: { cursor: "pointer" },
  });
  const columns: IColumn<TableRecord>[] = [
    {
      title: "Khóa bồi dưỡng tập huấn",
      width: 140,
      dataIndex: "tenKhoaBoiDuongTapHuan",
      filterType: "string",
      sortable: true,
      onCell,
    },
    {
      title: "Đơn vị tổ chức",
      width: 120,
      dataIndex: "donViToChuc",
      filterType: "string",
      sortable: true,
      onCell,
    },
    {
      title: "Loại bồi dưỡng",
      width: 120,
      dataIndex: "loaiBoiDuong",
      sortable: true,
      onCell,
    },
    {
      title: "Từ ngày",
      dataIndex: "thoiGianBatDau",
      width: 100,
      onCell: onCell,
      render: (value) => {
        return value ? moment(value).format("DD/MM/YYYY HH:mm") : null;
      },
    },
    {
      title: "Đến ngày",
      dataIndex: "thoiGianKetThuc",
      width: 100,
      render: (value) => {
        return value ? moment(value).format("DD/MM/YYYY HH:mm") : null;
      },
    },
  ];
  if (!isViewMode) {
    columns.push({
      title: "Thao tác",
      align: "center",
      width: 90,
      fixed: "right",
      render: (_, rec, index) => {
        return (
          <>
            <Tooltip title="Chỉnh sửa">
              <Button
                type="link"
                icon={<EditOutlined rev={undefined} />}
                onClick={() => handleEdit(rec, index)}
              />
            </Tooltip>

            <Tooltip title="Xóa">
              <Popconfirm
                onConfirm={() => handleDelete(index)}
                title="Bạn có chắc chắn muốn xóa quá trình được cử đi công tác, bồi dưỡng này?"
                placement="topLeft"
              >
                <Button
                  danger
                  type="link"
                  icon={<DeleteOutlined rev={undefined} />}
                />
              </Popconfirm>
            </Tooltip>
          </>
        );
      },
    });
  }

  return (
    <TableStaticData
      hasCreate={!isViewMode}
      hasTotal
      showEdit={visibleForm}
      setShowEdit={handleCreate}
      data={dsQuaTrinhCuDiDaoTaoBoiDuong ?? []}
      columns={columns}
      addStt
      widthDrawer={800}
      Form={FormQuaTrinhCuDiDaoTaoBoiDuong}
      formProps={
        {
          record,
          isEdit,
          isView,
          onCancel: handleCloseModal,
          onFinish: handleSubmitForm,
        } as FormQuaTrinhCuDiDaoTaoBoiDuongProps
      }
    />
  );
};
