import { Button, Card, Col, Form, Input, Row } from "antd";
import { useEffect, useState } from "react";
import rules from "../../../../utils/rules";
import MyDatePicker from "../../../MyDatePicker";
import UploadFile from "../../../Upload/UploadFile";
import { FormValues } from "../../KhenThuong";
import SelecLoaiBoiDuong from "./LoaiBoiDuong/select";

export type FormQuaTrinhCuDiDaoTaoBoiDuongValues =
  Required<FormValues>["dsQuaTrinhCuDiDaoTaoBoiDuong"][number] & {
    loaiBoiDuong: string;
  };

export interface FormQuaTrinhCuDiDaoTaoBoiDuongProps {
  onFinish: (values: FormQuaTrinhCuDiDaoTaoBoiDuongValues) => void;
  onCancel: () => void;
  isEdit: boolean;
  isView: boolean;
  record?: FormQuaTrinhCuDiDaoTaoBoiDuongValues;
}

export const FormQuaTrinhCuDiDaoTaoBoiDuong = ({
  isEdit,
  isView,
  onCancel,
  onFinish,
  record,
}: FormQuaTrinhCuDiDaoTaoBoiDuongProps) => {
  const [form] = Form.useForm<FormQuaTrinhCuDiDaoTaoBoiDuongValues>();

  const [ngayBatDau, setNgayBatDau] = useState<any>();

  useEffect(() => {
    if (record) {
      form.setFieldsValue(record);
      setNgayBatDau(record.thoiGianBatDau);
    } else {
      form.resetFields();
    }
  }, [record]);

  return (
    <Card
      bordered={false}
      title={
        (isView ? "Xem chi tiết " : isEdit ? "Chỉnh sửa " : "Thêm mới ") +
        "quá trình được cử đi đào tạo, bồi dưỡng"
      }
    >
      <Form onFinish={onFinish} form={form} layout="vertical" disabled={isView}>
        <Form.Item hidden name="loaiBoiDuong" />
        <Row gutter={[12, 0]} style={{ marginBottom: 12 }}>
          <Col span={24}>
            <Form.Item
              name="tenKhoaBoiDuongTapHuan"
              label="Tên khóa bồi dưỡng tập huấn"
              rules={[...rules.text, ...rules.length(250), ...rules.required]}
            >
              <Input
                placeholder="Tên khóa bồi dưỡng tập huấn"
                disabled={isView}
              />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="donViToChuc"
              label="Đơn vị tổ chức"
              rules={[...rules.text, ...rules.length(250)]}
            >
              <Input placeholder="Đơn vị tổ chức" disabled={isView} />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="loaiBoiDuongId"
              label="Loại bồi dưỡng"
              rules={[...rules.required]}
            >
              <SelecLoaiBoiDuong
                disabled={isView}
                onChange={(_, option) => {
                  form.setFieldsValue({ loaiBoiDuong: option?.rawData?.ten });
                }}
              />
            </Form.Item>
          </Col>

          <Col span={24} md={12}>
            <Form.Item
              name="diaDiemToChuc"
              label="Địa điểm tổ chức"
              rules={[...rules.text, ...rules.length(250)]}
            >
              <Input placeholder="Địa điểm tổ chức" disabled={isView} />
            </Form.Item>
          </Col>

          <Col span={24} md={12}>
            <Form.Item
              name="thoiGianBatDau"
              label="Thời gian bắt đầu"
              rules={[
                // ...rules.sauNgay(recordThongTinNhanSu?.ngaySinh, 'ngày sinh'),
                ...rules.nhoHonBangHomNay,
              ]}
            >
              <MyDatePicker
                disabled={isView}
                placeholder="Thời gian bắt đầu"
                onChange={(value) => {
                  setNgayBatDau(value);
                  form.validateFields(["thoiGianKetThuc"]);
                }}
              />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="thoiGianKetThuc"
              label="Thời gian kết thúc"
              rules={[
                ...(ngayBatDau
                  ? rules.sauNgay(new Date(ngayBatDau), "ngày bắt đầu")
                  : []),
                // ...rules.sauNgay(recordThongTinNhanSu?.ngaySinh, 'ngày sinh'),
              ]}
            >
              <MyDatePicker
                placeholder="Thời gian kết thúc"
                allowClear
                disabled={isView}
              />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="nguonKinhPhi"
              label="Nguồn kinh phí"
              rules={[...rules.text, ...rules.length(250)]}
            >
              <Input placeholder="Nguồn kinh phí" disabled={isView} />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="chungChi"
              label="Chứng chỉ"
              rules={[...rules.text, ...rules.length(250)]}
            >
              <Input placeholder="Chứng chỉ" disabled={isView} />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="ngayCap"
              label="Ngày cấp"
              // rules={[...rules.sauNgay(recordThongTinNhanSu?.ngaySinh, 'ngày sinh')]}
            >
              <MyDatePicker
                placeholder="Ngày cấp"
                allowClear
                disabled={isView}
              />
            </Form.Item>
          </Col>
          <Col span={24}>
            <Form.Item name="urlFileUpload" label="File đính kèm">
              <UploadFile
                otherProps={{
                  maxCount: 1,
                  multiple: true,
                  showUploadList: { showDownloadIcon: false },
                }}
              />
            </Form.Item>
          </Col>
        </Row>

        {!isView && (
          <Form.Item style={{ textAlign: "center", marginTop: 24 }}>
            <Button style={{ marginRight: 8 }} htmlType="submit" type="primary">
              {!isEdit ? "Thêm mới " : "Lưu lại"}
            </Button>

            <Button onClick={onCancel}>Đóng</Button>
          </Form.Item>
        )}
      </Form>
    </Card>
  );
};
