import { Select } from "antd";
import { type BaseOptionType } from "antd/lib/select";
import { useEffect, useState } from "react";
import { getLoaiBoiDuong } from "../../../../../service/core/loaiboiduong";

const SelecLoaiBoiDuong = (props: {
  value?: string;
  onChange?: (value: string, option: BaseOptionType) => void;
  multiple?: boolean;
  disabled?: boolean;
}) => {
  const { value, onChange, multiple, disabled } = props;
  const [danhSach, setDanhSach] = useState<LoaiBoiDuong.IRecord[]>([]);
  const getData = async () => {
    try {
      const res = await getLoaiBoiDuong();
      setDanhSach(res?.data?.data);
    } catch (e) {}
  };

  useEffect(() => {
    getData();
  }, []);
  return (
    <div style={{ display: "flex", gap: 8 }}>
      <div className={"fullWidth"}>
        <Select
          disabled={disabled}
          mode={multiple ? "multiple" : undefined}
          value={value}
          onChange={onChange}
          options={danhSach.map((item) => ({
            key: item._id,
            value: item._id,
            label: item.ten,
            rawData: item,
          }))}
          showSearch
          optionFilterProp="label"
          placeholder="Chọn loại bồi dưỡng"
        />
      </div>
    </div>
  );
};

export default SelecLoaiBoiDuong;
