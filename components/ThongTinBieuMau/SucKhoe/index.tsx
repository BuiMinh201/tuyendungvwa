
import { Col, Form, Input, InputNumber, Row, Select } from "antd";
import { ENhomMau } from "../../../utils/constant";
import rules from "../../../utils/rules";

const SucKhoe = (props:{disabled?:boolean}) => {
  // const isCreate = !edit && !isView && !isXemDanhSachCapNhat && !isViewHistory;
  // const disabledForm = isCreate ? false : !isDraff;
  const disabledForm = props?.disabled;

  return (
    <>
      <Row gutter={[12, 0]}>
        <Col span={24} md={6}>
          <Form.Item
            name="chieuCao"
            label="Chiều cao (cm)"
            rules={[...rules.float(999)]}
          >
            <InputNumber
              disabled={disabledForm}
              placeholder="Chiều cao (cm)"
              style={{ width: "100%" }}
              min={0}
            />
          </Form.Item>
        </Col>
        <Col span={24} md={6}>
          <Form.Item
            name="canNang"
            label="Cân nặng (kg)"
            rules={[...rules.float(999)]}
          >
            <InputNumber
              disabled={disabledForm}
              placeholder="Cân nặng (kg)"
              style={{ width: "100%" }}
              min={0}
            />
          </Form.Item>
        </Col>
        <Col span={24} md={6}>
          <Form.Item name="nhomMau" label="Nhóm máu">
            <Select
              disabled={disabledForm}
              placeholder="Nhóm máu"
              showSearch
              options={Object.values(ENhomMau).map((item) => ({
                key: item,
                value: item,
                label: item,
              }))}
              allowClear
            />
          </Form.Item>
        </Col>
        <Col span={24} md={6}>
          <Form.Item
            name="tinhTrangSucKhoe"
            label="Tình trạng"
            rules={[...rules.text, ...rules.length(550)]}
          >
            <Input disabled={disabledForm} placeholder="Tình trạng" />
          </Form.Item>
        </Col>
      </Row>
    </>
  );
};

export default SucKhoe;
