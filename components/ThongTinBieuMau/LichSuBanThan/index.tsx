
import { Col, Form, Input, Row } from 'antd';
import rules from '../../../utils/rules';

const LichSuBanThan = () => {
  const disabledForm = false;

  return (
    <>
      <Row gutter={[12, 0]}>
        <Col span={24}>
          <Form.Item
            name='lichSuBanThanKhaiRo'
            label='Khai rõ: bị bắt, bị tù, đã khai báo cho ai, những vấn đề gì, bản thân có làm việc trong chế độ cũ'
            rules={[...rules.text, ...rules.length(550)]}
          >
            <Input.TextArea
              disabled={disabledForm}
              placeholder={disabledForm ? undefined : 'Nhập thông tin'}
              rows={2}
            />
          </Form.Item>
        </Col>
        <Col span={24}>
          <Form.Item
            name='lichSuBanThanThamGia'
            label='Tham gia hoặc có quan hệ các tổ chức chính trị, kinh tế, xã hội nào ở nước ngoài'
            rules={[...rules.text, ...rules.length(550)]}
          >
            <Input.TextArea
              disabled={disabledForm}
              placeholder={disabledForm ? undefined : 'Nhập thông tin'}
              rows={2}
            />
          </Form.Item>
        </Col>
        <Col span={24}>
          <Form.Item
            name='lichSuBanThanCoThanNhan'
            label='Có thân nhân ở nước ngoài'
            rules={[...rules.text, ...rules.length(550)]}
          >
            <Input.TextArea
              disabled={disabledForm}
              placeholder={disabledForm ? undefined : 'Nhập thông tin'}
              rows={2}
            />
          </Form.Item>
        </Col>
      </Row>
    </>
  );
};

export default LichSuBanThan;
