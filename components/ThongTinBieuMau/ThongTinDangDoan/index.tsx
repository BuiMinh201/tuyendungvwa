import { type FormInstance } from "antd/es/form/Form";
import Dang from "../Dang";
import Doan from "../Doan";
import { FormValues } from "../KhenThuong";
import QuanDoi from "../QuanDoi";
import DanQuanTuVeView from "../QuanDanTuVe";

interface Props {
  form: FormInstance<FormValues>;
  disabled: boolean;
}

export const ThongTinDangDoan = ({
  disabled,
  form,
}: Props) => {
  return (
    <>
      <Dang form={form} disabled={disabled} />
      <Doan form={form} disabled={disabled} />
      <QuanDoi form={form} disabled={disabled} />
      <DanQuanTuVeView form={form} disabled={disabled} />
    </>
  );
};
