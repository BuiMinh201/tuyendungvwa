import {
  Button,
  Card,
  Checkbox,
  Col,
  Form,
  Input,
  InputNumber,
  Row,
  Select,
} from "antd";
import { useEffect, useState } from "react";
import moment from "moment";
import { useWatch } from "antd/lib/form/Form";
import { FormValues } from "../../KhenThuong";
import { DonViHanhChinh } from "../../../../utils/interface";
import { resetFieldsForm } from "../../../../utils/util";
import {
  getphuongXa,
  getQuanHuyen,
  getTinhThanhPho,
} from "../../../../service/core/donvihanhchinh";
import rules from "../../../../utils/rules";
import MyDatePicker from "../../../MyDatePicker";
import { EMoiQuanHeBanThan } from "../../../../utils/constant";
import CardForm from "../../../CardForm";

export type FormQuanHeGiaDinhVeBanThanValues =
  Required<FormValues>["dsQuanHeGiaDinhVeBanThan"][number] & {
    capQuanHeGiaDinhVeBanThan: string;
  };

export interface FormQuanHeGiaDinhVeBanThanProps {
  onFinish: (values: FormQuanHeGiaDinhVeBanThanValues) => void;
  onCancel: () => void;
  isEdit: boolean;
  isView: boolean;
  record?: FormQuanHeGiaDinhVeBanThanValues;
}

export const FormQuanHeGiaDinhVeBanThan = ({
  isEdit,
  isView,
  onCancel,
  onFinish,
  record,
}: FormQuanHeGiaDinhVeBanThanProps) => {
  const [form] = Form.useForm();
  const hoKhauThanhPhoMa = useWatch("hoKhauThanhPhoMa", form);
  const hoKhauQuanMa = useWatch("hoKhauQuanMa", form);

  const [maxDay, setMaxDay] = useState<number>(30);
  const [month, setMonth] = useState<number>();
  const [yearInput, setYearInput] = useState<number>();

  const [listTinh, setListTinh] = useState<DonViHanhChinh[]>([]);
  const [listHuyen, setListHuyen] = useState<DonViHanhChinh[]>([]);
  const [listXa, setListXa] = useState<DonViHanhChinh[]>([]);

  const thangNhuan = [1, 3, 5, 7, 8, 10, 12];

  const CreatArrayNumber = (num: number) => {
    const foo = [];
    for (let i = 1; i <= num; i++) {
      foo.push(i);
    }
    return foo;
  };
  const isNamNhuan = (year: number) => {
    return (
      (year % 4 === 0 && year % 100 !== 0 && year % 400 !== 0) ||
      (year % 100 === 0 && year % 400 === 0)
    );
  };

  useEffect(() => {
    resetFieldsForm(form, {
      ...record,
      nguoiPhuThuoc: record?.nguoiPhuThuoc ?? false,
    });
    setYearInput(record?.namSinh ?? undefined);
    setMonth(record?.thangSinh ?? undefined);
    if (isNamNhuan(Number(record?.namSinh)) && record?.thangSinh === 2) {
      setMaxDay(29);
    } else if (
      !isNamNhuan(Number(record?.namSinh)) &&
      record?.thangSinh === 2
    ) {
      setMaxDay(28);
    } else if (thangNhuan.find((item) => item === record?.thangSinh)) {
      setMaxDay(31);
    } else setMaxDay(30);
  }, [record, isView, isEdit]);

  useEffect(() => {
    getTinhThanhPho().then((response) => {
      setListTinh(response.data.data ?? []);
    });
  }, []);

  useEffect(() => {
    if (hoKhauThanhPhoMa) {
      getQuanHuyen(hoKhauThanhPhoMa).then((response) => {
        setListHuyen(response.data.data ?? []);
      });
    } else {
      setListHuyen([]);
    }
  }, [hoKhauThanhPhoMa]);

  useEffect(() => {
    if (hoKhauQuanMa) {
      getphuongXa(hoKhauQuanMa).then((response) => {
        setListXa(response.data.data ?? []);
      });
    } else {
      setListXa([]);
    }
  }, [hoKhauQuanMa]);

  return (
    <CardForm
      bordered={false}
      title={
        (isView ? "Chi tiết " : isEdit ? "Chỉnh sửa " : "Thêm mới ") +
        "mối quan hệ trong gia đình về bản thân"
      }
    >
      <Form
        id={"FormQuanHeBanThan"}
        onFinish={onFinish}
        form={form}
        layout="vertical"
      >
        <Row gutter={[12, 0]} style={{ marginBottom: 12 }}>
          <Col span={24}>
            <Form.Item
              name="hoVaTen"
              label="Họ và tên"
              rules={[...rules.required, ...rules.text, ...rules.length(250)]}
            >
              <Input disabled={isView} placeholder="Họ và tên" />
            </Form.Item>
          </Col>
          <Col span={24} md={8}>
            <Form.Item
              name="namSinh"
              label="Năm sinh"
              rules={[...rules.number(2023, 1900), ...rules.required]}
            >
              <InputNumber
                disabled={isView}
                placeholder="Năm"
                style={{ width: "100%" }}
                onChange={(e) => {
                  setYearInput(Number(e));
                  form.setFieldsValue({ ngaySinh: null });
                  if (isNamNhuan(Number(e)) && month === 2) {
                    setMaxDay(29);
                  } else if (!isNamNhuan(Number(e)) && month === 2) {
                    setMaxDay(28);
                  } else if (thangNhuan.find((item) => item === month)) {
                    setMaxDay(31);
                  } else setMaxDay(30);
                }}
              />
            </Form.Item>
          </Col>
          <Col span={24} md={8}>
            <Form.Item name="thangSinh" label="Tháng sinh">
              <Select
                allowClear
                disabled={isView}
                placeholder="Tháng"
                options={CreatArrayNumber(12).map((item) => ({
                  key: item,
                  value: item,
                  label: item,
                }))}
                onChange={(e) => {
                  form.setFieldsValue({ ngaySinh: null });
                  setMonth(Number(e));
                  if (isNamNhuan(Number(yearInput)) && e === 2) {
                    setMaxDay(29);
                  } else if (!isNamNhuan(Number(yearInput)) && e === 2) {
                    setMaxDay(28);
                  } else if (thangNhuan.find((item) => item === e)) {
                    setMaxDay(31);
                  } else setMaxDay(30);
                }}
              />
            </Form.Item>
          </Col>
          <Col span={24} md={8}>
            <Form.Item name="ngaySinh" label="Ngày sinh">
              <Select
                allowClear
                disabled={isView}
                placeholder="Ngày"
                options={CreatArrayNumber(Number(maxDay)).map((item) => ({
                  key: item,
                  value: item,
                  label: item,
                }))}
              />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              label="Số điện thoại"
              name="soDienThoai"
              rules={[...rules.soDienThoai]}
            >
              <Input placeholder="Số điện thoại" />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              label="CCCD/CMND"
              name="cccdCMND"
              rules={[...rules.CMND]}
            >
              <Input placeholder="CCCD/CMND" />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              label="Ngày cấp"
              name="ngayCap"
              rules={[
                ...rules.truocNgay(
                  moment().startOf("day").add(1, "d"),
                  "hôm nay"
                ),
              ]}
            >
              <MyDatePicker placeholder="Ngày cấp" />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              label="Nơi cấp"
              name="noiCap"
              rules={[...rules.text, ...rules.length(250)]}
            >
              <Input placeholder="Nơi cấp" maxLength={250} />
            </Form.Item>
          </Col>
          <Col span={24}>
            <Form.Item label="Hộ khẩu thường trú">
              <Row gutter={12} style={{ padding: "0px 12px" }}>
                <Col span={24} md={8}>
                  <Form.Item name="hoKhauThanhPhoTen" hidden />
                  <Form.Item name="hoKhauThanhPhoMa" label="Tỉnh/Thành phố">
                    <Select
                      onChange={(_, option) => {
                        if (!Array.isArray(option)) {
                          form.setFields([
                            {
                              name: "hoKhauThanhPhoTen",
                              value: option?.label ?? null,
                            },
                            { name: "hoKhauQuanMa", value: null },
                            { name: "hoKhauQuanTen", value: null },
                            { name: "hoKhauXaMa", value: null },
                            { name: "hoKhauXaTen", value: null },
                          ]);
                        }
                      }}
                      placeholder="Chọn tỉnh/thành phố"
                      options={listTinh?.map((item: any) => ({
                        key: item.ma,
                        value: item.ma,
                        label: item.tenDonVi,
                      }))}
                      allowClear
                    />
                  </Form.Item>
                </Col>
                <Col span={24} md={8}>
                  <Form.Item name="hoKhauQuanTen" hidden />
                  <Form.Item name="hoKhauQuanMa" label="Quận/Huyện">
                    <Select
                      onChange={(_, option) => {
                        if (!Array.isArray(option)) {
                          form.setFields([
                            {
                              name: "hoKhauQuanTen",
                              value: option?.label ?? null,
                            },
                            { name: "hoKhauXaMa", value: null },
                            { name: "hoKhauXaTen", value: null },
                          ]);
                        }
                      }}
                      placeholder="Chọn quận/huyện"
                      options={listHuyen?.map((item: any) => ({
                        key: item.ma,
                        value: item.ma,
                        label: item.tenDonVi,
                      }))}
                      allowClear
                    />
                  </Form.Item>
                </Col>
                <Col span={24} md={8}>
                  <Form.Item name="hoKhauXaTen" hidden />
                  <Form.Item name="hoKhauXaMa" label="Phường/Xã">
                    <Select
                      onChange={(_, option) => {
                        if (!Array.isArray(option)) {
                          form.setFields([
                            {
                              name: "hoKhauXaTen",
                              value: option?.label ?? null,
                            },
                          ]);
                        }
                      }}
                      placeholder="Chọn phường/xã"
                      options={listXa?.map((item: any) => ({
                        key: item.ma,
                        value: item.ma,
                        label: item.tenDonVi,
                      }))}
                      allowClear
                    />
                  </Form.Item>
                </Col>
                <Col span={24} md={24}>
                  <Form.Item
                    name="hoKhauSoNha"
                    label="Địa chỉ cụ thể"
                    rules={[...rules.text, ...rules.length(250)]}
                  >
                    <Input placeholder="Ví dụ: 05 đường Nguyễn Khuyến" />
                  </Form.Item>
                </Col>
              </Row>
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="moiQuanHe"
              label="Mối quan hệ"
              rules={[...rules.required]}
            >
              <Select
                allowClear
                disabled={isView}
                placeholder="Chọn mối quan hệ"
                showSearch
                options={Object.values(EMoiQuanHeBanThan).map((item) => ({
                  key: item,
                  value: item,
                  label: item,
                }))}
              />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="ngheNghiep"
              label="Nghề nghiệp"
              rules={[...rules.text, ...rules.length(250)]}
            >
              <Input disabled={isView} placeholder="Nghề nghiệp" />
            </Form.Item>
          </Col>
          <Col span={24}>
            <Form.Item
              name="noiCongTac"
              label="Nơi công tác"
              rules={[...rules.text, ...rules.length(250)]}
            >
              <Input disabled={isView} placeholder="Nơi công tác" />
            </Form.Item>
          </Col>

          <Col span={24}>
            <Form.Item
              name="noiDung"
              label="Nội dung"
              rules={[...rules.text, ...rules.length(550)]}
            >
              <Input.TextArea disabled={isView} placeholder="Nội dung" />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="nguoiPhuThuoc"
              valuePropName="checked"
              initialValue={false}
            >
              <Checkbox disabled={isView}>Người phụ thuộc</Checkbox>
            </Form.Item>
          </Col>
        </Row>

        {/*{!isView && (*/}
        {/*  <Form.Item style={{ textAlign: "center", marginTop: 24 }}>*/}
        {/*    <Button style={{ marginRight: 8 }} htmlType="submit" type="primary">*/}
        {/*      {!isEdit ? "Thêm mới " : "Lưu lại"}*/}
        {/*    </Button>*/}
        {/*    <Button onClick={onCancel}>Đóng</Button>*/}
        {/*  </Form.Item>*/}
        {/*)}*/}
        <div style={{ textAlign: "center", marginTop: 24 }}>
          {!isView && (
            <Button
              form="FormQuanHeBanThan"
              style={{ marginRight: 8 }}
              htmlType="submit"
              type="primary"
            >
              {!isEdit ? "Thêm mới " : "Lưu lại"}
            </Button>
          )}
          <Button
            onClick={() => {
              onCancel();
              resetFieldsForm(form);
            }}
          >
            Đóng
          </Button>
        </div>
      </Form>
    </CardForm>
  );
};
