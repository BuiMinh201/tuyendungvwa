import { type FormInstance } from "antd/es/form/Form";
import {FormValues, KhenThuong} from "../KhenThuong";
import {KyLuat} from "../KyLuat";

interface Props {
  form: FormInstance<FormValues>;
  disabled: boolean;
  isXemChiTietHoSo: boolean;
}

export const KhenThuongSangKienKyLuat = ({
  form,
  disabled,
  isXemChiTietHoSo = false,
}: Props) => {
  return (
    <>
      <KhenThuong form={form} isViewMode={disabled} />
      <KyLuat form={form} isViewMode={disabled} />
      {/* {isXemChiTietHoSo ? <SangKienModel disabled={disabled} /> : <SangKien form={form} disabled={disabled} />} */}
    </>
  );
};
