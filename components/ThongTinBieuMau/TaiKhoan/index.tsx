
import { Col, Divider, Form, Input, Row } from 'antd';
import { type FormInstance } from 'antd/es/form/Form';
import rules from '../../../utils/rules';

interface Props {
  form: FormInstance;
  disabled:boolean;
}

const TaiKhoan = (_: Props) => {
  // const isCreate = !edit && !isView && !isXemDanhSachCapNhat && !isViewHistory;
  // const disabledForm = isCreate ? false : !isDraff;
  const disabledForm = _?.disabled;

  return (
    <>
      <Divider orientation='center'>Tài khoản ngân hàng</Divider>
      <Row gutter={[12, 0]} style={{ marginBottom: 12 }}>
        <Col span={24} md={8}>
          <Form.Item name='tenNganHang' label='Tên ngân hàng'>
            <Input placeholder='Tên ngân hàng' disabled={disabledForm} />
          </Form.Item>
        </Col>
        <Col span={24} md={8}>
          <Form.Item name='chiNhanh' label='Chi nhánh' rules={[...rules.text, ...rules.length(250)]}>
            <Input placeholder='Chi nhánh' disabled={disabledForm} />
          </Form.Item>
        </Col>
        <Col span={24} md={8}>
          <Form.Item name='soTaiKhoan' label='Số tài khoản'>
            <Input placeholder='Số tài khoản' disabled={disabledForm} />
          </Form.Item>
        </Col>
      </Row>
      <Divider orientation='center'>Bảo hiểm xã hội, Thuế thu nhập cá nhân</Divider>
      <Row gutter={[12, 0]} style={{ marginBottom: 12 }}>
        <Col span={24} md={12}>
          <Form.Item name='soSoBHXH' label='Mã số bảo hiểm xã hội'>
            <Input placeholder='Mã số bảo hiểm xã hội' disabled={disabledForm} />
          </Form.Item>
        </Col>
        <Col span={24} md={12}>
          <Form.Item name='maSoThue' label='Mã số thuế thu nhập cá nhân'>
            <Input placeholder='Mã số thuế thu nhập cá nhân' disabled={disabledForm} />
          </Form.Item>
        </Col>
      </Row>
    </>
  );
};
export default TaiKhoan;
