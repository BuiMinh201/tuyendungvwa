import { type FormInstance } from 'antd/es/form/Form';
import { FormValues } from '../KhenThuong';
import { DienBienLuong } from './components/DienBienLuong';
import { DienBienPhuCap } from './components/DienBienPhuCap';

interface Props {
  form: FormInstance<FormValues>;
  disabled: boolean;
  isXemChiTietHoSo: boolean;
}

export const DienBienLuongPhuCap = ({ form, disabled, isXemChiTietHoSo = false }: Props) => {
  return (
    <>
        <DienBienLuong form={form} disabled={disabled} />
        <DienBienPhuCap form={form} disabled={disabled} />
    </>
  );
};
