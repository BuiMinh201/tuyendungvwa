import { PlusOutlined } from '@ant-design/icons';
import { Button, Modal, Select } from 'antd';
import {useEffect, useState} from 'react';
import { type BaseOptionType } from 'antd/lib/select';
import {getBacLuong} from "../../../../../../service/core/core";

const SelectBacLuong = (props: {
  value?: string;
  onChange?: (value: string, option: BaseOptionType) => void;
  hasCreate?: boolean;
  multiple?: boolean;
  idNgach?: string;
  disabled?: boolean;
  allowClear?: boolean;
  placeholder?: string;
}) => {
  const { value, onChange, hasCreate, multiple, idNgach, disabled, allowClear, placeholder } = props;
  const [danhSach, setDanhSach] = useState<BacLuong.IRecord[]>([]);
  const getData = async () => {
    try {
      const res = await getBacLuong();
      setDanhSach(res?.data?.data);
    } catch (e) {}
  };

  useEffect(() => {
    getData();
  }, []);
  return (

        <Select
          allowClear={allowClear}
          disabled={disabled}
          mode={multiple ? 'multiple' : undefined}
          value={value}
          onChange={onChange}
          options={(idNgach ? danhSach.filter((i) => i.ngachLuongId === idNgach) : danhSach).map((item) => ({
            key: item._id,
            value: item._id,
            label: item.bacLuong,
            rawData: item,
          }))}
          showSearch
          optionFilterProp='label'
          placeholder={placeholder ?? 'Chọn bậc lương'}
        />
  );
};

export default SelectBacLuong;
