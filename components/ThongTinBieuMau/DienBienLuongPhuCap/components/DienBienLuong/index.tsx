
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import {Button, Divider, Form, FormInstance, Popconfirm, Tooltip} from 'antd';
import moment from 'moment';
import { useState } from 'react';
import TableStaticData from '../../../../TableStaticData';
import { IColumn } from '../../../../TableStaticData/typing';
import { FormValues } from '../../../KhenThuong';
import { FormDienBienLuong, type FormDienBienLuongProps, type FormDienBienLuongValues } from './Form';
import {ELoaiLuong} from "../../../../../utils/constant";
import { tienVietNam } from '../../../../../utils/util';
import { useWatch } from 'antd/lib/form/Form';
import {buildUpLoadFile} from "../../../../../utils/uploadFile";

interface Props {
	form: FormInstance<FormValues>;
	disabled: boolean;
}

type TableRecord = FormDienBienLuongValues;
export const DienBienLuong = ({ form, disabled }: Props) => {
	const dsDienBienLuong = useWatch(['dsDienBienLuong'], form);

	const [visibleForm, setVisibleForm] = useState(false);
	const [record, setRecord] = useState<FormDienBienLuongValues>();
	const [isEdit, setIsEdit] = useState(false);
	const [isView, setIsView] = useState(false);
	const [indexRecordDangThaoTac, setIndexRecordDangThaoTac] = useState<number | undefined>(undefined);

	const handleCreate = (vi: boolean) => {
		setVisibleForm(vi);
		setIsEdit(false);
		setIsView(false);
		setRecord(undefined);
	};

	const handleEdit = (tableRecord: TableRecord, index: number) => {
		setVisibleForm(true);
		setIsEdit(true);
		setIsView(false);
		setRecord(tableRecord);
		setIndexRecordDangThaoTac(index);
	};

	const handleView = (tableRecord: TableRecord) => {
		setVisibleForm(true);
		setIsView(true);
		setIsEdit(false);
		setRecord(tableRecord);
	};

	const handleCloseModal = () => {
		setRecord(undefined);
		setVisibleForm(false);
		setIsEdit(false);
		setIsView(false);
	};

	const handleDelete = (index: number) => {
		const prevDsDienBienLuong: Required<FormValues>['dsDienBienLuong'] = form.getFieldValue('dsDienBienLuong') ?? [];
		form.setFieldsValue({
			dsDienBienLuong: prevDsDienBienLuong.filter((_, itemIndex) => itemIndex !== index),
		});
	};

	const handleSubmitForm = async (values: FormDienBienLuongValues) => {
		const prevDsDienBienLuong: Required<FormValues>['dsDienBienLuong'] = form.getFieldValue('dsDienBienLuong') ?? [];
		const url = await buildUpLoadFile(values, "fileDinhKem");
		const payload = {
			...values,
			fileDinhKem: url,
		};
		if (!isEdit) {
			form.setFieldsValue({
				dsDienBienLuong: prevDsDienBienLuong.prepend(payload),
			});
		} else if (isEdit) {
			form.setFieldsValue({
				dsDienBienLuong: prevDsDienBienLuong.map((item, index) => {
					if (indexRecordDangThaoTac === index) {
						return {
							...item,
							...payload,
						};
					}
					return item;
				}),
			});
		}

		setIndexRecordDangThaoTac(undefined);
		setVisibleForm(false);
		setIsEdit(false);
		setIsView(false);
	};

	const onCell = (rec: TableRecord) => ({
		onClick: () => handleView(rec),
		style: { cursor: 'pointer' },
	});
	const columns: IColumn<TableRecord>[] = [
		{
			title: 'Ngạch',
			dataIndex: ['ngachLuong', 'ten'],
			width: 80,
			filterType: 'number',
			onCell: onCell,
		},
		{
			title: 'Bậc',
			dataIndex: ['bacLuong', 'ten'],
			width: 80,
			filterType: 'number',
			onCell: onCell,
			sortable: true,
			align: 'center',
			render: (_, tableRecord) => {
				if (tableRecord.loaiLuong === ELoaiLuong.LUONG_THEO_VI_TRI_VIEC_LAM) return null;
				return tableRecord?.bacLuong?.bacLuong;
			},
		},
		{
			title: 'Hệ số',
			dataIndex: 'heSo',
			width: 80,
			filterType: 'number',
			onCell: onCell,
			sortable: true,
			align: 'center',
			render: (_, tableRecord) => {
				if (tableRecord.loaiLuong === ELoaiLuong.LUONG_THEO_VI_TRI_VIEC_LAM) return null;
				return tableRecord?.heSo;
			},
		},
		{
			title: 'Lương thỏa thuận',
			dataIndex: 'luongThoaThuan',
			width: 80,
			filterType: 'number',
			onCell: onCell,
			sortable: true,
			render: (_, tableRecord) => {
				if (tableRecord.loaiLuong === ELoaiLuong.LUONG_THEO_VI_TRI_VIEC_LAM) {
					return tienVietNam(tableRecord?.luongThoaThuan);
				}
				return null;
			},
		},
		{
			title: 'Từ ngày',
			dataIndex: 'tuNgay',
			width: 80,
			filterType: 'date',
			onCell: onCell,
			sortable: true,
			align: 'center',
			render: (_, tableRecord) => {
				return tableRecord.tuNgay && moment(tableRecord.tuNgay).format('DD/MM/YYYY');
			},
		},
		{
			title: 'Số quyết định',
			width: 120,
			dataIndex: 'soQuyetDinh',
			filterType: 'string',
			onCell: onCell,
			render: (_, tableRecord) => {
				return tableRecord?.soQuyetDinh;
			},
		},
	];

	if (!disabled) {
		columns.push({
			title: 'Thao tác',
			align: 'center',
			width: 90,
			fixed: 'right',
			render: (_, rec, index) => {
				return (
					<>
						<Tooltip title='Chỉnh sửa'>
							<Button type='link' icon={<EditOutlined rev={undefined} />} onClick={() => handleEdit(rec, index)} />
						</Tooltip>

						<Tooltip title='Xóa'>
							<Popconfirm
								onConfirm={() => handleDelete(index)}
								title='Bạn có chắc chắn muốn xóa diễn biến lương này?'
								placement='topLeft'
							>
								<Button danger type='link' icon={<DeleteOutlined rev={undefined} />} />
							</Popconfirm>
						</Tooltip>
					</>
				);
			},
		});
	}

	return (
		<Form.Item name='dsDienBienLuong'>
			<div id={"dien-bien-luong"}><Divider orientation='center'>Diễn biến lương</Divider></div>
			<TableStaticData
				hasCreate={!disabled}
				hasTotal
				showEdit={visibleForm}
				setShowEdit={handleCreate}
				data={dsDienBienLuong ?? []}
				columns={columns}
				addStt
				widthDrawer={700}
				Form={FormDienBienLuong}
				formProps={
					{
						record,
						isEdit,
						isView,
						onCancel: handleCloseModal,
						onFinish: handleSubmitForm,
					} as FormDienBienLuongProps
				}
			/>
		</Form.Item>
	);
};
