import { Select } from 'antd';
import { type BaseOptionType } from 'antd/lib/select';
import {type CSSProperties, useEffect, useState} from 'react';
import {getNgachLuong} from "../../../../../../service/core/core";

interface Props {
  value?: string;
  onChange?: (value?: string, option?: BaseOptionType) => void;
  hasCreate?: boolean;
  multiple?: boolean;
  style?: CSSProperties;
  placeholder?: string;
  disabled?: boolean;
  allowClear?: boolean;
}
export const SelectNgachLuongHemis = (props: Props) => {
  const { value, onChange, multiple, style, placeholder, disabled, allowClear } = props;
  const [danhSach, setDanhSach] = useState<NgachLuong.IRecordHemis[]>([]);
  const getData = async () => {
    try {
      const res = await getNgachLuong();
      setDanhSach(res?.data?.data);
    } catch (e) {}
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div className={'fullWidth'}>
      <Select
        allowClear={allowClear}
        disabled={disabled}
        style={style}
        mode={multiple ? 'multiple' : undefined}
        value={value}
        onChange={onChange}
        options={danhSach.map((item) => ({
          key: item._id,
          value: item._id,
          label: item.ten,
          rawData: item,
        }))}
        showSearch
        optionFilterProp='label'
        placeholder={placeholder ?? 'Chọn ngạch'}
      />
    </div>
  );
};
