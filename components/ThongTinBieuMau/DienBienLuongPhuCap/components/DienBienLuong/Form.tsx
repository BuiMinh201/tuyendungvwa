import { Button, Card, Col, Form, Input, InputNumber, Row, Select } from "antd";
import { useWatch } from "antd/lib/form/Form";
import moment from "moment";
import { useEffect } from "react";
import { FormValues } from "../../../KhenThuong";
import { ELoaiLuong } from "../../../../../utils/constant";
import { resetFieldsForm, tienVietNam } from "../../../../../utils/util";
import UploadFile from "../../../../Upload/UploadFile";
import MyDatePicker from "../../../../MyDatePicker";
import rules from "../../../../../utils/rules";
import { SelectNgachLuongHemis } from "./NgachLuong/select";
import SelectBacLuong from "./BacLuong/select";
import CardForm from "../../../../CardForm";

export type FormDienBienLuongValues =
  Required<FormValues>["dsDienBienLuong"][number];

export interface FormDienBienLuongProps {
  onFinish: (values: FormDienBienLuongValues) => void;
  onCancel: () => void;
  isEdit: boolean;
  isView: boolean;
  record?: FormDienBienLuongValues;
}

export const FormDienBienLuong = ({
  isEdit,
  isView,
  onCancel,
  onFinish,
  record,
}: FormDienBienLuongProps) => {
  const [form] = Form.useForm<DeepNullable<FormDienBienLuongValues>>();

  const tuNgay = useWatch(["tuNgay"], form);
  const loaiLuong = useWatch(["loaiLuong"], form);
  const ngachLuongId = useWatch(["ngachLuongId"], form);

  useEffect(() => {
    resetFieldsForm(form, {
      ...record,
      loaiLuong: record?.loaiLuong??ELoaiLuong.LUONG_THEO_NGACH_BAC,
    });
  }, [isEdit, isView, record]);

  return (
    <CardForm
      bordered={false}
      title={
        (isView ? "Xem chi tiết " : isEdit ? "Chỉnh sửa " : "Thêm mới ") +
        "diễn biến lương"
      }
    >
      <Form
        id="FormDienBienLuong"
        onFinish={onFinish}
        form={form as any}
        layout="vertical"
        disabled={isView}
      >
        <Form.Item hidden name="dieuChinh" />
        <Form.Item hidden name="ngachLuong" />
        <Form.Item hidden name="bacLuong" />
        <Row gutter={[12, 0]} style={{ marginBottom: 12 }}>
          <Col span={24}>
            <Form.Item
              name="loaiLuong"
              label="Loại lương"
              rules={[...rules.required]}
            >
              <Select
                options={Object.values(ELoaiLuong).map((item) => ({
                  label: item,
                  value: item,
                }))}
                placeholder="Loại lương"
                onChange={() => {
                  form.setFields([
                    { name: "luongThoaThuan", value: null },
                    { name: "ngachLuong", value: null },
                    { name: "ngachLuongId", value: null },
                    { name: "bacLuong", value: null },
                    { name: "bacLuongId", value: null },
                    { name: "heSo", value: null },
                  ]);
                }}
              />
            </Form.Item>
          </Col>
          {loaiLuong === ELoaiLuong.LUONG_THEO_NGACH_BAC && (
            <>
              <Col span={24} md={12}>
                <Form.Item
                  name="ngachLuongId"
                  label="Ngạch"
                  rules={[...rules.required]}
                >
                  <SelectNgachLuongHemis
                    disabled={isView}
                    hasCreate={false}
                    onChange={(_, option) => {
                      const rawData = option?.rawData as
                        | NgachLuong.IRecordHemis
                        | undefined;
                      form.setFieldsValue({
                        bacLuongId: null,
                        bacLuong: null,
                        heSo: null,
                        ngachLuong: rawData ?? null,
                      });
                    }}
                  />
                </Form.Item>
              </Col>
              <Col span={24} md={12}>
                <Form.Item
                  name="bacLuongId"
                  label="Bậc lương"
                  rules={[...rules.required]}
                >
                  <SelectBacLuong
                    disabled={isView}
                    hasCreate={false}
                    idNgach={ngachLuongId ?? undefined}
                    onChange={(_, option) => {
                      const rawData = option?.rawData as BacLuong.IRecord;
                      form.setFieldsValue({
                        heSo: rawData?.heSo ?? null,
                        bacLuong: rawData ?? null,
                      });
                    }}
                  />
                </Form.Item>
              </Col>
              <Col span={24} md={12}>
                <Form.Item
                  name="heSo"
                  label="Hệ số lương"
                  rules={[...rules.float(10)]}
                >
                  <InputNumber
                    placeholder="Hệ số lương"
                    style={{ width: "100%" }}
                    disabled
                    min={0}
                  />
                </Form.Item>
              </Col>
            </>
          )}
          {loaiLuong === ELoaiLuong.LUONG_THEO_VI_TRI_VIEC_LAM && (
            <Col span={24}>
              <Form.Item
                name="luongThoaThuan"
                label="Lương thỏa thuận"
                rules={[
                  ...rules.required,
                  ...rules.float(Number.MAX_SAFE_INTEGER, 0),
                ]}
              >
                <InputNumber
                  style={{ width: "100%" }}
                  placeholder="Lương thỏa thuận"
                  formatter={(value) => {
                    const value_ = Number(value);
                    return isNaN(value_) ? (value as any) : tienVietNam(value_);
                  }}
                  parser={(value) =>
                    value ? Number(value?.replace(/[^0-9]/g, "")) : ""
                  }
                />
              </Form.Item>
            </Col>
          )}
          <Col span={24} md={12}>
            <Form.Item
              name="phanTramHuong"
              label="Phần trăm hưởng"
              rules={[...rules.float(100, 0)]}
            >
              <InputNumber
                style={{ width: "100%" }}
                placeholder="Phần trăm hưởng"
                max={100}
                min={0}
              />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="phuCapVuotKhung"
              label="Phụ cấp vượt khung"
              rules={[...rules.float(99, 0)]}
            >
              <InputNumber
                placeholder="Phụ cấp vượt khung"
                style={{ width: "100%" }}
                min={0}
                disabled={isView}
              />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="tuNgay"
              label="Từ ngày"
              rules={[...rules.required, ...rules.nhoHonBangHomNay]}
            >
              <MyDatePicker
                onChange={(value) => {
                  form.setFields([
                    {
                      name: "mocXetNangLuong",
                      value: moment(value).add(3, "y"),
                    },
                  ]);
                }}
                disabled={isView}
                placeholder="Từ ngày"
              />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="denNgay"
              label="Đến ngày"
              rules={[
                ...(tuNgay ? rules.sauNgay(new Date(tuNgay), "từ ngày") : []),
              ]}
            >
              <MyDatePicker
                placeholder="Đến ngày"
                allowClear
                disabled={isView}
              />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="soQuyetDinh"
              label="Số quyết định"
              rules={[...rules.text, ...rules.length(20)]}
            >
              <Input placeholder="Số quyết định" disabled={isView} />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="mocXetNangLuong"
              label="Thời điểm xét nâng lương tiếp theo"
              rules={[...rules.required]}
            >
              <MyDatePicker
                placeholder="Thời điểm xét nâng lương tiếp theo"
                disabled
              />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="ngayQuyetDinh"
              label="Ngày quyết định"
              rules={[...rules.nhoHonBangHomNay]}
            >
              <MyDatePicker
                placeholder="Ngày quyết định"
                allowClear
                disabled={isView}
              />
            </Form.Item>
          </Col>
          <Col span={24}>
            <Form.Item
              name="fileDinhKem"
              label="File đính kèm"
              // rules={[...rules.fileRequired]}
            >
              <UploadFile
                maxCount={1}
                otherProps={{
                  maxCount: 1,
                  multiple: false,
                }}
              />
            </Form.Item>
          </Col>
        </Row>
      </Form>

      <div style={{ textAlign: "center", marginTop: 24 }}>
        {!isView && (
          <Button
            form="FormDienBienLuong"
            style={{ marginRight: 8 }}
            htmlType="submit"
            type="primary"
          >
            {!isEdit ? "Thêm mới " : "Lưu lại"}
          </Button>
        )}
        <Button
          onClick={() => {
            onCancel();
            resetFieldsForm(form);
          }}
        >
          Đóng
        </Button>
      </div>
    </CardForm>
  );
};
