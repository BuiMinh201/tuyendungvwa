
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { Button, Divider, Form, FormInstance, Popconfirm, Tooltip } from 'antd';
import { useState } from 'react';
import { FormDienBienPhuCap, type FormDienBienPhuCapProps, type FormDienBienPhuCapValues } from './Form';
import moment from 'moment';
import TableStaticData from "../../../../TableStaticData";
import {IColumn} from "../../../../TableStaticData/typing";
import {FormValues} from "../../../KhenThuong";
import { useWatch } from 'antd/lib/form/Form';
import {buildUpLoadFile} from "../../../../../utils/uploadFile";

interface Props {
	form: FormInstance<FormValues>;
	disabled: boolean;
}

type TableRecord = FormDienBienPhuCapValues;
export const DienBienPhuCap = ({ form, disabled }: Props) => {
	const dsDienBienPhuCap = useWatch(['dsDienBienPhuCap'], form);

	const [visibleForm, setVisibleForm] = useState(false);
	const [record, setRecord] = useState<FormDienBienPhuCapValues>();
	const [isEdit, setIsEdit] = useState(false);
	const [isView, setIsView] = useState(false);
	const [indexRecordDangThaoTac, setIndexRecordDangThaoTac] = useState<number | undefined>(undefined);

	const handleCreate = (vi: boolean) => {
		setVisibleForm(vi);
		setIsEdit(false);
		setIsView(false);
		setRecord(undefined);
	};

	const handleEdit = (tableRecord: TableRecord, index: number) => {
		setVisibleForm(true);
		setIsEdit(true);
		setIsView(false);
		setRecord(tableRecord);
		setIndexRecordDangThaoTac(index);
	};

	const handleView = (tableRecord: TableRecord) => {
		setVisibleForm(true);
		setIsView(true);
		setIsEdit(false);
		setRecord(tableRecord);
	};

	const handleCloseModal = () => {
		setRecord(undefined);
		setVisibleForm(false);
		setIsEdit(false);
		setIsView(false);
	};

	const handleDelete = (index: number) => {
		const prevdsDienBienPhuCap: Required<FormValues>['dsDienBienPhuCap'] = form.getFieldValue('dsDienBienPhuCap') ?? [];
		form.setFieldsValue({
			dsDienBienPhuCap: prevdsDienBienPhuCap.filter((_, itemIndex) => itemIndex !== index),
		});
	};

	const handleSubmitForm = async (values: FormDienBienPhuCapValues) => {
		const prevdsDienBienPhuCap: Required<FormValues>['dsDienBienPhuCap'] = form.getFieldValue('dsDienBienPhuCap') ?? [];
		const url = await buildUpLoadFile(values, "fileDinhKem");
		const payload = {
			...values,
			fileDinhKem: url,
		};
		if (!isEdit) {
			form.setFieldsValue({
				dsDienBienPhuCap: prevdsDienBienPhuCap.prepend(payload),
			});
		} else if (isEdit) {
			form.setFieldsValue({
				dsDienBienPhuCap: prevdsDienBienPhuCap.map((item, index) => {
					if (indexRecordDangThaoTac === index) {
						return {
							...item,
							...payload,
						};
					}
					return item;
				}),
			});
		}

		setIndexRecordDangThaoTac(undefined);
		setVisibleForm(false);
		setIsEdit(false);
		setIsView(false);
	};

	const onCell = (rec: TableRecord) => ({
		onClick: () => handleView(rec),
		style: { cursor: 'pointer' },
	});
	const columns: IColumn<TableRecord>[] = [
		{
			title: 'Loại phụ cấp',
			width: 120,
			dataIndex: ['loaiPhuCap', 'ten'],
			filterType: 'string',
			onCell: onCell,
		},
		{
			title: 'Mức phụ cấp',
			// align: 'center',
			width: 120,
			dataIndex: 'mucPhuCapId',
			onCell: onCell,
			render: (_, tableRecord) => {
				return tableRecord?.mucPhuCap?.ten;
			},
		},
		{
			title: 'Hệ số/Phần trăm hưởng/Giá trị',
			width: 120,
			dataIndex: 'heSo',
			align: 'center',
			onCell: onCell,
			render: (_, tableRecord) => {
				return tableRecord?.heSo;
			},
		},
		{
			title: 'Từ ngày',
			dataIndex: 'tuNgay',
			width: 120,
			filterType: 'date',
			onCell: onCell,
			sortable: true,
			align: 'center',
			render: (_, tableRecord) => {
				return tableRecord.tuNgay ? moment(tableRecord.tuNgay).format('DD/MM/YYYY') : null;
			},
		},

		{
			title: 'Đến ngày',
			dataIndex: 'denNgay',
			width: 120,
			filterType: 'date',
			onCell: onCell,
			sortable: true,
			align: 'center',
			render: (_, tableRecord) => {
				return tableRecord.denNgay ? moment(tableRecord.denNgay).format('DD/MM/YYYY') : null;
			},
		},

		{
			title: 'Mức hưởng (%)',
			// align: 'center',
			dataIndex: 'mucHuong',
			filterType: 'number',
			onCell: onCell,
			sortable: true,
			align: 'center',
			width: 120,
			render: (_, tableRecord) => {
				return tableRecord.mucHuong;
			},
		},
	];

	if (!disabled) {
		columns.push({
			title: 'Thao tác',
			align: 'center',
			width: 90,
			fixed: 'right',
			render: (_, rec, index) => {
				return (
					<>
						<Tooltip title='Chỉnh sửa'>
							<Button type='link' icon={<EditOutlined rev={undefined} />} onClick={() => handleEdit(rec, index)} />
						</Tooltip>

						<Tooltip title='Xóa'>
							<Popconfirm
								onConfirm={() => handleDelete(index)}
								title='Bạn có chắc chắn muốn xóa diễn biến phụ cấp này?'
								placement='topLeft'
							>
								<Button danger type='link' icon={<DeleteOutlined rev={undefined} />} />
							</Popconfirm>
						</Tooltip>
					</>
				);
			},
		});
	}

	return (
		<Form.Item name='dsDienBienPhuCap'>
			<Divider orientation='center'>Diễn biến phụ cấp</Divider>
			<TableStaticData
				hasCreate={!disabled}
				hasTotal
				showEdit={visibleForm}
				setShowEdit={handleCreate}
				data={dsDienBienPhuCap ?? []}
				columns={columns}
				addStt
				widthDrawer={700}
				Form={FormDienBienPhuCap}
				formProps={
					{
						record,
						isEdit,
						isView,
						onCancel: handleCloseModal,
						onFinish: handleSubmitForm,
					} as FormDienBienPhuCapProps
				}
			/>
		</Form.Item>
	);
};
