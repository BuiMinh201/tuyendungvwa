
import { Select } from 'antd';
import { type BaseOptionType } from 'antd/lib/select';
import { type CSSProperties, useEffect, useState } from 'react';
import { LoaiPhuCap } from '../../../../../../Interface/PhuCap/typing';
import {getMucPhuCap, getNgachLuong} from "../../../../../../service/core/core";

export const SelectMucPhuCap = (props: {
  allowClear?: boolean;
  value?: string;
  onChange?: (value: string, option: BaseOptionType) => void;
  multiple?: boolean;
  disabled?: boolean;
  loaiPhuCapId: string | undefined;
  placeholder?: string;
  style?: CSSProperties;
}) => {
  const { value, onChange, multiple, disabled, loaiPhuCapId, placeholder, style, allowClear } = props;
  const [danhSach, setDanhSach] = useState<LoaiPhuCap.IRecordMucPhuCap[]>([]);
  const getData = async () => {
    try {
      const res = await getMucPhuCap();
      setDanhSach(res?.data?.data);
    } catch (e) {}
  };

  useEffect(() => {
    getData();
  }, []);

  return (
        <Select
          allowClear={allowClear}
          style={style}
          disabled={disabled}
          mode={multiple ? 'multiple' : undefined}
          value={value}
          onChange={onChange}
          options={danhSach
            .filter((item) => item.trangThai)
            .map((item) => ({
              key: item._id,
              value: item._id,
              label: item.ten,
              rawData: item,
            }))}
          showSearch
          optionFilterProp='label'
          placeholder={placeholder ?? 'Chọn mức phụ cấp'}
        />
  );
};
