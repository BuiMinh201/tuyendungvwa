
import { Button, Modal, Select } from 'antd';
import {type CSSProperties, useEffect, useState} from 'react';
import { type BaseOptionType } from 'antd/lib/select';
import {getLoaiPhuCap, getNgachLuong} from "../../../../../../service/core/core";
import {LoaiPhuCap} from "../../../../../../Interface/PhuCap/typing";

export const SelectLoaiPhuCap = (props: {
  allowClear?: boolean;
  value?: string;
  onChange?: (value: string, option: BaseOptionType) => void;
  hasCreate?: boolean;
  multiple?: boolean;
  disabled?: boolean;
  placeholder?: string;
  style?: CSSProperties;
}) => {
  const { value, onChange, hasCreate, multiple, disabled, placeholder, style, allowClear } = props;
  const [danhSach, setDanhSach] = useState<LoaiPhuCap.IRecord[]>([]);
  const getData = async () => {
    try {
      const res = await getLoaiPhuCap();
      setDanhSach(res?.data?.data);
    } catch (e) {}
  };

  useEffect(() => {
    getData();
  }, []);
  return (
        <Select
          allowClear={allowClear}
          style={style}
          disabled={disabled}
          mode={multiple ? 'multiple' : undefined}
          value={value}
          onChange={onChange}
          options={danhSach.map((item) => ({
            key: item._id,
            value: item._id,
            label: item.ten,
            rawData: item,
          }))}
          showSearch
          optionFilterProp='label'
          placeholder={placeholder ?? 'Chọn loại phụ cấp'}
        />

  );
};
