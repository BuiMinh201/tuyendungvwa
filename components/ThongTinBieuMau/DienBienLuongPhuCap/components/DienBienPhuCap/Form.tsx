import { Button, Card, Checkbox, Col, Form, InputNumber, Row } from "antd";
import { useWatch } from "antd/lib/form/Form";
import moment from "moment";
import { useEffect } from "react";
import { resetFieldsForm } from "../../../../../utils/util";
import UploadFile from "../../../../Upload/UploadFile";
import MyDatePicker from "../../../../MyDatePicker";
import rules from "../../../../../utils/rules";
import { FormValues } from "../../../KhenThuong";
import { SelectLoaiPhuCap } from "./LoaiPhuCap/select";
import { LoaiPhuCap } from "../../../../../Interface/PhuCap/typing";
import { SelectMucPhuCap } from "./MucPhuCap";
import CardForm from "../../../../CardForm";

export type FormDienBienPhuCapValues =
  Required<FormValues>["dsDienBienPhuCap"][number];

export interface FormDienBienPhuCapProps {
  onFinish: (values: FormDienBienPhuCapValues) => void;
  onCancel: () => void;
  isEdit: boolean;
  isView: boolean;
  record?: FormDienBienPhuCapValues;
}

export const FormDienBienPhuCap = ({
  isEdit,
  isView,
  onCancel,
  onFinish,
  record,
}: FormDienBienPhuCapProps) => {
  const [form] = Form.useForm<FormDienBienPhuCapValues>();

  const loaiPhuCapId = useWatch(["loaiPhuCapId"], form);
  const loaiPhuCap = useWatch(["loaiPhuCap"], form);
  const tuNgay = useWatch(["tuNgay"], form);
  const denNgay = useWatch(["denNgay"], form);
  const phuCapTinhBHXH = useWatch(["phuCapTinhBHXH"], form);

  useEffect(() => {
    resetFieldsForm(form, {
      ...record,
      mucHuong: record?.mucHuong ?? 100,
    });
  }, [isEdit, isView, record]);

  return (
    <CardForm
      bordered={false}
      title={
        (isView ? "Xem chi tiết " : isEdit ? "Chỉnh sửa " : "Thêm mới ") +
        "diễn biến phụ cấp"
      }
    >
      <Form
        id="FormDienBienPhuCap"
        onFinish={onFinish}
        form={form}
        layout="vertical"
        disabled={isView}
      >
        <Row gutter={[12, 0]} style={{ marginBottom: 12 }}>
          <Col span={24} md={12}>
            <Form.Item name="loaiPhuCap" hidden />
            <Form.Item
              name="loaiPhuCapId"
              label="Loại phụ cấp"
              rules={[...rules.required]}
            >
              <SelectLoaiPhuCap
                onChange={(_, option) => {
                  const rawData = option?.rawData as
                    | LoaiPhuCap.IRecord
                    | undefined;
                  form.setFieldsValue({
                    loaiPhuCap: rawData,
                    mucPhuCap: null,
                    mucPhuCapId: null,
                    phuCapTinhBHXH: rawData?.phuCapBHXH,
                    denNgay: denNgay
                      ? denNgay
                      : tuNgay
                      ? moment(tuNgay)
                          .add(rawData?.thoiGianLenPhuCap ?? 0, "month")
                          .toISOString()
                      : undefined,
                  });
                }}
                placeholder="Loại phụ cấp"
                hasCreate={false}
                disabled={isView}
              />
            </Form.Item>
          </Col>
          {loaiPhuCap?.coMucPhuCap && (
            <Col span={24} md={12}>
              <Form.Item name="mucPhuCap" hidden />
              <Form.Item
                name="mucPhuCapId"
                label="Mức phụ cấp"
                rules={[...rules.required]}
              >
                <SelectMucPhuCap
                  loaiPhuCapId={loaiPhuCapId}
                  placeholder="Mức phụ cấp"
                  disabled={isView || !loaiPhuCapId}
                  onChange={(_, option) => {
                    const rawData = option?.rawData as
                      | LoaiPhuCap.IRecordMucPhuCap
                      | undefined;
                    form.setFieldsValue({
                      mucPhuCap: rawData,
                    });
                  }}
                />
              </Form.Item>
            </Col>
          )}
          <Col span={24} md={12}>
            <Form.Item
              name="heSo"
              label="Hệ số/Phần trăm hưởng/Giá trị"
              rules={[...rules.number(Number.MAX_SAFE_INTEGER)]}
            >
              <InputNumber
                style={{ width: "100%" }}
                placeholder="Hệ số/Phần trăm hưởng/Giá trị"
              />
            </Form.Item>
          </Col>

          <Col span={24} md={12}>
            <Form.Item
              name="tuNgay"
              label="Từ ngày"
              rules={[...rules.required, ...rules.nhoHonBangHomNay]}
            >
              <MyDatePicker
                onChange={(value) => {
                  form.setFieldsValue({
                    denNgay: denNgay
                      ? denNgay
                      : moment(value)
                          .add(loaiPhuCap?.thoiGianLenPhuCap ?? 0, "month")
                          .toISOString(),
                  });
                  form.validateFields(["denNgay"]);
                }}
                disabled={isView}
                placeholder="Từ ngày"
              />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="denNgay"
              label="Đến ngày"
              rules={[
                ...(tuNgay ? rules.sauNgay(new Date(tuNgay), "từ ngày") : []),
              ]}
            >
              <MyDatePicker
                disabled={isView}
                placeholder="Đến ngày"
                allowClear
              />
            </Form.Item>
          </Col>
          <Col span={24}>
            <Form.Item
              name="mucHuong"
              label="Mức hưởng (%)"
              rules={[...rules.float(Number.MAX_SAFE_INTEGER, 0)]}
            >
              <InputNumber
                style={{ width: "100%" }}
                placeholder="Mức hưởng (%)"
              />
            </Form.Item>
          </Col>
          <Col span={24} style={{ display: "flex", alignItems: "center" }}>
            <Form.Item name="phuCapTinhBHXH" valuePropName="checked">
              <Checkbox checked={phuCapTinhBHXH} disabled>
                Phụ cấp tính BHXH
              </Checkbox>
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="fileDinhKem"
              label="File đính kèm"
              // rules={[...rules.fileRequired]}
            >
              <UploadFile
                maxCount={1}
                otherProps={{
                  maxCount: 1,
                  multiple: false,
                }}
              />
            </Form.Item>
          </Col>
        </Row>
      </Form>
      <div style={{ textAlign: "center", marginTop: 24 }}>
        {!isView && (
          <Button
            form="FormDienBienPhuCap"
            style={{ marginRight: 8 }}
            htmlType="submit"
            type="primary"
          >
            {!isEdit ? "Thêm mới " : "Lưu lại"}
          </Button>
        )}
        <Button
          onClick={() => {
            onCancel();
            resetFieldsForm(form);
          }}
        >
          Đóng
        </Button>
      </div>
    </CardForm>
  );
};
