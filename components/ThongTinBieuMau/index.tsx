import { FormInstance } from "antd/es/form/Form";
import {
  Button,
  Col,
  Collapse,
  Divider,
  Form,
  Input,
  InputNumber,
  message,
  Popover,
  Row,
  Select,
  Space,
} from "antd";
import UploadFile from "../Upload/UploadFile";
import rules from "../../utils/rules";
import { EGioiTinh, EHocHam } from "../../utils/constant";
import ThongTinChung from "./ThongTinChung";
import { Key, useEffect, useState } from "react";
import MyDatePicker from "../MyDatePicker";
import { ThongTinNhanSu } from "../../Interface/ThongTinNhanSu/typing";
import { QuaTrinhCongTac } from "./QuaTrinhCongTac";
import { TrinhDoDaoTaoChungChi } from "./TrinhDoDaoTaoChungChi";
import { ThongTinDangDoan } from "./ThongTinDangDoan";
import { QuanHeGiaDinh } from "./QuanHeGiaDinh";
import { DacDiemLichSuBanThan } from "./DacDiemLichSuBanThan";
import { ThongTinKhac } from "./ThongTinKhac";
import { KhenThuongSangKienKyLuat } from "./KhenThuongKyLuat";
import { DienBienLuongPhuCap } from "./DienBienLuongPhuCap";
import TinyEditor from "../TinyEditor/Tiny";
import TableStaticData from "../TableStaticData";
import { IColumn } from "../TableStaticData/typing";
import {
  ExclamationCircleOutlined,
  QuestionCircleOutlined,
} from "@ant-design/icons";
import { handleScrollToDivElementById } from "../../utils/util";
import { useWatch } from "antd/lib/form/Form";
import { getTinhThanhPho } from "../../service/core/donvihanhchinh";

interface IProps {
  form: FormInstance;
  onFinish?: (val: any) => void;
  dataHoSo?: ThongTinNhanSu.IRecord;
  disabled?: boolean;
  dataViecLam?: ViTriViecLam.IRecord;
}

const ThongTinBieuMauForm = (props: IProps) => {
  const { form, onFinish, disabled, dataHoSo } = props;
  const [ngaySinhForm, setngaySinhForm] = useState<string | null>();
  const [openPanels, setOpenPanels] = useState<any>("1");
  const dsDienBienLuong = useWatch(["dsDienBienLuong"], form);
  const dsQuaTrinhDaoTaoBoiDuong = useWatch(["dsQuaTrinhDaoTaoBoiDuong"], form);
  const dsQuaTrinhCongTac = useWatch(["dsQuaTrinhCongTac"], form);
  const dsThongTinTrinhDoDaoTao = useWatch(["dsThongTinTrinhDoDaoTao"], form);
  const dsQuanHeGiaDinhVeBanThan = useWatch(["dsQuanHeGiaDinhVeBanThan"], form);
  const isCongTac = useWatch(["isCongTac"], form);
  const isDienBienLuong = useWatch(["isDienBienLuong"], form);
  const [dataTinh, setDataTinh] = useState<any[]>([]);

  useEffect(() => {
    getTinhThanhPho().then((data) => {
      setDataTinh(data.data);
    });
  }, []);
  useEffect(() => {
    form.setFieldsValue({
      dienBienLuong: dsDienBienLuong?.[0]?.loaiLuong,
      daoTaoBoiDuong: dsQuaTrinhDaoTaoBoiDuong?.[0]?.loaiBoiDuong?.ten,
      congTac: dsQuaTrinhCongTac?.[0]?.donViCongTac,
      trinhDoDaoTao: dsThongTinTrinhDoDaoTao?.[0]?.trinhDoDaoTao?.ten,
      isDienBienLuong: !!dsDienBienLuong,
      isCongTac: !!dsQuaTrinhCongTac,
      quanHeGiaDinh: dsQuanHeGiaDinhVeBanThan?.[0]
        ? "Đã nhập thông tin"
        : undefined,
    });
  }, [
    dsDienBienLuong,
    dsQuaTrinhDaoTaoBoiDuong,
    dataHoSo,
    dsQuaTrinhCongTac,
    dsThongTinTrinhDoDaoTao,
    dsQuanHeGiaDinhVeBanThan,
  ]);
  const columns2: IColumn<any>[] = [
    {
      title: "Tên giấy tờ",
      dataIndex: "tenFile",
      align: "center",
      width: 160,
    },
    {
      title: "File đính kèm",
      align: "center",
      width: 120,
      fixed: "right",
      render: (_, tableRecord, index) => {
        return (
          <div>
            {tableRecord?.file?.map(
              (val: string | undefined, index: number) => {
                return (
                  <a href={val} style={{ color: "#2387e3" }}>
                    File đính kèm {index + 1}
                  </a>
                );
              }
            )}
          </div>
        );
      },
    },
  ];
  const columns: IColumn<any>[] = [
    {
      title: "Tên giấy tờ",
      dataIndex: "tenGiayTo",
      width: 160,
      render: (_, tableRecord) => {
        if (tableRecord.urlFileHuongDan.length || tableRecord.huongDan) {
          return (
            <Space align="center">
              <span>{tableRecord.tenGiayTo}</span>
              <Popover
                title={
                  <div
                    style={{ display: "flex", gap: 8, alignItems: "center" }}
                  >
                    <ExclamationCircleOutlined
                      style={{ fontSize: 18 }}
                      className="color--primary"
                      rev={undefined}
                    />
                    File hướng dẫn đính kèm
                  </div>
                }
                content={
                  <Space direction="vertical">
                    {tableRecord.huongDan}
                    {tableRecord.urlFileHuongDan.map(
                      (item: string | undefined, index: number) => {
                        return (
                          // eslint-disable-next-line react/no-array-index-key
                          <Button
                            className="link__style_none"
                            target="_blank"
                            href={item}
                            key={index}
                            type="primary"
                          >
                            Xem tập tin {index + 1}
                          </Button>
                        );
                      }
                    )}
                  </Space>
                }
              >
                <QuestionCircleOutlined rev={undefined} />
              </Popover>
            </Space>
          );
        }
        return tableRecord?.tenGiayTo;
      },
    },
    {
      title: "Ghi chú",
      dataIndex: "ghiChuHoSoDuTuyen",
      width: 180,
      render: (value) => value,
    },
    {
      title: "Bắt buộc",
      dataIndex: "batBuoc",
      align: "center",
      width: 140,
      render: (value) => (value ? "Có" : "Không"),
    },
    {
      title: "File đính kèm",
      align: "center",
      width: 120,
      fixed: "right",
      render: (_, tableRecord, index) => {
        return (
          <div style={{ maxWidth: 240 }}>
            <Form.Item name={["taiLieuUngTuyen", index, "ten"]} hidden />
            <Form.Item
              className="FormItem__mb0"
              name={["taiLieuUngTuyen", index, "file"]}
              rules={tableRecord.batBuoc ? [...rules.fileRequired] : []}
            >
              <UploadFile
                maxCount={5}
                accept={".docx, .pdf, .png, .jpg"}
                otherProps={{ maxCount: 5, multiple: true }}
              />
            </Form.Item>
          </div>
        );
      },
    },
  ];
  return (
    <div className="px-[20px] xl:px-0 ">
      <Form
        id="FormKhaiBaoHoSoDuTuyen"
        layout="vertical"
        form={form}
        className="view-create-form"
        onFinish={onFinish}
        scrollToFirstError={{
          behavior: "smooth",
          block: "center",
          inline: "center",
        }}
        onFinishFailed={() => {
          if (
            props?.dataViecLam &&
            props?.dataViecLam?.dotTuyenDung?.hoSoDuTuyen?.length > 0
          ) {
            handleScrollToDivElementById("giay-to");
          }
          message.warning(
            "Một số trường thông tin còn thiếu vui lòng kiểm tra lại"
          );
        }}
      >
        <Row gutter={[12, 0]}>
          <Col
            span={24}
            sm={6}
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Form.Item name="urlAnhDaiDien" label="">
              <UploadFile isAvatar accept={".png, .jpg"} />
            </Form.Item>
          </Col>
          <Col span={24} sm={18}>
            <Row gutter={[12, 0]}>
              <Col xl={8} span={24}>
                <Form.Item
                  name="hoDem"
                  label="Họ đệm"
                  rules={[
                    ...rules.required,
                    ...rules.text,
                    ...rules.length(250),
                  ]}
                >
                  <Input disabled={disabled} autoFocus placeholder="Họ đệm" />
                </Form.Item>
              </Col>
              <Col xl={8} span={24}>
                <Form.Item
                  name="ten"
                  label="Tên"
                  rules={[
                    ...rules.required,
                    ...rules.text,
                    ...rules.length(250),
                  ]}
                >
                  <Input disabled={disabled} placeholder="Tên" />
                </Form.Item>
              </Col>
              {/*<Col xl={8} span={24}>*/}
              {/*  <Form.Item*/}
              {/*    name="tenGoiKhac"*/}
              {/*    label="Tên gọi khác"*/}
              {/*    rules={[...rules.text, ...rules.length(250)]}*/}
              {/*  >*/}
              {/*    <Input placeholder="Tên gọi khác" />*/}
              {/*  </Form.Item>*/}
              {/*</Col>*/}
              <Col xl={8} span={24}>
                <Form.Item
                  name="ngaySinh"
                  label="Ngày sinh"
                  rules={[...rules.ngaySinh, ...rules.required]}
                >
                  <MyDatePicker
                    disabled={disabled}
                    allowClear
                    placeholder="VD:14/02/2024"
                    onChange={(value) => setngaySinhForm(value as any)}
                  />
                </Form.Item>
              </Col>
              <Col xl={8} span={24}>
                <Form.Item
                  name="noiSinhThanhPhoMa"
                  label="Nơi sinh"
                  rules={[...rules.required]}
                >
                  <Select
                    disabled={disabled}
                    placeholder="Chọn tỉnh/thành phố"
                    //@ts-ignore
                    options={dataTinh?.data?.map((item: any) => ({
                      key: item.ma,
                      value: item.ma,
                      label: item.tenDonVi,
                    }))}
                    showSearch
                    optionFilterProp='label'
                    filterOption={(input, option) => ((option?.label as string)?.toLowerCase() ?? '')?.includes(input?.toLowerCase())}
                  />
                </Form.Item>
              </Col>
              <Col xl={8} span={24}>
                <Form.Item
                  name="gioiTinh"
                  label="Giới tính"
                  rules={[...rules.required]}
                >
                  <Select
                    disabled={disabled}
                    placeholder="Giới tính"
                    showSearch
                    options={Object.values(EGioiTinh).map((item) => ({
                      key: item,
                      value: item,
                      label: item,
                    }))}
                  />
                </Form.Item>
              </Col>

              <Col xl={8} span={24}>
                <Form.Item
                  name="email"
                  label="Email"
                  rules={[
                    ...rules.text,
                    ...rules.length(250),
                    ...rules.email,
                    ...rules.required,
                  ]}
                >
                  <Input disabled={disabled} placeholder="Email" />
                </Form.Item>
              </Col>
              <Col span={24} md={8}>
                <Form.Item
                  name="sdtCaNhan"
                  label="Số điện thoại"
                  rules={[...rules.soDienThoai, ...rules.required]}
                >
                  <Input disabled={disabled} placeholder="Số điện thoại" />
                </Form.Item>
              </Col>
              <Col span={24} md={8}>
                <Form.Item
                  name="cccdCMND"
                  label="CCCD/CMND"
                  rules={[...rules.CMND, ...rules.required]}
                >
                  <Input disabled={disabled} placeholder="CCCD/CMND" />
                </Form.Item>
              </Col>
              <Col span={24} md={8}>
                <Form.Item
                  name="noiCap"
                  label="Nơi cấp"
                  rules={[
                    ...rules.text,
                    ...rules.length(250),
                    ...rules.required,
                  ]}
                >
                  <Input disabled={disabled} placeholder="Nơi cấp" />
                </Form.Item>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row gutter={[12, 0]} style={{ marginBottom: 12 }}>
          <Col span={24} md={6}>
            <Form.Item
              name="ngayCap"
              label="Ngày cấp"
              rules={[
                ...rules.required,
                ...rules.nhoHonBangHomNay,
                ...(ngaySinhForm
                  ? rules.sauNgay(ngaySinhForm, "ngày sinh")
                  : []),
              ]}
            >
              <MyDatePicker
                disabled={disabled}
                allowClear
                placeholder="Ngày cấp"
              />
            </Form.Item>
          </Col>
          <Col span={24} md={6}>
            <Form.Item
              name="tenNganHang"
              label="Tên ngân hàng"
              rules={[...rules.required]}
            >
              <Input disabled={disabled} placeholder="Tên ngân hàng" />
            </Form.Item>
          </Col>
          <Col span={24} md={6}>
            <Form.Item
              name="chiNhanh"
              label="Chi nhánh"
              rules={[...rules.text, ...rules.length(250), ...rules.required]}
            >
              <Input disabled={disabled} placeholder="Chi nhánh" />
            </Form.Item>
          </Col>
          <Col span={24} md={6}>
            <Form.Item
              name="soTaiKhoan"
              label="Số tài khoản"
              rules={[...rules.required]}
            >
              <Input placeholder="Số tài khoản" disabled={disabled} />
            </Form.Item>
          </Col>
          <Col span={24} md={6}>
            <Form.Item
              name="soSoBHXH"
              label="Mã số bảo hiểm xã hội"
              rules={[...rules.required]}
            >
              <Input placeholder="Mã số bảo hiểm xã hội" disabled={disabled} />
            </Form.Item>
          </Col>
          <Col span={24} md={6}>
            <Form.Item
              name="maSoThue"
              label="Mã số thuế thu nhập cá nhân"
              rules={[...rules.required]}
            >
              <Input
                placeholder="Mã số thuế thu nhập cá nhân"
                disabled={disabled}
              />
            </Form.Item>
          </Col>
          <Col span={24} md={6}>
            <Form.Item
              name="chieuCao"
              label="Chiều cao (cm)"
              rules={[...rules.float(999), ...rules.required]}
            >
              <InputNumber
                disabled={disabled}
                placeholder="Chiều cao (cm)"
                style={{ width: "100%" }}
                min={0}
              />
            </Form.Item>
          </Col>
          <Col span={24} md={6}>
            <Form.Item
              name="canNang"
              label="Cân nặng (kg)"
              rules={[...rules.float(999), ...rules.required]}
            >
              <InputNumber
                disabled={disabled}
                placeholder="Cân nặng (kg)"
                style={{ width: "100%" }}
                min={0}
              />
            </Form.Item>
          </Col>
          <Col span={24} md={6}>
            <Form.Item
              name="isCongTac"
              label="Đã từng công tác"
              rules={[...rules.required]}
            >
              <Select
                disabled={disabled}
                placeholder={"Đã từng công tác"}
                options={[
                  { value: true, label: "Có" },
                  { value: false, label: "Không" },
                ]}
              />
            </Form.Item>
          </Col>
          <Col span={24} md={6}>
            <Form.Item
              name="isDienBienLuong"
              label="Đã từng nhận lương"
              rules={[...rules.required]}
            >
              <Select
                disabled={disabled}
                placeholder={"Đã từng nhận lượng"}
                options={[
                  { value: true, label: "Có" },
                  { value: false, label: "Không" },
                ]}
              />
            </Form.Item>
          </Col>
          <Col span={24} md={6}>
            <Form.Item
              tooltip={{
                title: (
                  <div>
                    Hoàn thiện thông tin đào tạo bồi dưỡng
                    <Button
                      style={{ margin: 0, padding: "0 0 0 4px" }}
                      type={"link"}
                      onClick={() => {
                        setOpenPanels("2");
                        handleScrollToDivElementById("trinh-do-dao-tao", 500);
                      }}
                    >
                      tại đây
                    </Button>
                  </div>
                ),
                icon: <QuestionCircleOutlined rev={undefined} />,
              }}
              name="trinhDoDaoTao"
              label="Trình độ đào tạo"
              rules={[...rules.text, ...rules.length(250), ...rules.required]}
            >
              <Input disabled={true} placeholder="Trình độ đào tạo" />
            </Form.Item>
          </Col>
          {isCongTac && (
            <Col span={24} md={6}>
              <Form.Item
                tooltip={{
                  title: (
                    <div>
                      Hoàn thiện thông tin quá trình công tác
                      <Button
                        style={{ margin: 0, padding: "0 0 0 4px" }}
                        type={"link"}
                        onClick={() => {
                          setOpenPanels("3");
                          handleScrollToDivElementById(
                            "qua-trinh-cong-tac",
                            500
                          );
                        }}
                      >
                        tại đây
                      </Button>
                    </div>
                  ),
                  icon: <QuestionCircleOutlined rev={undefined} />,
                }}
                name="congTac"
                label="Quá trình công tác"
                rules={[...rules.text, ...rules.length(250), ...rules.required]}
              >
                <Input disabled={true} placeholder="Quá trình công tác" />
              </Form.Item>
            </Col>
          )}
          {isDienBienLuong && (
            <Col span={24} md={6}>
              <Form.Item
                tooltip={{
                  title: (
                    <div>
                      Hoàn thiện thông tin diễn biến lương
                      <Button
                        style={{ margin: 0, padding: "0 0 0 4px" }}
                        type={"link"}
                        onClick={() => {
                          setOpenPanels("4");
                          handleScrollToDivElementById("dien-bien-luong", 500);
                        }}
                      >
                        tại đây
                      </Button>
                    </div>
                  ),
                  icon: <QuestionCircleOutlined rev={undefined} />,
                }}
                name="dienBienLuong"
                label="Diễn biến lương"
                rules={[...rules.text, ...rules.length(250), ...rules.required]}
              >
                <Input disabled={true} placeholder="Diễn biến lương" />
              </Form.Item>
            </Col>
          )}

          <Col span={24} md={6}>
            <Form.Item
              tooltip={{
                title: (
                  <div>
                    Hoàn thiện thông tin quan hệ gia đình
                    <Button
                      style={{ margin: 0, padding: "0 0 0 4px" }}
                      type={"link"}
                      onClick={() => {
                        setOpenPanels("7");
                        handleScrollToDivElementById("quan-he-gia-dinh", 500);
                      }}
                    >
                      tại đây
                    </Button>
                  </div>
                ),
                icon: <QuestionCircleOutlined rev={undefined} />,
              }}
              name="quanHeGiaDinh"
              label="Mối quan hệ gia đình"
              rules={[...rules.required]}
            >
              <Input disabled={true} placeholder="Mối quan hệ gia đình" />
            </Form.Item>
          </Col>
        </Row>
        <Collapse
          activeKey={openPanels}
          onChange={(value) => setOpenPanels(value)}
        >
          <Collapse.Panel header="Thông tin cơ bản" key="1">
            <ThongTinChung form={form} disabled={disabled} />
          </Collapse.Panel>
          <Collapse.Panel header="Đào tạo, bồi dưỡng" key="2">
            <TrinhDoDaoTaoChungChi
              isXemChiTietHoSo={false}
              form={form}
              disabled={disabled}
            />
          </Collapse.Panel>
          <Collapse.Panel header="Quá trình công tác" key="3">
            <QuaTrinhCongTac form={form} isViewMode={disabled} />
          </Collapse.Panel>
          <Collapse.Panel header="Diễn biến lương, phụ cấp" key="4">
            <DienBienLuongPhuCap
              isXemChiTietHoSo
              form={form}
              disabled={disabled ?? false}
            />
          </Collapse.Panel>
          <Collapse.Panel header="Thông tin Đảng, Đoàn" key="5">
            <ThongTinDangDoan form={form} disabled={disabled ?? false} />
          </Collapse.Panel>
          <Collapse.Panel header="Khen thưởng, Kỷ luật" key="6">
            <KhenThuongSangKienKyLuat
              isXemChiTietHoSo={false}
              form={form}
              disabled={disabled ?? false}
            />
          </Collapse.Panel>
          <Collapse.Panel header="Quan hệ gia đình" key="7">
            <QuanHeGiaDinh
              isXemChiTietHoSo={false}
              form={form}
              disabled={disabled ?? false}
            />
          </Collapse.Panel>
          <Collapse.Panel header="Đặc điểm lịch sử bản thân" key="8">
            <DacDiemLichSuBanThan
              isXemChiTietHoSo={false}
              form={form}
              disabled={disabled ?? false}
            />
          </Collapse.Panel>
          <Collapse.Panel header="Thông tin khác" key="9">
            <ThongTinKhac
              isXemChiTietHoSo={false}
              form={form}
              disabled={disabled ?? false}
            />
            <Col span={24}>
              <Divider orientation="center">Kinh nghiệm thành tích</Divider>
              <Form.Item
                name="kinhNghiemThanhTich"
                // label="Kinh nghiệm/Thành tích"
              >
                <TinyEditor />
              </Form.Item>
            </Col>
          </Collapse.Panel>

          {/*<Collapse.Panel header="Tài khoản - sổ BHXH" key="3">*/}
          {/*  <TaiKhoan form={form} />*/}
          {/*</Collapse.Panel>*/}
          {/*<Collapse.Panel header="Khen thưởng" key="4">*/}
          {/*  <Form.Item name="dsKhenThuong">*/}
          {/*    <KhenThuong form={form} isViewMode={disabled} />*/}
          {/*  </Form.Item>*/}
          {/*</Collapse.Panel>*/}
          {/*<Collapse.Panel header="Kỷ luật" key="5">*/}
          {/*  <Form.Item name="dsKyLuat">*/}
          {/*    <KyLuat form={form} isViewMode={disabled} />*/}
          {/*  </Form.Item>*/}
          {/*</Collapse.Panel>*/}
          {/*<Collapse.Panel*/}
          {/*  header="Quá trình được cử đi đào tạo, bồi dưỡng"*/}
          {/*  key="6"*/}
          {/*>*/}
          {/*  <Form.Item name="dsQuaTrinhCuDiDaoTaoBoiDuong">*/}
          {/*    <QuaTrinhCuDiDaoTaoBoiDuong isViewMode={disabled} form={form} dataHoSo={dataHoSo} />*/}
          {/*  </Form.Item>*/}
          {/*</Collapse.Panel>*/}
          {/*<Collapse.Panel header="Quá trình công tác" key="7">*/}
          {/*  <Form.Item name="dsQuaTrinhCongTac">*/}
          {/*    <QuaTrinhCongTac form={form} isViewMode={disabled} dataHoSo={dataHoSo}/>*/}
          {/*  </Form.Item>*/}
          {/*</Collapse.Panel>*/}
          {/*<Collapse.Panel header="Lịch sử bản thân" key="8">*/}
          {/*  <LichSuBanThan />*/}
          {/*</Collapse.Panel>*/}
          {/*<Collapse.Panel*/}
          {/*  header="Mối quan hệ trong gia đình về bản thân"*/}
          {/*  key="9"*/}
          {/*>*/}
          {/*  <Form.Item name="dsQuanHeGiaDinhVeBanThan">*/}
          {/*    <QuanHeGiaDinhVeBanThan isViewMode={disabled} form={form} dataHoSo={dataHoSo}/>*/}
          {/*  </Form.Item>*/}
          {/*</Collapse.Panel>*/}
          {/*<Collapse.Panel*/}
          {/*  header="Mối quan hệ trong gia đình về bên vợ chồng"*/}
          {/*  key="10"*/}
          {/*>*/}
          {/*  <Form.Item name="dsQuanHeGiaDinhVeBenVoChong">*/}
          {/*    <QuanHeGiaDinhVeBenVoChong isViewMode={disabled} form={form} dataHoSo={dataHoSo}/>*/}
          {/*  </Form.Item>*/}
          {/*</Collapse.Panel>*/}
        </Collapse>
        <Row gutter={[12, 0]} style={{ marginTop: 12 }}>
          {/*<Col span={24}>*/}
          {/*  <Form.Item*/}
          {/*    name="kinhNghiemThanhTich"*/}
          {/*    label="Kinh nghiệm/Thành tích"*/}
          {/*  >*/}
          {/*    <TinyEditor />*/}
          {/*  </Form.Item>*/}
          {/*</Col>*/}

          {dataHoSo ? (
            <Col span={24}>
              <div id={"giay-to"}>
                <Form.Item
                  // name="kinhNghiemThanhTich"
                  label="Danh sách giấy tờ nộp online"
                >
                  <TableStaticData
                    pagination={false}
                    data={dataHoSo?.taiLieuUngTuyen ?? []}
                    columns={columns2}
                  />
                </Form.Item>
              </div>
            </Col>
          ) : (
            <>
              {props?.dataViecLam &&
                props?.dataViecLam?.dotTuyenDung?.hoSoDuTuyen?.length > 0 && (
                  <Col span={24}>
                    <div id={"giay-to"}>
                      <Form.Item
                        // name="kinhNghiemThanhTich"
                        label="Danh sách giấy tờ nộp online"
                      >
                        <TableStaticData
                          pagination={false}
                          data={
                            props?.dataViecLam?.dotTuyenDung?.hoSoDuTuyen ?? []
                          }
                          columns={columns}
                        />
                      </Form.Item>
                    </div>
                  </Col>
                )}
            </>
          )}
        </Row>
      </Form>
    </div>
  );
};
export default ThongTinBieuMauForm;
