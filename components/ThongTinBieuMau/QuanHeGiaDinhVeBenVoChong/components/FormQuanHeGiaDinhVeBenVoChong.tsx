
import {
  Button,
  Card,
  Checkbox,
  Col,
  Form,
  Input,
  InputNumber,
  Row,
  Select,
} from "antd";
import { useEffect, useState } from "react";
import { EMoiQuanHeVoChong } from "../../../../utils/constant";
import rules from "../../../../utils/rules";
import CardForm from "../../../CardForm";
import { FormValues } from "../../KhenThuong";
import {resetFieldsForm} from "../../../../utils/util";

export type FormQuanHeGiaDinhVeBenVoChongValues =
  Required<FormValues>["dsQuanHeGiaDinhVeBenVoChong"][number] & {
    capQuanHeGiaDinhVeBenVoChong: string;
  };

export interface FormQuanHeGiaDinhVeBenVoChongProps {
  onFinish: (values: FormQuanHeGiaDinhVeBenVoChongValues) => void;
  onCancel: () => void;
  isEdit: boolean;
  isView: boolean;
  record?: FormQuanHeGiaDinhVeBenVoChongValues;
}

export const FormQuanHeGiaDinhVeBenVoChong = ({
  isEdit,
  isView,
  onCancel,
  onFinish,
  record,
}: FormQuanHeGiaDinhVeBenVoChongProps) => {
  const [form] = Form.useForm();

  const [maxDay, setMaxDay] = useState<number>(30);
  const [month, setMonth] = useState<number>();
  const [yearInput, setYearInput] = useState<number>();

  const thangNhuan = [1, 3, 5, 7, 8, 10, 12];

  const CreatArrayNumber = (num: number) => {
    const foo = [];
    for (let i = 1; i <= num; i++) {
      foo.push(i);
    }
    return foo;
  };
  const isNamNhuan = (year: number) => {
    return (
      (year % 4 === 0 && year % 100 !== 0 && year % 400 !== 0) ||
      (year % 100 === 0 && year % 400 === 0)
    );
  };

  useEffect(() => {
    if (record) {
      form.setFieldsValue(record);
    } else {
      form.resetFields();
    }
  }, [record]);

  return (
    <CardForm
      bordered={false}
      title={
        (isView ? "Chi tiết " : isEdit ? "Chỉnh sửa " : "Thêm mới ") +
        "mối quan hệ trong gia đình về bên vợ/chồng"
      }
    >
      <Form id={'FormQuanHeGiaDinh'} onFinish={onFinish} form={form} layout="vertical">
        <Row gutter={[12, 0]} style={{ marginBottom: 12 }}>
          <Col span={24}>
            <Form.Item
              name="hoVaTen"
              label="Họ và tên"
              rules={[...rules.required, ...rules.length(250)]}
            >
              <Input disabled={isView} placeholder="Họ và tên" />
            </Form.Item>
          </Col>
          <Col span={24}>
            <Row gutter={[6, 0]}>
              <Col span={24} md={8}>
                <Form.Item
                  name="namSinh"
                  label="Năm sinh"
                  rules={[...rules.number(2023, 1900), ...rules.required]}
                >
                  <InputNumber
                    disabled={isView}
                    placeholder="Năm"
                    style={{ width: "100%",height:36 }}
                    onChange={(e) => {
                      setYearInput(Number(e));
                      form.setFieldsValue({ ngaySinh: null });
                      if (isNamNhuan(Number(e)) && month === 2) {
                        setMaxDay(29);
                      } else if (!isNamNhuan(Number(e)) && month === 2) {
                        setMaxDay(28);
                      } else if (thangNhuan.find((item) => item === month)) {
                        setMaxDay(31);
                      } else setMaxDay(30);
                    }}
                  />
                </Form.Item>
              </Col>
              <Col span={24} md={8}>
                <Form.Item name="thangSinh" label="Tháng sinh">
                  <Select
                    allowClear
                    disabled={isView}
                    placeholder="Tháng"
                    options={CreatArrayNumber(12).map((item) => ({
                      key: item,
                      value: item,
                      label: item,
                    }))}
                    onChange={(e) => {
                      form.setFieldsValue({ ngaySinh: null });
                      setMonth(Number(e));
                      if (isNamNhuan(Number(yearInput)) && e === 2) {
                        setMaxDay(29);
                      } else if (!isNamNhuan(Number(yearInput)) && e === 2) {
                        setMaxDay(28);
                      } else if (thangNhuan.find((item) => item === e)) {
                        setMaxDay(31);
                      } else setMaxDay(30);
                    }}
                  />
                </Form.Item>
              </Col>
              <Col span={24} md={8}>
                <Form.Item name="ngaySinh" label="Ngày sinh">
                  <Select
                    allowClear
                    disabled={isView}
                    placeholder="Ngày"
                    options={CreatArrayNumber(Number(maxDay)).map((item) => ({
                      key: item,
                      value: item,
                      label: item,
                    }))}
                  />
                </Form.Item>
              </Col>
            </Row>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="moiQuanHe"
              label="Mối quan hệ"
              rules={[...rules.required]}
            >
              <Select
                allowClear
                disabled={isView}
                placeholder="Chọn mối quan hệ"
                showSearch
                options={Object.values(EMoiQuanHeVoChong).map((item) => ({
                  key: item,
                  value: item,
                  label: item,
                }))}
              />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="ngheNghiep"
              label="Nghề nghiệp"
              rules={[...rules.text, ...rules.length(250)]}
            >
              <Input disabled={isView} placeholder="Nghề nghiệp" />
            </Form.Item>
          </Col>
          <Col span={24}>
            <Form.Item
              name="noiCongTac"
              label="Nơi công tác"
              rules={[...rules.text, ...rules.length(250)]}
            >
              <Input disabled={isView} placeholder="Nơi công tác" />
            </Form.Item>
          </Col>
          <Col span={24}>
            <Form.Item name="noiDung" label="Nội dung" rules={[...rules.text]}>
              <Input.TextArea disabled={isView} placeholder="Nội dung" />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="nguoiPhuThuoc"
              valuePropName="checked"
              initialValue={false}
            >
              <Checkbox disabled={isView}>Người phụ thuộc</Checkbox>
            </Form.Item>
          </Col>
        </Row>

        {/*{!isView && (*/}
        {/*  <Form.Item style={{ textAlign: "center", marginTop: 24 }}>*/}
        {/*    <Button style={{ marginRight: 8 }} htmlType="submit" type="primary">*/}
        {/*      {!isEdit ? "Thêm mới " : "Lưu lại"}*/}
        {/*    </Button>*/}
        {/*    <Button onClick={onCancel}>Đóng</Button>*/}
        {/*  </Form.Item>*/}
        {/*)}*/}
        <div style={{ textAlign: 'center', marginTop: 24 }}>
          {!isView && (
            <Button form='FormQuanHeGiaDinh' style={{ marginRight: 8 }} htmlType='submit' type='primary'>
              {!isEdit ? 'Thêm mới ' : 'Lưu lại'}
            </Button>
          )}
          <Button
            onClick={() => {
              onCancel();
              resetFieldsForm(form);
            }}
          >
            Đóng
          </Button>
        </div>
      </Form>
    </CardForm>
  );
};
