import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { Button, Checkbox, Divider, Form, Popconfirm, Tooltip, type FormInstance } from "antd";
import { useWatch } from "antd/lib/form/Form";
import { useState } from "react";
import TableStaticData from "../../TableStaticData";
import { IColumn } from "../../TableStaticData/typing";
import { FormValues } from "../KhenThuong";
import {
  FormQuanHeGiaDinhVeBenVoChong,
  FormQuanHeGiaDinhVeBenVoChongProps,
  FormQuanHeGiaDinhVeBenVoChongValues,
} from "./components/FormQuanHeGiaDinhVeBenVoChong";
import { ThongTinNhanSu } from "../../../Interface/ThongTinNhanSu/typing";

interface Props {
  form: FormInstance<FormValues>;
  isViewMode?: boolean;
  dataHoSo?: ThongTinNhanSu.IRecord;
}

type TableRecord = FormQuanHeGiaDinhVeBenVoChongValues;
export const QuanHeGiaDinhVeBenVoChong = ({
  form,
  isViewMode,
  dataHoSo,
}: Props) => {
  const dataDanhSach = useWatch(["dsQuanHeGiaDinhVeBenVoChong"], form);
  const dsQuanHeGiaDinhVeBenVoChong =
    dataHoSo?.danhSachQuanHeGiaDinhVeBenVoChong??dataDanhSach;

  const [visibleForm, setVisibleForm] = useState(false);
  const [record, setRecord] = useState<FormQuanHeGiaDinhVeBenVoChongValues>();
  const [isEdit, setIsEdit] = useState(false);
  const [isView, setIsView] = useState(false);
  const [indexRecordDangThaoTac, setIndexRecordDangThaoTac] = useState<
    number | undefined
  >(undefined);

  const handleCreate = (vi: boolean) => {
    setVisibleForm(vi);
    setIsEdit(false);
    setIsView(false);
    setRecord(undefined);
  };

  const handleEdit = (tableRecord: TableRecord, index: number) => {
    setVisibleForm(true);
    setIsEdit(true);
    setIsView(false);
    setRecord(tableRecord);
    setIndexRecordDangThaoTac(index);
  };

  const handleView = (tableRecord: TableRecord) => {
    setVisibleForm(true);
    setIsView(true);
    setIsEdit(false);
    setRecord(tableRecord);
  };

  const handleCloseModal = () => {
    setRecord(undefined);
    setVisibleForm(false);
    setIsEdit(false);
    setIsView(false);
  };

  const handleDelete = (index: number) => {
    const prevDsQuanHeGiaDinhVeBenVoChong: Required<FormValues>["dsQuanHeGiaDinhVeBenVoChong"] =
      form.getFieldValue("dsQuanHeGiaDinhVeBenVoChong") ?? [];
    form.setFieldsValue({
      dsQuanHeGiaDinhVeBenVoChong: prevDsQuanHeGiaDinhVeBenVoChong.filter(
        (_, QuanHeGiaDinhVeBenVoChongIndex) =>
          QuanHeGiaDinhVeBenVoChongIndex !== index
      ),
    });
  };

  const handleSubmitForm = (values: FormQuanHeGiaDinhVeBenVoChongValues) => {
    const prevDsQuanHeGiaDinhVeBenVoChong: Required<FormValues>["dsQuanHeGiaDinhVeBenVoChong"] =
      form.getFieldValue("dsQuanHeGiaDinhVeBenVoChong") ?? [];

    if (!isEdit) {
      form.setFieldsValue({
        dsQuanHeGiaDinhVeBenVoChong:
          prevDsQuanHeGiaDinhVeBenVoChong.concat(values),
      });
    } else if (isEdit) {
      form.setFieldsValue({
        dsQuanHeGiaDinhVeBenVoChong: prevDsQuanHeGiaDinhVeBenVoChong.map(
          (thanhVien, index) => {
            if (indexRecordDangThaoTac === index) {
              return {
                ...thanhVien,
                ...values,
              };
            }
            return thanhVien;
          }
        ),
      });
    }

    setIndexRecordDangThaoTac(undefined);
    setVisibleForm(false);
    setIsEdit(false);
    setIsView(false);
  };

  const onCell = (rec: TableRecord) => ({
    onClick: () => handleView(rec),
    style: { cursor: "pointer" },
  });
  const columns: IColumn<TableRecord>[] = [
    {
      title: "Họ và tên",
      dataIndex: "hoVaTen",
      width: 120,
      filterType: "string",
      sortable: true,
      onCell,
    },
    {
      title: "Mối quan hệ",
      dataIndex: "moiQuanHe",
      width: 140,
      sortable: true,
      onCell,
    },
    {
      title: "Năm sinh",
      width: 100,
      dataIndex: "namSinh",
      sortable: true,
      onCell,
    },
    {
      title: "Là người phụ thuộc",
      dataIndex: "nguoiPhuThuoc",
      width: 100,
      align: "center",
      onCell,
      render: (val) => val && <Checkbox checked={val} />,
    },
    {
      title: "Nội dung",
      width: 200,
      dataIndex: "noiDung",
      filterType: "string",
      onCell,
    },
  ];
  if (!isViewMode) {
    columns.push({
      title: "Thao tác",
      align: "center",
      width: 90,
      fixed: "right",
      render: (_, rec, index) => {
        return (
          <>
            <Tooltip title="Chỉnh sửa">
              <Button
                type="link"
                icon={<EditOutlined rev={undefined} />}
                onClick={() => handleEdit(rec, index)}
              />
            </Tooltip>

            <Tooltip title="Xóa">
              <Popconfirm
                onConfirm={() => handleDelete(index)}
                title="Bạn có chắc chắn muốn xóa quan hệ gia đình về bản thân này?"
                placement="topLeft"
              >
                <Button
                  danger
                  type="link"
                  icon={<DeleteOutlined rev={undefined} />}
                />
              </Popconfirm>
            </Tooltip>
          </>
        );
      },
    });
  }

  return (
    <Form.Item name='dsQuanHeGiaDinhVeBenVoChong'>
      <Divider orientation='center'>Mối quan hệ trong gia đình về bên vợ chồng</Divider>
      <TableStaticData
        hasTotal
        widthDrawer={800}
        hasCreate={!isViewMode}
        showEdit={visibleForm}
        setShowEdit={handleCreate}
        data={dsQuanHeGiaDinhVeBenVoChong ?? []}
        columns={columns}
        addStt
        Form={FormQuanHeGiaDinhVeBenVoChong}
        formProps={
          {
            record,
            isEdit,
            isView,
            onCancel: handleCloseModal,
            onFinish: handleSubmitForm,
          } as FormQuanHeGiaDinhVeBenVoChongProps
        }
      />
    </Form.Item>
  );
};
