import { Select } from "antd";
import { useEffect, useState } from "react";
import { getDanToc } from "../../../../../service/core/dantoc";
import {getLoaiKhenThuong} from "../../../../../service/khenthuong/loaikhenthuong";

const SelectLoaiKhenThuong = (props: {
  value?: string;
  onChange?: any;
  multiple?: boolean;
  listLoaiKhenThuongId?: string[];
  disabled?: boolean;
  allowClear?: boolean;
}) => {
  const {
    value,
    onChange,
    multiple,
    listLoaiKhenThuongId,
    disabled,
    allowClear,
  } = props;
  const [danhSach, setDanhSach] = useState<KhenThuong.LoaiKhenThuong[]>([]);
  const getData = async () => {
    try {
      const res = await getLoaiKhenThuong();
      setDanhSach(res?.data?.data);
    } catch (e) {}
  };

  useEffect(() => {
    getData();
  }, []);
  return (
    <div style={{ display: "flex", gap: 8 }}>
      <div className={"fullWidth"}>
        <Select
          allowClear={allowClear}
          disabled={disabled}
          mode={multiple ? "multiple" : undefined}
          value={value}
          onChange={onChange}
          options={danhSach
            .filter(
              (item) =>
                !listLoaiKhenThuongId ||
                listLoaiKhenThuongId?.includes(item._id)
            )
            .map((item) => ({
              key: item._id,
              value: item._id,
              label: item.ten,
            }))}
          showSearch
          optionFilterProp="label"
          placeholder="Chọn loại khen thưởng"
        />
      </div>
    </div>
  );
};

export default SelectLoaiKhenThuong;
