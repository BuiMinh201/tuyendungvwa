import { Select } from "antd";
import { useEffect, useState } from "react";
import { getDanToc } from "../../../../../service/core/dantoc";
import {getHinhThucKhenThuong} from "../../../../../service/khenthuong/hinhthuckhenthuong";

const SelectHinhThucKhenThuong = (props: {
  value?: string;
  onChange?: any;
  multiple?: boolean;
  listHinhThucKhenThuong?: string[];
  disable?: boolean;
  allowClear?: boolean;
}) => {
  const {
    value,
    onChange,
    multiple,
    listHinhThucKhenThuong,
    disable,
    allowClear,
  } = props;
  const [danhSach, setDanhSach] = useState<KhenThuong.HinhThucKhenThuong[]>([]);
  const getData = async () => {
    try {
      const res = await getHinhThucKhenThuong();
      setDanhSach(res?.data?.data);
    } catch (e) {}
  };

  useEffect(() => {
    getData();
  }, []);
  return (
    <div style={{ display: "flex", gap: 8 }}>
      <div className={"fullWidth"}>
        <Select
          allowClear={allowClear}
          mode={multiple ? "multiple" : undefined}
          value={value}
          onChange={onChange}
          options={danhSach
            .filter(
              (item) =>
                !listHinhThucKhenThuong ||
                listHinhThucKhenThuong?.includes(item._id)
            )
            .map((item) => ({
              key: item._id,
              value: item._id,
              label: item.ten,
            }))}
          showSearch
          disabled={disable}
          optionFilterProp="label"
          placeholder="Chọn hình thức khen thưởng"
        />
      </div>
    </div>
  );
};

export default SelectHinhThucKhenThuong;
