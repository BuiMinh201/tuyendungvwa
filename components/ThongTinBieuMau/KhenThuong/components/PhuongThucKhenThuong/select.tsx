import { Select } from 'antd';
import {useEffect, useState} from 'react';
import {getDanToc} from "../../../../../service/core/dantoc";
import {getPhuongThucKhenThuong} from "../../../../../service/khenthuong/phuongthuckhenthuong";

const SelectPhuongThucKhenThuong = (props: {
  value?: string;
  onChange?: any;
  multiple?: boolean;
  disabled?: boolean;
  allowClear?: boolean;
}) => {
  const { value, onChange, multiple, disabled, allowClear } = props;
  const [danhSach, setDanhSach] = useState<KhenThuong.PhuongThucKhenThuong[]>([]);
  const getData = async () => {
    try {
      const res = await getPhuongThucKhenThuong();
      setDanhSach(res?.data?.data);
    } catch (e) {}
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div style={{ display: 'flex', gap: 8 }}>
      <div className={'fullWidth'}>
        <Select
          allowClear={allowClear}
          disabled={disabled}
          mode={multiple ? 'multiple' : undefined}
          value={value}
          onChange={onChange}
          options={danhSach.map((item) => ({
            key: item._id,
            value: item._id,
            label: item.ten,
          }))}
          showSearch
          optionFilterProp='label'
          placeholder='Chọn phương thức khen thưởng'
        />
      </div>
    </div>
  );
};

export default SelectPhuongThucKhenThuong;
