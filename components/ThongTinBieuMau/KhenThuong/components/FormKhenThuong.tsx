import { Button, Card, Col, Form, Input, InputNumber, Row } from "antd";
import { useEffect } from "react";
import rules from "../../../../utils/rules";
import CardForm from "../../../CardForm";
import MyDatePicker from "../../../MyDatePicker";
import UploadFile from "../../../Upload/UploadFile";
import { FormValues } from "../index";
import SelectCapKhenThuong from "./CapKhenThuong/select";
import SelectHinhThucKhenThuong from "./HinhThucKhenThuong/select";
import SelectLoaiKhenThuong from "./LoaiKhenThuong/select";
import SelectPhuongThucKhenThuong from "./PhuongThucKhenThuong/select";
import {resetFieldsForm} from "../../../../utils/util";

export type FormKhenThuongValues =
  Required<FormValues>["dsKhenThuong"][number] & {
    capKhenThuong: string;
  };

export interface FormKhenThuongProps {
  onFinish: (values: FormKhenThuongValues) => void;
  onCancel: () => void;
  isEdit: boolean;
  isView: boolean;
  record?: FormKhenThuongValues;
}

export const FormKhenThuong = ({
  isEdit,
  isView,
  onCancel,
  onFinish,
  record,
}: FormKhenThuongProps) => {
  const [form] = Form.useForm<FormKhenThuongValues>();

  // const { setCondition, condition } = useModel('danhmuc.hinhthuckhenthuong');

  useEffect(() => {
    if (record) {
      form.setFieldsValue(record);
    } else {
      form.resetFields();
    }
  }, [record]);

  return (
    <CardForm
      bordered={false}
      title={
        (isView ? "Xem chi tiết " : isEdit ? "Chỉnh sửa " : "Thêm mới ") +
        "khen thưởng"
      }
    >
      <Form id={'FormKhenThuong'} onFinish={onFinish} form={form} layout="vertical" disabled={isView}>
        <Form.Item hidden name="idQuyetDinhKhenThuong" />
        <Form.Item hidden name="capKhenThuong" />
        <Row gutter={[12, 0]} style={{ marginBottom: 12 }}>
          <Col span={24}>
            <Form.Item
              name="soQuyetDinh"
              label="Số quyết định"
              rules={[...rules.required, ...rules.text, ...rules.length(255)]}
            >
              <Input placeholder={"Số quyết định"} />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="coQuanQuyetDinh"
              label="Cơ quan quyết định"
              rules={[...rules.text, ...rules.length(250)]}
            >
              <Input placeholder="Cơ quan quyết định" />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="ngayQuyetDinh"
              label="Ngày quyết định"
              rules={[...rules.required, ...rules.nhoHonBangHomNay]}
            >
              <MyDatePicker placeholder="Ngày quyết định" allowClear />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="nguoiKy"
              label="Người ký"
              rules={[...rules.text, ...rules.length(250)]}
            >
              <Input placeholder="Người ký" />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="ngayKy"
              label="Ngày ký"
              rules={[...rules.nhoHonBangHomNay]}
            >
              <MyDatePicker placeholder="Ngày ký" allowClear />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="capKhenThuongId"
              label="Cấp khen thưởng"
              rules={[...rules.required]}
            >
              <SelectCapKhenThuong />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="loaiKhenThuongId"
              label="Loại khen thưởng"
              rules={[...rules.required]}
            >
              <SelectLoaiKhenThuong
                onChange={() => {
                  form.setFieldsValue({
                    hinhThucKhenThuong: null,
                    hinhThucKhenThuongId: null,
                    anhHuongThoiGianKhenThuong: null,
                    thoiGianDieuChinh: null,
                  });
                }}
              />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="phuongThucKhenThuongId"
              label="Phương thức khen thưởng"
              rules={[...rules.required]}
            >
              <SelectPhuongThucKhenThuong />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="hinhThucKhenThuongId"
              label="Hình thức khen thưởng"
              rules={[...rules.required]}
            >
              <SelectHinhThucKhenThuong />
            </Form.Item>
          </Col>

          {/*<Col*/}
          {/*  span={24}*/}
          {/*  md={12}*/}
          {/*  style={{ display: anhHuongThoiGianKhenThuong ? 'flex' : 'none', alignItems: 'center' }}*/}
          {/*>*/}
          {/*  <Form.Item name='daXetDieuChinhTangLuong' valuePropName='checked' initialValue={false}>*/}
          {/*    <Checkbox disabled={isView}>Đã xét điều chỉnh lương</Checkbox>*/}
          {/*  </Form.Item>*/}
          {/*</Col>*/}
          <Col span={24} md={24}>
            <Form.Item
              name="noiDung"
              label="Nội dung"
              rules={[...rules.text, ...rules.length(550)]}
            >
              <Input.TextArea placeholder="Nội dung" disabled={isView} />
            </Form.Item>
          </Col>
          <Col span={24}>
            <Form.Item
              name="urlFileUpload"
              //  rules={[...rules.fileRequired]}
              label="File đính kèm"
            >
              <UploadFile
                otherProps={{
                  maxCount: 1,
                  multiple: true,
                  // showUploadList: { showDownloadIcon: false },
                }}
              />
            </Form.Item>
          </Col>
        </Row>

        {/*{!isView && (*/}
        {/*  <Form.Item style={{ textAlign: "center", marginTop: 24 }}>*/}
        {/*    <Button style={{ marginRight: 8 }} htmlType="submit" type="primary">*/}
        {/*      {!isEdit ? "Thêm mới " : "Lưu lại"}*/}
        {/*    </Button>*/}

        {/*    <Button onClick={onCancel}>Đóng</Button>*/}
        {/*  </Form.Item>*/}
        {/*)}*/}
        <div style={{ textAlign: "center", marginTop: 24 }}>
          {!isView && (
            <Button
              form="FormKhenThuong"
              style={{ marginRight: 8 }}
              htmlType="submit"
              type="primary"
            >
              {!isEdit ? "Thêm mới " : "Lưu lại"}
            </Button>
          )}
          <Button
            onClick={() => {
              onCancel();
              resetFieldsForm(form);
            }}
          >
            Đóng
          </Button>
        </div>
      </Form>
    </CardForm>
  );
};
