import { Select } from "antd";
import { type BaseOptionType } from "antd/lib/select";
import { useEffect, useState } from "react";
import { getDanToc } from "../../../../../service/core/dantoc";
import { getCapKhenThuong } from "../../../../../service/khenthuong/capkhenthuong";

const SelectCapKhenThuong = (props: {
  value?: string;
  onChange?: (value: string, option: BaseOptionType) => void;
  multiple?: boolean;
  disabled?: boolean;
  allowClear?: boolean;
  loadData?: boolean;
}) => {
  const { value, onChange, multiple, disabled, allowClear, loadData } = props;
  const [danhSach, setDanhSach] = useState<KhenThuong.CapKhenThuong[]>([]);
  const getData = async () => {
    try {
      const res = await getCapKhenThuong();
      setDanhSach(res?.data?.data);
    } catch (e) {}
  };

  useEffect(() => {
    if (loadData !== false) getData();
  }, []);

  return (
    <div style={{ display: "flex", gap: 8 }}>
      <div className={"fullWidth"}>
        <Select
          allowClear={allowClear}
          disabled={disabled}
          mode={multiple ? "multiple" : undefined}
          value={value}
          onChange={onChange}
          options={danhSach.map((item) => ({
            key: item._id,
            value: item._id,
            label: item.ten,
            rawData: item,
          }))}
          showSearch
          optionFilterProp="label"
          placeholder="Chọn cấp khen thưởng"
        />
      </div>
    </div>
  );
};

export default SelectCapKhenThuong;
