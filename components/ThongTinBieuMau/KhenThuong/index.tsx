import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import {
  Button,
  Form,
  Popconfirm,
  Tooltip,
  type FormInstance,
  Divider,
} from "antd";
import { useWatch } from "antd/lib/form/Form";
import { useState } from "react";
import moment from "moment";
import { ThongTinNhanSu } from "../../../Interface/ThongTinNhanSu/typing";
import { IColumn } from "../../TableStaticData/typing";
import TableStaticData from "../../TableStaticData";
import { FormKhenThuong } from "./components/FormKhenThuong";
import {buildUpLoadFile} from "../../../utils/uploadFile";
export type FormValues = ThongTinNhanSu.IRecord;

interface Props {
  form: FormInstance<FormValues>;
  isViewMode?: boolean;
}
export type FormKhenThuongValues =
  Required<FormValues>["dsKhenThuong"][number] & {
    capKhenThuong: string;
  };
type TableRecord = FormKhenThuongValues;

export interface FormKhenThuongProps {
  onFinish: (values: FormKhenThuongValues) => void;
  onCancel: () => void;
  isEdit: boolean;
  isView: boolean;
  record?: FormKhenThuongValues;
}

export const KhenThuong = ({ form, isViewMode }: Props) => {
  const dsKhenThuong = useWatch(["dsKhenThuong"], form);

  const [visibleForm, setVisibleForm] = useState(false);
  const [record, setRecord] = useState<FormKhenThuongValues>();
  const [isEdit, setIsEdit] = useState(false);
  const [isView, setIsView] = useState(false);
  const [indexRecordDangThaoTac, setIndexRecordDangThaoTac] = useState<
    number | undefined
  >(undefined);

  const handleCreate = (vi: boolean) => {
    setVisibleForm(vi);
    setIsEdit(false);
    setIsView(false);
    setRecord(undefined);
  };

  const handleEdit = (tableRecord: any, index: number) => {
    setVisibleForm(true);
    setIsEdit(true);
    setIsView(false);
    setRecord(tableRecord);
    setIndexRecordDangThaoTac(index);
  };

  const handleView = (tableRecord: any) => {
    setVisibleForm(true);
    setIsView(true);
    setIsEdit(false);
    setRecord(tableRecord);
  };

  const handleCloseModal = () => {
    setRecord(undefined);
    setVisibleForm(false);
    setIsEdit(false);
    setIsView(false);
  };

  const handleDelete = (index: number) => {
    const prevDsKhenThuong: Required<FormValues>["dsKhenThuong"] =
      form.getFieldValue("dsKhenThuong") ?? [];
    form.setFieldsValue({
      dsKhenThuong: prevDsKhenThuong.filter(
        (_, khenThuongIndex) => khenThuongIndex !== index
      ),
    });
  };

  const handleSubmitForm = async (values: FormKhenThuongValues) => {
    const prevDsKhenThuong: Required<FormValues>["dsKhenThuong"] =
      form.getFieldValue("dsKhenThuong") ?? [];
    const url = await buildUpLoadFile(values, "fileDinhKem");
    const payload = {
      ...values,
      fileDinhKem: url,
    };
    if (!isEdit) {
      form.setFieldsValue({
        dsKhenThuong: prevDsKhenThuong.concat(payload),
      });
    } else if (isEdit) {
      form.setFieldsValue({
        dsKhenThuong: prevDsKhenThuong.map(
          (thanhVien: any, index: number | undefined) => {
            if (indexRecordDangThaoTac === index) {
              return {
                ...thanhVien,
                ...payload,
              };
            }
            return thanhVien;
          }
        ),
      });
    }

    setIndexRecordDangThaoTac(undefined);
    setVisibleForm(false);
    setIsEdit(false);
    setIsView(false);
  };

  const onCell = (rec: TableRecord) => ({
    onClick: () => handleView(rec),
    style: { cursor: "pointer" },
  });
  const columns: IColumn<TableRecord>[] = [
    {
      title: "Cơ quan quyết định",
      dataIndex: "coQuanQuyetDinh",
      width: 120,
      filterType: "string",
      onCell,
    },
    {
      title: "Cấp khen thưởng",
      dataIndex: ["capKhenThuong",'ten'],
      width: 120,
      filterType: "string",
      onCell,
    },
    {
      title: "Số quyết định",
      dataIndex: "soQuyetDinh",
      width: 120,
      filterType: "string",
      onCell,
    },
    {
      title: "Người ký",
      dataIndex: "nguoiKy",
      width: 120,
      filterType: "string",
      onCell,
    },
    {
      title: "Ngày quyết định",
      dataIndex: "ngayQuyetDinh",
      width: 120,
      onCell,
      render: (value: moment.MomentInput) => {
        if (value) {
          return moment(value).format("DD/MM/YYYY HH:mm");
        }
        return null;
      },
    },
  ];

  if (!isViewMode) {
    columns.push({
      title: "Thao tác",
      align: "center",
      width: 90,
      fixed: "right",
      render: (_: any, rec: any, index: number) => {
        return (
          <>
            <Tooltip title="Chỉnh sửa">
              <Button
                type="link"
                icon={<EditOutlined rev={undefined} />}
                onClick={() => handleEdit(rec, index)}
              />
            </Tooltip>

            <Tooltip title="Xóa">
              <Popconfirm
                onConfirm={() => handleDelete(index)}
                title="Bạn có chắc chắn muốn xóa khen thưởng này?"
                placement="topLeft"
              >
                <Button
                  danger
                  type="link"
                  icon={<DeleteOutlined rev={undefined} />}
                />
              </Popconfirm>
            </Tooltip>
          </>
        );
      },
    });
  }

  return (
    <Form.Item name="dsKhenThuong">
      <Divider orientation="center">Khen thưởng</Divider>
      <TableStaticData
        hasCreate={!isViewMode}
        hasTotal
        showEdit={visibleForm}
        setShowEdit={handleCreate}
        data={dsKhenThuong ?? []}
        columns={columns}
        addStt
        Form={FormKhenThuong}
        widthDrawer={700}
        formProps={
          {
            record,
            isEdit,
            isView,
            onCancel: handleCloseModal,
            onFinish: handleSubmitForm,
          } as FormKhenThuongProps
        }
      />
    </Form.Item>
  );
};
