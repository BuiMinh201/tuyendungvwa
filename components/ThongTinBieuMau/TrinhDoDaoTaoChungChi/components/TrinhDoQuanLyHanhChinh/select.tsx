import { PlusOutlined } from "@ant-design/icons";
import { Button, Modal, Select } from "antd";
import { useEffect, useState } from "react";
import { type BaseOptionType } from "antd/lib/select";
import {
  getTinHoc,
  getTrinhDoQuanLy,
  getTrinhDoQuanLyNhaNuoc,
} from "../../../../../service/core/core";

const SelectTrinhDoQuanLyHanhChinh = (props: {
  value?: string;
  onChange?: (value?: string, option?: BaseOptionType) => void;
  hasCreate?: boolean;
  multiple?: boolean;
  allowClear?: boolean;
  disabled?: boolean;
  placeholder?: string;
}) => {
  const {
    value,
    onChange,
    hasCreate,
    multiple,
    allowClear,
    disabled,
    placeholder,
  } = props;
  const [danhSach, setDanhSach] = useState<TrinhDoQuanLyNhaNuoc.IRecord[]>([]);
  const getData = async () => {
    try {
      const res = await getTrinhDoQuanLy();
      setDanhSach(res?.data?.data);
    } catch (e) {}
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <Select
      mode={multiple ? "multiple" : undefined}
      value={value}
      onChange={onChange}
      options={danhSach.map((item) => ({
        key: item._id,
        value: item._id,
        label: item.ten,
        rawData: item,
      }))}
      disabled={disabled}
      showSearch
      optionFilterProp="label"
      placeholder={placeholder ?? "Chọn trình độ quản lý hành chính"}
      allowClear={allowClear}
    />
  );
};

export default SelectTrinhDoQuanLyHanhChinh;
