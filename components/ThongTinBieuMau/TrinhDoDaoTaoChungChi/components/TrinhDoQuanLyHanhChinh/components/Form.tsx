import { Button, Card, Checkbox, Col, Form, Input, Row } from "antd";
import { useWatch } from "antd/lib/form/Form";
import { useEffect } from "react";
import rules from "../../../../../../utils/rules";
import { resetFieldsForm } from "../../../../../../utils/util";
import CardForm from "../../../../../CardForm";
import MyDatePicker from "../../../../../MyDatePicker";
import UploadFile from "../../../../../Upload/UploadFile";
import { FormValues } from "../../../../KhenThuong";
import SelectHinhThucDaoTao from "../../HinhThucDaoTao/select";
import SelectTrinhDoQuanLyHanhChinh from "../select";

export type FormTrinhDoQuanLyHanhChinhValues =
  Required<FormValues>["dsThongTinTrinhDoQuanLyHanhChinh"][number];

export interface FormTrinhDoQuanLyHanhChinhProps {
  onFinish: (values: FormTrinhDoQuanLyHanhChinhValues) => void;
  onCancel: () => void;
  isEdit: boolean;
  isView: boolean;
  record?: FormTrinhDoQuanLyHanhChinhValues;
  ngaySinh?: string;
}

export const FormTrinhDoQuanLyHanhChinh = ({
  isEdit,
  isView,
  onCancel,
  onFinish,
  record,
  ngaySinh,
}: FormTrinhDoQuanLyHanhChinhProps) => {
  const [form] = Form.useForm<FormTrinhDoQuanLyHanhChinhValues>();

  const thoiGianBatDau = useWatch(["thoiGianBatDau"], form);

  useEffect(() => {
    resetFieldsForm(form, record);
  }, [isEdit, isView, record]);

  return (
    <CardForm bordered={false}
      title={
        (isView ? "Xem chi tiết " : isEdit ? "Chỉnh sửa " : "Thêm mới ") +
        "trình độ quản lý hành chính"
      }
    >
      <Form
        id="FormTrinhDoQuanLyHanhChinh"
        onFinish={onFinish}
        form={form}
        layout="vertical"
        disabled={isView}
      >
        <Form.Item name="hinhThucDaoTao" hidden />
        <Form.Item name="trinhDoQuanLyHanhChinh" hidden />
        <Row gutter={[12, 0]} style={{ marginBottom: 12 }}>
          <Col span={24} md={12}>
            <Form.Item
              name="thoiGianBatDau"
              // rules={[...rules.required]}
              label="Thời gian bắt đầu"
            >
              <MyDatePicker placeholder="Thời gian bắt đầu" />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="thoiGianKetThuc"
              label="Thời gian kết thúc"
              rules={[
                // ...rules.required,
                ...rules.sauNgay(thoiGianBatDau, "thời gian bắt đầu"),
              ]}
            >
              <MyDatePicker placeholder="Thời gian kết thúc" />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="hinhThucDaoTaoId"
              label="Hình thức đào tạo"
              rules={[...rules.required]}
            >
              <SelectHinhThucDaoTao
                hasCreate={false}
                placeholder="Hình thức đào tạo"
                onChange={(_, option) => {
                  const rawData = option?.rawData as
                    | HinhThucDaoTao.IRecord
                    | undefined;
                  form.setFieldsValue({
                    hinhThucDaoTao: rawData ?? null,
                  });
                }}
              />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="trinhDoQuanLyHanhChinhId"
              label="Trình độ quản lý hành chính"
              rules={[...rules.required]}
            >
              <SelectTrinhDoQuanLyHanhChinh
                hasCreate={false}
                placeholder="Trình độ quản lý hành chính"
                onChange={(_, option) => {
                  const rawData = option?.rawData as
                    | TrinhDoQuanLyHanhChinh.IRecord
                    | undefined;
                  form.setFieldsValue({
                    trinhDoQuanLyHanhChinh: rawData ?? null,
                  });
                }}
              />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="vanBangChungChi"
              label="Văn bằng/Chứng chỉ/Chứng nhận được cấp"
            >
              <Input placeholder="Văn bằng/Chứng chỉ/Chứng nhận được cấp" />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="coSoDaoTao"
              rules={[...rules.required, ...rules.text, ...rules.length(255)]}
              label="Cơ sở đào tạo"
            >
              <Input placeholder="Cơ sở đào tạo" />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="fileDinhKem"
              label="File đính kèm"
              // rules={[...rules.fileRequired]}
            >
              <UploadFile
                maxCount={1}
                otherProps={{
                  maxCount: 1,
                  multiple: false,
                }}
              />
            </Form.Item>
          </Col>
          <Col
            span={24}
            md={12}
            style={{ display: "flex", alignItems: "center" }}
          >
            <Form.Item
              name="apDung"
              valuePropName="checked"
              initialValue={false}
            >
              <Checkbox disabled={isView}>Là trình độ chính</Checkbox>
            </Form.Item>
          </Col>
        </Row>
      </Form>

      <div style={{ textAlign: "center", marginTop: 24 }}>
        {!isView && (
          <Button
            form="FormTrinhDoQuanLyHanhChinh"
            style={{ marginRight: 8 }}
            htmlType="submit"
            type="primary"
          >
            {!isEdit ? "Thêm mới " : "Lưu lại"}
          </Button>
        )}
        <Button
          disabled={false}
          onClick={() => {
            onCancel();
            resetFieldsForm(form);
          }}
        >
          Đóng
        </Button>
      </div>
    </CardForm>
  );
};
