
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { Button, Popconfirm, Tooltip, type FormInstance, Form, Divider, Checkbox } from 'antd';
import { useWatch } from 'antd/lib/form/Form';
import { useState } from 'react';
import { FormNgoaiNgu, type FormNgoaiNguProps, type FormNgoaiNguValues } from './components/Form';
import moment from 'moment';
import { FormValues } from '../../../KhenThuong';
import { IColumn } from '../../../../TableStaticData/typing';
import TableStaticData from '../../../../TableStaticData';
import {buildUpLoadFile} from "../../../../../utils/uploadFile";

interface Props {
	form: FormInstance<FormValues>;
	disabled: boolean;
}

type TableRecord = FormNgoaiNguValues;
export const NgoaiNgu = ({ form, disabled }: Props) => {
	const dsThongTinNgoaiNgu = useWatch(['dsThongTinNgoaiNgu'], form);

	const [visibleForm, setVisibleForm] = useState(false);
	const [record, setRecord] = useState<FormNgoaiNguValues>();
	const [isEdit, setIsEdit] = useState(false);
	const [isView, setIsView] = useState(false);
	const [indexRecordDangThaoTac, setIndexRecordDangThaoTac] = useState<number | undefined>(undefined);

	const handleCreate = (vi: boolean) => {
		setVisibleForm(vi);
		setIsEdit(false);
		setIsView(false);
		setRecord(undefined);
	};

	const handleEdit = (tableRecord: TableRecord, index: number) => {
		setVisibleForm(true);
		setIsEdit(true);
		setIsView(false);
		setRecord(tableRecord);
		setIndexRecordDangThaoTac(index);
	};

	const handleView = (tableRecord: TableRecord) => {
		setVisibleForm(true);
		setIsView(true);
		setIsEdit(false);
		setRecord(tableRecord);
	};

	const handleCloseModal = () => {
		setRecord(undefined);
		setVisibleForm(false);
		setIsEdit(false);
		setIsView(false);
	};

	const handleDelete = (index: number) => {
		const prevDsNgoaiNgu: Required<FormValues>['dsThongTinNgoaiNgu'] = form.getFieldValue('dsThongTinNgoaiNgu') ?? [];
		form.setFieldsValue({
			dsThongTinNgoaiNgu: prevDsNgoaiNgu.filter((_, itemIndex) => itemIndex !== index),
		});
	};

	const handleSubmitForm = async (values: FormNgoaiNguValues) => {
		const prevDsNgoaiNgu_: Required<FormValues>['dsThongTinNgoaiNgu'] = form.getFieldValue('dsThongTinNgoaiNgu') ?? [];

		const prevDsNgoaiNgu = values.apDung
			? prevDsNgoaiNgu_.map((item) => ({ ...item, apDung: false }))
			: prevDsNgoaiNgu_;
		const url = await buildUpLoadFile(values, "fileDinhKem");
		const payload = {
			...values,
			fileDinhKem: url,
		};
		if (!isEdit) {
			form.setFieldsValue({
				dsThongTinNgoaiNgu: prevDsNgoaiNgu.prepend(payload),
			});
		} else if (isEdit) {
			form.setFieldsValue({
				dsThongTinNgoaiNgu: prevDsNgoaiNgu.map((item, index) => {
					if (indexRecordDangThaoTac === index) {
						return {
							...item,
							...payload,
						};
					}
					return item;
				}),
			});
		}

		setIndexRecordDangThaoTac(undefined);
		setVisibleForm(false);
		setIsEdit(false);
		setIsView(false);
	};

	const onCell = (rec: TableRecord) => ({
		onClick: () => handleView(rec),
		style: { cursor: 'pointer' },
	});
	const columns: IColumn<TableRecord>[] = [
		{
			title: 'Hình thức đào tạo',
			dataIndex: ['hinhThucDaoTao', 'ten'],
			width: 160,
			onCell,
			filterType: 'string',
		},
		{
			title: 'Ngoại ngữ',
			dataIndex: ['ngoaiNgu', 'tenNgonNgu'],
			width: 160,
			onCell,
			filterType: 'string',
		},
		{
			title: 'Khung năng lực ngoại ngữ',
			dataIndex: ['khungNangLucNgoaiNgu', 'khungNangLuc'],
			width: 160,
			onCell,
			filterType: 'string',
		},
		{
			title: 'Cơ sở đào tạo',
			dataIndex: 'coSoDaoTao',
			width: 160,
			onCell,
			filterType: 'string',
		},
		{
			title: 'Thời gian bắt đầu',
			dataIndex: 'thoiGianBatDau',
			width: 140,
			align: 'center',
			onCell,
			sortable: true,
			filterType: 'date',
			render: (_, tableRecord) => {
				return tableRecord.thoiGianBatDau ? moment(tableRecord.thoiGianBatDau).format('DD/MM/YYYY') : null;
			},
		},
		{
			title: 'Thời gian kết thúc',
			dataIndex: 'thoiGianKetThuc',
			width: 140,
			align: 'center',
			onCell,
			sortable: true,
			filterType: 'date',
			render: (_, tableRecord) => {
				return tableRecord.thoiGianKetThuc ? moment(tableRecord.thoiGianKetThuc).format('DD/MM/YYYY') : null;
			},
		},
		{
			title: 'Là trình độ chính',
			width: 120,
			dataIndex: 'apDung',
			align: 'center',
			render: (value) => (value ? <Checkbox disabled checked={value} /> : null),
		},
	];

	if (!disabled) {
		columns.push({
			title: 'Thao tác',
			align: 'center',
			width: 90,
			fixed: 'right',
			render: (_, rec, index) => {
				return (
					<>
						<Tooltip title='Chỉnh sửa'>
							<Button type='link' icon={<EditOutlined rev={undefined} />} onClick={() => handleEdit(rec, index)} />
						</Tooltip>

						<Tooltip title='Xóa'>
							<Popconfirm
								onConfirm={() => handleDelete(index)}
								title='Bạn có chắc chắn muốn xóa ngoại ngữ này?'
								placement='topLeft'
							>
								<Button danger type='link' icon={<DeleteOutlined rev={undefined} />} />
							</Popconfirm>
						</Tooltip>
					</>
				);
			},
		});
	}

	return (
		<Form.Item name='dsThongTinNgoaiNgu'>
			<Divider orientation='center'>Ngoại ngữ</Divider>
			<TableStaticData
				hasCreate={!disabled}
				hasTotal
				showEdit={visibleForm}
				setShowEdit={handleCreate}
				data={dsThongTinNgoaiNgu ?? []}
				columns={columns}
				addStt
				Form={FormNgoaiNgu}
				widthDrawer={700}
				formProps={
					{
						record,
						isEdit,
						isView,
						onCancel: handleCloseModal,
						onFinish: handleSubmitForm,
					} as FormNgoaiNguProps
				}
			/>
		</Form.Item>
	);
};
