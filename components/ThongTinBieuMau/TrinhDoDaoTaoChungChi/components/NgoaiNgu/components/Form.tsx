import { Button, Card, Checkbox, Col, Form, Input, Row } from 'antd';
import { useWatch } from 'antd/lib/form/Form';
import { useEffect } from 'react';
import rules from '../../../../../../utils/rules';
import { resetFieldsForm } from '../../../../../../utils/util';
import CardForm from '../../../../../CardForm';
import MyDatePicker from '../../../../../MyDatePicker';
import UploadFile from '../../../../../Upload/UploadFile';
import { FormValues } from '../../../../KhenThuong';
import SelectHinhThucDaoTao from '../../HinhThucDaoTao/select';
import { SelectKhungNangLucNgoaiNguHemis } from '../../KhungNangLucNgoaiNgu/select';
import { SelectNgoaiNguHemis } from '../../NgoaiNguHemis/select';

export type FormNgoaiNguValues = Required<FormValues>['dsThongTinNgoaiNgu'][number];

export interface FormNgoaiNguProps {
	onFinish: (values: FormNgoaiNguValues) => void;
	onCancel: () => void;
	isEdit: boolean;
	isView: boolean;
	record?: FormNgoaiNguValues;
	ngaySinh?: string;
}

export const FormNgoaiNgu = ({ isEdit, isView, onCancel, onFinish, record, ngaySinh }: FormNgoaiNguProps) => {
	const [form] = Form.useForm<DeepNullable<FormNgoaiNguValues>>();

	const thoiGianBatDau = useWatch(['thoiGianBatDau'], form);
	const ngoaiNgu = useWatch(['ngoaiNgu'], form);

	useEffect(() => {
		resetFieldsForm(form, record);
	}, [isEdit, isView, record]);

	return (
		<CardForm bordered={false} title={(isView ? 'Xem chi tiết ' : isEdit ? 'Chỉnh sửa ' : 'Thêm mới ') + 'trình độ ngoại ngữ'}>
			<Form id='FormNgoaiNgu' onFinish={onFinish} form={form as any} layout='vertical' disabled={isView}>
				<Form.Item name='hinhThucDaoTao' hidden />
				<Form.Item name='ngoaiNgu' hidden />
				<Form.Item name='khungNangLucNgoaiNgu' hidden />
				<Row gutter={[12, 0]} style={{ marginBottom: 12 }}>
					<Col span={24} md={12}>
						<Form.Item
							name='thoiGianBatDau'
							// rules={[...rules.required]}
							label='Thời gian bắt đầu'
						>
							<MyDatePicker placeholder='Thời gian bắt đầu' />
						</Form.Item>
					</Col>
					<Col span={24} md={12}>
						<Form.Item
							name='thoiGianKetThuc'
							label='Thời gian kết thúc'
							rules={[
								// ...rules.required,
								...rules.sauNgay(thoiGianBatDau, 'thời gian bắt đầu'),
							]}
						>
							<MyDatePicker placeholder='Thời gian kết thúc' />
						</Form.Item>
					</Col>
					<Col span={24} md={12}>
						<Form.Item name='hinhThucDaoTaoId' label='Hình thức đào tạo' rules={[...rules.required]}>
							<SelectHinhThucDaoTao
								hasCreate={false}
								placeholder='Hình thức đào tạo'
								onChange={(_, option) => {
									const rawData = option?.rawData as HinhThucDaoTao.IRecord | undefined;
									form.setFieldsValue({
										hinhThucDaoTao: rawData ?? null,
									});
								}}
							/>
						</Form.Item>
					</Col>
					<Col span={24} md={12}>
						<Form.Item name='ngoaiNguId' label='Ngoại ngữ' rules={[...rules.required]}>
							<SelectNgoaiNguHemis
								hienThiMa={false}
								placeholder='Ngoại ngữ'
								onChange={(_, option) => {
									const rawData = option?.rawData as NgoaiNgu.IRecord | undefined;
									form.setFieldsValue({
										ngoaiNgu: rawData ?? null,
										khungNangLucNgoaiNgu: null,
										khungNangLucNgoaiNguId: null,
									});
								}}
							/>
						</Form.Item>
					</Col>
					<Col span={24} md={12}>
						<Form.Item name='khungNangLucNgoaiNguId' label='Khung năng lực ngoại ngữ'>
							<SelectKhungNangLucNgoaiNguHemis
								tenNgoaiNgu={ngoaiNgu?.tenNgonNgu ?? undefined}
								disabled={!ngoaiNgu}
								hienThiMa={false}
								placeholder='Khung năng lực ngoại ngữ'
								onChange={(_, option) => {
									const rawData = option?.rawData as KhungNangLucNgoaiNgu.IRecord | undefined;
									form.setFieldsValue({
										khungNangLucNgoaiNgu: rawData ?? null,
									});
								}}
							/>
						</Form.Item>
					</Col>
					<Col span={24} md={12}>
						<Form.Item name='vanBangChungChi' label='Văn bằng/Chứng chỉ/Chứng nhận được cấp'>
							<Input placeholder='Văn bằng/Chứng chỉ/Chứng nhận được cấp' />
						</Form.Item>
					</Col>
					<Col span={24} md={12}>
						<Form.Item
							name='coSoDaoTao'
							rules={[...rules.required, ...rules.text, ...rules.length(255)]}
							label='Cơ sở đào tạo'
						>
							<Input placeholder='Cơ sở đào tạo' />
						</Form.Item>
					</Col>
					<Col span={24} md={12}>
						<Form.Item
							name='fileDinhKem'
							label='File đính kèm'
							// rules={[...rules.fileRequired]}
						>
							<UploadFile
								maxCount={1}
								otherProps={{
									maxCount: 1,
									multiple: false,
								}}
							/>
						</Form.Item>
					</Col>
					<Col span={24} md={12} style={{ display: 'flex', alignItems: 'center' }}>
						<Form.Item name='apDung' valuePropName='checked' initialValue={false}>
							<Checkbox disabled={isView}>Là trình độ chính</Checkbox>
						</Form.Item>
					</Col>
				</Row>
			</Form>

			<div style={{ textAlign: 'center', marginTop: 24 }}>
				{!isView && (
					<Button form='FormNgoaiNgu' style={{ marginRight: 8 }} htmlType='submit' type='primary'>
						{!isEdit ? 'Thêm mới ' : 'Lưu lại'}
					</Button>
				)}
				<Button
					disabled={false}
					onClick={() => {
						onCancel();
						resetFieldsForm(form);
					}}
				>
					Đóng
				</Button>
			</div>
		</CardForm>
	);
};
