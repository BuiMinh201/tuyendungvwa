import { AutoComplete } from 'antd';
import { type BaseOptionType } from 'antd/lib/select';
import {useEffect, useState} from 'react';
import {getNganhDaoTao, getTrinhDoQuanDaoTao} from "../../../../../../service/core/core";

interface Props {
  disabled?: boolean;
  placeholder?: string;
  value?: string;
  onChange?: (value?: string, option?: BaseOptionType) => void;
}

const getTenNganhKemMa = (item: Pick<NganhDaoTao.IRecord, 'ten' | 'ma'>) => {
  return !item.ten ? '' : item.ma ? `${item.ten} (${item.ma})` : item.ten;
};
export const AutoCompleteDMNganhDaoTao = ({ disabled, placeholder, value, onChange }: Props) => {
  const [danhSach, setDanhSach] = useState<NganhDaoTao.IRecord[]>([]);
  const getData = async () => {
    try {
      const res = await getNganhDaoTao();
      setDanhSach(res?.data?.data);
    } catch (e) {}
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <AutoComplete
      value={value}
      disabled={disabled}
      placeholder={placeholder ?? 'Ngành đào tạo'}
      filterOption={(inputValue, option) => {
        return option!.value?.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1;
      }}
      options={danhSach.map((item) => ({
        key: item.ma,
        value: getTenNganhKemMa(item),
        label: getTenNganhKemMa(item),
        rawData: item,
      }))}
      onChange={onChange}
    />
  );
};

AutoCompleteDMNganhDaoTao.getTenNganhKemMa = getTenNganhKemMa;
