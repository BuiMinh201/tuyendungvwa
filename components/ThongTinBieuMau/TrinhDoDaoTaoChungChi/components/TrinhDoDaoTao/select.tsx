import { PlusOutlined } from "@ant-design/icons";
import { Button, Modal, Select } from "antd";
import { useEffect, useState } from "react";
import { type BaseOptionType } from "antd/lib/select";
import {
  getLyLuan,
  getTrinhDoQuanDaoTao,
} from "../../../../../service/core/core";

const SelectTrinhDoDaoTao = (props: {
  value?: string;
  onChange?: (value?: string, option?: BaseOptionType) => void;
  hasCreate?: boolean;
  multiple?: boolean;
  disabled?: boolean;
  allowClear?: boolean;
  placeholder?: string;
}) => {
  const {
    value,
    onChange,
    hasCreate,
    multiple,
    allowClear,
    placeholder,
    disabled,
  } = props;
  const [danhSach, setDanhSach] = useState<TrinhDoDaoTao.IRecord[]>([]);
  const getData = async () => {
    try {
      const res = await getTrinhDoQuanDaoTao();
      setDanhSach(res?.data?.data);
    } catch (e) {}
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <Select
      disabled={disabled}
      allowClear={allowClear}
      mode={multiple ? "multiple" : undefined}
      value={value}
      onChange={onChange}
      options={danhSach.map((item) => ({
        key: item._id,
        value: item._id,
        label: item.ten,
        rawData: item,
      }))}
      showSearch
      optionFilterProp="label"
      placeholder={placeholder ?? "Chọn trình độ"}
    />
  );
};

export default SelectTrinhDoDaoTao;
