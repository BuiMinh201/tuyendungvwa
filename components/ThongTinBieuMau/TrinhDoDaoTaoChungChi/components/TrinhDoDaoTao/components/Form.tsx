import { Button, Card, Col, Form, Input, InputNumber, Row } from "antd";
import { useWatch } from "antd/lib/form/Form";
import { useEffect } from "react";
import rules from "../../../../../../utils/rules";
import { resetFieldsForm } from "../../../../../../utils/util";
import MyDatePicker from "../../../../../MyDatePicker";
import UploadFile from "../../../../../Upload/UploadFile";
import { FormValues } from "../../../../KhenThuong";
import SelectHinhThucDaoTao from "../../HinhThucDaoTao/select";
import { AutoCompleteDMNganhDaoTao } from "../NganhDaoTao/select";
import SelectTrinhDoDaoTao from "../select";
import moment from "moment";
import CardForm from "../../../../../CardForm";

export type FormTrinhDoDaoTaoValues =
  Required<FormValues>["dsThongTinTrinhDoDaoTao"][number] & {
    tenNganhKemMa?: string;
  };

export interface FormTrinhDoDaoTaoProps {
  onFinish: (values: FormTrinhDoDaoTaoValues) => void;
  onCancel: () => void;
  isEdit: boolean;
  isView: boolean;
  record?: FormTrinhDoDaoTaoValues;
  isGiangVienThinhGiangHoacNhanSuKhac?: boolean;
}

export const FormTrinhDoDaoTao = ({
  isEdit,
  isView,
  onCancel,
  onFinish,
  record,
}: FormTrinhDoDaoTaoProps) => {
  const [form] = Form.useForm<FormTrinhDoDaoTaoValues>();

  const thoiGianBatDau = useWatch(["thoiGianBatDau"], form);

  useEffect(() => {
    resetFieldsForm(form, {
      ...record,
      tenNganhKemMa: record
      ? AutoCompleteDMNganhDaoTao.getTenNganhKemMa({
      		ten: record?.nganh ?? '',
      		ma: record?.maNganh ?? '',
        })
      : undefined,
    });
  }, [isEdit, isView, record]);

  return (
    <CardForm
      bordered={false}
      title={
        (isView ? "Xem chi tiết " : isEdit ? "Chỉnh sửa " : "Thêm mới ") +
        "trình độ đào tạo"
      }
    >
      <Form
        id="FormTrinhDoDaoTao"
        onFinish={onFinish}
        form={form}
        layout="vertical"
        disabled={isView}
      >
        <Row gutter={[12, 0]} style={{ marginBottom: 12 }}>
          <Col span={24} md={24}>
            <Form.Item hidden name="trinhDoDaoTao" />
            <Form.Item
              name="trinhDoDaoTaoId"
              label="Trình độ"
              rules={[...rules.required]}
            >
              <SelectTrinhDoDaoTao
                disabled={isView}
                hasCreate={false}
                onChange={(_, option) => {
                  const rawData = option?.rawData as
                    | TrinhDoDaoTao.IRecord
                    | undefined;
                  form.setFields([{ name: "trinhDoDaoTao", value: rawData }]);
                }}
              />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="noiDaoTao"
              label="Cơ sở đào tạo"
              rules={[...rules.required, ...rules.text, ...rules.length(250)]}
            >
              <Input placeholder="Cơ sở đào tạo" disabled={isView} />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item name="nuocDaoTao" label="Nước đào tạo">
              <Input placeholder="Nước đào tạo" />
            </Form.Item>
          </Col>

          <Col span={24} md={24}>
            <Form.Item hidden name="nganh" />
            <Form.Item hidden name="maNganh" />
            <Form.Item
              name="tenNganhKemMa"
              label="Ngành đào tạo"
              rules={[...rules.text, ...rules.length(250)]}
            >
              <AutoCompleteDMNganhDaoTao
                disabled={isView}
                placeholder="Ngành đào tạo"
                onChange={(value, option) => {
                  if (!Array.isArray(option)) {
                    const rawData = option?.rawData as
                      | NganhDaoTao.IRecord
                      | undefined;
                    form.setFieldsValue({
                      nganh: rawData?.ten ?? value ?? null,
                      maNganh: rawData?.ma ?? null,
                    });
                  }
                }}
              />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item name="thoiGianBatDau" label="Thời gian bắt đầu">
              <MyDatePicker placeholder="Thời gian bắt đầu" />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="thoiGianKetThuc"
              label="Thời gian kết thúc"
              rules={[...rules.sauNgay(thoiGianBatDau, "thời gian bắt đầu")]}
            >
              <MyDatePicker placeholder="Thời gian kết thúc" />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item name="hinhThucDaoTaoId" label="Hình thức đào tạo">
              <SelectHinhThucDaoTao
                hasCreate={false}
                placeholder="Hình thức đào tạo"
              />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item name="vanBangChungChi" label="Văn bằng được cấp">
              <Input placeholder="Văn bằng được cấp" />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="namTotNghiep"
              label="Năm tốt nghiệp"
              rules={[...rules.number(moment().year() + 1, 1900)]}
            >
              <InputNumber
                placeholder="Năm tốt nghiệp"
                style={{ width: "100%" }}
                disabled={isView}
              />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="fileDinhKem"
              label="File đính kèm"
              // rules={isGiangVienThinhGiangHoacNhanSuKhac ? [] : [...rules.fileRequired]}
            >
              <UploadFile
                maxCount={1}
                otherProps={{
                  maxCount: 1,
                  multiple: false,
                }}
              />
            </Form.Item>
          </Col>
        </Row>
      </Form>
      <div style={{ textAlign: "center", marginTop: 24 }}>
        {!isView && (
          <Button
            form="FormTrinhDoDaoTao"
            style={{ marginRight: 8 }}
            htmlType="submit"
            type="primary"
          >
            {!isEdit ? "Thêm mới " : "Lưu lại"}
          </Button>
        )}
        <Button disabled={false} onClick={onCancel}>Đóng</Button>
      </div>
    </CardForm>
  );
};
