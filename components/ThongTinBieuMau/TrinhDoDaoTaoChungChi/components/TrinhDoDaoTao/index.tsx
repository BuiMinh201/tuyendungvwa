import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import {
  Button,
  Popconfirm,
  Tooltip,
  type FormInstance,
  Form,
  Divider,
} from "antd";
import { useWatch } from "antd/lib/form/Form";
import { useState } from "react";
import TableStaticData from "../../../../TableStaticData";
import { IColumn } from "../../../../TableStaticData/typing";
import { FormValues } from "../../../KhenThuong";
import {
  FormTrinhDoDaoTao,
  type FormTrinhDoDaoTaoProps,
  type FormTrinhDoDaoTaoValues,
} from "./components/Form";
import { buildUpLoadFile } from "../../../../../utils/uploadFile";

interface Props {
  form: FormInstance<FormValues>;
  disabled?: boolean;
  isGiangVienThinhGiangHoacNhanSuKhac: boolean;
}

type TableRecord = FormTrinhDoDaoTaoValues;
export const TrinhDoDaoTao = ({
  form,
  disabled,
  isGiangVienThinhGiangHoacNhanSuKhac,
}: Props) => {
  const dsThongTinTrinhDoDaoTao = useWatch(["dsThongTinTrinhDoDaoTao"], form);

  const [visibleForm, setVisibleForm] = useState(false);
  const [record, setRecord] = useState<FormTrinhDoDaoTaoValues>();
  const [isEdit, setIsEdit] = useState(false);
  const [isView, setIsView] = useState(false);
  const [indexRecordDangThaoTac, setIndexRecordDangThaoTac] = useState<
    number | undefined
  >(undefined);

  const handleCreate = (vi: boolean) => {
    setVisibleForm(vi);
    setIsEdit(false);
    setIsView(false);
    setRecord(undefined);
  };

  const handleEdit = (tableRecord: TableRecord, index: number) => {
    setVisibleForm(true);
    setIsEdit(true);
    setIsView(false);
    setRecord(tableRecord);
    setIndexRecordDangThaoTac(index);
  };

  const handleView = (tableRecord: TableRecord) => {
    setVisibleForm(true);
    setIsView(true);
    setIsEdit(false);
    setRecord(tableRecord);
  };

  const handleCloseModal = () => {
    setRecord(undefined);
    setVisibleForm(false);
    setIsEdit(false);
    setIsView(false);
  };

  const handleDelete = (index: number) => {
    const prevDsThongTinTrinhDoDaoTao: Required<FormValues>["dsThongTinTrinhDoDaoTao"] =
      form.getFieldValue("dsThongTinTrinhDoDaoTao") ?? [];
    form.setFieldsValue({
      dsThongTinTrinhDoDaoTao: prevDsThongTinTrinhDoDaoTao.filter(
        (_, itemIndex) => itemIndex !== index
      ),
    });
  };

  const handleSubmitForm = async (values: FormTrinhDoDaoTaoValues) => {
    const prevDsThongTinTrinhDoDaoTao: Required<FormValues>["dsThongTinTrinhDoDaoTao"] =
      form.getFieldValue("dsThongTinTrinhDoDaoTao") ?? [];
    const url = await buildUpLoadFile(values, "fileDinhKem");
    const payload = {
      ...values,
      fileDinhKem: url,
    };
    if (!isEdit) {
      form.setFieldsValue({
        dsThongTinTrinhDoDaoTao: prevDsThongTinTrinhDoDaoTao.prepend(payload),
      });
    } else if (isEdit) {
      form.setFieldsValue({
        dsThongTinTrinhDoDaoTao: prevDsThongTinTrinhDoDaoTao.map(
          (item, index) => {
            if (indexRecordDangThaoTac === index) {
              return {
                ...item,
                ...payload,
              };
            }
            return item;
          }
        ),
      });
    }

    setIndexRecordDangThaoTac(undefined);
    setVisibleForm(false);
    setIsEdit(false);
    setIsView(false);
  };

  const onCell = (rec: TableRecord) => ({
    onClick: () => handleView(rec),
    style: { cursor: "pointer" },
  });
  const columns: IColumn<TableRecord>[] = [
    {
      title: "Trình độ",
      width: 100,
      onCell: onCell,
      dataIndex: ["trinhDoDaoTao", "ten"],
      filterType: "string",
    },
    {
      title: "Nơi đào tạo",
      dataIndex: "noiDaoTao",
      width: 100,
      filterType: "string",
      onCell: onCell,
    },
    {
      title: "Ngành",
      dataIndex: "nganh",
      width: 100,
      filterType: "string",
      onCell,
    },
    {
      title: "Năm tốt nghiệp",
      width: 100,
      dataIndex: "namTotNghiep",
      onCell: onCell,
      sortable: true,
      align: "center",
      filterType: "number",
    },
  ];
  if (!disabled) {
    columns.push({
      title: "Thao tác",
      align: "center",
      width: 90,
      fixed: "right",
      render: (_, rec, index) => {
        return (
          <>
            <Tooltip title="Chỉnh sửa">
              <Button
                type="link"
                icon={<EditOutlined rev={undefined} />}
                onClick={() => handleEdit(rec, index)}
              />
            </Tooltip>

            <Tooltip title="Xóa">
              <Popconfirm
                onConfirm={() => handleDelete(index)}
                title="Bạn có chắc chắn muốn xóa trình độ đào tạo này?"
                placement="topLeft"
              >
                <Button
                  danger
                  type="link"
                  icon={<DeleteOutlined rev={undefined} />}
                />
              </Popconfirm>
            </Tooltip>
          </>
        );
      },
    });
  }

  return (
    <Form.Item name="dsThongTinTrinhDoDaoTao">
      <div id={"trinh-do-dao-tao"}>
        <Divider orientation="center">Thông tin trình độ đào tạo</Divider>
      </div>
      <TableStaticData
        pagination={false}
        hasCreate={!disabled}
        hasTotal
        showEdit={visibleForm}
        setShowEdit={handleCreate}
        data={dsThongTinTrinhDoDaoTao ?? []}
        columns={columns}
        addStt
        Form={FormTrinhDoDaoTao}
        widthDrawer={700}
        formProps={
          {
            record,
            isEdit,
            isView,
            onCancel: handleCloseModal,
            onFinish: handleSubmitForm,
            isGiangVienThinhGiangHoacNhanSuKhac,
          } as FormTrinhDoDaoTaoProps
        }
      />
    </Form.Item>
  );
};
