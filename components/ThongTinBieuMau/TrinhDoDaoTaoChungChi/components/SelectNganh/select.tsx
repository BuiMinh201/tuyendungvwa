import { Select } from 'antd';
import { type BaseOptionType } from 'antd/lib/select';
import {useEffect, useState} from 'react';
import {getLoaiBoiDuong} from "../../../../../service/core/loaiboiduong";
import {getNganhDaoTao} from "../../../../../service/core/nganhdaotao";

export const SelectDMNganhDaoTao = (props: {
  value?: string;
  onChange?: (value?: string, option?: BaseOptionType) => void;
  multiple?: boolean;
  disabled?: boolean;
  allowClear?: boolean;
  placeholder?: string;
  fieldKey?: keyof NganhDaoTao.IRecord;
}) => {
  const { value, onChange, multiple, allowClear = true, placeholder, fieldKey = 'ma' } = props;
  const [danhSach, setDanhSach] = useState<NganhDaoTao.IRecord[]>([]);
  const getData = async () => {
    try {
      const res = await getNganhDaoTao();
      setDanhSach(res?.data?.data);
    } catch (e) {}
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <Select
      allowClear={allowClear}
      disabled={props.disabled}
      mode={multiple ? 'multiple' : undefined}
      value={value}
      onChange={onChange}
      options={danhSach.map((item) => ({
        key: item[fieldKey],
        value: item[fieldKey],
        label: `${item.ten} (${item.ma})`,
        rawData: item,
      }))}
      showSearch
      optionFilterProp='label'
      placeholder={placeholder ?? 'Chọn ngành đào tạo'}
    />
  );
};
