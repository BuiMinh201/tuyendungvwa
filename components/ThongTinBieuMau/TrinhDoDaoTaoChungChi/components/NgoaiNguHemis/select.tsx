import { Select } from 'antd';
import { type BaseOptionType } from 'antd/lib/select';
import {useEffect, useState} from 'react';
import {getNgoaiNgu} from "../../../../../service/core/ngoaingu";

export const SelectNgoaiNguHemis = (props: {
  value?: string;
  onChange?: (value?: string, option?: BaseOptionType) => void;
  multiple?: boolean;
  placeholder?: string;
  disabled?: boolean;
  hienThiMa?: false;
}) => {
  const { value, onChange, multiple, placeholder, disabled, hienThiMa = true } = props;
  const [danhSach, setDanhSach] = useState<NgoaiNgu.IRecord[]>([]);
  const getData = async () => {
    try {
      const res = await getNgoaiNgu();
      setDanhSach(res?.data?.data);
    } catch (e) {}
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div style={{ display: 'flex', gap: 8 }}>
      <div className={'fullWidth'}>
        <Select
          disabled={disabled}
          mode={multiple ? 'multiple' : undefined}
          value={value}
          onChange={onChange}
          options={danhSach.map((item) => ({
            key: item._id,
            value: item._id,
            label: hienThiMa ? `${item.tenNgonNgu} (${item.ma})` : item.tenNgonNgu,
            rawData: item,
          }))}
          showSearch
          optionFilterProp='label'
          placeholder={placeholder ?? 'Chọn ngoại ngữ'}
        />
      </div>
    </div>
  );
};
