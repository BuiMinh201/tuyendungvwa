import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import {
  Button,
  Popconfirm,
  Tooltip,
  type FormInstance,
  Form,
  Divider,
} from "antd";
import { useWatch } from "antd/lib/form/Form";
import { useState } from "react";
import {
  FormQuaTrinhDaoTaoBoiDuong,
  type FormQuaTrinhDaoTaoBoiDuongProps,
  type FormQuaTrinhDaoTaoBoiDuongValues,
} from "./Form";
import moment from "moment";
import { FormValues } from "../../../KhenThuong";
import { IColumn } from "../../../../TableStaticData/typing";
import TableStaticData from "../../../../TableStaticData";
import { buildUpLoadFile } from "../../../../../utils/uploadFile";

interface Props {
  form: FormInstance<FormValues>;
  disabled: boolean;
}

type TableRecord = FormQuaTrinhDaoTaoBoiDuongValues;
export const QuaTrinhDaoTaoBoiDuong = ({ form, disabled }: Props) => {
  const dsQuaTrinhDaoTaoBoiDuong = useWatch(["dsQuaTrinhDaoTaoBoiDuong"], form);
  const [visibleForm, setVisibleForm] = useState(false);
  const [record, setRecord] = useState<FormQuaTrinhDaoTaoBoiDuongValues>();
  const [isEdit, setIsEdit] = useState(false);
  const [isView, setIsView] = useState(false);
  const [indexRecordDangThaoTac, setIndexRecordDangThaoTac] = useState<
    number | undefined
  >(undefined);

  const handleCreate = (vi: boolean) => {
    setVisibleForm(vi);
    setIsEdit(false);
    setIsView(false);
    setRecord(undefined);
  };

  const handleEdit = (tableRecord: TableRecord, index: number) => {
    setVisibleForm(true);
    setIsEdit(true);
    setIsView(false);
    setRecord(tableRecord);
    setIndexRecordDangThaoTac(index);
  };

  const handleView = (tableRecord: TableRecord) => {
    setVisibleForm(true);
    setIsView(true);
    setIsEdit(false);
    setRecord(tableRecord);
  };

  const handleCloseModal = () => {
    setRecord(undefined);
    setVisibleForm(false);
    setIsEdit(false);
    setIsView(false);
  };

  const handleDelete = (index: number) => {
    const prevDsQuaTrinhDaoTaoBoiDuong: Required<FormValues>["dsQuaTrinhDaoTaoBoiDuong"] =
      form.getFieldValue("dsQuaTrinhDaoTaoBoiDuong") ?? [];
    form.setFieldsValue({
      dsQuaTrinhDaoTaoBoiDuong: prevDsQuaTrinhDaoTaoBoiDuong.filter(
        (_, QuaTrinhDaoTaoBoiDuongIndex) =>
          QuaTrinhDaoTaoBoiDuongIndex !== index
      ),
    });
  };

  const handleSubmitForm = async (values: FormQuaTrinhDaoTaoBoiDuongValues) => {
    const prevDsQuaTrinhDaoTaoBoiDuong: Required<FormValues>["dsQuaTrinhDaoTaoBoiDuong"] =
      form.getFieldValue("dsQuaTrinhDaoTaoBoiDuong") ?? [];
    const url = await buildUpLoadFile(values, "fileDinhKem");
    const payload = {
      ...values,
      fileDinhKem: url,
    };
    if (!isEdit) {
      form.setFieldsValue({
        dsQuaTrinhDaoTaoBoiDuong: prevDsQuaTrinhDaoTaoBoiDuong.prepend(payload),
      });
    } else if (isEdit) {
      form.setFieldsValue({
        dsQuaTrinhDaoTaoBoiDuong: prevDsQuaTrinhDaoTaoBoiDuong.map(
          (item, index) => {
            if (indexRecordDangThaoTac === index) {
              return {
                ...item,
                ...payload,
              };
            }
            return item;
          }
        ),
      });
    }

    setIndexRecordDangThaoTac(undefined);
    setVisibleForm(false);
    setIsEdit(false);
    setIsView(false);
  };

  const onCell = (rec: TableRecord) => ({
    onClick: () => handleView(rec),
    style: { cursor: "pointer" },
  });
  const columns: IColumn<TableRecord>[] = [
    {
      title: "Khóa bồi dưỡng tập huấn",
      width: 140,
      dataIndex: "khoaBoiDuongTapHuan",
      filterType: "string",
      sortable: true,
      onCell,
    },
    {
      title: "Đơn vị tổ chức",
      width: 120,
      dataIndex: "donViToChuc",
      filterType: "string",
      sortable: true,
      onCell,
    },
    {
      title: "Loại bồi dưỡng",
      width: 120,
      dataIndex: ["loaiBoiDuong", "ten"],
      sortable: true,
      onCell,
    },
    {
      title: "Từ ngày",
      dataIndex: "tuNgay",
      width: 100,
      onCell: onCell,
      render: (value) => {
        return value ? moment(value).format("DD/MM/YYYY") : null;
      },
    },
    {
      title: "Đến ngày",
      dataIndex: "denNgay",
      width: 100,
      render: (value) => {
        return value ? moment(value).format("DD/MM/YYYY") : null;
      },
    },
  ];

  if (!disabled) {
    columns.push({
      title: "Thao tác",
      align: "center",
      width: 90,
      fixed: "right",
      render: (_, rec, index) => {
        return (
          <>
            <Tooltip title="Chỉnh sửa">
              <Button
                type="link"
                icon={<EditOutlined rev={undefined} />}
                onClick={() => handleEdit(rec, index)}
              />
            </Tooltip>

            <Tooltip title="Xóa">
              <Popconfirm
                onConfirm={() => handleDelete(index)}
                title="Bạn có chắc chắn muốn xóa quá trình đào tạo, bồi dưỡng này?"
                placement="topLeft"
              >
                <Button
                  danger
                  type="link"
                  icon={<DeleteOutlined rev={undefined} />}
                />
              </Popconfirm>
            </Tooltip>
          </>
        );
      },
    });
  }

  return (
    <Form.Item name="dsQuaTrinhDaoTaoBoiDuong">
      <div id={"qua-trinh-dao-tao"}>
        <Divider orientation="center">Quá trình đào tạo, bồi dưỡng</Divider>
      </div>
      <TableStaticData
        hasCreate={!disabled}
        hasTotal
        showEdit={visibleForm}
        setShowEdit={handleCreate}
        data={dsQuaTrinhDaoTaoBoiDuong ?? []}
        columns={columns}
        addStt
        widthDrawer={800}
        Form={FormQuaTrinhDaoTaoBoiDuong}
        formProps={
          {
            record,
            isEdit,
            isView,
            onCancel: handleCloseModal,
            onFinish: handleSubmitForm,
          } as FormQuaTrinhDaoTaoBoiDuongProps
        }
      />
    </Form.Item>
  );
};
