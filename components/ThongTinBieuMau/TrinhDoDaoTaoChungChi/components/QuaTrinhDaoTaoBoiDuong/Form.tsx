
import { Button, Card, Col, Form, Input, Row } from 'antd';
import { useWatch } from 'antd/lib/form/Form';
import { useEffect } from 'react';
import rules from '../../../../../utils/rules';
import { resetFieldsForm } from '../../../../../utils/util';
import CardForm from '../../../../CardForm';
import MyDatePicker from '../../../../MyDatePicker';
import UploadFile from '../../../../Upload/UploadFile';
import { FormValues } from '../../../KhenThuong';
import SelecLoaiBoiDuong from '../../../QuaTrinhCuDiDaoTaoBoiDuong/components/LoaiBoiDuong/select';

export type FormQuaTrinhDaoTaoBoiDuongValues = Required<FormValues>['dsQuaTrinhDaoTaoBoiDuong'][number];

export interface FormQuaTrinhDaoTaoBoiDuongProps {
	onFinish: (values: FormQuaTrinhDaoTaoBoiDuongValues) => void;
	onCancel: () => void;
	isEdit: boolean;
	isView: boolean;
	record?: FormQuaTrinhDaoTaoBoiDuongValues;
}

export const FormQuaTrinhDaoTaoBoiDuong = ({
	isEdit,
	isView,
	onCancel,
	onFinish,
	record,
}: FormQuaTrinhDaoTaoBoiDuongProps) => {
	const [form] = Form.useForm<FormQuaTrinhDaoTaoBoiDuongValues>();
	const tuNgay = useWatch(['tuNgay'], form);

	useEffect(() => {
		resetFieldsForm(form, record);
	}, [record, isEdit, isView]);

	return (
		<CardForm  bordered={false} title={(isView ? 'Xem chi tiết ' : isEdit ? 'Chỉnh sửa ' : 'Thêm mới ') + 'quá trình đào tạo, bồi dưỡng'}>
			<Form id='FormQuaTrinhDuocDaoTao' onFinish={onFinish} form={form} layout='vertical' disabled={isView}>
				<Form.Item name='loaiBoiDuong' hidden />
				<Row gutter={[12, 0]} style={{ marginBottom: 12 }}>
					{/* <Col span={24}>
						<Form.Item name='loaiQuyetDinh' label='Loại quyết định' rules={[...rules.required]}>
							<Select
								options={Object.values(ELoaiQuyetDinh).map((item) => ({ label: item, value: item }))}
								placeholder='Loại quyết định'
								disabled={isView}
							/>
						</Form.Item>
					</Col> */}
					<Col span={24}>
						<Form.Item
							name='khoaBoiDuongTapHuan'
							label='Tên khóa bồi dưỡng tập huấn'
							rules={[...rules.text, ...rules.length(250), ...rules.required]}
						>
							<Input placeholder='Tên khóa bồi dưỡng tập huấn' disabled={isView} />
						</Form.Item>
					</Col>
					<Col span={24} md={12}>
						<Form.Item name='donViToChuc' label='Đơn vị tổ chức' rules={[...rules.text, ...rules.length(250)]}>
							<Input placeholder='Đơn vị tổ chức' disabled={isView} />
						</Form.Item>
					</Col>
					<Col span={24} md={12}>
						<Form.Item name='loaiBoiDuongId' label='Loại bồi dưỡng' rules={[...rules.required]}>
							<SelecLoaiBoiDuong
								disabled={isView}
								onChange={(_, option) => {
									const rawData = option?.rawData as LoaiBoiDuong.IRecord | undefined;
									form.setFieldsValue({ loaiBoiDuong: rawData ?? null });
								}}
							/>
						</Form.Item>
					</Col>

					<Col span={24} md={12}>
						<Form.Item name='diaDiemToChuc' label='Địa điểm tổ chức' rules={[...rules.text, ...rules.length(250)]}>
							<Input placeholder='Địa điểm tổ chức' disabled={isView} />
						</Form.Item>
					</Col>

					<Col span={24} md={12}>
						<Form.Item name='tuNgay' label='Thời gian bắt đầu' rules={[...rules.nhoHonBangHomNay]}>
							<MyDatePicker disabled={isView} placeholder='Thời gian bắt đầu' />
						</Form.Item>
					</Col>
					<Col span={24} md={12}>
						<Form.Item
							name='denNgay'
							label='Thời gian kết thúc'
							rules={[...rules.sauNgay(new Date(tuNgay), 'ngày bắt đầu')]}
						>
							<MyDatePicker placeholder='Thời gian kết thúc' allowClear disabled={isView} />
						</Form.Item>
					</Col>
					<Col span={24} md={12}>
						<Form.Item name='nguonKinhPhi' label='Nguồn kinh phí' rules={[...rules.text, ...rules.length(250)]}>
							<Input placeholder='Nguồn kinh phí' disabled={isView} />
						</Form.Item>
					</Col>
					<Col span={24} md={12}>
						<Form.Item name='chungChi' label='Chứng chỉ' rules={[...rules.text, ...rules.length(250)]}>
							<Input placeholder='Chứng chỉ' disabled={isView} />
						</Form.Item>
					</Col>
					<Col span={24} md={12}>
						<Form.Item name='ngayCap' label='Ngày cấp'>
							<MyDatePicker placeholder='Ngày cấp' allowClear disabled={isView} />
						</Form.Item>
					</Col>
					<Col span={24}>
						<Form.Item
							name='urlFileUpload'
							// rules={[...rules.fileRequired]}
							label='File đính kèm'
						>
							<UploadFile
								otherProps={{
									maxCount: 1,
									multiple: true,
									showUploadList: { showDownloadIcon: false },
								}}
							/>
						</Form.Item>
					</Col>
				</Row>
			</Form>
			<div style={{ textAlign: 'center', marginTop: 24 }}>
				{!isView && (
					<Button form='FormQuaTrinhDuocDaoTao' style={{ marginRight: 8 }} htmlType='submit' type='primary'>
						{!isEdit ? 'Thêm mới ' : 'Lưu lại'}
					</Button>
				)}
				<Button
					disabled={false}
					onClick={() => {
						onCancel();
						resetFieldsForm(form);
					}}
				>
					Đóng
				</Button>
			</div>
		</CardForm>
	);
};
