import { PlusOutlined } from '@ant-design/icons';
import { Button, Modal, Select } from 'antd';
import {useEffect, useState} from 'react';
import { type BaseOptionType } from 'antd/lib/select';
import {getTrinhDoQuanLy, getTrinhDoQuanLyNhaNuoc} from "../../../../../service/core/core";

const SelectTrinhDoQuanLyNhaNuoc = (props: {
  value?: string;
  onChange?: (value?: string, option?: BaseOptionType) => void;
  hasCreate?: boolean;
  multiple?: boolean;
  allowClear?: boolean;
  disabled?: boolean;
  placeholder?: string;
}) => {
  const { value, onChange, hasCreate, multiple, allowClear, disabled, placeholder } = props;
  const [danhSach, setDanhSach] = useState<TrinhDoQuanLyHanhChinh.IRecord[]>(
    []
  );
  const getData = async () => {
    try {
      const res = await getTrinhDoQuanLyNhaNuoc();
      setDanhSach(res?.data?.data);
    } catch (e) {}
  };

  useEffect(() => {
    getData();
  }, []);

  return (
        <Select
          disabled={disabled}
          mode={multiple ? 'multiple' : undefined}
          value={value}
          onChange={onChange}
          allowClear={allowClear}
          options={danhSach.map((item) => ({
            key: item._id,
            value: item._id,
            label: item.ten,
            rawData: item,
          }))}
          showSearch
          optionFilterProp='label'
          placeholder={placeholder ?? 'Chọn trình độ quản lý nhà nước'}
        />
  );
};

export default SelectTrinhDoQuanLyNhaNuoc;
