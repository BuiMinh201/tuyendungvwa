import { Select } from 'antd';
import { type BaseOptionType } from 'antd/lib/select';
import {useEffect, useState} from 'react';
import {getTinHoc} from "../../../../../service/core/core";

export const SelectTrinhDoTinHocHemis = (props: {
  value?: string;
  onChange?: (value?: string, option?: BaseOptionType) => void;
  multiple?: boolean;
  placeholder?: string;
  disabled?: boolean;
  hienThiMa?: false;
}) => {
  const { value, onChange, multiple, placeholder, disabled, hienThiMa = true } = props;
  const [danhSach, setDanhSach] = useState<TrinhDoTinHoc.IRecord[]>([]);
  const getData = async () => {
    try {
      const res = await getTinHoc();
      setDanhSach(res?.data?.data);
    } catch (e) {}
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div style={{ display: 'flex', gap: 8 }}>
      <div className={'fullWidth'}>
        <Select
          disabled={disabled}
          mode={multiple ? 'multiple' : undefined}
          value={value}
          onChange={onChange}
          options={danhSach.map((item) => ({
            key: item._id,
            value: item._id,
            label: hienThiMa ? `${item.trinhDoTinHoc} (${item.ma})` : item.trinhDoTinHoc,
            rawData: item,
          }))}
          showSearch
          optionFilterProp='label'
          placeholder={placeholder ?? 'Chọn trình độ tin học'}
        />
      </div>
    </div>
  );
};
