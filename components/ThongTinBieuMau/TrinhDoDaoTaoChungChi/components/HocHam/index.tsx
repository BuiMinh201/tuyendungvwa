import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import {
  Button,
  Popconfirm,
  Tooltip,
  type FormInstance,
  Divider,
  Form,
} from "antd";
import { useWatch } from "antd/lib/form/Form";
import { useState } from "react";
import {
  FormHocHam,
  type FormHocHamProps,
  type FormHocHamValues,
} from "./components/Form";
import moment from "moment";
import { FormValues } from "../../../KhenThuong";
import { IColumn } from "../../../../TableStaticData/typing";
import TableStaticData from "../../../../TableStaticData";
import { buildUpLoadFile } from "../../../../../utils/uploadFile";

interface Props {
  form: FormInstance<FormValues>;
  disabled?: boolean;
  isGiangVienThinhGiangHoacNhanSuKhac: boolean;
}

type TableRecord = FormHocHamValues;
export const HocHam = ({
  form,
  disabled,
  isGiangVienThinhGiangHoacNhanSuKhac,
}: Props) => {
  const dsHocHam = useWatch(["dsHocHam"], form);

  const [visibleForm, setVisibleForm] = useState(false);
  const [record, setRecord] = useState<FormHocHamValues>();
  const [isEdit, setIsEdit] = useState(false);
  const [isView, setIsView] = useState(false);
  const [indexRecordDangThaoTac, setIndexRecordDangThaoTac] = useState<
    number | undefined
  >(undefined);

  const handleCreate = (vi: boolean) => {
    setVisibleForm(vi);
    setIsEdit(false);
    setIsView(false);
    setRecord(undefined);
  };

  const handleEdit = (tableRecord: TableRecord, index: number) => {
    setVisibleForm(true);
    setIsEdit(true);
    setIsView(false);
    setRecord(tableRecord);
    setIndexRecordDangThaoTac(index);
  };

  const handleView = (tableRecord: TableRecord) => {
    setVisibleForm(true);
    setIsView(true);
    setIsEdit(false);
    setRecord(tableRecord);
  };

  const handleCloseModal = () => {
    setRecord(undefined);
    setVisibleForm(false);
    setIsEdit(false);
    setIsView(false);
  };

  const handleDelete = (index: number) => {
    const prevDsHocHam: Required<FormValues>["dsHocHam"] =
      form.getFieldValue("dsHocHam") ?? [];
    form.setFieldsValue({
      dsHocHam: prevDsHocHam.filter((_, itemIndex) => itemIndex !== index),
    });
  };

  const handleSubmitForm = async (values: FormHocHamValues) => {
    const prevDsHocHam: Required<FormValues>["dsHocHam"] =
      form.getFieldValue("dsHocHam") ?? [];
    const url = await buildUpLoadFile(values, "fileDinhKem");
    const payload = {
      ...values,
      fileDinhKem: url,
    };
    if (!isEdit) {
      form.setFieldsValue({
        dsHocHam: prevDsHocHam.prepend(payload),
      });
    } else if (isEdit) {
      form.setFieldsValue({
        dsHocHam: prevDsHocHam.map((item, index) => {
          if (indexRecordDangThaoTac === index) {
            return {
              ...item,
              ...payload,
            };
          }
          return item;
        }),
      });
    }

    setIndexRecordDangThaoTac(undefined);
    setVisibleForm(false);
    setIsEdit(false);
    setIsView(false);
  };

  const onCell = (rec: TableRecord) => ({
    onClick: () => handleView(rec),
    style: { cursor: "pointer" },
  });
  const columns: IColumn<TableRecord>[] = [
    {
      title: "Danh hiệu",
      dataIndex: "danhHieu",
      width: 120,
      onCell,
    },
    {
      title: "Loại quyết định",
      dataIndex: "loaiQuyetDinh",
      width: 120,
      filterType: "string",
      onCell,
    },
    {
      title: "Ngày có hiệu lực",
      width: 100,
      dataIndex: "ngayCoHieuLuc",
      align: "center",
      sortable: true,
      onCell,
      render: (value) => {
        if (value) {
          return moment(value).format("DD/MM/YYYY");
        }
        return null;
      },
    },
    {
      title: "Số quyết định",
      width: 100,
      dataIndex: "soQĐ",
      filterType: "string",
      onCell,
    },
    {
      title: "Năm đạt được",
      dataIndex: "nam",
      width: 80,
      align: "center",
      sortable: true,
      onCell,
    },
  ];

  if (!disabled) {
    columns.push({
      title: "Thao tác",
      align: "center",
      width: 90,
      fixed: "right",
      render: (_, rec, index) => {
        return (
          <>
            <Tooltip title="Chỉnh sửa">
              <Button
                type="link"
                icon={<EditOutlined rev={undefined} />}
                onClick={() => handleEdit(rec, index)}
              />
            </Tooltip>

            <Tooltip title="Xóa">
              <Popconfirm
                onConfirm={() => handleDelete(index)}
                title="Bạn có chắc chắn muốn xóa học hàm này?"
                placement="topLeft"
              >
                <Button
                  danger
                  type="link"
                  icon={<DeleteOutlined rev={undefined} />}
                />
              </Popconfirm>
            </Tooltip>
          </>
        );
      },
    });
  }

  return (
    <Form.Item name="dsHocHam">
      <Divider orientation="center">Học hàm</Divider>
      <TableStaticData
        pagination={false}
        hasCreate={!disabled}
        hasTotal
        showEdit={visibleForm}
        setShowEdit={handleCreate}
        data={dsHocHam ?? []}
        columns={columns}
        addStt
        widthDrawer={800}
        Form={FormHocHam}
        formProps={
          {
            record,
            isEdit,
            isView,
            onCancel: handleCloseModal,
            onFinish: handleSubmitForm,
            isGiangVienThinhGiangHoacNhanSuKhac,
          } as FormHocHamProps
        }
      />
    </Form.Item>
  );
};
