
import { Button, Card, Col, Form, Input, InputNumber, Row, Select } from 'antd';
import { useEffect } from 'react';
import { EDanhhieu } from '../../../../../../utils/constant';
import rules from '../../../../../../utils/rules';
import { resetFieldsForm } from '../../../../../../utils/util';
import MyDatePicker from "../../../../../MyDatePicker";
import UploadFile from '../../../../../Upload/UploadFile';
import { FormValues } from '../../../../KhenThuong';
import CardForm from "../../../../../CardForm";

export type FormHocHamValues = Required<FormValues>['dsHocHam'][number] & {
	capHocHam: string;
};

export interface FormHocHamProps {
	onFinish: (values: FormHocHamValues) => void;
	onCancel: () => void;
	isEdit: boolean;
	isView: boolean;
	record?: FormHocHamValues;
	ngaySinh?: string;
	isGiangVienThinhGiangHoacNhanSuKhac: boolean;
}

export const FormHocHam = ({
	isEdit,
	isView,
	onCancel,
	onFinish,
	record,
	ngaySinh,
	isGiangVienThinhGiangHoacNhanSuKhac,
}: FormHocHamProps) => {
	const [form] = Form.useForm<FormHocHamValues>();

	useEffect(() => {
		resetFieldsForm(form, record);
	}, [isEdit, isView, record]);

	return (
		<CardForm bordered={false} title={(isView ? 'Xem chi tiết ' : isEdit ? 'Chỉnh sửa ' : 'Thêm mới ') + 'học hàm'}>
			<Form id='FormHocHam' onFinish={onFinish} form={form} layout='vertical' disabled={isView}>
				<Row gutter={[12, 0]} style={{ marginBottom: 12 }}>
					<Col span={24} md={12}>
						<Form.Item name='danhHieu' label='Danh hiệu' rules={[...rules.required]}>
							<Select
								disabled={isView}
								placeholder='Danh hiệu'
								showSearch
								options={Object.values(EDanhhieu).map((item) => ({
									key: item,
									value: item,
									label: item,
								}))}
							/>
						</Form.Item>
					</Col>
					<Col span={24} md={12}>
						<Form.Item name='nam' label='Năm đạt được' rules={[...rules.number(2023, 1900)]}>
							<InputNumber placeholder='Năm đạt được' style={{ width: '100%' }} disabled={isView} />
						</Form.Item>
					</Col>
					<Col span={24} md={12}>
						<Form.Item name='soQĐ' label='Số quyết định' rules={[...rules.text, ...rules.length(20)]}>
							<Input placeholder='Số quyết định' disabled={isView} />
						</Form.Item>
					</Col>
					<Col span={24} md={12}>
						<Form.Item name='loaiQuyetDinh' label='Loại quyết định' rules={[...rules.text, ...rules.length(250)]}>
							<Input placeholder='Loại quyết định' disabled={isView} />
						</Form.Item>
					</Col>
					<Col span={24} md={12}>
						<Form.Item
							name='ngayQĐ'
							label='Ngày quyết định'
							rules={[...rules.nhoHonBangHomNay, ...(ngaySinh ? rules.sauNgay(ngaySinh, 'ngày sinh') : [])]}
						>
							<MyDatePicker placeholder='Ngày quyết định' allowClear disabled={isView} />
						</Form.Item>
					</Col>
					<Col span={24} md={12}>
						<Form.Item
							name='ngayCoHieuLuc'
							label='Ngày có hiệu lực'
							rules={[...rules.nhoHonBangHomNay, ...(ngaySinh ? rules.sauNgay(ngaySinh, 'ngày sinh') : [])]}
						>
							<MyDatePicker placeholder='Ngày có hiệu lực' allowClear disabled={isView} />
						</Form.Item>
					</Col>
					<Col span={24}>
						<Form.Item
							name='fileDinhKem'
							label='File đính kèm'
							// rules={isGiangVienThinhGiangHoacNhanSuKhac ? [] : [...rules.fileRequired]}
						>
							<UploadFile
								maxCount={1}
								otherProps={{
									maxCount: 1,
									multiple: false,
								}}
							/>
						</Form.Item>
					</Col>
					<Col span={24} xl={24}>
						<Form.Item name='noiDung' label='Nội dung' rules={[...rules.text, ...rules.length(550)]}>
							<Input.TextArea placeholder='Nội dung' disabled={isView} />
						</Form.Item>
					</Col>
				</Row>
			</Form>
			<div style={{ textAlign: 'center', marginTop: 24 }}>
				{!isView && (
					<Button form='FormHocHam' style={{ marginRight: 8 }} htmlType='submit' type='primary'>
						{!isEdit ? 'Thêm mới ' : 'Lưu lại'}
					</Button>
				)}
				<Button disabled={false} onClick={onCancel}>Đóng</Button>
			</div>
		</CardForm>
	);
};
