
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { Button, Popconfirm, Tooltip, type FormInstance, Form, Divider, Checkbox } from 'antd';
import { useWatch } from 'antd/lib/form/Form';
import { useState } from 'react';
import {
	FormTrinhDoLyLuanChinhTri,
	type FormTrinhDoLyLuanChinhTriProps,
	type FormTrinhDoLyLuanChinhTriValues,
} from './components/Form';
import moment from 'moment';
import { FormValues } from '../../../KhenThuong';
import { IColumn } from '../../../../TableStaticData/typing';
import TableStaticData from '../../../../TableStaticData';
import {buildUpLoadFile} from "../../../../../utils/uploadFile";

interface Props {
	form: FormInstance<FormValues>;
	disabled: boolean;
}

type TableRecord = FormTrinhDoLyLuanChinhTriValues;
export const TrinhDoLyLuanChinhTri = ({ form, disabled }: Props) => {
	const dsThongTinTrinhDoLyLuanChinhTri = useWatch(['dsThongTinTrinhDoLyLuanChinhTri'], form);

	const [visibleForm, setVisibleForm] = useState(false);
	const [record, setRecord] = useState<FormTrinhDoLyLuanChinhTriValues>();
	const [isEdit, setIsEdit] = useState(false);
	const [isView, setIsView] = useState(false);
	const [indexRecordDangThaoTac, setIndexRecordDangThaoTac] = useState<number | undefined>(undefined);

	const handleCreate = (vi: boolean) => {
		setVisibleForm(vi);
		setIsEdit(false);
		setIsView(false);
		setRecord(undefined);
	};

	const handleEdit = (tableRecord: TableRecord, index: number) => {
		setVisibleForm(true);
		setIsEdit(true);
		setIsView(false);
		setRecord(tableRecord);
		setIndexRecordDangThaoTac(index);
	};

	const handleView = (tableRecord: TableRecord) => {
		setVisibleForm(true);
		setIsView(true);
		setIsEdit(false);
		setRecord(tableRecord);
	};

	const handleCloseModal = () => {
		setRecord(undefined);
		setVisibleForm(false);
		setIsEdit(false);
		setIsView(false);
	};

	const handleDelete = (index: number) => {
		const prevDsTrinhDoLyLuanChinhTri: Required<FormValues>['dsThongTinTrinhDoLyLuanChinhTri'] =
			form.getFieldValue('dsThongTinTrinhDoLyLuanChinhTri') ?? [];
		form.setFieldsValue({
			dsThongTinTrinhDoLyLuanChinhTri: prevDsTrinhDoLyLuanChinhTri.filter((_, itemIndex) => itemIndex !== index),
		});
	};

	const handleSubmitForm = async (values: FormTrinhDoLyLuanChinhTriValues) => {
		const prevDsTrinhDoLyLuanChinhTri_: Required<FormValues>['dsThongTinTrinhDoLyLuanChinhTri'] =
			form.getFieldValue('dsThongTinTrinhDoLyLuanChinhTri') ?? [];

		const prevDsTrinhDoLyLuanChinhTri = values.apDung
			? prevDsTrinhDoLyLuanChinhTri_.map((item) => ({ ...item, apDung: false }))
			: prevDsTrinhDoLyLuanChinhTri_;

		const url = await buildUpLoadFile(values, "fileDinhKem");
		const payload = {
			...values,
			fileDinhKem: url,
		};
		if (!isEdit) {
			form.setFieldsValue({
				dsThongTinTrinhDoLyLuanChinhTri: prevDsTrinhDoLyLuanChinhTri.prepend(payload),
			});
		} else if (isEdit) {
			form.setFieldsValue({
				dsThongTinTrinhDoLyLuanChinhTri: prevDsTrinhDoLyLuanChinhTri.map((item, index) => {
					if (indexRecordDangThaoTac === index) {
						return {
							...item,
							...payload,
						};
					}
					return item;
				}),
			});
		}

		setIndexRecordDangThaoTac(undefined);
		setVisibleForm(false);
		setIsEdit(false);
		setIsView(false);
	};

	const onCell = (rec: TableRecord) => ({
		onClick: () => handleView(rec),
		style: { cursor: 'pointer' },
	});
	const columns: IColumn<TableRecord>[] = [
		{
			title: 'Hình thức đào tạo',
			dataIndex: ['hinhThucDaoTao', 'ten'],
			width: 160,
			filterType: 'string',
			onCell,
		},
		{
			title: 'Trình độ lý luận chính trị',
			dataIndex: ['trinhDoLyLuanChinhTri', 'ten'],
			width: 160,
			filterType: 'string',
			onCell,
		},
		{
			title: 'Cơ sở đào tạo',
			dataIndex: 'coSoDaoTao',
			width: 160,
			onCell,
			filterType: 'string',
			render: (_, tableRecord) => {
				return tableRecord.coSoDaoTao;
			},
		},
		{
			title: 'Thời gian bắt đầu',
			dataIndex: 'thoiGianBatDau',
			width: 140,
			align: 'center',
			onCell,
			sortable: true,
			filterType: 'date',
			render: (_, tableRecord) => {
				return tableRecord.thoiGianBatDau ? moment(tableRecord.thoiGianBatDau).format('DD/MM/YYYY') : null;
			},
		},
		{
			title: 'Thời gian kết thúc',
			dataIndex: 'thoiGianKetThuc',
			width: 140,
			align: 'center',
			onCell,
			sortable: true,
			filterType: 'date',
			render: (_, tableRecord) => {
				return tableRecord.thoiGianKetThuc ? moment(tableRecord.thoiGianKetThuc).format('DD/MM/YYYY') : null;
			},
		},
		{
			title: 'Là trình độ chính',
			width: 120,
			dataIndex: 'apDung',
			align: 'center',
			render: (value) => (value ? <Checkbox disabled checked={value} /> : null),
		},
	];

	if (!disabled) {
		columns.push({
			title: 'Thao tác',
			align: 'center',
			width: 90,
			fixed: 'right',
			render: (_, rec, index) => {
				return (
					<>
						<Tooltip title='Chỉnh sửa'>
							<Button type='link' icon={<EditOutlined rev={undefined} />} onClick={() => handleEdit(rec, index)} />
						</Tooltip>

						<Tooltip title='Xóa'>
							<Popconfirm
								onConfirm={() => handleDelete(index)}
								title='Bạn có chắc chắn muốn xóa trình độ lý luận chính trị này?'
								placement='topLeft'
							>
								<Button danger type='link' icon={<DeleteOutlined rev={undefined} />} />
							</Popconfirm>
						</Tooltip>
					</>
				);
			},
		});
	}

	return (
		<Form.Item name='dsThongTinTrinhDoLyLuanChinhTri'>
			<Divider orientation='center'>Trình độ lý luận chính trị</Divider>
			<TableStaticData
				pagination={false}
				hasCreate={!disabled}
				hasTotal
				showEdit={visibleForm}
				setShowEdit={handleCreate}
				data={dsThongTinTrinhDoLyLuanChinhTri ?? []}
				columns={columns}
				addStt
				Form={FormTrinhDoLyLuanChinhTri}
				widthDrawer={700}
				formProps={
					{
						record,
						isEdit,
						isView,
						onCancel: handleCloseModal,
						onFinish: handleSubmitForm,
					} as FormTrinhDoLyLuanChinhTriProps
				}
			/>
		</Form.Item>
	);
};
