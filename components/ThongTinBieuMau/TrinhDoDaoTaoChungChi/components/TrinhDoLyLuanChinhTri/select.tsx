import { PlusOutlined } from "@ant-design/icons";
import { Button, Modal, Select } from "antd";
import { useEffect, useState } from "react";
import { type BaseOptionType } from "antd/lib/select";
import { getNganhDaoTao } from "../../../../../service/core/nganhdaotao";
import { getLyLuan } from "../../../../../service/core/core";

const SelectTrinhDoLyLuanChinhTri = (props: {
  value?: string;
  onChange?: (value?: string, option?: BaseOptionType) => void;
  hasCreate?: boolean;
  multiple?: boolean;
  allowClear?: boolean;
  disabled?: boolean;
  placeholder?: string;
}) => {
  const {
    value,
    onChange,
    hasCreate,
    multiple,
    allowClear,
    disabled,
    placeholder,
  } = props;
  const [danhSach, setDanhSach] = useState<TrinhDoLyLuanChinhTri.IRecord[]>([]);
  const getData = async () => {
    try {
      const res = await getLyLuan();
      setDanhSach(res?.data?.data);
    } catch (e) {}
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <Select
      mode={multiple ? "multiple" : undefined}
      value={value}
      allowClear={allowClear}
      onChange={onChange}
      options={danhSach.map((item) => ({
        key: item._id,
        value: item._id,
        label: item.ten,
        rawData: item,
      }))}
      disabled={disabled}
      showSearch
      optionFilterProp="label"
      placeholder={placeholder ?? "Chọn trình độ lý luận chính trị"}
    />
  );
};

export default SelectTrinhDoLyLuanChinhTri;
