
import { Button, Card, Checkbox, Col, Form, Input, Row } from 'antd';
import { useWatch } from 'antd/lib/form/Form';
import { useEffect } from 'react';
import rules from '../../../../../../utils/rules';
import { resetFieldsForm } from '../../../../../../utils/util';
import CardForm from '../../../../../CardForm';
import MyDatePicker from '../../../../../MyDatePicker';
import UploadFile from '../../../../../Upload/UploadFile';
import { FormValues } from '../../../../KhenThuong';
import SelectHinhThucDaoTao from '../../HinhThucDaoTao/select';
import SelectTrinhDoLyLuanChinhTri from '../select';

export type FormTrinhDoLyLuanChinhTriValues = Required<FormValues>['dsThongTinTrinhDoLyLuanChinhTri'][number];

export interface FormTrinhDoLyLuanChinhTriProps {
	onFinish: (values: FormTrinhDoLyLuanChinhTriValues) => void;
	onCancel: () => void;
	isEdit: boolean;
	isView: boolean;
	record?: FormTrinhDoLyLuanChinhTriValues;
	ngaySinh?: string;
}

export const FormTrinhDoLyLuanChinhTri = ({
	isEdit,
	isView,
	onCancel,
	onFinish,
	record,
	ngaySinh,
}: FormTrinhDoLyLuanChinhTriProps) => {
	const [form] = Form.useForm<FormTrinhDoLyLuanChinhTriValues>();

	const thoiGianBatDau = useWatch(['thoiGianBatDau'], form);

	useEffect(() => {
		resetFieldsForm(form, record);
	}, [isEdit, isView, record]);

	return (
		<CardForm bordered={false} title={(isView ? 'Xem chi tiết ' : isEdit ? 'Chỉnh sửa ' : 'Thêm mới ') + 'trình độ lý luận chính trị'}>
			<Form id='FormTrinhDoLyLuanChinhTri' onFinish={onFinish} form={form} layout='vertical' disabled={isView}>
				<Form.Item name='hinhThucDaoTao' hidden />
				<Form.Item name='trinhDoLyLuanChinhTri' hidden />
				<Row gutter={[12, 0]} style={{ marginBottom: 12 }}>
					<Col span={24} md={12}>
						<Form.Item
							name='thoiGianBatDau'
							// rules={[...rules.required]}
							label='Thời gian bắt đầu'
						>
							<MyDatePicker placeholder='Thời gian bắt đầu' />
						</Form.Item>
					</Col>
					<Col span={24} md={12}>
						<Form.Item
							name='thoiGianKetThuc'
							label='Thời gian kết thúc'
							rules={[
								// ...rules.required,
								...rules.sauNgay(thoiGianBatDau, 'thời gian bắt đầu'),
							]}
						>
							<MyDatePicker placeholder='Thời gian kết thúc' />
						</Form.Item>
					</Col>
					<Col span={24} md={12}>
						<Form.Item name='hinhThucDaoTaoId' label='Hình thức đào tạo' rules={[...rules.required]}>
							<SelectHinhThucDaoTao
								hasCreate={false}
								placeholder='Hình thức đào tạo'
								onChange={(_, option) => {
									const rawData = option?.rawData as HinhThucDaoTao.IRecord | undefined;
									form.setFieldsValue({
										hinhThucDaoTao: rawData ?? null,
									});
								}}
							/>
						</Form.Item>
					</Col>
					<Col span={24} md={12}>
						<Form.Item name='trinhDoLyLuanChinhTriId' label='Trình độ lý luận chính trị' rules={[...rules.required]}>
							<SelectTrinhDoLyLuanChinhTri
								hasCreate={false}
								placeholder='Trình độ lý luận chính trị'
								onChange={(_, option) => {
									const rawData = option?.rawData as TrinhDoLyLuanChinhTri.IRecord | undefined;
									form.setFieldsValue({
										trinhDoLyLuanChinhTri: rawData ?? null,
									});
								}}
							/>
						</Form.Item>
					</Col>
					<Col span={24} md={12}>
						<Form.Item name='vanBangChungChi' label='Văn bằng/Chứng chỉ/Chứng nhận được cấp'>
							<Input placeholder='Văn bằng/Chứng chỉ/Chứng nhận được cấp' />
						</Form.Item>
					</Col>
					<Col span={24} md={12}>
						<Form.Item
							name='coSoDaoTao'
							rules={[...rules.required, ...rules.text, ...rules.length(255)]}
							label='Cơ sở đào tạo'
						>
							<Input placeholder='Cơ sở đào tạo' />
						</Form.Item>
					</Col>
					<Col span={24} md={12}>
						<Form.Item
							name='fileDinhKem'
							label='File đính kèm'
							// rules={[...rules.fileRequired]}
						>
							<UploadFile
								maxCount={1}
								otherProps={{
									maxCount: 1,
									multiple: false,
								}}
							/>
						</Form.Item>
					</Col>
					<Col span={24} md={12} style={{ display: 'flex', alignItems: 'center' }}>
						<Form.Item name='apDung' valuePropName='checked' initialValue={false}>
							<Checkbox disabled={isView}>Là trình độ chính</Checkbox>
						</Form.Item>
					</Col>
				</Row>
			</Form>

			<div style={{ textAlign: 'center', marginTop: 24 }}>
				{!isView && (
					<Button form='FormTrinhDoLyLuanChinhTri' style={{ marginRight: 8 }} htmlType='submit' type='primary'>
						{!isEdit ? 'Thêm mới ' : 'Lưu lại'}
					</Button>
				)}
				<Button
					disabled={false}
					onClick={() => {
						onCancel();
						resetFieldsForm(form);
					}}
				>
					Đóng
				</Button>
			</div>
		</CardForm>
	);
};
