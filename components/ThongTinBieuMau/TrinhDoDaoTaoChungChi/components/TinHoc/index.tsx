
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { Button, Popconfirm, Tooltip, type FormInstance, Form, Divider, Checkbox } from 'antd';
import { useWatch } from 'antd/lib/form/Form';
import { useState } from 'react';
import { FormTinHoc, type FormTinHocProps, type FormTinHocValues } from './components/Form';
import moment from 'moment';
import { FormValues } from '../../../KhenThuong';
import { IColumn } from '../../../../TableStaticData/typing';
import TableStaticData from '../../../../TableStaticData';
import {buildUpLoadFile} from "../../../../../utils/uploadFile";

interface Props {
	form: FormInstance<FormValues>;
	disabled: boolean;
}

type TableRecord = FormTinHocValues;
export const TinHoc = ({ form, disabled }: Props) => {
	const dsThongTinTinHoc = useWatch(['dsThongTinTinHoc'], form);

	const [visibleForm, setVisibleForm] = useState(false);
	const [record, setRecord] = useState<FormTinHocValues>();
	const [isEdit, setIsEdit] = useState(false);
	const [isView, setIsView] = useState(false);
	const [indexRecordDangThaoTac, setIndexRecordDangThaoTac] = useState<number | undefined>(undefined);

	const handleCreate = (vi: boolean) => {
		setVisibleForm(vi);
		setIsEdit(false);
		setIsView(false);
		setRecord(undefined);
	};

	const handleEdit = (tableRecord: TableRecord, index: number) => {
		setVisibleForm(true);
		setIsEdit(true);
		setIsView(false);
		setRecord(tableRecord);
		setIndexRecordDangThaoTac(index);
	};

	const handleView = (tableRecord: TableRecord) => {
		setVisibleForm(true);
		setIsView(true);
		setIsEdit(false);
		setRecord(tableRecord);
	};

	const handleCloseModal = () => {
		setRecord(undefined);
		setVisibleForm(false);
		setIsEdit(false);
		setIsView(false);
	};

	const handleDelete = (index: number) => {
		const prevDsTinHoc: Required<FormValues>['dsThongTinTinHoc'] = form.getFieldValue('dsThongTinTinHoc') ?? [];
		form.setFieldsValue({
			dsThongTinTinHoc: prevDsTinHoc.filter((_, itemIndex) => itemIndex !== index),
		});
	};

	const handleSubmitForm = async (values: FormTinHocValues) => {
		const prevDsTinHoc_: Required<FormValues>['dsThongTinTinHoc'] = form.getFieldValue('dsThongTinTinHoc') ?? [];

		const prevDsTinHoc = values.apDung ? prevDsTinHoc_.map((item) => ({ ...item, apDung: false })) : prevDsTinHoc_;
		const url = await buildUpLoadFile(values, "fileDinhKem");
		const payload = {
			...values,
			fileDinhKem: url,
		};
		if (!isEdit) {
			form.setFieldsValue({
				dsThongTinTinHoc: prevDsTinHoc.prepend(payload),
			});
		} else if (isEdit) {
			form.setFieldsValue({
				dsThongTinTinHoc: prevDsTinHoc.map((item, index) => {
					if (indexRecordDangThaoTac === index) {
						return {
							...item,
							...payload,
						};
					}
					return item;
				}),
			});
		}

		setIndexRecordDangThaoTac(undefined);
		setVisibleForm(false);
		setIsEdit(false);
		setIsView(false);
	};

	const onCell = (rec: TableRecord) => ({
		onClick: () => handleView(rec),
		style: { cursor: 'pointer' },
	});
	const columns: IColumn<TableRecord>[] = [
		{
			title: 'Hình thức đào tạo',
			dataIndex: ['hinhThucDaoTao', 'ten'],
			width: 160,
			onCell,
			filterType: 'string',
		},
		{
			title: 'Trình độ tin học',
			dataIndex: ['tinHoc', 'trinhDoTinHoc'],
			width: 160,
			onCell,
			filterType: 'string',
		},
		{
			title: 'Cơ sở đào tạo',
			dataIndex: 'coSoDaoTao',
			width: 160,
			onCell,
			filterType: 'string',
		},
		{
			title: 'Thời gian bắt đầu',
			dataIndex: 'thoiGianBatDau',
			width: 140,
			align: 'center',
			onCell,
			sortable: true,
			filterType: 'date',
			render: (_, tableRecord) => {
				return tableRecord.thoiGianBatDau ? moment(tableRecord.thoiGianBatDau).format('DD/MM/YYYY') : null;
			},
		},
		{
			title: 'Thời gian kết thúc',
			dataIndex: 'thoiGianKetThuc',
			width: 140,
			align: 'center',
			onCell,
			sortable: true,
			filterType: 'date',
			render: (_, tableRecord) => {
				return tableRecord.thoiGianKetThuc ? moment(tableRecord.thoiGianKetThuc).format('DD/MM/YYYY') : null;
			},
		},
		{
			title: 'Là trình độ chính',
			width: 120,
			dataIndex: 'apDung',
			align: 'center',
			render: (value) => (value ? <Checkbox disabled checked={value} /> : null),
		},
	];

	if (!disabled) {
		columns.push({
			title: 'Thao tác',
			align: 'center',
			width: 90,
			fixed: 'right',
			render: (_, rec, index) => {
				return (
					<>
						<Tooltip title='Chỉnh sửa'>
							<Button type='link' icon={<EditOutlined rev={undefined} />} onClick={() => handleEdit(rec, index)} />
						</Tooltip>

						<Tooltip title='Xóa'>
							<Popconfirm
								onConfirm={() => handleDelete(index)}
								title='Bạn có chắc chắn muốn xóa tin học này?'
								placement='topLeft'
							>
								<Button danger type='link' icon={<DeleteOutlined rev={undefined} />} />
							</Popconfirm>
						</Tooltip>
					</>
				);
			},
		});
	}

	return (
		<Form.Item name='dsThongTinTinHoc'>
			<Divider orientation='center'>Tin học</Divider>
			<TableStaticData
				hasCreate={!disabled}
				hasTotal
				showEdit={visibleForm}
				setShowEdit={handleCreate}
				data={dsThongTinTinHoc ?? []}
				columns={columns}
				addStt
				Form={FormTinHoc}
				widthDrawer={700}
				formProps={
					{
						record,
						isEdit,
						isView,
						onCancel: handleCloseModal,
						onFinish: handleSubmitForm,
					} as FormTinHocProps
				}
			/>
		</Form.Item>
	);
};
