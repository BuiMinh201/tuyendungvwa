
import { Button, Card, Col, Form, Input, Row } from 'antd';
import { useWatch } from 'antd/lib/form/Form';
import { useEffect } from 'react';
import rules from '../../../../../../utils/rules';
import { resetFieldsForm } from '../../../../../../utils/util';
import MyDatePicker from '../../../../../MyDatePicker';
import UploadFile from '../../../../../Upload/UploadFile';
import { FormValues } from '../../../../KhenThuong';
import CardForm from "../../../../../CardForm";

export type FormBoiDuongQPANValues = Required<FormValues>['dsBoiDuongQuocPhongAnNinh'][number];

export interface FormBoiDuongQPANProps {
	onFinish: (values: FormBoiDuongQPANValues) => void;
	onCancel: () => void;
	isEdit: boolean;
	isView: boolean;
	record?: FormBoiDuongQPANValues;
	ngaySinh?: string;
}

export const FormBoiDuongQPAN = ({ isEdit, isView, onCancel, onFinish, record, ngaySinh }: FormBoiDuongQPANProps) => {
	const [form] = Form.useForm<FormBoiDuongQPANValues>();

	const thoiGianBatDau = useWatch(['thoiGianBatDau'], form);

	useEffect(() => {
		resetFieldsForm(form, record);
	}, [isEdit, isView, record]);

	return (
		<CardForm bordered={false} title={(isView ? 'Xem chi tiết ' : isEdit ? 'Chỉnh sửa ' : 'Thêm mới ') + 'bồi dưỡng Quốc phòng - An ninh'}>
			<Form id='FormBoiDuongQPAN' onFinish={onFinish} form={form} layout='vertical' disabled={isView}>
				<Row gutter={[12, 0]} style={{ marginBottom: 12 }}>
					<Col span={24} md={12}>
						<Form.Item name='thoiGianBatDau' label='Thời gian bắt đầu'>
							<MyDatePicker placeholder='Thời gian bắt đầu' />
						</Form.Item>
					</Col>
					<Col span={24} md={12}>
						<Form.Item
							name='thoiGianKetThuc'
							label='Thời gian kết thúc'
							rules={[...rules.sauNgay(thoiGianBatDau, 'thời gian bắt đầu')]}
						>
							<MyDatePicker placeholder='Thời gian kết thúc' />
						</Form.Item>
					</Col>
					{/* <Col span={24} md={12}>
						<Form.Item name='hinhThucDaoTaoId' label='Hình thức đào tạo' rules={[...rules.required]}>
							<SelectHinhThucDaoTao hasCreate={false} placeholder='Hình thức đào tạo' />
						</Form.Item>
					</Col> */}
					<Col span={24} md={12}>
						<Form.Item name='vanBangChungChi' label='Văn bằng/Chứng chỉ/Chứng nhận được cấp'>
							<Input placeholder='Văn bằng/Chứng chỉ/Chứng nhận được cấp' />
						</Form.Item>
					</Col>
					<Col span={24} md={12}>
						<Form.Item
							name='coSoDaoTao'
							rules={[...rules.required, ...rules.text, ...rules.length(255)]}
							label='Cơ sở đào tạo'
						>
							<Input placeholder='Cơ sở đào tạo' />
						</Form.Item>
					</Col>
					<Col span={24} md={12}>
						<Form.Item
							name='fileDinhKem'
							label='File đính kèm'
							// rules={[...rules.fileRequired]}
						>
							<UploadFile
								maxCount={1}
								otherProps={{
									maxCount: 1,
									multiple: false,
								}}
							/>
						</Form.Item>
					</Col>
				</Row>
			</Form>

			<div style={{ textAlign: 'center', marginTop: 24 }}>
				{!isView && (
					<Button form='FormBoiDuongQPAN' style={{ marginRight: 8 }} htmlType='submit' type='primary'>
						{!isEdit ? 'Thêm mới ' : 'Lưu lại'}
					</Button>
				)}
				<Button
					disabled={false}
					onClick={() => {
						onCancel();
						resetFieldsForm(form);
					}}
				>
					Đóng
				</Button>
			</div>
		</CardForm>
	);
};
