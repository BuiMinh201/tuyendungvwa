import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import {
  Button,
  Divider,
  Form,
  Popconfirm,
  Tooltip,
  type FormInstance,
} from "antd";
import { useWatch } from "antd/lib/form/Form";
import moment from "moment";
import { useState } from "react";
import {
  FormBoiDuongQPAN,
  type FormBoiDuongQPANProps,
  type FormBoiDuongQPANValues,
} from "./components/Form";
import { FormValues } from "../../../KhenThuong";
import { IColumn } from "../../../../TableStaticData/typing";
import TableStaticData from "../../../../TableStaticData";
import { buildUpLoadFile } from "../../../../../utils/uploadFile";

interface Props {
  form: FormInstance<FormValues>;
  disabled: boolean;
}

type TableRecord = FormBoiDuongQPANValues;
export const BoiDuongQPAN = ({ form, disabled }: Props) => {
  const dsBoiDuongQuocPhongAnNinh = useWatch(
    ["dsBoiDuongQuocPhongAnNinh"],
    form
  );

  const [visibleForm, setVisibleForm] = useState(false);
  const [record, setRecord] = useState<FormBoiDuongQPANValues>();
  const [isEdit, setIsEdit] = useState(false);
  const [isView, setIsView] = useState(false);
  const [indexRecordDangThaoTac, setIndexRecordDangThaoTac] = useState<
    number | undefined
  >(undefined);

  const handleCreate = (vi: boolean) => {
    setVisibleForm(vi);
    setIsEdit(false);
    setIsView(false);
    setRecord(undefined);
  };

  const handleEdit = (tableRecord: TableRecord, index: number) => {
    setVisibleForm(true);
    setIsEdit(true);
    setIsView(false);
    setRecord(tableRecord);
    setIndexRecordDangThaoTac(index);
  };

  const handleView = (tableRecord: TableRecord) => {
    setVisibleForm(true);
    setIsView(true);
    setIsEdit(false);
    setRecord(tableRecord);
  };

  const handleCloseModal = () => {
    setRecord(undefined);
    setVisibleForm(false);
    setIsEdit(false);
    setIsView(false);
  };

  const handleDelete = (index: number) => {
    const prevDsBoiDuongQPAN: Required<FormValues>["dsBoiDuongQuocPhongAnNinh"] =
      form.getFieldValue("dsBoiDuongQuocPhongAnNinh") ?? [];
    form.setFieldsValue({
      dsBoiDuongQuocPhongAnNinh: prevDsBoiDuongQPAN.filter(
        (_, itemIndex) => itemIndex !== index
      ),
    });
  };

  const handleSubmitForm = async (values: FormBoiDuongQPANValues) => {
    const prevDsBoiDuongQPAN: Required<FormValues>["dsBoiDuongQuocPhongAnNinh"] =
      form.getFieldValue("dsBoiDuongQuocPhongAnNinh") ?? [];
    const url = await buildUpLoadFile(values, "fileDinhKem");
    const payload = {
      ...values,
      fileDinhKem: url,
    };
    if (!isEdit) {
      form.setFieldsValue({
        dsBoiDuongQuocPhongAnNinh: prevDsBoiDuongQPAN.prepend(payload),
      });
    } else if (isEdit) {
      form.setFieldsValue({
        dsBoiDuongQuocPhongAnNinh: prevDsBoiDuongQPAN.map((item, index) => {
          if (indexRecordDangThaoTac === index) {
            return {
              ...item,
              ...payload,
            };
          }
          return item;
        }),
      });
    }

    setIndexRecordDangThaoTac(undefined);
    setVisibleForm(false);
    setIsEdit(false);
    setIsView(false);
  };

  const onCell = (rec: TableRecord) => ({
    onClick: () => handleView(rec),
    style: { cursor: "pointer" },
  });
  const columns: IColumn<TableRecord>[] = [
    {
      title: "Cơ sở đào tạo",
      dataIndex: "coSoDaoTao",
      width: 160,
      onCell,
      filterType: "string",
    },
    {
      title: "Thời gian bắt đầu",
      dataIndex: "thoiGianBatDau",
      width: 140,
      align: "center",
      onCell,
      sortable: true,
      filterType: "date",
      render: (_, tableRecord) => {
        return tableRecord.thoiGianBatDau
          ? moment(tableRecord.thoiGianBatDau).format("DD/MM/YYYY")
          : null;
      },
    },
    {
      title: "Thời gian kết thúc",
      dataIndex: "thoiGianKetThuc",
      width: 140,
      align: "center",
      onCell,
      sortable: true,
      filterType: "date",
      render: (_, tableRecord) => {
        return tableRecord.thoiGianKetThuc
          ? moment(tableRecord.thoiGianKetThuc).format("DD/MM/YYYY")
          : null;
      },
    },
  ];

  if (!disabled) {
    columns.push({
      title: "Thao tác",
      align: "center",
      width: 90,
      fixed: "right",
      render: (_, rec, index) => {
        return (
          <>
            <Tooltip title="Chỉnh sửa">
              <Button
                type="link"
                icon={<EditOutlined rev={undefined} />}
                onClick={() => handleEdit(rec, index)}
              />
            </Tooltip>

            <Tooltip title="Xóa">
              <Popconfirm
                onConfirm={() => handleDelete(index)}
                title="Bạn có chắc chắn muốn xóa bồi dưỡng Quốc phòng - An ninh này?"
                placement="topLeft"
              >
                <Button
                  danger
                  type="link"
                  icon={<DeleteOutlined rev={undefined} />}
                />
              </Popconfirm>
            </Tooltip>
          </>
        );
      },
    });
  }

  return (
    <Form.Item name="dsBoiDuongQuocPhongAnNinh">
      <Divider orientation="center">Bồi dưỡng Quốc phòng - An ninh</Divider>
      <TableStaticData
        hasCreate={!disabled}
        hasTotal
        showEdit={visibleForm}
        setShowEdit={handleCreate}
        data={dsBoiDuongQuocPhongAnNinh ?? []}
        columns={columns}
        addStt
        Form={FormBoiDuongQPAN}
        widthDrawer={800}
        formProps={
          {
            record,
            isEdit,
            isView,
            onCancel: handleCloseModal,
            onFinish: handleSubmitForm,
          } as FormBoiDuongQPANProps
        }
      />
    </Form.Item>
  );
};
