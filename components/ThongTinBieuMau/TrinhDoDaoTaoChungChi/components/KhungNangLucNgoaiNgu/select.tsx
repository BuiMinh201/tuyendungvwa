import { Select } from 'antd';
import { type BaseOptionType } from 'antd/lib/select';
import {useEffect, useState} from 'react';
import {getKhungNgoaiNgu} from "../../../../../service/core/ngoaingu";

export const SelectKhungNangLucNgoaiNguHemis = (props: {
  value?: string;
  onChange?: (value?: string, option?: BaseOptionType) => void;
  multiple?: boolean;
  placeholder?: string;
  disabled?: boolean;
  hienThiMa?: false;
  tenNgoaiNgu?: string;
}) => {
  const { value, onChange, multiple, placeholder, disabled, hienThiMa = true, tenNgoaiNgu } = props;
  const [danhSach, setDanhSach] = useState<KhungNangLucNgoaiNgu.IRecord[]>([]);
  const getData = async () => {
    try {
      const res = await getKhungNgoaiNgu();
      setDanhSach(res?.data?.data);
    } catch (e) {}
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div style={{ display: 'flex', gap: 8 }}>
      <div className={'fullWidth'}>
        <Select
          disabled={disabled}
          mode={multiple ? 'multiple' : undefined}
          value={value}
          onChange={onChange}
          options={danhSach.map((item) => ({
            key: item._id,
            value: item._id,
            label: hienThiMa ? `${item.khungNangLuc} (${item.ma})` : item.khungNangLuc,
            rawData: item,
          }))}
          showSearch
          optionFilterProp='label'
          placeholder={placeholder ?? 'Chọn ngoại ngữ'}
        />
      </div>
    </div>
  );
};
