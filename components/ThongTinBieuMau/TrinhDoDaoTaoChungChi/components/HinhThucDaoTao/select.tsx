import { PlusOutlined } from '@ant-design/icons';
import { Button, Modal, Select } from 'antd';
import {type CSSProperties, useEffect, useState} from 'react';
import { type BaseOptionType } from 'antd/lib/select';
import {getLoaiBoiDuong} from "../../../../../service/core/loaiboiduong";
import {getNganhDaoTao} from "../../../../../service/core/nganhdaotao";
import {getHinhThucDaoTao} from "../../../../../service/core/hinhthucdaotao";

interface Props {
  disabled?: boolean;
  value?: string;
  onChange?: (value?: string, option?: BaseOptionType) => void;
  hasCreate?: boolean;
  multiple?: boolean;
  placeholder?: string;
  style?: CSSProperties;
}

const SelectHinhThucDaoTao = (props: Props) => {
  const { value, onChange, hasCreate, multiple, placeholder, style, disabled } = props;
  const [danhSach, setDanhSach] = useState<HinhThucDaoTao.IRecord[]>([]);
  const getData = async () => {
    try {
      const res = await getHinhThucDaoTao();
      setDanhSach(res?.data?.data);
    } catch (e) {}
  };

  useEffect(() => {
    getData();
  }, []);

  return (
        <Select
          disabled={disabled}
          style={style}
          mode={multiple ? 'multiple' : undefined}
          value={value}
          onChange={onChange}
          options={danhSach.map((item) => ({
            key: item._id,
            value: item._id,
            label: item.ten,
            rawData: item,
          }))}
          showSearch
          optionFilterProp='label'
          placeholder={placeholder ?? 'Chọn hình thức đào tạo'}
        />
  );
};

export default SelectHinhThucDaoTao;
