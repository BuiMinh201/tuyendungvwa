import { Col, Form, Input, Row, Select } from "antd";
import { type FormInstance } from "antd/es/form/Form";
import { ETrinhDoGiaoDucPhoThong } from "../../../utils/constant";
import rules from "../../../utils/rules";
import { FormValues } from "../KhenThuong";
import { TrinhDoDaoTao } from "./components/TrinhDoDaoTao";
import { HocHam } from "./components/HocHam";
import { TrinhDoLyLuanChinhTri } from "./components/TrinhDoLyLuanChinhTri";
import { TrinhDoQuanLyHanhChinh } from "./components/TrinhDoQuanLyHanhChinh";
import { TrinhDoQuanLyNhaNuoc } from "./components/TrinhDoQuanLyNhaNuoc";
import { NgoaiNgu } from "./components/NgoaiNgu";
import { TinHoc } from "./components/TinHoc";
import { BoiDuongQPAN } from "./components/BoiDuongQPAN";
import { QuaTrinhDaoTaoBoiDuong } from "./components/QuaTrinhDaoTaoBoiDuong";

interface Props {
  form: FormInstance<FormValues>;
  disabled?: boolean;
  isXemChiTietHoSo: boolean;
}

export const TrinhDoDaoTaoChungChi = ({
  form,
  disabled,
  isXemChiTietHoSo = false,
}: Props) => {
  return (
    <>
      <Row gutter={12}>
        <Col xl={6} span={24}>
          <Form.Item
            name="trinhDoGiaoDucPhoThongId"
            label={"Trình độ giáo dục phổ thông"}
          >
            <Select
              disabled={disabled}
              allowClear
              placeholder="Chọn trình độ giáo dục phổ thông"
              showSearch
              options={Object.values(ETrinhDoGiaoDucPhoThong).map((item) => ({
                key: item,
                value: item,
                label: item,
              }))}
            />
          </Form.Item>
        </Col>
        <Col xl={6} span={24}>
          <Form.Item
            name="soTruongCongTac"
            label={"Sở trường công tác"}
            rules={[...rules.text, ...rules.length(250)]}
          >
            <Input placeholder="Sở trường công tác" disabled={disabled} />
          </Form.Item>
        </Col>
      </Row>

      <HocHam
        isGiangVienThinhGiangHoacNhanSuKhac={false}
        form={form}
        disabled={disabled}
      />

      <TrinhDoDaoTao
        isGiangVienThinhGiangHoacNhanSuKhac={false}
        form={form}
        disabled={disabled}
      />

      <TrinhDoLyLuanChinhTri form={form} disabled={disabled??false} />

      <TrinhDoQuanLyHanhChinh form={form} disabled={disabled??false} />

      <TrinhDoQuanLyNhaNuoc form={form} disabled={disabled??false} />

      <NgoaiNgu form={form} disabled={disabled??false} />
      <TinHoc form={form} disabled={disabled??false} />
      <BoiDuongQPAN form={form} disabled={disabled??false} />

      <QuaTrinhDaoTaoBoiDuong form={form} disabled={disabled??false} />
    </>
  );
};
