import { type FormInstance } from "antd/es/form/Form";
import SucKhoe from "../SucKhoe";
import { FormValues } from "../KhenThuong";
import TaiKhoan from "../TaiKhoan";
import { DoiTuongChinhSach } from "./components/DoiTuongChinhSach";

interface Props {
  form: FormInstance<FormValues>;
  disabled: boolean;
  isXemChiTietHoSo: boolean;
}

export const ThongTinKhac = ({
  form,
  disabled,
  isXemChiTietHoSo = false,
}: Props) => {
  return (
    <>
      <SucKhoe disabled={disabled} />
      <TaiKhoan form={form} disabled={disabled}/>
      <DoiTuongChinhSach form={form} disabled={disabled} />
    </>
  );
};
