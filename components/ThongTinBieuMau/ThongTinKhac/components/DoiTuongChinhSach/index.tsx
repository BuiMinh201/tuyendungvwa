import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { Button, Divider, Form, FormInstance, Popconfirm, Tooltip } from 'antd';
import { useState } from 'react';
import { FormDoiTuongChinhSach, type FormDoiTuongChinhSachProps, type FormDoiTuongChinhSachValues } from './Form';
import moment from 'moment';
import TableStaticData from "../../../../TableStaticData";
import {IColumn} from "../../../../TableStaticData/typing";
import {FormValues} from "../../../KhenThuong";
import {useWatch} from "antd/lib/form/Form";
import {buildUpLoadFile} from "../../../../../utils/uploadFile";

interface Props {
	form: FormInstance<FormValues>;
	disabled: boolean;
}

type TableRecord = FormDoiTuongChinhSachValues;
export const DoiTuongChinhSach = ({ form, disabled }: Props) => {
	const dsDoiTuongChinhSach = useWatch(['dsDoiTuongChinhSach'], form);

	const [visibleForm, setVisibleForm] = useState(false);
	const [record, setRecord] = useState<FormDoiTuongChinhSachValues>();
	const [isEdit, setIsEdit] = useState(false);
	const [isView, setIsView] = useState(false);
	const [indexRecordDangThaoTac, setIndexRecordDangThaoTac] = useState<number | undefined>(undefined);

	const handleCreate = (vi: boolean) => {
		setVisibleForm(vi);
		setIsEdit(false);
		setIsView(false);
		setRecord(undefined);
	};

	const handleEdit = (tableRecord: TableRecord, index: number) => {
		setVisibleForm(true);
		setIsEdit(true);
		setIsView(false);
		setRecord(tableRecord);
		setIndexRecordDangThaoTac(index);
	};

	const handleView = (tableRecord: TableRecord) => {
		setVisibleForm(true);
		setIsView(true);
		setIsEdit(false);
		setRecord(tableRecord);
	};

	const handleCloseModal = () => {
		setRecord(undefined);
		setVisibleForm(false);
		setIsEdit(false);
		setIsView(false);
	};

	const handleDelete = (index: number) => {
		const prevDsDoiTuongChinhSach: Required<FormValues>['dsDoiTuongChinhSach'] =
			form.getFieldValue('dsDoiTuongChinhSach') ?? [];
		form.setFieldsValue({
			dsDoiTuongChinhSach: prevDsDoiTuongChinhSach.filter((_, itemIndex) => itemIndex !== index),
		});
	};

	const handleSubmitForm = async (values: FormDoiTuongChinhSachValues) => {
		const prevDsDoiTuongChinhSach: Required<FormValues>['dsDoiTuongChinhSach'] =
			form.getFieldValue('dsDoiTuongChinhSach') ?? [];
		const url = await buildUpLoadFile(values, "fileDinhKem");
		const payload = {
			...values,
			fileDinhKem: url,
		};
		if (!isEdit) {
			form.setFieldsValue({
				dsDoiTuongChinhSach: prevDsDoiTuongChinhSach.prepend(payload),
			});
		} else if (isEdit) {
			form.setFieldsValue({
				dsDoiTuongChinhSach: prevDsDoiTuongChinhSach.map((item, index) => {
					if (indexRecordDangThaoTac === index) {
						return {
							...item,
							...payload,
						};
					}
					return item;
				}),
			});
		}

		setIndexRecordDangThaoTac(undefined);
		setVisibleForm(false);
		setIsEdit(false);
		setIsView(false);
	};

	const onCell = (rec: TableRecord) => ({
		onClick: () => handleView(rec),
		style: { cursor: 'pointer' },
	});
	const columns: IColumn<TableRecord>[] = [
		{
			title: 'Đối tượng',
			width: 120,
			dataIndex: 'doiTuongChinhSach',
			onCell: onCell,
			sortable: true,
			filterType: 'string',
		},

		{
			title: 'Bắt đầu từ',
			dataIndex: 'tuNgay',
			width: 100,
			filterType: 'date',
			align: 'center',
			sortable: true,
			onCell: onCell,
			render: (_, tableRecord) => {
				return tableRecord.tuNgay ? moment(tableRecord.tuNgay).format('DD/MM/YYYY') : null;
			},
		},
		{
			title: 'Kết thúc vào',
			width: 100,
			dataIndex: 'denNgay',
			filterType: 'date',
			align: 'center',
			sortable: true,
			onCell: onCell,
			render: (_, tableRecord) => {
				return tableRecord.denNgay ? moment(tableRecord.denNgay).format('DD/MM/YYYY') : null;
			},
		},
		{
			title: 'Ghi chú',
			// align: 'center',
			dataIndex: 'ghiChu',
			width: 120,
		},
	];

	if (!disabled) {
		columns.push({
			title: 'Thao tác',
			align: 'center',
			width: 90,
			fixed: 'right',
			render: (_, rec, index) => {
				return (
					<>
						<Tooltip title='Chỉnh sửa'>
							<Button type='link' icon={<EditOutlined rev={undefined} />} onClick={() => handleEdit(rec, index)} />
						</Tooltip>

						<Tooltip title='Xóa'>
							<Popconfirm
								onConfirm={() => handleDelete(index)}
								title='Bạn có chắc chắn muốn xóa đối tượng chính sách này?'
								placement='topLeft'
							>
								<Button danger type='link' icon={<DeleteOutlined rev={undefined} />} />
							</Popconfirm>
						</Tooltip>
					</>
				);
			},
		});
	}

	return (
		<Form.Item name='dsDoiTuongChinhSach'>
			<Divider orientation='center'>Đối tượng chính sách</Divider>
			<TableStaticData
				hasCreate={!disabled}
				hasTotal
				showEdit={visibleForm}
				setShowEdit={handleCreate}
				data={dsDoiTuongChinhSach ?? []}
				columns={columns}
				addStt
				widthDrawer={700}
				Form={FormDoiTuongChinhSach}
				formProps={
					{
						record,
						isEdit,
						isView,
						onCancel: handleCloseModal,
						onFinish: handleSubmitForm,
					} as FormDoiTuongChinhSachProps
				}
			/>
		</Form.Item>
	);
};
