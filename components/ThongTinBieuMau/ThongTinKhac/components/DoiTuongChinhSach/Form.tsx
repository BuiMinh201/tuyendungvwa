import { Button, Card, Col, Form, Input, Row, Select } from 'antd';
import { useWatch } from 'antd/lib/form/Form';
import { useEffect } from 'react';
import UploadFile from "../../../../Upload/UploadFile";
import MyDatePicker from "../../../../MyDatePicker";
import rules from "../../../../../utils/rules";
import {resetFieldsForm} from "../../../../../utils/util";
import {FormValues} from "../../../KhenThuong";
import { EDoiTuongChinhSach } from '../../../../../utils/constant';
import CardForm from '../../../../CardForm';

export type FormDoiTuongChinhSachValues = Required<FormValues>['dsDoiTuongChinhSach'][number];

export interface FormDoiTuongChinhSachProps {
	onFinish: (values: FormDoiTuongChinhSachValues) => void;
	onCancel: () => void;
	isEdit: boolean;
	isView: boolean;
	record?: FormDoiTuongChinhSachValues;
}

export const FormDoiTuongChinhSach = ({ isEdit, isView, onCancel, onFinish, record }: FormDoiTuongChinhSachProps) => {
	const [form] = Form.useForm<FormDoiTuongChinhSachValues>();

	const tuNgay = useWatch(['tuNgay'], form);

	useEffect(() => {
		resetFieldsForm(form, record);
	}, [isEdit, isView, record]);
	return (
		<CardForm bordered={false} title={(isView ? 'Chi tiết ' : isEdit ? 'Chỉnh sửa ' : 'Thêm mới ') + 'đối tượng chính sách'}>
			<Form id='FormDoiTuongChinhSach' onFinish={onFinish} form={form} layout='vertical' disabled={isView}>
				<Row gutter={[12, 0]} style={{ marginBottom: 12 }}>
					<Col span={24} md={24}>
						<Form.Item name='doiTuongChinhSach' label='Đối tượng chính sách' rules={[...rules.required]}>
							<Select
								disabled={isView}
								placeholder='Đối tượng chính sách'
								showSearch
								options={Object.values(EDoiTuongChinhSach).map((item) => ({
									key: item,
									value: item,
									label: item,
								}))}
							/>
						</Form.Item>
					</Col>

					<Col span={24} md={12}>
						<Form.Item name='tuNgay' label='Từ ngày' rules={[...rules.required, ...rules.nhoHonBangHomNay]}>
							<MyDatePicker disabled={isView} placeholder='Từ ngày' />
						</Form.Item>
					</Col>
					<Col span={24} md={12}>
						<Form.Item
							name='denNgay'
							label='Đến ngày'
							rules={[...(tuNgay ? rules.sauNgay(new Date(tuNgay), 'từ ngày') : [])]}
						>
							<MyDatePicker placeholder='Đến ngày' allowClear disabled={isView} />
						</Form.Item>
					</Col>
					<Col span={24} md={24}>
						<Form.Item name='ghiChu' label='Ghi chú' rules={[...rules.text, ...rules.length(550)]}>
							<Input.TextArea placeholder='Ghi chú' disabled={isView} />
						</Form.Item>
					</Col>
					<Col span={24}>
						<Form.Item name='urlFileUpload' label='File đính kèm'>
							<UploadFile
								otherProps={{
									maxCount: 1,
									multiple: true,
									showUploadList: { showDownloadIcon: false },
								}}
							/>
						</Form.Item>
					</Col>
				</Row>
			</Form>
			<div style={{ textAlign: 'center', marginTop: 24 }}>
				{!isView && (
					<Button form='FormDoiTuongChinhSach' style={{ marginRight: 8 }} htmlType='submit' type='primary'>
						{!isEdit ? 'Thêm mới ' : 'Lưu lại'}
					</Button>
				)}
				<Button
					onClick={() => {
						onCancel();
						resetFieldsForm(form);
					}}
				>
					Đóng
				</Button>
			</div>
		</CardForm>
	);
};
