import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import {
  Button,
  Divider,
  Form,
  Popconfirm,
  Tooltip,
  type FormInstance,
} from "antd";
import { useWatch } from "antd/lib/form/Form";
import { useState } from "react";
import moment from "moment";
import { FormValues } from "../KhenThuong";
import {
  FormKyLuat,
  FormKyLuatProps,
  FormKyLuatValues,
} from "./components/FormKyLuat";
import { IColumn } from "../../TableStaticData/typing";
import TableStaticData from "../../TableStaticData";
import {buildUpLoadFile} from "../../../utils/uploadFile";

interface Props {
  form: FormInstance<FormValues>;
  isViewMode?: boolean;
}

type TableRecord = FormKyLuatValues;
export const KyLuat = ({ form, isViewMode }: Props) => {
  const dsKyLuat = useWatch(["dsKyLuat"], form);

  const [visibleForm, setVisibleForm] = useState(false);
  const [record, setRecord] = useState<FormKyLuatValues>();
  const [isEdit, setIsEdit] = useState(false);
  const [isView, setIsView] = useState(false);
  const [indexRecordDangThaoTac, setIndexRecordDangThaoTac] = useState<
    number | undefined
  >(undefined);

  const handleCreate = (vi: boolean) => {
    setVisibleForm(vi);
    setIsEdit(false);
    setIsView(false);
    setRecord(undefined);
  };

  const handleEdit = (tableRecord: TableRecord, index: number) => {
    setVisibleForm(true);
    setIsEdit(true);
    setIsView(false);
    setRecord(tableRecord);
    setIndexRecordDangThaoTac(index);
  };

  const handleView = (tableRecord: TableRecord) => {
    setVisibleForm(true);
    setIsView(true);
    setIsEdit(false);
    setRecord(tableRecord);
  };

  const handleCloseModal = () => {
    setRecord(undefined);
    setVisibleForm(false);
    setIsEdit(false);
    setIsView(false);
  };

  const handleDelete = (index: number) => {
    const prevDsKyLuat: Required<FormValues>["dsKyLuat"] =
      form.getFieldValue("dsKyLuat") ?? [];
    form.setFieldsValue({
      dsKyLuat: prevDsKyLuat.filter((_, KyLuatIndex) => KyLuatIndex !== index),
    });
  };

  const handleSubmitForm = async (values: FormKyLuatValues) => {
    const prevDsKyLuat: Required<FormValues>["dsKyLuat"] =
      form.getFieldValue("dsKyLuat") ?? [];
    const url = await buildUpLoadFile(values, "fileDinhKem");
    const payload = {
      ...values,
      fileDinhKem: url,
    };
    if (!isEdit) {
      form.setFieldsValue({
        dsKyLuat: prevDsKyLuat.concat(payload),
      });
    } else if (isEdit) {
      form.setFieldsValue({
        dsKyLuat: prevDsKyLuat.map((thanhVien, index) => {
          if (indexRecordDangThaoTac === index) {
            return {
              ...thanhVien,
              ...payload,
            };
          }
          return thanhVien;
        }),
      });
    }

    setIndexRecordDangThaoTac(undefined);
    setVisibleForm(false);
    setIsEdit(false);
    setIsView(false);
  };

  const onCell = (rec: TableRecord) => ({
    onClick: () => handleView(rec),
    style: { cursor: "pointer" },
  });
  const columns: IColumn<TableRecord>[] = [
    {
      title: "Cơ quan quyết định",
      dataIndex: "coQuanQuyetDinh",
      width: 120,
      filterType: "string",
      onCell,
    },
    {
      title: "Cấp kỷ luật",
      dataIndex: ["capKyLuat",'ten'],
      width: 120,
      filterType: "string",
      onCell,
    },
    {
      title: "Số quyết định",
      dataIndex: "soQuyetDinh",
      width: 120,
      filterType: "string",
      onCell,
    },
    {
      title: "Người ký",
      dataIndex: "nguoiKy",
      width: 120,
      filterType: "string",
      onCell,
    },
    {
      title: "Ngày quyết định",
      dataIndex: "ngayQuyetDinh",
      width: 120,
      onCell,
      render: (value) => {
        if (value) {
          return moment(value).format("DD/MM/YYYY HH:mm");
        }
        return null;
      },
    },
  ];
  if (!isViewMode) {
    columns.push({
      title: "Thao tác",
      align: "center",
      width: 90,
      fixed: "right",
      render: (_, rec, index) => {
        return (
          <>
            <Tooltip title="Chỉnh sửa">
              <Button
                type="link"
                icon={<EditOutlined rev={undefined} />}
                onClick={() => handleEdit(rec, index)}
              />
            </Tooltip>

            <Tooltip title="Xóa">
              <Popconfirm
                onConfirm={() => handleDelete(index)}
                title="Bạn có chắc chắn muốn xóa kỷ luật này?"
                placement="topLeft"
              >
                <Button
                  danger
                  type="link"
                  icon={<DeleteOutlined rev={undefined} />}
                />
              </Popconfirm>
            </Tooltip>
          </>
        );
      },
    });
  }

  return (
    <Form.Item name="dsKyLuat">
      <Divider orientation="center">Kỷ luật</Divider>
      <TableStaticData
        hasCreate={!isViewMode}
        hasTotal
        showEdit={visibleForm}
        setShowEdit={handleCreate}
        data={dsKyLuat ?? []}
        columns={columns}
        addStt
        widthDrawer={700}
        Form={FormKyLuat}
        formProps={
          {
            record,
            isEdit,
            isView,
            onCancel: handleCloseModal,
            onFinish: handleSubmitForm,
          } as FormKyLuatProps
        }
      />
    </Form.Item>
  );
};
