import { Button, Card, Col, Form, Input, Row } from "antd";
import { useEffect } from "react";
import rules from "../../../../utils/rules";
import MyDatePicker from "../../../MyDatePicker";
import UploadFile from "../../../Upload/UploadFile";
import { FormValues } from "../../KhenThuong";
import SelectCapKyLuat from "./CapKyLuat/Select";
import SelectHinhThucKyLuat from "./HinhThucKyLuat/Select";
import CardForm from "../../../CardForm";

export type FormKyLuatValues = Required<FormValues>["dsKyLuat"][number] & {
  capKyLuat: string;
};

export interface FormKyLuatProps {
  onFinish: (values: FormKyLuatValues) => void;
  onCancel: () => void;
  isEdit: boolean;
  isView: boolean;
  record?: FormKyLuatValues;
}

export const FormKyLuat = ({
  isEdit,
  isView,
  onCancel,
  onFinish,
  record,
}: FormKyLuatProps) => {
  const [form] = Form.useForm<FormKyLuatValues>();

  useEffect(() => {
    if (record) {
      form.setFieldsValue(record);
    } else {
      form.resetFields();
    }
  }, [record]);

  return (
    <CardForm
      bordered={false}
      title={
        (isView ? "Xem chi tiết " : isEdit ? "Chỉnh sửa " : "Thêm mới ") +
        "kỷ luật"
      }
    >
      <Form onFinish={onFinish} form={form} layout="vertical" disabled={isView}>
        <Form.Item name="capKyLuat" hidden />
        <Row gutter={[12, 0]} style={{ marginBottom: 12 }}>
          <Col span={24}>
            <Form.Item
              name="soQuyetDinh"
              label="Số quyết định"
              rules={[...rules.text, ...rules.length(20), ...rules.required]}
            >
              <Input placeholder="Số quyết định" disabled={isView} />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="ngayQuyetDinh"
              label="Ngày quyết định"
              rules={[...rules.required, ...rules.nhoHonBangHomNay]}
            >
              <MyDatePicker
                placeholder="Ngày quyết định"
                allowClear
                disabled={isView}
              />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item rules={[]} name="ngayKy" label="Ngày ký">
              <MyDatePicker
                placeholder="Ngày ký"
                allowClear
                disabled={isView}
              />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="coQuanQuyetDinh"
              label="Cơ quan quyết định"
              rules={[...rules.text, ...rules.length(250)]}
            >
              <Input placeholder="Cơ quan quyết định" disabled={isView} />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item
              name="nguoiKy"
              label="Người ký"
              rules={[...rules.text, ...rules.length(250)]}
            >
              <Input placeholder="Người ký" disabled={isView} />
            </Form.Item>
          </Col>

          <Col span={24} md={12}>
            <Form.Item name='capKyLuatId' label='Cấp kỷ luật' rules={[...rules.required]}>
              <SelectCapKyLuat
                disabled={isView}
              />
            </Form.Item>
          </Col>
          <Col span={24} md={12}>
            <Form.Item name='hinhThucKyLuatId' label='Hình thức kỷ luật' rules={[...rules.required]}>
              <SelectHinhThucKyLuat
              />
            </Form.Item>
          </Col>
          {/*<Col span={24} md={12} style={{ display: 'flex', alignItems: 'center' }}>*/}
          {/*  <Form.Item name='anhHuongThoiGianKyLuat' valuePropName='checked'>*/}
          {/*    <Checkbox*/}
          {/*      // disabled*/}
          {/*      onChange={() => {*/}
          {/*        form.setFields([*/}
          {/*          { name: 'thoiGianDieuChinh', value: null },*/}
          {/*          { name: 'daXetDieuChinhTangLuong', value: true },*/}
          {/*        ]);*/}
          {/*      }}*/}
          {/*    >*/}
          {/*      Ảnh hưởng đến thời gian điều chỉnh lương*/}
          {/*    </Checkbox>*/}
          {/*  </Form.Item>*/}
          {/*</Col>*/}
          <Col span={24} md={24}>
            <Form.Item
              name="noiDung"
              label="Nội dung"
              rules={[...rules.text, ...rules.length(550)]}
            >
              <Input.TextArea placeholder="Nội dung" disabled={isView} />
            </Form.Item>
          </Col>
          <Col span={24}>
            <Form.Item
              name="urlFileUpload"
              // rules={[...rules.fileRequired]}
              label="File đính kèm"
            >
              <UploadFile
                otherProps={{
                  maxCount: 1,
                  multiple: true,
                  // showUploadList: { showDownloadIcon: false },
                }}
              />
            </Form.Item>
          </Col>
        </Row>

        {!isView && (
          <Form.Item style={{ textAlign: "center", marginTop: 24 }}>
            <Button style={{ marginRight: 8 }} htmlType="submit" type="primary">
              {!isEdit ? "Thêm mới " : "Lưu lại"}
            </Button>

            <Button onClick={onCancel}>Đóng</Button>
          </Form.Item>
        )}
      </Form>
    </CardForm>
  );
};
