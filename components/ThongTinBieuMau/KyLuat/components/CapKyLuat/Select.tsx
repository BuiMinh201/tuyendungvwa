import { Select } from "antd";
import { type BaseOptionType } from "antd/lib/select";
import { useEffect, useState } from "react";
import {getCapKyLuat} from "../../../../../service/kyluat/kyluat";

const SelectCapKyLuat = (props: {
  value?: string;
  onChange?: (value: string, option: BaseOptionType) => void;
  multiple?: boolean;
  disabled?: boolean;
  allowClear?: boolean;
}) => {
  const { value, onChange, multiple, disabled, allowClear } = props;
  const [danhSach, setDanhSach] = useState<KyLuat.CapKyLuat[]>([]);
  const getData = async () => {
    try {
      const res = await getCapKyLuat();
      setDanhSach(res?.data?.data);
    } catch (e) {}
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div style={{ display: "flex", gap: 8 }}>
      <div className={"fullWidth"}>
        <Select
          allowClear={allowClear}
          disabled={disabled}
          mode={multiple ? "multiple" : undefined}
          value={value}
          onChange={onChange}
          options={danhSach.map((item) => ({
            key: item._id,
            value: item._id,
            label: item.ten,
            rawData: item,
          }))}
          showSearch
          optionFilterProp="label"
          placeholder="Chọn cấp kỷ luật"
        />
      </div>
    </div>
  );
};

export default SelectCapKyLuat;
