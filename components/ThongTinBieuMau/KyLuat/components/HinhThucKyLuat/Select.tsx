import { Select } from "antd";
import { useEffect, useState } from "react";
import { getHinhThucKyLuat } from "../../../../../service/kyluat/kyluat";

const SelectHinhThucKyLuat = (props: {
  value?: string;
  onChange?: any;
  multiple?: boolean;
  disable?: boolean;
  allowClear?: boolean;
}) => {
  const { value, onChange, multiple, disable, allowClear } = props;
  const [danhSach, setDanhSach] = useState<KyLuat.HinhThucKyLuat[]>([]);
  const getData = async () => {
    try {
      const res = await getHinhThucKyLuat();
      setDanhSach(res?.data?.data);
    } catch (e) {}
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div style={{ display: "flex", gap: 8 }}>
      <div className={"fullWidth"}>
        <Select
          allowClear={allowClear}
          mode={multiple ? "multiple" : undefined}
          value={value}
          onChange={onChange}
          options={danhSach.map((item) => ({
            key: item._id,
            value: item._id,
            label: item.ten,
          }))}
          showSearch
          disabled={disable}
          optionFilterProp="label"
          placeholder="Chọn hình thức kỷ luật"
        />
      </div>
    </div>
  );
};

export default SelectHinhThucKyLuat;
