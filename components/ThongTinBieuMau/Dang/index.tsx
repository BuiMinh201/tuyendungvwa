
import {Col, DatePicker, Divider, Form, Input, Row} from 'antd';
import rules from '../../../utils/rules';
import {FormInstance} from "antd/es/form/Form";
import MyDatePicker from "../../MyDatePicker";

const DangView = (props: { form: FormInstance,disabled?:boolean }) => {
  const { form,disabled } = props;
  // const isCreate = !edit && !isView && !isXemDanhSachCapNhat && !isViewHistory;
  // ;
  const ngaySinh = Form.useWatch("ngaySinh", form);
  // const isCreate = !edit && !isView && !isXemDanhSachCapNhat && !isViewHistory;
  // const disabledForm = isCreate ? false : !isDraff;
  const disabledForm = disabled;

  return (
    <>
      <Divider orientation="center">Đảng</Divider>
      <Row gutter={[12, 0]} style={{ marginBottom: 12 }}>
        {/* <Col span={24} md={6}>
          <Form.Item name="chucVuDangId" label="chức vụ đảng">
            <SelectChucVuDang hasCreate={false} />
          </Form.Item>
        </Col> */}
        <Col span={24} md={6}>
          <Form.Item name='soTheDang' label='Số thẻ Đảng'>
            <Input placeholder='Số thẻ Đảng' disabled={disabledForm} />
          </Form.Item>
        </Col>
        <Col span={24} md={6}>
          <Form.Item name='noiVaoDang' label='Nơi kết nạp'>
            <Input placeholder='Nơi kết nạp' disabled={disabledForm} />
          </Form.Item>
        </Col>
        <Col span={24} md={6}>
          <Form.Item
            rules={[
              ...(ngaySinh ? rules.sauNgay(ngaySinh, 'ngày sinh') : []),
            ]}
            name='ngayVaoDangDuBi'
            label='Ngày vào Đảng dự bị'
          >
            <MyDatePicker placeholder='Ngày vào Đảng dự bị' allowClear disabled={disabledForm} />
          </Form.Item>
        </Col>
        <Col span={24} md={6}>
          <Form.Item
            rules={[
              ...(ngaySinh ? rules.sauNgay(ngaySinh, 'ngày sinh') : []),
            ]}
            name='ngayChinhThuc'
            label='Ngày vào Đảng chính thức'
          >
            <MyDatePicker placeholder='Ngày vào Đảng chính thức' allowClear disabled={disabledForm} />
          </Form.Item>
        </Col>
      </Row>
    </>
  );
};

export default DangView;
