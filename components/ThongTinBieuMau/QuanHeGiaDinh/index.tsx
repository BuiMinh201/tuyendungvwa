import { type FormInstance } from "antd/es/form/Form";
import { FormValues } from "../KhenThuong";
import { QuanHeGiaDinhVeBanThan } from "../QuanHeGiaDinhVeBanThan";
import { QuanHeGiaDinhVeBenVoChong } from "../QuanHeGiaDinhVeBenVoChong";

interface Props {
  form: FormInstance<FormValues>;
  disabled: boolean;
  isXemChiTietHoSo: boolean;
}

export const QuanHeGiaDinh = ({
  form,
  disabled,
  isXemChiTietHoSo = false,
}: Props) => {
  return (
    <>
      <QuanHeGiaDinhVeBanThan form={form} isViewMode={disabled} />

      <QuanHeGiaDinhVeBenVoChong form={form} isViewMode={disabled} />
    </>
  );
};
