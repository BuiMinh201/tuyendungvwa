import {Col, DatePicker, Divider, Form, Input, Row} from "antd";
import { FormInstance } from "antd/es/form/Form";
import rules from "../../../utils/rules";
import MyDatePicker from "../../MyDatePicker";

const QuanDoiView = (props: { form: FormInstance,disabled?:boolean }) => {
  const { form,disabled } = props;
  // const isCreate = !edit && !isView && !isXemDanhSachCapNhat && !isViewHistory;
  // ;
  const ngaySinh = Form.useWatch("ngaySinh", form);
  const disabledForm = disabled;

  return (
    <>
      <Divider orientation="center">Quân đội</Divider>
      <Row gutter={[12, 0]} style={{ marginBottom: 12 }}>
        <Col span={24} md={6}>
          <Form.Item
            rules={[...(ngaySinh ? rules.sauNgay(ngaySinh, "ngày sinh") : [])]}
            name="ngayNhapNgu"
            label="Ngày nhập ngũ"
          >
            <MyDatePicker
              placeholder="Ngày nhập ngũ"
              allowClear
              disabled={disabledForm}
            />
          </Form.Item>
        </Col>
        <Col span={24} md={6}>
          <Form.Item
            rules={[...(ngaySinh ? rules.sauNgay(ngaySinh, "ngày sinh") : [])]}
            name="ngayXuatNgu"
            label="Ngày xuất ngũ"
          >
            <MyDatePicker
              placeholder="Ngày xuất ngũ"
              allowClear
              disabled={disabledForm}
            />
          </Form.Item>
        </Col>
        <Col span={24} md={6}>
          <Form.Item name="donViQuanDoi" label="Đơn vị quân đội">
            <Input placeholder="Đơn vị quân đội" disabled={disabledForm} />
          </Form.Item>
        </Col>
        <Col span={24} md={6}>
          <Form.Item name="chucVuQuanDoi" label="Chức vụ quân đội">
            <Input placeholder="Chức vụ quân đội" disabled={disabledForm} />
          </Form.Item>
        </Col>
      </Row>
    </>
  );
};

export default QuanDoiView;
