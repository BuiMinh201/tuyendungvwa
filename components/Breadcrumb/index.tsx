import { Breadcrumb } from "flowbite-react";
import styled from "styled-components";

interface IProps {
  data: {
    title: string;
    path: string;
    active?: boolean;
  }[];
}
const BreadcrumbPage = (props: IProps) => {
  return (
    <BreadcrumbPageWrapper>
      <div className="mb-[30px]">
        <Breadcrumb aria-label="Default breadcrumb example">
          {props.data.map((val, i) => {
            return (
              <Breadcrumb.Item
                className={`${val?.active ? "active" : ""}`}
                href={val.path}
                key={i}
              >
                {val?.title?.toUpperCase()}
              </Breadcrumb.Item>
            );
          })}
        </Breadcrumb>
        <div className="w-[80px] border-b-2 border-primary mt-[10px]"></div>
      </div>
    </BreadcrumbPageWrapper>
  );
};

const BreadcrumbPageWrapper = styled.div`
  ol {
    li {
      a {
        color: #000;
        font-family: "Exo 2", sans-serif;
        font-size: 16px;
        font-style: normal;
        font-weight: 600;
        line-height: normal;
      }
    }
  }
  .active {
    a {
      color: #0353aa;
      font-family: "Exo 2", sans-serif;
      font-size: 18px;
      font-style: normal;
      font-weight: 600;
      line-height: normal;
    }
  }
	@media screen and (max-width: 640px) {
		ol {
			li {
				a {
					font-size: 14px;
				}
			}
		}
		.active {
			a {
				font-size: 14px;
			}
		}
	}
  @media screen and (min-width: 640px) {
    ol {
      li {
        a {
          font-size: 16px;
        }
      }
    }
    .active {
      a {
        font-size: 16px;
      }
    }
  }
`;
export default BreadcrumbPage;
