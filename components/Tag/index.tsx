// @ts-ignore
import styled from "styled-components";
interface IProps {
  children: any;
  color: string;
  style?: any;
  textColor?: string;
}
export const tuple = <T extends string[]>(...args: T) => args;
export const PresetStatusColorTypes = tuple(
  "success",
  "processing",
  "error",
  "default",
  "warning"
);
// eslint-disable-next-line import/prefer-default-export
export const PresetColorTypes = tuple(
  "pink",
  "red",
  "yellow",
  "orange",
  "cyan",
  "green",
  "blue",
  "purple",
  "geekblue",
  "magenta",
  "volcano",
  "gold",
  "lime"
);
const PresetColorRegex = new RegExp(
  `^(${PresetColorTypes.join("|")})(-inverse)?$`
);
const PresetStatusColorRegex = new RegExp(
  `^(${PresetStatusColorTypes.join("|")})$`
);
const Tag = (props: IProps) => {
  const isPresetColor = (): boolean => {
    if (!props.color) {
      return false;
    }
    return (
      PresetColorRegex.test(props.color) ||
      PresetStatusColorRegex.test(props.color)
    );
  };
  const tagStyle = {
    backgroundColor: props.color,
    color: props.textColor ? props.textColor : "#ffffff",
    borderColor: props.color,
    ...props.style,
  };
  return (
    <TagWrapper>
      <span className="tag" style={tagStyle}>
        {props.children}
      </span>
    </TagWrapper>
  );
};
export default Tag;

const TagWrapper = styled.div`
display: flex;
align-items: center;
  justify-content: center;
  .tag {
    box-sizing: border-box;
    //margin: 0 8px 0 0;
    color: #000000d9;
    font-size: 14px;
    font-variant: tabular-nums;
    line-height: 1.5715;
    list-style: none;
    font-feature-settings: "tnum";
    display: inline-block;
    height: auto;
    padding: 0 7px;
    font-size: 12px;
    line-height: 20px;
    white-space: nowrap;
    background: #fafafa;
    border: 1px solid #d9d9d9;
    border-radius: 2px;
    opacity: 1;
    transition: all 0.3s;
  }
`;
