import styled, { CSSProperties } from "styled-components";

interface IProps {
  type?: "primary" | "default";
  htmlType?: "submit" | "reset" | "button" | undefined;
  children?: any;
  style?: CSSProperties;
  classname?: string;
  uppercase?: boolean;
  disabled?: boolean;
  shadow?: boolean;
  onClick?: () => void;
}

const Button = (props: IProps) => {
  return (
    <ButtonWrapper>
      <button
        type={props?.htmlType ?? "button"}
        onClick={props?.onClick}
        className={`${
          props?.shadow && "shadow-btn"
        } button-default  lg:px-[40px] lg:py-[8px] px-[28px] py-[10px] ${
          props?.uppercase ? "uppercase" : ""
        }  ${
          props.type === "primary"
            ? "bg-primary text-white"
            : "secondary-bg text-secondary"
        } ${props?.classname}`}
        style={props.style}
        disabled={props?.disabled}
      >
        {props.children}
      </button>
    </ButtonWrapper>
  );
};

const ButtonWrapper = styled.div`
  .shadow-btn {
    box-shadow: 4px 4px 0px 0px rgba(79, 155, 249, 0.36);
  }
  .button-default {
    font-family: "Exo 2", sans-serif;
    font-style: normal;
    font-weight: 700;
    font-size: 18px;
    line-height: normal;
    /* identical to box height, or 175% */

    display: flex;
    align-items: center;
    text-align: center;
    //color: #ffffff;

    //box-shadow: 4px 4px 4px rgba(0, 0, 0, 0.1);
  }
`;
export default Button;
