import styled from "styled-components";
// @ts-ignore
import Slider from "react-slick";
import React, { useRef } from "react";
import { Tooltip } from "antd";
interface IProps {
  data: { label: string | undefined; value: string | undefined }[];
  onChange: (value: string) => void;
  value: string;
  showArrow?: boolean;
  isSlide?: boolean;
  column?: number;
}
const NavBar = (props: IProps) => {
  const SliderRef = useRef(null);
  const scrollRef = useRef(null);
  const handleChangeData = (value: string) => {
    props.onChange(value);
  };
  const settings = {
    dots: false,
    infinite: true,
    arrows: false,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          // centerMode: true,
          slidesToScroll: 2,
          infinite: true,
          dots: false,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  const handleNext = () => {
    scroll(20);
    if (SliderRef && SliderRef.current) {
      //@ts-ignore
      SliderRef?.current?.slickNext();
    }
  };
  const handlePrev = () => {
    scroll(-20);
    if (SliderRef && SliderRef.current) {
      //@ts-ignore
      SliderRef?.current?.slickPrev();
    }
  };
  const scroll = (scrollOffset: number) => {
    if (scrollRef.current) {
      console.log(scrollRef);
    }
  };
  return (
    <NavBarWrapper>
      {/*<div className={`lg:grid  gap-[36px] grid-flow-col  lg:grid-cols-${props?.column??6}`}>*/}
      {/*  {props.data?.map((val) => {*/}
      {/*    return (*/}
      {/*      <div*/}
      {/*        className={`cursor-pointer item ${props.value===val.value?'active':''}`}*/}
      {/*        onClick={() => {*/}
      {/*          handleChangeData(val.value??'');*/}
      {/*        }}*/}
      {/*      >*/}
      {/*        {val.label}*/}
      {/*      </div>*/}
      {/*    );*/}
      {/*  })}*/}
      {/*</div>*/}
      {props?.isSlide ? (
        <div className="relative">
          {props?.showArrow && (
            <button
              className=" lg:flex absolute top-[-6px] lg:left-[-35px] left-[-30px] next-btn mr-[16px]  items-center justify-center  z-10"
              onClick={handlePrev}
            >
              <img
                className={"w-[36px]"}
                src="/images/icons/arrow-left.svg"
                alt={"image"}
              />
            </button>
          )}
          {/*<div*/}
          {/*  ref={scrollRef}*/}
          {/*  className={`w-full flex gap-[32px] snap-x overflow-x-auto no-scrollbar ${*/}
          {/*    props?.data?.length <= 2 ? "justify-center" : ""*/}
          {/*  }`}*/}
          {/*>*/}
          {/*  {props.data?.map((val) => {*/}
          {/*    return (*/}
          {/*      <div*/}
          {/*        className={`snap-start scroll-ml-6 shrink-0 relative first:pl-0 last:pr-6  cursor-pointer item ${*/}
          {/*          props.value === val.value ? "active" : ""*/}
          {/*        }`}*/}
          {/*        onClick={() => {*/}
          {/*          handleChangeData(val.value ?? "");*/}
          {/*        }}*/}
          {/*      >*/}
          {/*        <Tooltip*/}
          {/*          className="z-50"*/}
          {/*          content={<div>{val.label}</div>}*/}
          {/*          style={"light"}*/}
          {/*          placement="top"*/}
          {/*        >*/}
          {/*          {val.label}*/}
          {/*        </Tooltip>*/}
          {/*      </div>*/}
          {/*    );*/}
          {/*  })}*/}
          {/*</div>*/}
          <Slider {...settings} ref={SliderRef}>
            {props.data?.map((val) => {
              return (
                <div>
                  <div
                    className={`pr-[60px]  cursor-pointer  `}
                    onClick={() => {
                      handleChangeData(val.value ?? "");
                    }}
                  >
                    <Tooltip placement="top" title={val.label}>
                      <div
                        className={`item ${
                          props.value === val.value ? "active" : ""
                        }`}
                      >
                        {val.label}
                      </div>
                    </Tooltip>
                  </div>
                </div>
              );
            })}
          </Slider>
          {props?.showArrow && (
            <button
              className=" lg:flex absolute top-[-6px] lg:right-[-35px] right-[-30px] prev-btn  items-center justify-center  z-10"
              onClick={handleNext}
            >
              <img
                className={"w-[36px]"}
                src="/images/icons/arrow-right.svg"
                alt={"image"}
              />
            </button>
          )}
        </div>
      ) : (
        <div
          className={`w-full flex gap-[32px] snap-x overflow-x-auto no-scrollbar ${
            props?.data?.length <= 2 ? "justify-center" : ""
          }`}
        >
          {props.data?.map((val) => {
            return (
              <div
                className={`snap-start scroll-ml-6 shrink-0 relative first:pl-0 last:pr-6  cursor-pointer item ${
                  props.value === val.value ? "active" : ""
                }`}
                onClick={() => {
                  handleChangeData(val.value ?? "");
                }}
              >
                {val.label}
              </div>
            );
          })}
        </div>
      )}
    </NavBarWrapper>
  );
};
const NavBarWrapper = styled.div`
  .active {
    color: #0353aa !important;
    text-decoration-line: underline;
  }
  .item {
    color: #aeaeae;
    font-family: "Exo 2", sans-serif;
    font-size: 18px;
    font-style: normal;
    font-weight: 600;
    line-height: normal;
    text-transform: uppercase;

    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 1;
    -webkit-box-orient: vertical;
    &:hover {
      color: #0353aa;
    }
  }
  /* width */
  &::-webkit-scrollbar {
    width: 0px;
  }

  /* Track */
  &::-webkit-scrollbar-track {
    background: #f1f1f1;
  }

  /* Handle */
  &::-webkit-scrollbar-thumb {
    background: #888;
  }

  /* Handle on hover */
  &::-webkit-scrollbar-thumb:hover {
    background: #555;
  }
`;
export default NavBar;
