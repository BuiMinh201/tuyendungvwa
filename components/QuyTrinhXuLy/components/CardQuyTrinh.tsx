import styled from "styled-components";
interface IProps {
  type: "xuoi" | "nguoc";
  title: string;
  number: string;
}
const CardQuyTrinh = (props: IProps) => {
  const { type, number, title } = props;
  return (
    <CardQuyTrinhWrapper>
      {type === "xuoi" ? (
        <div className="card-xuoi xl:block flex items-center ">
          <div className="flex justify-center">
            <div className="relative xl:w-[200px]">
              <img
                src="/images/card-xuoi.png"
                className="w-full xl:block hidden"
              />
              <div className="xl:absolute top-[-7px] left-[3px] xl:w-full xl:h-full flex justify-center items-center number-card xl:text-[48px] text-[32px]">
                {number}
              </div>
            </div>
          </div>
          <div className="h-[2px] bg-[#2387E3] w-[30px] xl:hidden block mx-[30px]"></div>
          <div className="title-card text-center xl:mt-[16px]">{title}</div>
        </div>
      ) : (
        <div className="card-nguoc xl:block flex items-center ">
          <div className="title-card text-center mb-[16px] hidden xl:block">
            {title}
          </div>
          <div className="flex justify-center ">
            <div className="relative xl:w-[200px]">
              <img
                src="/images/card-nguoc.png"
                className="w-full  xl:block hidden"
              />
              <div className="xl:absolute top-[7px] left-[-3px] w-full h-full flex justify-center items-center number-card text-[32px] xl:text-[48px]">
                {number}
              </div>
            </div>
          </div>
          <div className="h-[2px] bg-[#2387E3] w-[30px] xl:hidden block mx-[30px]"></div>
          <div className="title-card text-center xl:mb-[16px] block xl:hidden">
            {title}
          </div>
        </div>
      )}
    </CardQuyTrinhWrapper>
  );
};

const CardQuyTrinhWrapper = styled.div`
  .number-card {
    color: var(--Main-2, #2387e3);
    text-align: center;
    font-family: Arial;
    font-style: normal;
    font-weight: 700;
    line-height: 125%; /* 60px */
  }
  .title-card {
    color: var(--TEXT-01, #212529);
    text-align: center;
    font-family: Arial;
    font-size: 20px;
    font-style: normal;
    font-weight: 700;
    line-height: 125%; /* 25px */
  }
`;
export default CardQuyTrinh;
