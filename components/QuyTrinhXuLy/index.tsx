import styled from "styled-components";
import Title from "../Title";
import CardQuyTrinh from "./components/CardQuyTrinh";

const QuyTrinhXuLy = () => {
  return (
    <QuyTrinhXuLyWrapper>
      <div
        className="container mx-auto py-[50px] px-[20px] xl:px-0"
        id={"quy-trinh"}
      >
        <Title
          title={"Quy trình xử lý hồ sơ"}
          align={"center"}
          hideSearch={true}
          hideArrows
        />

        <div className="grid xl:grid-cols-4 md:grid-cols-2 grid-cols-1 gap-[30px] mt-[36px] wow fadeInDown">
          <CardQuyTrinh
            type={"xuoi"}
            title={"Nộp hồ sơ ứng tuyển"}
            number={"01"}
          />
          <CardQuyTrinh type={"nguoc"} title={"Nhận lịch thi"} number={"02"} />
          <CardQuyTrinh
            type={"xuoi"}
            title={"Nhận kết quả thi"}
            number={"03"}
          />
          <CardQuyTrinh
            type={"nguoc"}
            title={"Kết quả trúng tuyển"}
            number={"04"}
          />
        </div>
      </div>
    </QuyTrinhXuLyWrapper>
  );
};

const QuyTrinhXuLyWrapper = styled.div`
  background-image: url("/images/bg-quy-trinh.png");
  background-repeat: no-repeat;
  background-size: cover;
  background-position: bottom;
`;
export default QuyTrinhXuLy;
