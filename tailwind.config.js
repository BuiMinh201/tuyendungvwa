module.exports = {
	content: [
		"./pages/**/*.{js,ts,jsx,tsx}",
		"./components/**/*.{js,ts,jsx,tsx}",
		"./layouts/**/*.{js,ts,jsx,tsx}",
		"./node_modules/flowbite-react/**/*.js",
	],
	theme: {
		darkMode: 'class',
		extend: {
			colors: {
				primary: {
					50: "#e5e8f9",
					100: "#b2bcef",
					200: "#7f84e5",
					300: "#4c64db",
					400: "#1928d1",
					500: "#003D98",
					600: "#002bb7",
					700: "#0021a3",
					800: "#00188e",
					900: "#002b5f",
					DEFAULT: "#0353AA", //500
					on: "#f9e6e5", //50
					dark: {
						DEFAULT: "#5274fa", //300
						on: "#001ba3", //800
					},
				},
				secondary: {
					50: "#fbf4f4",
					100: "#f6e9e9",
					200: "#e9c9c9",
					300: "#dba8a8",
					400: "#c16767",
					500: "#a62626",
					600: "#952222",
					700: "#7d1d1d",
					800: "#641717",
					900: "#51131a",
					DEFAULT: "#FF1818", //500
					on: "#f4fbfa", //50
					dark: {
						DEFAULT: "#dba8a8", //300
						on: "#641717", //800
					},
				},
				white: {
					50: "#fbf4f4",
					100: "#f6e9e9",
					200: "#e9c9c9",
					300: "#dba8a8",
					400: "#c16767",
					500: "#FFFFFF",
					600: "#952222",
					700: "#7d1d1d",
					800: "#641717",
					900: "#51131a",
					DEFAULT: "#FFFFFF", //500
					on: "#f4fbfa", //50
					dark: {
						DEFAULT: "#dba8a8", //300
						on: "#641717", //800
					},
				},
			},
		},
	},
	plugins: [require("flowbite/plugin")],
	darkMode: ['class', '[data-mode="dark-version"]'],
};
