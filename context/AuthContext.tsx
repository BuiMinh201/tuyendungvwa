import React, { createContext, useContext, useEffect, useState } from "react";
import { axios } from "../service";

import i18n from "../i18n";
import { USER_ROLE } from "../utils/constant";
import { getRequest } from "../service";
import {
  DataConfig,
  DataConfigAll,
  IDataDaoTaoAll,
  IDataHomeAll,
  MainMenu,
} from "../utils/interface";

const defaultValue: any = {
  isAuthenticated: false,
  userLoading: true,
  setUser: () => null,
  setShowAuthModal: () => null,
  setLangCode: () => null,
  // login: () => Promise.resolve({} as User.IRoot),
  // oauthLogin: () => Promise.resolve({} as User.IRoot),
  logout: () => Promise.resolve(1),
  allBookmark: [],
  setAllBookmark: () => null,
  allApply: [],
  setAllApply: () => null,
  allFollow: [],
  setAllFollow: () => null,
  OAuthData: {} as any,
  typeLogin: "default",
  setTypeLogin: () => null,
  setMenu: () => null,
  setDataConfig: () => null,
  setDataDaoTao: () => null,
  setDataHome: () => null,
  dataDaoTao: {} as IDataDaoTaoAll,
  dataHome: {} as IDataHomeAll,
  menu: [],
  dataConfig: {} as DataConfigAll,
};
export const AuthContext = createContext<any>(defaultValue);

export const AuthProvider = (props: { children: React.ReactNode }) => {
  const [user, setUser] = useState<any | undefined>(undefined);
  const [langCode, setLangCode] = useState<string>(i18n.language);
  const [menu, setMenu] = useState<MainMenu[]>([]);
  const [dataConfig, setDataConfig] = useState<DataConfigAll>(
    {} as DataConfigAll
  );
  const [userLoading, setUserLoading] = useState(true);
  const [showAuthModal, setShowAuthModal] = useState("");
  const [allBookmark, setAllBookmark] = useState<string[]>([]);
  const [allApply, setAllApply] = useState<string[]>([]);
  const [allFollow, setAllFollow] = useState<string[]>([]);
  const [dataHome, setDataHome] = useState<any>();
  const [dataDaoTao, setDataDaoTao] = useState<any>();
  const [OAuthData, setOAuthData] = useState<any>({} as any);
  const [typeLogin, setTypeLogin] = useState<"keycloak" | "default">(
    typeof window !== "undefined" && localStorage.getItem("typeLogin")
      ? (localStorage.getItem("typeLogin") as "keycloak" | "default")
      : "default"
  );

  useEffect(() => {
    // loadUserFromLocal()
    // 	.then((user) => {
    // 		if (user?.systemRole === USER_ROLE.USER) {
    // 			getAllBookmark().then((res) =>
    // 				setAllBookmark(res.data?.map((item: { idTinDang: string }) => item?.idTinDang))
    // 			);
    // 			getAllApply().then((res) => setAllApply(res.data?.map((item: { idTinDang: string }) => item?.idTinDang)));
    // 			getAllFollow().then((res) => setAllFollow(res.data?.map((item: { idTheoDoi: string }) => item?.idTheoDoi)));
    // 		}
    // 	})
    // 	.finally(() => setUserLoading(false));
    // getClientOAuth().then((res) => {
    // 	setOAuthData(res.data[0]);
    // });
  }, []);

  const handleLogin = (user: any, accessToken: any) => {
    if (accessToken) {
      // localStorage.setItem("token", accessToken);
      // axios.defaults.headers.Authorization = `Bearer ${accessToken}`;
      // if (user) {
      // 	setUserLoading(true);
      // 	getUserProfile()
      // 		.then((response) => {
      // 			if (user.systemRole === USER_ROLE.USER) user.profileUser = response.data;
      // 			else user.profileDoanhNghiep = response.data;
      // 			setUser(user);
      //
      // 			if (user?.systemRole === USER_ROLE.USER) {
      // 				getAllBookmark().then((res) =>
      // 					setAllBookmark(res.data?.map((item: { idTinDang: string }) => item?.idTinDang))
      // 				);
      // 				getAllApply().then((res) => setAllApply(res.data?.map((item: { idTinDang: string }) => item?.idTinDang)));
      // 				getAllFollow().then((res) =>
      // 					setAllFollow(res.data?.map((item: { idTheoDoi: string }) => item?.idTheoDoi))
      // 				);
      // 			}
      // 		})
      // 		.finally(() => setUserLoading(false));
      // }
      // return user as User.IRoot;
    }
  };

  const logout = async () => {};

  return (
    <AuthContext.Provider
      value={{
        isAuthenticated: !!user,
        userLoading,
        user,
        setUser,
        setLangCode,
        langCode,
        showAuthModal,
        setShowAuthModal,
        // login,
        // oauthLogin,
        logout,
        allApply,
        setAllApply,
        allBookmark,
        setAllBookmark,
        allFollow,
        setAllFollow,
        OAuthData,
        typeLogin: typeLogin,
        setTypeLogin,
        menu,
        setMenu,
        dataConfig,
        setDataConfig,
        setDataHome,
        dataHome,
        setDataDaoTao,
        dataDaoTao,
      }}
    >
      {props.children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => useContext(AuthContext);
