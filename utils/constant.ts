// User roles
export enum USER_ROLE {
  USER = "USER",
  DOANH_NGHIEP = "DOANH_NGHIEP",
  ADMIN = "ADMIN",
}

export const dataGender = [
  {
    value: "Male",
    label: "Nam",
  },
  {
    value: "Female",
    label: "Nữ",
  },
];

export const dataLevel = [1, 2, 3, 4, 5].map((item) => ({
  value: item,
  label: item,
}));
export enum EGioiTinh {
  Nam = "Nam",
  Nu = "Nữ",
}
export enum ENhomMau {
  O = "O",
  A = "A",
  B = "B",
  AB = "AB",
}
export enum EMoiQuanHeBanThan {
  BO_DE = "Bố đẻ",
  ME_DE = "Mẹ đẻ",
  ANH_RUOT = "Anh ruột",
  CHI_RUOT = "Chị ruột",
  EM_RUOT = "Em ruột",
  CHONG = "Chồng",
  VO = "Vợ",
  CON_DE = "Con đẻ",
  CON_NUOI = "Con nuôi",
}
export enum EMoiQuanHeVoChong {
  BO_VO_CHONG = "Bố vợ (chồng)",
  ME_VO_CHONG = "Mẹ vợ (chồng)",
  ANH_VO_CHONG = "Anh vợ (chồng)",
  CHI_VO_CHONG = "Chị vợ (chồng)",
  EM_VO_CHONG = "Em vợ (chồng)",
}
export enum ETrinhDoGiaoDucPhoThong {
  TRINH_DO_1 = "12/12",
  TRINH_DO_2 = "10/12",
  TRINH_DO_3 = "9/12",
  TRINH_DO_4 = "8/12",
  TRINH_DO_5 = "10/10",
  TRINH_DO_6 = "9/10",
  TRINH_DO_7 = "7/10",
}
export enum EDanhhieu {
  GIAO_SU = "Giáo sư",
  PHO_GIAO_SU = "Phó giáo sư",
}
export enum EDoiTuongChinhSach {
  ANH_HUNG_LUC_LUONG_VU_TRANG = "Anh hùng lực lượng vũ trang",
  ANH_HUNG_LAO_DONG = "Anh hùng lao động",
  THUONG_BINH = "Thương binh",
  CON_LIET_SI = "Con của liệt sỹ",
  CON_THUONG_BINH = "Con của thương binh",
  CON_BENH_BINH = "Con của bệnh binh",
  CON_NGUOI_HUONG_CHINH_SACH = "Con của người hưởng chính sách",
}
export enum ELoaiQuyetDinh {
  LUAN_CHUYEN = "Luân chuyển",
  DIEU_DONG = "Điều động",
  BO_NHIEM = "Bổ nhiệm",
  KIEM_NHIEM = "Kiêm nhiệm",
  MIEN_NHIEM = "Miễn nhiệm",
  BIET_PHAT = "Biệt phái",
  TIEP_NHAN = "Tiếp nhận",
  TUYEN_DUNG = "Tuyển dụng",
  KHAC = "Khác",
}
export enum ELoaiLuong {
  LUONG_THEO_NGACH_BAC = "Lương theo ngạch bậc",
  LUONG_THEO_VI_TRI_VIEC_LAM = "Lương theo vị trí việc làm",
}
export enum EHocHam {
  GIAO_SU = "Giáo sư",
  PHO_GIAO_SU = "Phó giáo sư",
}
export enum ECapHanhChinh {
  CAP_1 = 1,
  CAP_2 = 2,
  CAP_3 = 3,
}