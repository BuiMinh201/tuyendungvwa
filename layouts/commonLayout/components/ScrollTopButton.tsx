import { HiChevronUp } from "react-icons/hi";
import ScrollToTop from "react-scroll-to-top";
import styled from "styled-components";

const ScrollTopButton = () => {
	return (
		<ScrollTopButtonWrapper>
			<div className="fixed bottom-[125px] right-[25px] z-30" >
				<div className="flex justify-end mb-[16px]">
					<ScrollToTop
						smooth
						component={<HiChevronUp className="text-2xl mx-auto" />}
					/>
				</div>
			</div>
		</ScrollTopButtonWrapper>
	);
};
const ScrollTopButtonWrapper = styled.div`
  .scroll-to-top {
    position: relative !important;
    right: 0;
    bottom: 0;
  }
`;
export default ScrollTopButton;
