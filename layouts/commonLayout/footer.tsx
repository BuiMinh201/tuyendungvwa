import { Footer, Tooltip } from "flowbite-react";
import Image from "next/image";
import Link from "next/link";
import { useTranslation } from "react-i18next";
import { BsFacebook, BsGithub, BsInstagram, BsTwitter } from "react-icons/bs";
import styled from "styled-components";
import React, { useEffect, useRef, useState } from "react";
import { Router } from "../../config";
import { FacebookShareButton, TwitterShareButton } from "react-share";
import { useRouter } from "next/router";
import axios from "axios";
import { ip, ip3 } from "../../service/ip";
import { DataConfig, DataConfigAll } from "../../utils/interface";
import { useAuth } from "../../context/AuthContext";

const MainFooter = () => {
  const [common] = useTranslation("common");
  const router = useRouter();
  const [dataConfigFooter, setDataConfigFooter] = useState<DataConfigAll>();
  const { menu, setDataConfig, dataHome } = useAuth();

  const footerRef = useRef(null);

  const [visible, setVisible] = useState<boolean>(false);

  const callBackFunction = (entries: any) => {
    const [entry] = entries;
    setVisible(entry.isIntersecting);
  };
  const options = {
    root: null,
    rootMargin: "10px",
    threshold: 0.5,
  };
  useEffect(() => {
    let observer = new IntersectionObserver(callBackFunction, options);
    if (footerRef.current) observer.observe(footerRef.current);
    return () => {
      if (footerRef.current) observer.unobserve(footerRef.current);
    };
  }, [footerRef, options]);

  useEffect(() => {
    // getData();
  }, [router]);
  useEffect(() => {
    // getData();
    if (dataHome) {
      setDataConfigFooter(dataHome?.config);
      setDataConfig(dataHome?.config);
    }
  }, [dataHome]);

  const getDataConfig = (type: string): any => {
    // @ts-ignore
    return dataConfigFooter?.[type];
  };
  return (
    <FooterWrapper>
      <div
        className="bg-image pt-[40px] pb-[20px] px-[20px] xl:px-0"
        ref={footerRef}
      >
        <div className="container mx-auto">
          <div className="flex justify-between wow fadeInLeft">
            {/*<img src="/header/logo.svg" />*/}
            <img
              className={"max-w-[400px] w-full"}
              src="https://portal.hvpnvn.edu.vn/wp-content/uploads/2023/09/Frame-18031.png"
            />
            <div className="socical flex">
              <a
                href={
                  "https://www.facebook.com/TUYENSINHHOCVIENPHUNUVN/?locale=vi_VN"
                }
              >
                <img className="" src="/icon/facebook-icon.svg" />
              </a>

              <a
                href={
                  "https://www.youtube.com/channel/UCnI-mRrP64YcwowBgeQpCFg"
                }
              >
                <img src="/icon/youtube-icon.svg" />
              </a>
            </div>
          </div>
          <div className="info xl:flex lg:flex gap-[30px]  items-center mt-[28px] wow fadeInRight">
            <div className="flex justify-center xl:block">
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.3327760546813!2d105.8048631759!3d21.019366780627724!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab6712302e73%3A0x14692d80d1ed6481!2zSOG7jWMgdmnhu4duIFBo4bulIG7hu68gVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1705943996096!5m2!1svi!2s"
                width="312"
                height="182"
                style={{ border: 0 }}
                allowFullScreen={false}
                loading="lazy"
                referrerPolicy="no-referrer-when-downgrade"
              ></iframe>
            </div>
            <div
              className={
                "xl:flex grid grid-cols-1 justify-between w-full xl:pl-[50px] "
              }
            >
              <div className="pr-[50px]">
                <ul className="menu-footer  grid xl:block grid-cols-2">
                  <li>
                    <a href={"/"}>Trang chủ</a>
                  </li>
                  {/*<li>*/}
                  {/*  <a*/}
                  {/*    href={*/}
                  {/*      "https://portal.hvpnvn.edu.vn/gioi-thieu/lich-su-hinh-thanh-hoc-vien-phu-nu-viet-nam/"*/}
                  {/*    }*/}
                  {/*    target={"_blank"}*/}
                  {/*  >*/}
                  {/*    Giới thiệu*/}
                  {/*  </a>*/}
                  {/*</li>*/}
                  {/*<li>*/}
                  {/*  <a*/}
                  {/*    href={"https://portal.hvpnvn.edu.vn/tin-tuc/"}*/}
                  {/*    target={"_blank"}*/}
                  {/*  >*/}
                  {/*    Tin tức*/}
                  {/*  </a>*/}
                  {/*</li>*/}
                  {/*<li>*/}
                  {/*  <a*/}
                  {/*    href={"https://portal.hvpnvn.edu.vn/category/boi-duong/"}*/}
                  {/*    target={"_blank"}*/}
                  {/*  >*/}
                  {/*    Bồi dưỡng*/}
                  {/*  </a>*/}
                  {/*</li>*/}
                  {/*<li>*/}
                  {/*  <a*/}
                  {/*    href={*/}
                  {/*      "https://portal.hvpnvn.edu.vn/category/dao-tao-parent/"*/}
                  {/*    }*/}
                  {/*    target={"_blank"}*/}
                  {/*  >*/}
                  {/*    {" "}*/}
                  {/*    Đào tạo*/}
                  {/*  </a>*/}
                  {/*</li>*/}
                  <li>
                    <a
                      href={"/#vi-tri-viec-lam"}
                      // target={"_blank"}
                    >
                      Vị trí việc làm
                    </a>
                  </li>{" "}
                  <li>
                    <a
                      href={"/#dot-tuyen-dung"}
                      // target={"_blank"}
                    >
                      Đợt tuyển dụng
                    </a>
                  </li>{" "}
                  <li>
                    <a
                      href={"/#quy-trinh"}
                      // target={"_blank"}
                    >
                      Quy trình xử lý
                    </a>
                  </li>
                </ul>
              </div>
              {/*<div>*/}
              {/*  <ul className="menu-footer ">*/}
              {/*    <li>*/}
              {/*      <a*/}
              {/*        href={"https://portal.hvpnvn.edu.vn/category/tuyen-sinh/"}*/}
              {/*        target={"_blank"}*/}
              {/*      >*/}
              {/*        Tuyển sinh*/}
              {/*      </a>*/}
              {/*    </li>*/}
              {/*    <li>*/}
              {/*      <a*/}
              {/*        href={*/}
              {/*          "https://portal.hvpnvn.edu.vn/category/nghien-cuu-khoa-hoc/"*/}
              {/*        }*/}
              {/*        target={"_blank"}*/}
              {/*      >*/}
              {/*        Khoa học công nghệ*/}
              {/*      </a>*/}
              {/*    </li>*/}
              {/*    <li>*/}
              {/*      <a*/}
              {/*        href={*/}
              {/*          "https://portal.hvpnvn.edu.vn/category/hop-tac-quoc-te/"*/}
              {/*        }*/}
              {/*        target={"_blank"}*/}
              {/*      >*/}
              {/*        Hợp tác quốc tế*/}
              {/*      </a>*/}
              {/*    </li>*/}
              {/*    <li>*/}
              {/*      <a*/}
              {/*        href={*/}
              {/*          "https://portal.hvpnvn.edu.vn/category/bao-dam-chat-luong/"*/}
              {/*        }*/}
              {/*        target={"_blank"}*/}
              {/*      >*/}
              {/*        Đảm bảo chất lượng*/}
              {/*      </a>*/}
              {/*    </li>*/}
              {/*    <li>*/}
              {/*      <a*/}
              {/*        href={*/}
              {/*          "https://portal.hvpnvn.edu.vn/category/cong-tac-sinh-vien/"*/}
              {/*        }*/}
              {/*        target={"_blank"}*/}
              {/*      >*/}
              {/*        Công tác sinh viên*/}
              {/*      </a>*/}
              {/*    </li>*/}
              {/*  </ul>*/}
              {/*</div>*/}
              <div>
                <div className="flex items-center mt-[16px]">
                  <svg
                    className="flex-shrink-0"
                    xmlns="http://www.w3.org/2000/svg"
                    width="17"
                    height="17"
                    viewBox="0 0 17 17"
                    fill="none"
                  >
                    <g clip-path="url(#clip0_7644_1643)">
                      <path
                        d="M8.5 0.188477C5.17171 0.188477 2.46397 2.89622 2.46397 6.22448C2.46397 10.3549 7.86563 16.4187 8.09561 16.6748C8.31162 16.9154 8.68877 16.915 8.9044 16.6748C9.13438 16.4187 14.536 10.3549 14.536 6.22448C14.536 2.89622 11.8283 0.188477 8.5 0.188477ZM8.5 9.26136C6.82546 9.26136 5.46315 7.89902 5.46315 6.22448C5.46315 4.54993 6.82549 3.18763 8.5 3.18763C10.1745 3.18763 11.5368 4.54997 11.5368 6.22451C11.5368 7.89906 10.1745 9.26136 8.5 9.26136Z"
                        fill="#FFFFFF"
                      />
                    </g>
                    <defs>
                      <clipPath id="clip0_7644_1643">
                        <rect
                          width="16.6667"
                          height="16.6667"
                          fill="white"
                          transform="translate(0.166667 0.188477)"
                        />
                      </clipPath>
                    </defs>
                  </svg>
                  <div className="ml-[12px] title-info">
                    68 Đ. Nguyễn Chí Thanh, Láng Thượng, Đống Đa, Hà Nội
                  </div>
                </div>
                <div className="flex items-center mt-[16px]">
                  <svg
                    className="flex-shrink-0"
                    xmlns="http://www.w3.org/2000/svg"
                    width="17"
                    height="17"
                    viewBox="0 0 17 17"
                    fill="none"
                  >
                    <g clip-path="url(#clip0_7644_1649)">
                      <path
                        d="M9.89287 10.3994C9.47822 10.6759 8.99658 10.822 8.5 10.822C8.00345 10.822 7.52181 10.6759 7.10716 10.3994L0.277637 5.84629C0.239935 5.82108 0.202931 5.79485 0.166667 5.76761L0.166667 13.2285C0.166667 14.0839 0.86084 14.7627 1.70094 14.7627H15.299C16.1544 14.7627 16.8333 14.0686 16.8333 13.2285V5.76758C16.797 5.79489 16.7599 5.82118 16.7221 5.84642L9.89287 10.3994Z"
                        fill="#FFFFFF"
                      />
                      <path
                        d="M0.819336 5.03392L7.64886 9.58711C7.90739 9.75947 8.20368 9.84564 8.49997 9.84564C8.79629 9.84564 9.09261 9.75944 9.35114 9.58711L16.1807 5.03392C16.5894 4.76162 16.8333 4.30589 16.8333 3.81403C16.8333 2.96829 16.1453 2.28027 15.2996 2.28027H1.70042C0.85472 2.28031 0.166667 2.96833 0.166667 3.81484C0.166413 4.05612 0.225879 4.29371 0.339761 4.50642C0.453642 4.71914 0.618399 4.90035 0.819336 5.03392Z"
                        fill="#FFFFFF"
                      />
                    </g>
                    <defs>
                      <clipPath id="clip0_7644_1649">
                        <rect
                          width="16.6667"
                          height="16.6667"
                          fill="white"
                          transform="translate(0.166667 0.188477)"
                        />
                      </clipPath>
                    </defs>
                  </svg>
                  <div className="ml-[12px] title-info">
                    <a href="mailto:phongtchc@vwa.edu.vn">phongtchc@vwa.edu.vn</a>
                  </div>
                </div>
                <div className="flex items-center mt-[16px]">
                  <svg
                    className="flex-shrink-0"
                    xmlns="http://www.w3.org/2000/svg"
                    width="17"
                    height="17"
                    viewBox="0 0 17 17"
                    fill="none"
                  >
                    <g clip-path="url(#clip0_7644_1656)">
                      <path
                        d="M10.6264 2.90225C10.2094 1.82002 9.68509 0.908561 9.08262 0.209701C8.8839 0.195603 8.68474 0.188524 8.48551 0.188477C8.28522 0.188477 8.0861 0.195736 7.88838 0.209701C7.28578 0.908594 6.76152 1.81989 6.34456 2.90225C6.2099 3.25169 6.08887 3.61393 5.98141 3.98678H10.9896C10.882 3.61393 10.761 3.25169 10.6264 2.90225ZM5.73639 12.0803H11.2345C11.4447 11.1035 11.5668 10.0697 11.5955 9.01009H5.37539C5.40413 10.0698 5.52621 11.1035 5.73639 12.0803ZM11.2345 4.96335H5.73639C5.52621 5.94014 5.40413 6.97393 5.37539 8.03353H11.5955C11.5668 6.9738 11.4447 5.94014 11.2345 4.96335ZM6.34443 14.1414C6.76152 15.2236 7.28578 16.1351 7.88825 16.8339C8.0861 16.8479 8.28522 16.8551 8.48551 16.8551C8.68565 16.8551 8.88477 16.8479 9.08262 16.8339C9.68509 16.135 10.2094 15.2237 10.6263 14.1414C10.761 13.7919 10.882 13.4297 10.9895 13.0568H5.98141C6.08884 13.4297 6.20993 13.7919 6.34443 14.1414ZM12.2303 4.96335C12.4272 5.93838 12.5447 6.97064 12.5723 8.03353H16.8042C16.7424 6.95638 16.4769 5.91764 16.0252 4.96257C16.0205 4.9627 16.016 4.96335 16.0113 4.96335H12.2303ZM4.74076 12.0803C4.54365 11.1052 4.42617 10.073 4.39857 9.01009H0.166667C0.228451 10.0872 0.493978 11.126 0.945638 12.0811C0.950326 12.0809 0.954916 12.0803 0.959636 12.0803H4.74076ZM12.0011 13.0568C11.6376 14.4125 11.1132 15.6277 10.4593 16.6208C11.9307 16.265 13.2804 15.5118 14.378 14.4144C14.7912 14.0014 15.1603 13.5464 15.4792 13.0568H12.0011ZM4.96973 3.98678C5.33343 2.63115 5.85768 1.41592 6.51152 0.422819C5.04037 0.778646 3.69043 1.53177 2.59281 2.62923C2.17961 3.04229 1.81057 3.49726 1.49163 3.98678H4.96973ZM4.96973 13.0568H1.49167C1.8106 13.5464 2.17965 14.0013 2.59284 14.4144C3.69046 15.5118 5.04024 16.265 6.51146 16.6208C5.85772 15.6277 5.33346 14.4124 4.96979 13.0568H4.96973ZM12.0011 3.98678H15.4792C15.1603 3.49723 14.7913 3.04226 14.3781 2.62923C13.2804 1.53177 11.9307 0.778646 10.4593 0.422852C11.1132 1.41592 11.6376 2.63119 12.0011 3.98678ZM12.5723 9.01009C12.5447 10.073 12.4273 11.1052 12.2303 12.0803H16.0113C16.016 12.0803 16.0205 12.0809 16.0253 12.0811C16.4769 11.126 16.7424 10.0872 16.8042 9.01009H12.5723ZM4.39857 8.03353C4.42617 6.97064 4.54365 5.93838 4.74076 4.96335H0.959603C0.954916 4.96335 0.950326 4.9627 0.945606 4.96257C0.494011 5.91764 0.228516 6.95638 0.166667 8.03353H4.39857Z"
                        fill="#FFFFFF"
                      />
                    </g>
                    <defs>
                      <clipPath id="clip0_7644_1656">
                        <rect
                          width="16.6667"
                          height="16.6667"
                          fill="white"
                          transform="translate(0.166667 0.188477)"
                        />
                      </clipPath>
                    </defs>
                  </svg>
                  <div className="ml-[12px] title-info">
                    <a href={"http://hvpnvn.edu.vn/"} target={"_blank"}>
                      hvpnvn.edu.vn
                    </a>
                  </div>
                </div>
                <div className="flex mt-[16px]">
                  <svg
                    className="flex-shrink-0"
                    xmlns="http://www.w3.org/2000/svg"
                    width="17"
                    height="18"
                    viewBox="0 0 17 18"
                    fill="none"
                  >
                    <g clip-path="url(#clip0_7644_1662)">
                      <path
                        d="M16.4039 12.9861L14.3325 10.9143C13.7798 10.3616 12.8142 10.3607 12.2607 10.9143L11.9152 11.2597L16.0586 15.4028L16.4039 15.0575C16.9774 14.484 16.9755 13.5568 16.4039 12.9861ZM11.2056 11.931C10.7697 12.2686 10.1479 12.2552 9.74963 11.856L5.65403 7.75758C5.25484 7.3584 5.2414 6.73613 5.57906 6.30134L1.44525 2.16775C-0.333103 4.24066 -0.263572 7.36413 1.69915 9.32685L8.18036 15.8109C10.0638 17.6943 13.1489 17.9441 15.3396 16.0647L11.2056 11.931ZM6.59579 3.17517L4.52441 1.10333C3.97174 0.550664 3.00615 0.54972 2.45257 1.10333L2.10716 1.44874L6.25051 5.59186L6.59579 5.24655C7.16929 4.67305 7.1674 3.74584 6.59579 3.17517Z"
                        fill="#FFFFFF"
                      />
                    </g>
                    <defs>
                      <clipPath id="clip0_7644_1662">
                        <rect
                          width="16.6667"
                          height="16.6667"
                          fill="white"
                          transform="translate(0.166667 0.688477)"
                        />
                      </clipPath>
                    </defs>
                  </svg>
                  <div className="ml-[12px] title-info">
                    <div>
                      (Tel): <a href="tel:02437759907">84-243 7759907</a>
                    </div>
                    <div>
                      (Fax): <a href="tel:2437730283">84-243 7730283</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="flex justify-center mb-[20px] mt-[52px]">
            <div className="line h-[1px] w-[600px]"></div>
          </div>
          <div className="copyright text-center  wow fadeInUp">
            © Copyright 2024 HocvienPhunuVietNam, All rights reserved ® Học viện
            Phụ nữ Việt Nam giữ bản quyền nội dung trên website này
          </div>
        </div>
      </div>
    </FooterWrapper>
  );
};
const FooterWrapper = styled.div`
  //background: linear-gradient(0deg, rgba(212, 237, 255, 0.20) 0%, rgba(212, 237, 255, 0.20) 100%), #FFF;
  background-image: url("https://portal.hvpnvn.edu.vn/wp-content/uploads/2023/10/Footer-4.png");
  background-repeat: no-repeat;
  background-position: center;
  .copyright {
    color: var(--Dark-Gray, #ffffff);
    font-family: Arial;
    font-size: 10px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
  }
  .title-info {
    color: var(--Dark-Gray, #ffffff);
    font-feature-settings: "clig" off, "liga" off;
    font-family: Arial;
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    line-height: 24.043px; /* 150.269% */
  }
  .menu-footer {
    li {
      color: #ffffff;
      margin-top: 16px;
      font-size: 16px;
      cursor: pointer;
    }
  }
  .line {
    background-color: rgb(3, 48, 88);
  }
`;
export default MainFooter;
