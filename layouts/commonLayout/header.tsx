import { useRouter } from "next/router";
import React, { useContext, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { AuthContext, useAuth } from "../../context/AuthContext";
import styled from "styled-components";
import { useForm } from "react-hook-form";
import axios from "axios";
import { ip, urlDangNhap } from "../../service/ip";
import { MainMenu } from "../../utils/interface";
import { renderImage } from "../../utils/util";
import HeaderTop from "../../components/HeaderChildren/HeaderTop";
import HeaderBot from "../../components/HeaderChildren/HeaderBot";

interface IProps {
  language: string;
  handleChangeLanguage: (lang: string) => void;
}

const Header = (props: IProps) => {
  const [common] = useTranslation("common");
  const router = useRouter();
  const [showMenu, setShowMenu] = useState<boolean>(false);
  const [typeMenu, setTypeMenu] = useState<string>("");
  const [dataMenu, setDataMenu] = useState<any>();
  const [linkLogo, setLinkLogo] = useState<string>();
  const [loading, setLoading] = useState<boolean>(false);
  const [mainMenu, setMainMenu] = useState<MainMenu[]>([]);
  const [isScroll, setIsScroll] = useState<boolean>(false);
  const { dataHome } = useContext(AuthContext);
  const { language, handleChangeLanguage } = props;
  const {
    isAuthenticated,
    user,
    dataConfig,
    showAuthModal: showModal,
    setShowAuthModal: setShowModal,
    userLoading,
    setMenu,
  } = useAuth();
  const fetch = async () => {

  };
  const getDataConfig = (type: string, valueGet?: string): any => {
    // @ts-ignore
    let obj = dataConfig?.[type];
    if (valueGet) {
      return obj?.id;
    } else {
      return obj?.value;
    }
  };
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const {
    register: register2,
    formState: { errors: errors2 },
    handleSubmit: handleSubmit2,
  } = useForm();
  const onSubmit = (data: any) => {
    if (data?.keyword) {
      router.push(`/tim-kiem?keyword=${data?.keyword}`);
    }
  };
  useEffect(() => {

  }, [isAuthenticated]);
  useEffect(() => {
    if (router && router.pathname) {
      switch ((router.pathname as string)?.replace("/", "")?.split("/")?.[0]) {
        case "tin-tuc":
          setTypeMenu("tin-tuc");
          break;
        case "gioi-thieu":
          setTypeMenu("gioi-thieu");
          break;
        case "tuyen-sinh":
          setTypeMenu("tuyen-sinh");
          break;
        case "chuong-trinh-dao-tao":
          setTypeMenu("chuong-trinh-dao-tao");
          break;
        case "lien-he":
          setTypeMenu("lien-he");
          break;
        case "tin-tuyen-sinh":
          setTypeMenu("tin-tuyen-sinh");
          break;
        case "su-kien":
          setTypeMenu("su-kien");
          break;
        case "thong-bao":
          setTypeMenu("thong-bao");
          break;
        case "so-do":
          setTypeMenu("so-do");
          break;
        case "tra-cuu-van-bang":
          setTypeMenu("tra-cuu-van-bang");
          break;
        case "tra-cuu":
          setTypeMenu("tra-cuu-van-bang");
          break;
        case "van-ban":
          setTypeMenu("van-ban");
          break;
        case "hdsd":
          setTypeMenu("hdsd");
          break;
        case "/":
          setTypeMenu("");
          break;
        default:
          setTypeMenu("");
          break;
      }
    }
  }, [router]);

  useEffect(() => {
  }, [dataHome]);

  return (
    <HeaderWrapper className="sticky top-0 sm:z-50 lg:z-20">
     <HeaderTop/>
      <HeaderBot/>
    </HeaderWrapper>
  );
};

const HeaderWrapper = styled.div`
  box-shadow: 0px 0px 8px 0px rgba(0, 0, 0, 0.10);
`;
export default React.memo(Header);
