import { useRouter } from "next/router";
import React, {lazy, useContext, useEffect, useRef, useState} from "react";
import {AuthContext, useAuth} from "../../context/AuthContext";
import i18n from "../../i18n";
import ScrollTopButton from "./components/ScrollTopButton";
// import Footer from "./footer";
// import Header from "./header";
import Head from "next/head";
import { da } from "date-fns/locale";
import dynamic from "next/dynamic";
import axios from "axios";
import {gateWay, ip3} from "../../service/ip";

const Footer = dynamic(import("./footer"),{ssr:false});
const Header = dynamic(import("./header"),{ssr:false});


const CommonLayout = ({ children }: any) => {
  const [language, setLanguage] = useState<string>("");
  const { setLangCode } = useAuth();
  const { pathname, asPath } = useRouter();

  const dataFetchedRef = useRef(false);
  const { setDataHome, setDataDaoTao } = useContext(AuthContext);


  useEffect(() => {
    (async () => {
      const langCode = localStorage.getItem("langCode") || "vi";
      setLanguage(langCode);
      i18n.changeLanguage(langCode);
    })();
  }, []);


  const getDataHome = async () => {
    try {

    } catch (e) {
      console.log(e);
    }
  };

  const getData = async () => {
    try {
      console.log('ass',asPath)
      getDataHome()


    } catch (e) {
      console.log(e);
    }
  };
  useEffect(()=>{
    if (dataFetchedRef.current) return;
    dataFetchedRef.current = true;
    getData()

  },[])


  const handleChangeLanguage = (lang: string) => {
    i18n.changeLanguage(lang);
    setLanguage(lang);
    localStorage.setItem("langCode", lang);
    setLangCode(lang);
  };
  const dataHidden = [""];
  return (
    <>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta httpEquiv="X-UA-Compatible" content="ie=edge" />
        <link rel="icon" href="/favicon.ico"></link>
        <title>Trang thông tin tuyển dụng</title>
      </Head>
      <div className="">
       <div className="sticky top-0 z-50">
         {!dataHidden?.includes(asPath) && (
           <Header
             language={language}
             handleChangeLanguage={handleChangeLanguage}
           />
         )}
       </div>

        <div className="flex-auto  bg-[#F8F8F8] overflow-hidden">{children}</div>
        {!dataHidden?.includes(asPath) && <Footer />}

        <ScrollTopButton />
      </div>
    </>
  );
};
export default React.memo(CommonLayout);
