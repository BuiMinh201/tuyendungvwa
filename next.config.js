/** @type {import('next').NextConfig} */

const withBundleAnalyzer = require("@next/bundle-analyzer")({
  enabled: process.env.ANALYZE === "true",
});

module.exports = withBundleAnalyzer({
  env: {
    BASE_URL: process.env.BASE_URL,
    GA: process.env.GA,
  },
  images: {
    domains: ["ais.aisenote.com"],
  },
  poweredByHeader: false,
  reactStrictMode: false,
  swcMinify: true,
  // time in seconds of no pages generating during static
  // generation before timing out
  staticPageGenerationTimeout: 1000,
  // headers: {
  // 	'Content-Security-Policy': `default-src 'self'; script-src 'self' https://chuyendoisovietnam.edu.vn/;`,
  // 	'Strict-Transport-Security': 'max-age=31536000; https://chuyendoisovietnam.edu.vn/',
  // 	'X-Powered-By': '',
  // 	'X-Content-Type-Options': 'nosniff',
  // 	'Cache-Control': 'no-cache, no-store, must-revalidate',
  // 	'Pragma': 'no-cache',
  // },
  async headers() {
    return [
      {
        source: "/",
        headers: [
          {
            key: "Strict-Transport-Security",
            value: "max-age=63072000;",
          },
          // {
          //   key: "Content-Security-Policy",
          //   value: `default-src 'self'; script-src 'self' 'nonce-xyz123' https://cdnjs.cloudflare.com/; style-src 'sha256-a4ayc/80/OGda4BO/1o/V0etpOqiLx1JwB5S3beHW0s=' 'self' https://fonts.googleapis.com/ https://fonts.gstatic.com; font-src 'self' https://fonts.gstatic.com;`,
          // },
          {
            key: "X-Content-Type-Options",
            value: "nosniff",
          },
          {
            key: "X-Frame-Options",
            value: "SAMEORIGIN",
          },
        ],
      },
    ];
  },
});
