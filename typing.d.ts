type TakeIfIsTupleOrNever<T> = T extends any[]
  ? any[] extends T
    ? never
    : T
  : never;
type Primitive = string | number | boolean | bigint | symbol | undefined | null;
type BuiltIn = Primitive | Function | Date | Error | RegExp;
type DeepNullable<T> = T extends BuiltIn
  ? T | null
  : T extends Map<infer K, infer V>
  ? Map<DeepNullable<K>, DeepNullable<V>>
  : T extends ReadonlyMap<infer K, infer V>
  ? ReadonlyMap<DeepNullable<K>, DeepNullable<V>>
  : T extends WeakMap<infer K, infer V>
  ? WeakMap<DeepNullable<K>, DeepNullable<V>>
  : T extends Set<infer U>
  ? Set<DeepNullable<U>>
  : T extends ReadonlySet<infer U>
  ? ReadonlySet<DeepNullable<U>>
  : T extends WeakSet<infer U>
  ? WeakSet<DeepNullable<U>>
  : T extends (infer U)[]
  ? T extends TakeIfIsTupleOrNever<T>
    ? { [K in keyof T]: DeepNullable<T[K]> | null }
    : DeepNullable<U>[]
  : T extends Promise<infer U>
  ? Promise<DeepNullable<U>>
  : T extends object
  ? { [K in keyof T]: DeepNullable<T[K]> }
  : T | null;
