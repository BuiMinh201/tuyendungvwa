import axios from "../axios";
import {ip3} from "../ip";

export async function getLoaiBoiDuong() {
  return axios.get(`${ip3}/loai-boi-duong/public/many`);
}
