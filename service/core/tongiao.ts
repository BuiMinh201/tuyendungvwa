import axios from "../axios";
import { ipCore } from "../ip";

export async function getTonGiao() {
  return axios.get(`${ipCore}/dm-ton-giao/public/many`);
}
