import axios from "../axios";
import {ip3} from "../ip";

export async function getHinhThucDaoTao() {
  return axios.get(`${ip3}/hinh-thuc-dao-tao/public/many`);
}