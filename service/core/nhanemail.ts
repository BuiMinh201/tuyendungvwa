import axios from "../axios";
import {ip3} from "../ip";

export async function luuEmail(email: string) {
  return axios.post(`${ip3}/nhan-thong-bao/public`, { email: email });
}
