import axios from "../axios";
import { ipCore } from "../ip";

export async function getQuanHuyen(maTinh: string) {
  return axios.get(`${ipCore}/don-vi-hanh-chinh/quan-huyen/maTinh/${maTinh}`);
}

export async function getTinhThanhPho() {
  return axios.get(`${ipCore}/don-vi-hanh-chinh/tinh`);
}

export async function getphuongXa(maQH: string) {
  return axios.get(`${ipCore}/don-vi-hanh-chinh/xa-phuong/ma-quan-huyen/${maQH}`);
}

export async function getAllDVHC() {
  return axios.get(`${ipCore}/don-vi-hanh-chinh/all-data`);
}

