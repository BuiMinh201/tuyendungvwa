import axios from "../axios";
import {ip3, ipDaoTao} from "../ip";

export async function getTinHoc() {
  return axios.get(`${ip3}/trinh-do-tin-hoc-hemis/public/many`);
}
export async function getNganh() {
  return axios.get(`${ip3}/dm-nganh/public/many`);
}
export async function getLyLuan() {
  return axios.get(`${ip3}/trinh-do-ly-luan-chinh-tri/public/many`);
}
export async function getTrinhDoQuanLy() {
  return axios.get(`${ip3}/trinh-do-quan-ly-hanh-chinh/public/many`);
}
export async function getTrinhDoQuanLyNhaNuoc() {
  return axios.get(`${ip3}/trinh-do-quan-ly-nha-nuoc/public/many`);
}
export async function getTrinhDoQuanDaoTao() {
  return axios.get(`${ip3}/trinh-do-dao-tao/public/many`);
}
export async function getNganhDaoTao() {
  return axios.get(`${ipDaoTao}/dm-nganh/public/many`);
}
export async function getNgachLuong() {
  return axios.get(`${ip3}/ngach-luong/public/many`);
}
export async function getBacLuong() {
  return axios.get(`${ip3}/bac-luong/public/many`);
}
export async function getLoaiPhuCap() {
  return axios.get(`${ip3}/loai-phu-cap/public/many`);
}
export async function getMucPhuCap() {
  return axios.get(`${ip3}/muc-phu-cap/public/many`);
}