import axios from "../axios";
import {ipCore} from "../ip";

export async function getQuocTich() {
  return axios.get(`${ipCore}/dm-quoc-tich/public/many`);
}
