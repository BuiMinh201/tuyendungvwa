import axios from "../axios";
import {ip3} from "../ip";

export async function getNgoaiNgu() {
  return axios.get(`${ip3}/ngoai-ngu-hemis/public/many`);
}
export async function getKhungNgoaiNgu() {
  return axios.get(`${ip3}/khung-nang-luc-ngoai-ngu-hemis/public/many`);
}