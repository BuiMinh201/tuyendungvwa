
import {ipCore} from "../ip";
import axios from "../axios";

export async function getDanToc() {
  return axios.get(`${ipCore}/dm-dan-toc/public/many`);
}
