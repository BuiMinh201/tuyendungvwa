import axios from "../axios";
import {ip3, ipCore} from "../ip";

export async function getTinhTrangHonNhan() {
  return axios.get(`${ip3}/tinh-trang-hon-nhan/public/many`);
}
