import axios from "../axios";
import {ip3} from "../ip";

export async function getNganhDaoTao() {
  return axios.get(`${ip3}/dm-nganh/public/many`);
}