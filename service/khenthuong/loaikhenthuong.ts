import axios from "../axios";
import {ip3} from "../ip";

export async function getLoaiKhenThuong() {
  return axios.get(`${ip3}/loai-khen-thuong/public/many`);
}
