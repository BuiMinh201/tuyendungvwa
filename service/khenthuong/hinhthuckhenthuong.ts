import axios from "../axios";
import {ip3 } from "../ip";

export async function getHinhThucKhenThuong() {
  return axios.get(`${ip3}/hinh-thuc-khen-thuong/public/many`);
}
