import axios from "../axios";
import {ip3} from "../ip";

export async function getCapKhenThuong() {
  return axios.get(`${ip3}/cap-khen-thuong/public/many`);
}
