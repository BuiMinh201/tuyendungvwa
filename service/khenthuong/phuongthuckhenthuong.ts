import axios from "../axios";
import { ip3 } from "../ip";

export async function getPhuongThucKhenThuong() {
  return axios.get(`${ip3}/phuong-thuc-khen-thuong/public/many`);
}
