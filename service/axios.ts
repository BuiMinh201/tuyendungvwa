// import { refreshAccesssToken } from '@/services/ant-design-pro/api';
import { message, notification } from 'antd';
import axios from 'axios';
import data from './data';

// Add a response interceptor
axios.interceptors.response.use(
  (response) =>
    // Do something with response data
    response,
  (error) => {
    const er = error?.response?.data;
    const descriptionError = Array.isArray(er?.detail?.exception?.response?.message)
      ? er?.detail?.exception?.response?.message?.join(', ')
      : // Sequelize validation Errors
      Array.isArray(er?.detail?.exception?.errors)
        ? er?.detail?.exception?.errors?.map((e: any) => e?.message)?.join(', ')
        : data.error[er?.detail?.errorCode || er?.errorCode] ||
        er?.detail?.message ||
        er?.message ||
        er?.errorDescription;

    const originalRequest = error.config;
    let originData = originalRequest?.data;
    if (typeof originData === 'string') originData = JSON.parse(originData);
    if (
      typeof originData !== 'object' ||
      !Object.keys(originData ?? {}).includes('silent') ||
      !originData?.silent
    )
      switch (error?.response?.status) {
        case 400:
          notification.error({
            message: 'Bad request',
            description: descriptionError,
          });
          break;

        case 401:
          if (originalRequest._retry) break;
          break;
        case 403:
        case 405:
          notification.error({
            message: 'Thao tác không được phép',
            description: descriptionError,
          });
          break;

        case 404:
          notification.error({
            message: 'Không tìm thấy dữ liệu',
            description: descriptionError,
          });
          break;

        case 409:
          notification.error({
            message: 'Dữ liệu chưa đúng',
            description: descriptionError,
          });
          break;

        case 500:
        case 502:
          notification.error({
            message: 'Server gặp lỗi',
            description: descriptionError,
          });
          break;

        default:
          message.error('Có lỗi xảy ra. Vui lòng thử lại');
          break;
      }
    // Do something with response error
    return Promise.reject(error);
  },
);

export default axios;
