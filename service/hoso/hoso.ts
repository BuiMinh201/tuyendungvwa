import axios from "../axios";
import {ip3 } from "../ip";

export async function traCuuHoSo(cccd?:string,maHoSo?:string) {
  return axios.get(`${ip3}/ung-tuyen/tra-cu-ho-so`,{params:{cccd:cccd,maHoSo:maHoSo}});
}
export async function exportHoSo(id:string) {
  return axios.get(`${ip3}/ung-tuyen/export-ho-so/${id}`,{responseType:'arraybuffer'});
}
