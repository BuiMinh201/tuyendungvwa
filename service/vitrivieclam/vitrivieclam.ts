import axios from "../axios";
import { ip3 } from "../ip";

export async function getDetailViecLam(id: string) {
  return axios.get(`${ip3}/vi-tri-tuyen-dung-public/${id}`);
}
