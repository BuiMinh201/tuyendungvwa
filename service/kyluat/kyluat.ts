import axios from "../axios";
import { ip3 } from "../ip";

export async function getHinhThucKyLuat() {
  return axios.get(`${ip3}/hinh-thuc-ky-luat/public/many`);
}
export async function getCapKyLuat() {
  return axios.get(`${ip3}/cap-ky-luat/public/many`);
}