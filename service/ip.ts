const domain = "https://ais.aisenote.com/";
const gateWay = "gwtestv5";
const ip3 = domain + 'tcns'; // prod
const ipDaoTao = domain + 'qldt'; // prod
const ip4 = "mis.chuyendoisovietnam.edu.vn"; // prod
const ipCore = domain + 'core';
const ip5 = `${domain}/public`;
const ip = `${domain}/${gateWay}`;
const urlDangKyLopHoc = `${domain}/congnguoidung/danhsach-khoahoc-modangky`; //prod
const urlDangNhap = `${domain}/user/`; //prod


// const domain = "https://ais.aisenote.com/dev/";
// const gateWay = "gwtestv5";
// const ip3 = domain + 'tcns'; // prod
// const ipDaoTao = domain + 'qldt'; // prod
// const ip4 = "mis.chuyendoisovietnam.edu.vn"; // prod
// const ipCore = domain + 'core';
// const ip5 = `${domain}/public`;
// const ip = `${domain}/${gateWay}`;
// const urlDangKyLopHoc = `${domain}/congnguoidung/danhsach-khoahoc-modangky`; //prod
// const urlDangNhap = `${domain}/user/`; //prod


export { ip4, ip3, ip, gateWay, urlDangKyLopHoc, urlDangNhap, ip5,ipCore,ipDaoTao };
