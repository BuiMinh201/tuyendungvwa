import { Breadcrumb, Form, Modal, Popconfirm, Spin } from "antd";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import styled from "styled-components";
import { ip3 } from "../../service/ip";
import { chuanHoaObject, resetFieldsForm } from "../../utils/util";
import KetQuaUngTuyen from "../../components/KetQuaUngTuyen";
import ThongTinBieuMau from "../../components/ThongTinBieuMau";
import moment from "moment";
import { ThongTinNhanSu } from "../../Interface/ThongTinNhanSu/typing";
import {
  buildUpLoadFile,
  buildUpLoadMultiFile,
  EFileScope,
} from "../../utils/uploadFile";
import axios from "../../service/axios";
import CardTime from "../../components/CardTime";

const UngTuyenChiTiet = () => {
  const router = useRouter();
  const [form] = Form.useForm();

  const [visible, setVisible] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);
  const [dataViecLam, setDataViecLam] = useState<ViTriViecLam.IRecord>();
  const [dataNhanSuNew, setDataNhanSuNew] = useState<ThongTinNhanSu.IRecord>();

  const getDataById = async (id: string) => {
    try {
      setLoading(true);
      const res = await axios.get(`${ip3}/vi-tri-tuyen-dung-public/${id}`);
      if (res) {
        setDataViecLam(res?.data?.data);
      }
    } catch (e) {
      console.log(e);
    } finally {
      setLoading(false);
    }
  };
  const handleUngTuyen = async (data: any) => {
    try {
      const payload = chuanHoaObject(data);
      const res = await axios.post(`${ip3}/ung-tuyen/khoi-tao`, { ...payload });
      if (res) {
        setDataNhanSuNew(res?.data?.data);
        setVisible(true);
        form.resetFields();
      }
    } catch (e) {
      console.log(e);
    }
  };
  const onFinish = async (values: any) => {
    try {
      setLoading(true);
      const url = await buildUpLoadFile(values, "urlAnhDaiDien");
      const dataTaiLieuUngTuyen = dataViecLam?.dotTuyenDung?.hoSoDuTuyen ?? [];
      const dataTaiLieu = [];
      for (let i = 0; i < dataTaiLieuUngTuyen?.length; i++) {
        const urlList = await buildUpLoadMultiFile(
          values?.taiLieuUngTuyen?.[i],
          "file",
          EFileScope.PUBLIC
        );
        dataTaiLieu?.push({
          tenFile: dataTaiLieuUngTuyen?.[i]?.tenGiayTo,
          file: urlList,
        });
      }
      const body: any = {
        ...values,
        dotTuyenDungId: dataViecLam?.dotTuyenDungId,
        viTriUngTuyen: dataViecLam?._id ?? undefined,
        tenDonViTuyenDung: dataViecLam?.donVi?.ten ?? "",
        tenDonViViTriTuyenDung: dataViecLam?.tenViTri ?? "",
        urlAnhDaiDien: url,
        taiLieuUngTuyen: dataTaiLieu,
        khac: "",
      };
      handleUngTuyen({ ...body }).then((res) => {});
    } catch (e) {
      console.log(e);
    } finally {
      setLoading(false);
    }
  };
  useEffect(() => {
    if (router?.query?.id) {
      getDataById(router?.query?.id?.toString());
    }
  }, [router]);
  return (
    <UngTuyenChiTietWrapper>
      <div className="container mx-auto">
        <div className="my-[10px]  px-[20px] xl:px-0">
          <Breadcrumb
            separator=">"
            items={[
              {
                title: "Trang chủ",
                href: "/",
              },
              {
                title: "Vị trí việc làm",
                href: "/#vi-tri-viec-lam",
              },
              {
                title: "Ứng tuyển vị trí",
              },
            ]}
          />
        </div>
      </div>
      <Spin spinning={loading}>
        <div className="bg-white ">
          <div className="container mx-auto">
            <div className="title-ung-tuyen py-[27px]">
              <div className="title-ung-tuyen-1 text-center">
                {dataViecLam?.tenViTri}
              </div>
              <div className="title-ung-tuyen-2 text-center mt-[8px]">
                {dataViecLam?.dotTuyenDung?.tenDotTuyenDung}
              </div>
              <CardTime
                timeStart={dataViecLam?.dotTuyenDung?.thoiGianBDNhanHoSo}
                timeEnd={dataViecLam?.dotTuyenDung?.thoiGianKTNhanHoSo}
                align={"center"}
              />
              <div className="mt-[26px] flex justify-center">
                {/*<Popconfirm*/}
                {/*  title={*/}
                {/*    "Thông tin hồ sơ của bạn sẽ được gửi đi, bạn có chắc đã kiểm tra đầy đủ thông tin?"*/}
                {/*  }*/}
                {/*  onConfirm={() => {*/}
                {/*    form.submit();*/}
                {/*  }}*/}
                {/*>*/}
                <button
                  disabled={
                    !dataViecLam?._id ||
                    moment().isBefore(
                      moment(dataViecLam?.dotTuyenDung?.thoiGianBDNhanHoSo)
                    ) ||
                    moment().isAfter(
                      dataViecLam?.dotTuyenDung?.thoiGianKTNhanHoSo
                    )
                  }
                  className="btn-nop w-[223px] h-[42px] flex justify-center items-center"
                  onClick={() => {
                    form
                      .validateFields()
                      .then((val) => {
                        Modal.confirm({
                          title: "Thông báo",
                          content:
                            "Thông tin hồ sơ của bạn sẽ được gửi tới phòng Tổ chức hành chính, bạn có chắc chắn đã kiểm tra đầy đủ thông tin?",
                          okText: "Đồng ý",
                          cancelText: "Đóng",
                          onOk: () => {
                            form.submit();
                          },
                        });
                      })
                      .catch(() => {
                        form.submit();
                      });
                  }}
                >
                  Nộp hồ sơ dự tuyển
                </button>
                {/*</Popconfirm>*/}
              </div>
            </div>
          </div>
        </div>
        <div className="form-ung-tuyen mt-[12px] bg-white pt-[24px] pb-[40px]">
          <div className="container mx-auto">
            <ThongTinBieuMau
              form={form}
              onFinish={onFinish}
              dataViecLam={dataViecLam}
            />
          </div>
          <div className="mt-[26px] flex justify-center">
            {/*<Popconfirm*/}
            {/*  title={*/}
            {/*    "Thông tin hồ sơ của bạn sẽ được gửi đi, bạn có chắc đã kiểm tra đầy đủ thông tin?"*/}
            {/*  }*/}
            {/*  onConfirm={() => {*/}
            {/*    // form.submit();*/}
            {/*    form*/}
            {/*      .validateFields()*/}
            {/*      .then((val) => {*/}
            {/*        Modal.confirm({*/}
            {/*          title: "Thông báo",*/}
            {/*          content: "Thông tin hồ sơ của bạn sẽ được gửi đi, bạn có chắc đã kiểm tra đầy đủ thông tin?",*/}
            {/*          okText: "Đồng ý",*/}
            {/*          cancelText: "Đóng",*/}
            {/*          onOk: () => {*/}
            {/*            form.submit()*/}
            {/*          },*/}
            {/*        });*/}
            {/*      })*/}
            {/*      .catch(() => {*/}
            {/*        console.log("caccc");*/}
            {/*      });*/}
            {/*  }}*/}
            {/*>*/}
            <button
              disabled={
                !dataViecLam?._id ||
                moment().isBefore(
                  moment(dataViecLam?.dotTuyenDung?.thoiGianBDNhanHoSo)
                ) ||
                moment().isAfter(dataViecLam?.dotTuyenDung?.thoiGianKTNhanHoSo)
              }
              className="btn-nop w-[223px] h-[42px] flex justify-center items-center"
              onClick={() => {
                form
                  .validateFields()
                  .then((val) => {
                    Modal.confirm({
                      title: "Thông báo",
                      content:
                        "Thông tin hồ sơ của bạn sẽ được gửi tới phòng Tổ chức hành chính, bạn có chắc chắn đã kiểm tra đầy đủ thông tin?",
                      okText: "Đồng ý",
                      cancelText: "Đóng",
                      onOk: () => {
                        form.submit();
                      },
                    });
                  })
                  .catch(() => {
                    form.submit();
                  });
              }}
            >
              Nộp hồ sơ dự tuyển
            </button>
            {/*</Popconfirm>*/}
          </div>
        </div>
      </Spin>

      <KetQuaUngTuyen
        visible={visible}
        setVisible={setVisible}
        data={dataNhanSuNew}
        isModal={true}
      />
    </UngTuyenChiTietWrapper>
  );
};

const UngTuyenChiTietWrapper = styled.div`
  .title-ung-tuyen-1 {
    color: var(--TEXT-01, #212529);
    text-align: center;
    font-family: Arial;
    font-size: 24px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
  }
  .title-ung-tuyen-2 {
    color: var(--Dark-Gray, #2d2d2d);
    text-align: center;
    font-family: Arial;
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
  }
  .btn-nop {
    color: #fff;
    text-align: center;
    font-family: Arial;
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;

    border-radius: 4px;
    background: var(--Main-2, #2387e3);
  }
`;
export default UngTuyenChiTiet;
