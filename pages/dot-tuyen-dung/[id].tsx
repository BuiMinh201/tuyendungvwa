import { useRouter } from "next/router";
import { useEffect, useRef, useState } from "react";
import axios from "axios";
import { ip3 } from "../../service/ip";
import styled from "styled-components";
import { Breadcrumb, Tabs, TabsProps } from "antd";
import ThongTinChung from "../../components/ChiTietDotTuyenDung/ThongTinChung";
import moment from "moment";
import DanhSachViecLam from "../../components/ChiTietDotTuyenDung/DanhSachViecLam";
import Title from "../../components/Title";
import SlideComponents from "../../components/Slide";
import CardDotTuyenDung from "../../components/DotTuyenDung/components/CardDotTuyenDung";

const DotTuyenDungChiTiet = () => {
  const router = useRouter();
  const slideRef = useRef(null);
  const [dataDot, setDataDot] = useState<DotTuyenDung.IRecord>();
  const [dataDotKhac, setDataDotKhac] = useState<DotTuyenDung.IRecord[]>([]);
  const handleGetDetail = async (id: string) => {
    try {
      const res = await axios.get(`${ip3}/dot-tuyen-dung-public/${id}`);
      if (res) {
        setDataDot(res?.data?.data);
      }
    } catch (e) {
      console.log(e);
    }
  };
  const handleGetDotKhac = async (id?: string) => {
    try {
      const res = await axios.get(`${ip3}/dot-tuyen-dung-public/page`, {
        params: {
          page: 1,
          limit: 8,
        },
      });
      if (res) {
        let data = res?.data?.data?.result ?? [];
        setDataDotKhac(
          data?.filter((item: { _id: string | undefined }) => item?._id !== id)
        );
      }
    } catch (e) {
      console.log(e);
    }
  };
  const onChange = (key: string) => {
    console.log(key);
  };

  const items: TabsProps["items"] = [
    {
      key: "1",
      label: "Thông tin chung",
      children: <ThongTinChung dataDot={dataDot as DotTuyenDung.IRecord} />,
    },
    {
      key: "2",
      label: "Danh sách vị trí việc làm",
      children: <DanhSachViecLam dataDot={dataDot as DotTuyenDung.IRecord} />,
    },
  ];

  useEffect(() => {
    if (router?.query?.id) {
      handleGetDetail(router?.query?.id?.toString());
      handleGetDotKhac(router?.query?.id?.toString());
    }
  }, [router]);

  return (
    <DotTuyenDungChiTietWrapper >
      <div className="container mx-auto ">
        <div className="my-[10px]  px-[20px] xl:px-0">
          <Breadcrumb
            separator=">"
            items={[
              {
                title: "Trang chủ",
                href: "/",
              },
              {
                title: "Đợt tuyển dụng",
                href: "/#vi-tri-viec-lam",
              },
              {
                title: dataDot?.tenDotTuyenDung,
              },
            ]}
          />
        </div>
        <div className="flex justify-center">
          <div className="bg-white p-[20px] mb-[50px] xl:w-[1000px] w-full relative">
            <div className="title-chi-tiet text-center">
              {dataDot?.tenDotTuyenDung}
            </div>
            <div className="time flex justify-center mt-[20px]">
              <div className="card-time xl:text-[14px] text-[12px] pt-[6px] px-[12px]">
                Ngày bắt đầu{" "}
                {moment(dataDot?.thoiGianBDNhanHoSo)?.format("DD/MM/YYYY")}
              </div>{" "}
              <div className="mx-[12px]">-</div>
              <div className="card-time xl:text-[14px] text-[12px] pt-[6px] px-[12px]">
                Ngày kết thúc{" "}
                {moment(dataDot?.thoiGianKTThiTuyen)?.format("DD/MM/YYYY")}
              </div>
            </div>
            <div className="line my-[28px] h-[1px] bg-[#F4F4F4]"></div>

            <Tabs defaultActiveKey="1" items={items} onChange={onChange} />
          </div>
        </div>
      </div>
      {dataDotKhac?.length > 0 && (
        <>
          {" "}
          <div className="khac bg-white py-[50px]  px-[20px] xl:px-0">
            <div className="container mx-auto">
              <Title
                title={"Đợt tuyển dụng khác"}
                hideSearch={true}
                hideArrows={dataDotKhac?.length<3}
                handleNext={() => {
                  if (slideRef.current) {
                    // @ts-ignore
                    slideRef?.current?.handleNext();
                  }
                }}
                handlePrev={() => {
                  if (slideRef.current) {
                    // @ts-ignore
                    slideRef?.current?.handlePrev();
                  }
                }}
              />
              <div className="mt-[28px]">
                {dataDotKhac?.length >= 3 ? (
                  <>
                    {" "}
                    <SlideComponents slidesToShow={3} auto ref={slideRef}>
                      {dataDotKhac?.map((val) => {
                        return (
                          <div
                            className="col-span-1"
                            onClick={() => {
                              router.push(`/dot-tuyen-dung/${val?._id}`);
                            }}
                          >
                            <CardDotTuyenDung
                              title={val?.tenDotTuyenDung}
                              time={val?.thoiGianBDNhanHoSo}
                            />
                          </div>
                        );
                      })}
                    </SlideComponents>
                  </>
                ) : (
                  <>
                    {" "}
                    <div className="grid xl:grid-cols-3 md:grid-cols-2  grid-cols-1 gap-[20px] mt-[28px]">
                      {dataDotKhac?.map((val) => {
                        return (
                          <div
                            className="col-span-1"
                            onClick={() => {
                              router.push(`/dot-tuyen-dung/${val?._id}`);
                            }}
                          >
                            <CardDotTuyenDung
                              title={val?.tenDotTuyenDung}
                              time={val?.thoiGianBDNhanHoSo}
                            />
                          </div>
                        );
                      })}
                    </div>
                  </>
                )}
              </div>
            </div>
          </div>
        </>
      )}
    </DotTuyenDungChiTietWrapper>
  );
};

const DotTuyenDungChiTietWrapper = styled.div`
  .title-chi-tiet {
    color: var(--TEXT-01, #212529);
    font-family: Arial;
    font-size: 24px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
  }
  .time {
    .card-time {
      color: var(--TEXT-01, #212529);
      font-family: Arial;
      font-style: normal;
      font-weight: 400;
      line-height: normal;

      border-radius: 8px;
      background: var(--Gray, #f4f4f4);
    }
  }
`;
export default DotTuyenDungChiTiet;
