import styled from "styled-components";
import { useRouter } from "next/router";
import { useContext, useEffect, useState } from "react";
import { exportHoSo, traCuuHoSo } from "../../service/hoso/hoso";
import { Breadcrumb, Form } from "antd";
import ThongTinBieuMau from "../../components/ThongTinBieuMau";
import { AuthContext } from "../../context/AuthContext";
import fileDownload from "js-file-download";
import { getFilenameHeader } from "../../utils/util";
import { ThongTinNhanSu } from "../../Interface/ThongTinNhanSu/typing";
import moment from "moment";
import CardTime from "../../components/CardTime";

const ChiTietHoSo = () => {
  const router = useRouter();
  const [form] = Form.useForm();
  const { setDataDaoTao } = useContext(AuthContext);
  const [dataChiTietHoSo, setDataChiTietHoSo] =
    useState<ThongTinNhanSu.ThongTinTraCuu>();
  const getData = async (id: string) => {
    try {
      const res = await traCuuHoSo(undefined, id);
      if (res) {
        setDataChiTietHoSo(res?.data?.data);
      }
    } catch (e) {
      console.log(e);
    }
  };
  const handleExportHoSo = async (id?: string) => {
    try {
      if (id) {
        const res = await exportHoSo(id);
        if (res) {
          fileDownload(res?.data, getFilenameHeader(res));
        }
      }
    } catch (e) {
      console.log(e);
    }
  };
  useEffect(() => {
    if (router?.query?.id) {
      getData(router?.query?.id?.toString());
    }
  }, [router]);

  useEffect(() => {
    if (dataChiTietHoSo?.hoSo) {
      form.setFieldsValue({
        ...dataChiTietHoSo?.hoSo,

        dienBienLuong:
          dataChiTietHoSo?.hoSo?.danhSachDienBienLuong?.[0]?.ngachLuong?.ten,
        daoTaoBoiDuong:
          dataChiTietHoSo?.hoSo?.danhSachQuaTrinhCuDiDaoTaoBoiDuong?.[0]
            ?.loaiBoiDuong?.ten,
        congTac:
          dataChiTietHoSo?.hoSo?.danhSachQuaTrinhCongTac?.[0]?.donViCongTac,

        dsQuanHeGiaDinhVeBanThan:
          dataChiTietHoSo?.hoSo?.danhSachQuanHeGiaDinhVeBanThan,
        dsKhenThuong: dataChiTietHoSo?.hoSo?.danhSachKhenThuong,
        dsKyLuat: dataChiTietHoSo?.hoSo?.danhSachKyLuat,
        dsQuanHeGiaDinhVeBenVoChong:
          dataChiTietHoSo?.hoSo?.danhSachQuanHeGiaDinhVeBenVoChong,
        dsQuaTrinhCongTac: dataChiTietHoSo?.hoSo?.danhSachQuaTrinhCongTac,
        dsQuaTrinhDaoTaoBoiDuong:
          dataChiTietHoSo?.hoSo?.danhSachQuaTrinhDaoTaoBoiDuong,
        dsHocHam: dataChiTietHoSo?.hoSo?.danhSachHocHam,
        dsThongTinTrinhDoDaoTao:
          dataChiTietHoSo?.hoSo?.danhSachThongTinTrinhDoDaoTao,

        dsBoiDuongQuocPhongAnNinh:
          dataChiTietHoSo?.hoSo?.danhSachBoiDuongQuocPhongAnNinh,
        dsDienBienKhoan: dataChiTietHoSo?.hoSo?.danhSachDienBienKhoan,
        dsDienBienLuong: dataChiTietHoSo?.hoSo?.danhSachDienBienLuong,

        dsDienBienPhuCap: dataChiTietHoSo?.hoSo?.danhSachDienBienPhuCap,
        dsDoiTuongChinhSach: dataChiTietHoSo?.hoSo?.danhSachDoiTuongChinhSach,
        dsDonViCanBoViTri: dataChiTietHoSo?.hoSo?.danhSachDonViCanBoViTri,
        dsThongTinNgoaiNgu: dataChiTietHoSo?.hoSo?.danhSachThongTinNgoaiNgu,
        dsThongTinTrinhDoLyLuanChinhTri:
          dataChiTietHoSo?.hoSo?.danhSachThongTinTrinhDoLyLuanChinhTri,
        dsThongTinTrinhDoQuanLyHanhChinh:
          dataChiTietHoSo?.hoSo?.danhSachThongTinTrinhDoQuanLyHanhChinh,
        dsThongTinTrinhDoQuanLyNhaNuoc:
          dataChiTietHoSo?.hoSo?.danhSachThongTinTrinhDoQuanLyNhaNuoc,

        dsThongTinTinHoc: dataChiTietHoSo?.hoSo?.danhSachThongTinTinHoc,
      });
      setDataDaoTao(dataChiTietHoSo?.hoSo);
    }
  }, [dataChiTietHoSo]);
  return (
    <ChiTietHoSoWrapper>
      <div className="container mx-auto">
        <div className="my-[10px]  px-[20px] xl:px-0">
          <Breadcrumb
            separator=">"
            items={[
              {
                title: "Trang chủ",
                href: "/",
              },
              {
                title: "Vị trí việc làm",
                href: "/#vi-tri-viec-lam",
              },
              {
                title: "Xem hồ sơ",
              },
            ]}
          />
        </div>
      </div>
      <div className="bg-white ">
        <div className="container mx-auto">
          <div className="title-ung-tuyen py-[27px]">
            <div className="title-ung-tuyen-1 text-center">
              {dataChiTietHoSo?.viTri?.tenViTri ??
                dataChiTietHoSo?.hoSo?.viTriUngTuyen}
            </div>
            <div className="title-ung-tuyen-2 text-center mt-[8px]">
              {dataChiTietHoSo?.viTri?.dotTuyenDung?.tenDotTuyenDung}
            </div>
            <CardTime timeStart={dataChiTietHoSo?.viTri?.dotTuyenDung?.thoiGianBDNhanHoSo} timeEnd={dataChiTietHoSo?.viTri?.dotTuyenDung?.thoiGianKTNhanHoSo} align={'center'}/>
            <div className="mt-[26px] flex justify-center">
              <button
                className="btn-nop w-[223px] h-[42px] flex justify-center items-center"
                onClick={() => {
                  handleExportHoSo(dataChiTietHoSo?.hoSo?._id);
                }}
              >
                In hồ sơ
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className="bg-white mt-[12px] pt-[24px] pb-[40px]">
        <div className="container mx-auto">
          <ThongTinBieuMau
            form={form}
            disabled={true}
            dataHoSo={dataChiTietHoSo?.hoSo}
          />
        </div>
      </div>
    </ChiTietHoSoWrapper>
  );
};

const ChiTietHoSoWrapper = styled.div`
  .title-ung-tuyen-1 {
    color: var(--TEXT-01, #212529);
    text-align: center;
    font-family: Arial;
    font-size: 24px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
  }
  .title-ung-tuyen-2 {
    color: var(--Dark-Gray, #2d2d2d);
    text-align: center;
    font-family: Arial;
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
  }
  .btn-nop {
    color: #fff;
    text-align: center;
    font-family: Arial;
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;

    border-radius: 4px;
    background: var(--Main-2, #2387e3);
  }
`;
export default ChiTietHoSo;
