import React, { useContext, useEffect, useRef, useState } from "react";
import styled from "styled-components";
import { useForm } from "react-hook-form";
import { useRouter } from "next/router";
import HomePage from "../components/Home";
import { AuthContext } from "../context/AuthContext";
import axios from "axios";
import { gateWay, ip, ip3 } from "../service/ip";

const Home = () => {
  const [showMenu, setShowMenu] = useState<boolean>(false);
  const [typeMenu, setTypeMenu] = useState<string>("home");

  const { setDataHome, setDataDaoTao } = useContext(AuthContext);
  const dataFetchedRef = useRef(false);
  const router = useRouter();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const {
    register: register2,
    formState: { errors: errors2 },
    handleSubmit: handleSubmit2,
  } = useForm();
  const onSubmit = (data: any) => {
    if (data?.keyword) {
      router.push(`/tim-kiem?keyword=${data?.keyword}`);
    }
  };
  useEffect(() => {
    if (router) {
      if (router.asPath === "/#tuyen-sinh") {
        setTypeMenu("tuyen-sinh");
      } else {
        setTypeMenu("home");
      }
    }
  }, [router]);

  useEffect(() => {
    if (dataFetchedRef.current) return;
    dataFetchedRef.current = true;

  }, []);

  return (
    <HomeWrapper>
       <HomePage />
    </HomeWrapper>
  );
};
const HomeWrapper = styled.div`
 
`;
export default Home;
// export const getStaticPaths = (async () => {
//   return {
//     paths: [
//       {
//         params: {
//
//         },
//       }, // See the "paths" section below
//     ],
//     fallback: true, // false or "blocking"
//   }
// })
// export async function getServerSideProps() {
//     const res = await axios.post(`${ip}/daotao/v5/KhoaDaoTao/GetData/Free`, {
//       pageInfo: {
//         page: 1,
//         pageSize: 10,
//       },
//       fields: "id,avatar,ten,tuNgay,denNgay,ghiChu,soLopDangMo",
//     });
//     const data = await res?.data;
//     const res2 = await axios.post(
//       `${ip}/daotao/v5/DMNhomChuongTrinhDaoTao/GetData/Public`,
//       {
//         filters: [],
//         sorts: [
//           {
//             field: "thuTu",
//             dir: -1,
//           },
//         ],
//         includes: [],
//         pageInfo: {
//           page: 1,
//           pageSize: 15,
//         },
//         fields: "id,ma,ten,suDung,ghiChu,avatar",
//       }
//     );
//     const dataNhomChuongTrinh = await res2?.data;
//
//     return {
//       props: {
//         dataKhoaHocFree: data,
//         dataNhomChuongTrinh: dataNhomChuongTrinh,
//       },
//     };
// }
