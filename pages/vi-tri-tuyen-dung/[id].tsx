import styled from "styled-components";
import { useRouter } from "next/router";
import { useEffect, useRef, useState } from "react";
import { getDetailViecLam } from "../../service/vitrivieclam/vitrivieclam";
import moment from "moment";
import { Breadcrumb } from "antd";
import Title from "../../components/Title";
import SlideComponents from "../../components/Slide";
import axios from "axios";
import { ip3 } from "../../service/ip";
import CardViTri from "../../components/ViTriViecLam/components/CardViTri";
import CardTime from "../../components/CardTime";

const ViTriTuyenDungDetai = () => {
  const router = useRouter();
  const slideRef = useRef(null);
  const [dataDetail, setDataDetail] = useState<ViTriViecLam.IRecord>();
  const [dataViTriKhac, setDataViTriKhac] = useState<ViTriViecLam.IRecord[]>(
    []
  );
  const getDataDetail = async (id: string) => {
    try {
      const res = await getDetailViecLam(id);
      if (res) {
        setDataDetail(res?.data?.data);
      }
    } catch (e) {
      console.log(e);
    }
  };
  const getDataViTriKhac = async (id: string) => {
    try {
      const res = await axios.get(`${ip3}/vi-tri-tuyen-dung-public/many`, {
        params: {
          // page: 1,
          // limit: 10,
          // condition: JSON.stringify({
          //   filters: [{ field: "tenViTri", operator: "contain", values: [key] }],
          // }),
        },
      });
      if (res) {
        const data = res?.data?.data ?? [];
        setDataViTriKhac(
          data?.filter((item: { _id: string }) => item?._id !== id)
        );
      }
    } catch (e) {
      console.log(e);
    }
  };
  useEffect(() => {
    if (router?.query?.id) {
      getDataDetail(router?.query?.id?.toString());
      getDataViTriKhac(router?.query?.id?.toString());
    }
  }, [router]);
  return (
    <ViTriTuyenDungDetaiWrapper>
      <div className="container mx-auto">
        <div className="my-[10px]  px-[20px] xl:px-0">
          <Breadcrumb
            separator=">"
            items={[
              {
                title: "Trang chủ",
                href: "/",
              },
              {
                title: "Vị trí việc làm",
                href: "/#vi-tri-viec-lam",
              },
              {
                title: dataDetail?.tenViTri,
              },
            ]}
          />
        </div>
        <div className="flex justify-center">
          <div className="bg-white py-[28px] px-[20px] mb-[50px] xl:w-[1000px] w-full">
            <div className="top-content">
              <div className="title">{dataDetail?.tenViTri}</div>
              <div className="information xl:flex grid grid-cols-2 justify-between mt-[20px] gap-[20px]">
                <div className="flex items-center">
                  <img src="/icon/icon-hoc.svg" />
                  <div className="ml-[12px]">
                    <div className="icon-info-1 mb-[6px]">Số lượng</div>
                    <div className="icon-info-2">{dataDetail?.soLuong}</div>
                  </div>
                </div>
                <div className="flex items-center">
                  <img src="/icon/icon-depart.svg" />
                  <div className="ml-[12px]">
                    <div className="icon-info-1 mb-[6px]">Đơn vị/Phòng ban</div>
                    <div className="icon-info-2">{dataDetail?.donVi?.ten}</div>
                  </div>
                </div>
                <div className="flex items-center">
                  <img src="/icon/icon-chuyen-mon.svg" />
                  <div className="ml-[12px]">
                    <div className="icon-info-1 mb-[6px]">
                      Trình độ chuyên môn
                    </div>
                    <div className="icon-info-2">
                      {dataDetail?.trinhDoChuyenMon}
                    </div>
                  </div>
                </div>
              </div>
              <CardTime timeStart={dataDetail?.dotTuyenDung?.thoiGianBDNhanHoSo} timeEnd={dataDetail?.dotTuyenDung?.thoiGianKTNhanHoSo} align={'left'}/>
              <div className="mt-[28px]">
                <button
                  disabled={
                    moment().isBefore(
                      moment(dataDetail?.dotTuyenDung?.thoiGianBDNhanHoSo)
                    ) ||
                    moment().isAfter(
                      dataDetail?.dotTuyenDung?.thoiGianKTNhanHoSo
                    )
                  }
                  className="btn-ung-tuyen w-[190px] h-[42px] flex justify-center items-center"
                  onClick={() => {
                    router.push(`/ung-tuyen/${dataDetail?._id}`);
                  }}
                >
                  Ứng tuyển vị trí
                </button>
              </div>
            </div>
            <div className=" bg-[#F4F4F4] h-[1px] my-[28px]"></div>
            <div className="content-bottom">
              {dataDetail?.dotTuyenDung?.tieuChuanDieuKienChung && (
                <>
                  {" "}
                  <div className="title-bottom">
                    Tiêu chuẩn, điều kiện chung
                  </div>
                  <div
                    dangerouslySetInnerHTML={{
                      __html:
                        dataDetail?.dotTuyenDung?.tieuChuanDieuKienChung ?? "",
                    }}
                  ></div>
                </>
              )}

              {dataDetail?.yeuCau && (
                <>
                  <div className="title-bottom mt-[28px]">Yêu cầu ứng viên</div>
                  <div
                    dangerouslySetInnerHTML={{
                      __html: dataDetail?.yeuCau ?? "",
                    }}
                  ></div>
                </>
              )}
            </div>
          </div>
        </div>
      </div>
      {dataViTriKhac?.length > 0 && (
        <>
          {" "}
          <div className="khac bg-white py-[50px]  px-[20px] xl:px-0">
            <div className="container mx-auto">
              <Title
                title={"Gợi ý tương tự"}
                uppercase
                hideSearch={true}
                hideArrows={dataViTriKhac?.length<3}
                handleNext={() => {
                  if (slideRef.current) {
                    // @ts-ignore
                    slideRef?.current?.handleNext();
                  }
                }}
                handlePrev={() => {
                  if (slideRef.current) {
                    // @ts-ignore
                    slideRef?.current?.handlePrev();
                  }
                }}
              />
              <div className="mt-[28px]">
                {dataViTriKhac?.length >= 3 ? (
                  <>
                    {" "}
                    <SlideComponents slidesToShow={2} auto ref={slideRef}>
                      {dataViTriKhac?.map((val) => {
                        return (
                          <div className="col-span-1">
                            <CardViTri
                              data={{
                                title: val?.tenViTri ?? "",
                                content: val?.yeuCau,
                                tags: [
                                  { name: `${val?.soLuong} số lượng` },
                                  {
                                    name: `${val?.trinhDoChuyenMon}`,
                                  },
                                  { name: val?.donVi?.ten },
                                ],
                                timeEnd: val?.dotTuyenDung?.thoiGianKTNhanHoSo,
                                timeStart:
                                  val?.dotTuyenDung?.thoiGianBDNhanHoSo,
                              }}
                              handleUngTuyen={(id) => {
                                router.push(`/ung-tuyen/${id}`);
                              }}
                              handleViewDetail={(id) => {
                                router.push(`/vi-tri-tuyen-dung/${id}`);
                              }}
                              id={val?._id}
                              disabled={
                                moment().isBefore(
                                  moment(val?.dotTuyenDung?.thoiGianBDNhanHoSo)
                                ) ||
                                moment().isAfter(
                                  moment(val?.dotTuyenDung?.thoiGianKTNhanHoSo)
                                )
                              }
                            />
                          </div>
                        );
                      })}
                    </SlideComponents>
                  </>
                ) : (
                  <>
                    {" "}
                    <div className="grid grid-cols-2 gap-[20px] mt-[28px]">
                      {dataViTriKhac?.map((val) => {
                        return (
                          <div className="col-span-1">
                            <CardViTri
                              data={{
                                title: val?.tenViTri ?? "",
                                content: val?.yeuCau,
                                tags: [
                                  { name: `${val?.soLuong} số lượng` },
                                  {
                                    name: `${val?.trinhDoChuyenMon}`,
                                  },
                                  { name: val?.donVi?.ten },
                                ],
                                timeEnd: val?.dotTuyenDung?.thoiGianKTNhanHoSo,
                                timeStart:
                                  val?.dotTuyenDung?.thoiGianBDNhanHoSo,
                              }}
                              handleUngTuyen={(id) => {
                                router.push(`/ung-tuyen/${id}`);
                              }}
                              handleViewDetail={(id) => {
                                router.push(`/vi-tri-tuyen-dung/${id}`);
                              }}
                              id={val?._id}
                              disabled={
                                moment().isBefore(
                                  moment(val?.dotTuyenDung?.thoiGianBDNhanHoSo)
                                ) ||
                                moment().isAfter(
                                  moment(val?.dotTuyenDung?.thoiGianKTNhanHoSo)
                                )
                              }
                            />
                          </div>
                        );
                      })}
                    </div>
                  </>
                )}
              </div>
            </div>
          </div>
        </>
      )}
    </ViTriTuyenDungDetaiWrapper>
  );
};

const ViTriTuyenDungDetaiWrapper = styled.div`
  .information {
    .icon-info-1 {
      color: var(--Normal-gray, #b2b5c1);
      font-family: Arial;
      font-size: 15px;
      font-style: normal;
      font-weight: 400;
      line-height: normal;
    }
    .icon-info-2 {
      color: var(--TEXT-01, #212529);
      font-family: Arial;
      font-size: 16px;
      font-style: normal;
      font-weight: 400;
      line-height: normal;
    }
  }
  .title-bottom {
    color: var(--TEXT-01, #212529);
    font-family: Arial;
    font-size: 16px;
    font-style: normal;
    font-weight: 700;
    line-height: 125%; /* 20px */
  }
  .btn-ung-tuyen {
    border-radius: 4px;
    background: var(--Main-2, #2387e3);

    color: #fff;
    text-align: center;
    font-family: Arial;
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
  }
  .top-content {
    .title {
      color: var(--TEXT-01, #212529);
      font-family: Arial;
      font-size: 24px;
      font-style: normal;
      font-weight: 700;
      line-height: normal;
    }
  }
`;
export default ViTriTuyenDungDetai;
