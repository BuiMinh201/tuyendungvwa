import styled from "styled-components";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { traCuuHoSo } from "../service/hoso/hoso";
import { Spin, Steps } from "antd";
import { CopyOutlined } from "@ant-design/icons";
import KetQuaUngTuyen from "../components/KetQuaUngTuyen";
import { ThongTinNhanSu } from "../Interface/ThongTinNhanSu/typing";
import NoData from "../components/NoData";

const TraCuu = () => {
  const router = useRouter();
  const [dataChiTietHoSo, setDataChiTietHoSo] =
    useState<ThongTinNhanSu.ThongTinTraCuu>();
  const [current, setCurrent] = useState<number>(0);
  const [loading, setLoading] = useState<boolean>(false);
  const getData = async (cccd?: string, maHoSo?: string) => {
    try {
      setLoading(true);
      const res = await traCuuHoSo(cccd, maHoSo);
      if (res) {
        setDataChiTietHoSo(res?.data?.data);
        const dataRes = res?.data?.data;
        if (dataRes?.buoc1) setCurrent(0);
        if (dataRes?.buoc2) setCurrent(1);
        if (dataRes?.buoc3) setCurrent(2);
        if (dataRes?.buoc4) setCurrent(3);
      }
    } catch (e) {
      console.log(e);
    } finally {
      setLoading(false);
    }
  };
  useEffect(() => {
    if (router?.query?.cccd || router?.query?.maHoSo) {
      getData(
        router?.query?.cccd?.toString(),
        router?.query?.maHoSo?.toString()
      );
    }
  }, [router]);
  if (!loading) {
    if (dataChiTietHoSo) {
      return (
        <TraCuuWrapper>
          <div>
            <div className="ket-qua bg-white  mb-[12px]">
              <div className="container mx-auto">
                <div className="xl:flex justify-between items-center py-[24px]">
                  <div className="title-1 text-center xl:text-left">
                    Kết quả tra cứu
                  </div>
                  <div className=" flex items-center justify-center xl:justify-start mt-[8px] xl:mt-0">
                    <div className="title-info lg:w-[105px] mr-[8px]">
                      Mã hồ sơ
                    </div>
                    <div className="content-info">
                      <b>{router?.query?.maHoSo}</b>
                    </div>
                  </div>
                  <div className=" flex items-center justify-center xl:justify-start mt-[8px] xl:mt-0">
                    <div className="title-info lg:w-[105px] mr-[8px]">
                      CCCD/CMND
                    </div>
                    <div className="content-info">
                      <b>{router?.query?.cccd}</b>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="bg-white px-[20px] xl:px-0">
              <div className="container mx-auto pt-[24px] pb-[40px]">
                {" "}
                <div className="flex justify-between ">
                  <div className="w-3/4">
                    <Steps
                      onChange={(val) => setCurrent(val)}
                      current={current}
                      items={[
                        {
                          title: "Nộp hồ sơ dự tuyển",
                          disabled: !dataChiTietHoSo?.buoc1,
                        },
                        {
                          title: "Nhận lịch thi",
                          disabled: !dataChiTietHoSo?.buoc2,
                        },
                        {
                          title: "Nhận kết quả thi",
                          disabled: !dataChiTietHoSo?.buoc3,
                        },
                        {
                          title: "Kết quả trúng tuyển",
                          disabled: !dataChiTietHoSo?.buoc4,
                        },
                      ]}
                    />
                  </div>
                  <div>
                    <button
                      className="xem-ho-so w-[119px] h-[36px] flex justify-center items-center"
                      onClick={() => {
                        if (dataChiTietHoSo?.hoSo?.maHoSo)
                          router.push(
                            `/ho-so/${dataChiTietHoSo?.hoSo?.maHoSo}`
                          );
                      }}
                    >
                      Xem hồ sơ
                    </button>
                  </div>
                </div>
                <div className="mt-[32px]">
                  {current === 0 && (
                    <KetQuaUngTuyen
                      isModal={false}
                      data={dataChiTietHoSo?.hoSo}
                      type={"ho-so"}
                      dataVitri={dataChiTietHoSo?.viTri}
                    />
                  )}
                  {current === 1 && (
                    <KetQuaUngTuyen
                      isModal={false}
                      data={dataChiTietHoSo?.hoSo}
                      type={"lich-thi"}
                      dataVitri={dataChiTietHoSo?.viTri}
                    />
                  )}
                  {current === 2 && (
                    <KetQuaUngTuyen
                      isModal={false}
                      data={dataChiTietHoSo?.hoSo}
                      type={"ket-qua"}
                      dataVitri={dataChiTietHoSo?.viTri}
                    />
                  )}
                  {current === 3 && (
                    <KetQuaUngTuyen
                      isModal={false}
                      data={dataChiTietHoSo?.hoSo}
                      type={"trung-tuyen"}
                      dataVitri={dataChiTietHoSo?.viTri}
                    />
                  )}
                </div>
              </div>
            </div>
          </div>
        </TraCuuWrapper>
      );
    } else {
      return (
        <TraCuuWrapper>
          <div className="ket-qua bg-white  mb-[12px]">
            <div className="container mx-auto">
              <div className="xl:flex justify-between items-center py-[24px]">
                <div className="title-1 text-center xl:text-left">
                  Kết quả tra cứu
                </div>
                <div className=" flex items-center justify-center xl:justify-start mt-[8px] xl:mt-0">
                  <div className="title-info lg:w-[105px] mr-[8px]">
                    Mã hồ sơ
                  </div>
                  <div className="content-info">
                    <b>{router?.query?.maHoSo}</b>
                  </div>
                </div>
                <div className=" flex items-center justify-center xl:justify-start mt-[8px] xl:mt-0">
                  <div className="title-info lg:w-[105px] mr-[8px]">
                    CCCD/CMND
                  </div>
                  <div className="content-info">
                    <b>{router?.query?.cccd}</b>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <NoData
            title={
              "Thông tin hồ sơ của bạn hiện tại không có trên hệ thống vui lòng kiểm tra lại thông tin tra cứu"
            }
            hideTitleButton
          />
        </TraCuuWrapper>
      );
    }
  } else {
    return <Spin style={{height:'500px'}} spinning={loading}></Spin>;
  }
};

const TraCuuWrapper = styled.div`
  .xem-ho-so {
    border-radius: 4px;
    border: 1px solid var(--Main-2, #2387e3);

    color: var(--Main-2, #2387e3);
    text-align: center;
    font-family: Arial;
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
  }
  .title-1 {
    color: var(--TEXT-01, #212529);
    font-family: Arial;
    font-size: 24px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
  }
  .title-info {
    color: var(--Normal-gray, #b2b5c1);
    font-family: Arial;
    font-size: 15px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
  }
  .content-info {
    color: var(--TEXT-01, #212529);
    font-family: Arial;
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
  }
`;
export default TraCuu;
