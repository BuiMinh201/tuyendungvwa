export const dataEvent = [
	{
		imageUrl: "/images/home/event1.png",
		content: "Lễ ký kết chương trình phối hợp công tác giữa Trường Đào tạo,...",
		dateTime: "07/02/2023, 11:23",
		link: "https://canbotttt.edu.vn/2023/02/27/le-ky-ket-chuong-trinh-phoi-hop-cong-tac-giua-truong-dao-tao-boi-duong-can-bo-quan-ly-thong-tin-va-truyen-thong-va-vien-chien-luoc-chuyen-doi-so/",
	},
	{
		imageUrl: "/images/home/event2.png",
		content: "Khai giảng lớp “ Đào tạo nâng cao năng lực đội ngũ cán bộ...",
		dateTime: "07/02/2023, 11:23",
		link: "https://canbotttt.edu.vn/2023/02/20/khai-giang-lop-dao-tao-nang-cao-nang-luc-doi-ngu-can-bo-lam-cong-tac-thong-tin-doi-ngoai-nam-2020-tai-dien-bien/",
	},
	{
		imageUrl: "/images/home/event3.png",
		content: "Chi bộ Trường tổ chức Hội nghị tổng kết công tác Đảng năm 2022",
		dateTime: "07/02/2023, 11:23",
		link: "https://canbotttt.edu.vn/2022/12/28/chi-bo-truong-to-chuc-hoi-nghi-tong-ket-cong-tac-dang-nam-2022-va-trien-khai-phuong-huong-nhiem-vu-nam-2023/",
	},
	{
		imageUrl: "/images/home/event4.png",
		content: "Trường Đào tạo, Bồi dưỡng cán bộ quản lý Thông tin và Truyền thông",
		dateTime: "07/02/2023, 11:23",
		link: "https://canbotttt.edu.vn/2022/11/29/truong-dao-tao-boi-duong-can-bo-quan-ly-thong-tin-va-truyen-thong-to-chuc-ky-niem-40-nam-ngay-nha-giao-viet-nam-20-11-1982-20-11-2022/",
	},
	{
		imageUrl: "/images/home/event3.png",
		content: "Chi bộ Trường tổ chức Hội nghị tổng kết công tác Đảng năm 2022",
		dateTime: "07/02/2023, 11:23",
		link: "https://canbotttt.edu.vn/2023/02/27/le-ky-ket-chuong-trinh-phoi-hop-cong-tac-giua-truong-dao-tao-boi-duong-can-bo-quan-ly-thong-tin-va-truyen-thong-va-vien-chien-luoc-chuyen-doi-so/",
	},
];
export const dataEvent2 = [
	{
		imageUrl: "/images/home/image-event.png",
		content: "Lễ ký kết chương trình phối hợp công tác giữa Trường Đào tạo,...",
		description: "Ngày 3/3/2023, tại Sơn La, Công đoàn TT&TT Việt Nam tổ chức Hội nghị sơ kết phong trào thi ...",
		dateTime: "12/02/2023, 11:23",
		link: "https://canbotttt.edu.vn/2023/02/27/le-ky-ket-chuong-trinh-phoi-hop-cong-tac-giua-truong-dao-tao-boi-duong-can-bo-quan-ly-thong-tin-va-truyen-thong-va-vien-chien-luoc-chuyen-doi-so/",
	},
	{
		imageUrl: "/images/home/image-event-2.png",
		content: "Khai giảng lớp “ Đào tạo nâng cao năng lực đội ngũ cán bộ...",
		description: "Ngày 3/3/2023, tại Sơn La, Công đoàn TT&TT Việt Nam tổ chức Hội nghị sơ kết phong trào thi ...",
		dateTime: "07/02/2023, 11:23",
		link: "https://canbotttt.edu.vn/2023/02/20/khai-giang-lop-dao-tao-nang-cao-nang-luc-doi-ngu-can-bo-lam-cong-tac-thong-tin-doi-ngoai-nam-2020-tai-dien-bien/",
	},
	{
		imageUrl: "/images/home/image-event-3.png",
		content: "Chi bộ Trường tổ chức Hội nghị tổng kết công tác Đảng năm 2022",
		description: "Ngày 3/3/2023, tại Sơn La, Công đoàn TT&TT Việt Nam tổ chức Hội nghị sơ kết phong trào thi ...",
		dateTime: "07/02/2023, 11:23",
		link: "https://canbotttt.edu.vn/2022/12/28/chi-bo-truong-to-chuc-hoi-nghi-tong-ket-cong-tac-dang-nam-2022-va-trien-khai-phuong-huong-nhiem-vu-nam-2023/",
	},
	{
		imageUrl: "/images/home/image-event-4.png",
		content: "Trường Đào tạo, Bồi dưỡng cán bộ quản lý Thông tin và Truyền thông",
		description: "Ngày 3/3/2023, tại Sơn La, Công đoàn TT&TT Việt Nam tổ chức Hội nghị sơ kết phong trào thi ...",
		dateTime: "07/02/2023, 11:23",
		link: "https://canbotttt.edu.vn/2022/11/29/truong-dao-tao-boi-duong-can-bo-quan-ly-thong-tin-va-truyen-thong-to-chuc-ky-niem-40-nam-ngay-nha-giao-viet-nam-20-11-1982-20-11-2022/",
	},
	{
		imageUrl: "/images/home/image-event-2.png",
		content: "Khai giảng lớp “ Đào tạo nâng cao năng lực đội ngũ cán bộ...",
		description: "Ngày 3/3/2023, tại Sơn La, Công đoàn TT&TT Việt Nam tổ chức Hội nghị sơ kết phong trào thi ...",
		dateTime: "07/02/2023, 11:23",
		link: "https://canbotttt.edu.vn/2023/02/20/khai-giang-lop-dao-tao-nang-cao-nang-luc-doi-ngu-can-bo-lam-cong-tac-thong-tin-doi-ngoai-nam-2020-tai-dien-bien/",
	},
];
export const dataCTDaoTao = [
	{
		imageUrl: "/images/home/image-event.png",
		content: "Lễ ký kết chương trình phối hợp công tác giữa Trường Đào tạo,...",
		description: "Ngày 3/3/2023, tại Sơn La, Công đoàn TT&TT Việt Nam tổ chức Hội nghị sơ kết phong trào thi ...",
		dateTime: "12/02/2023, 11:23",
		link: "https://canbotttt.edu.vn/2023/02/27/le-ky-ket-chuong-trinh-phoi-hop-cong-tac-giua-truong-dao-tao-boi-duong-can-bo-quan-ly-thong-tin-va-truyen-thong-va-vien-chien-luoc-chuyen-doi-so/",
	},
	{
		imageUrl: "/images/home/image-event-2.png",
		content: "Khai giảng lớp “ Đào tạo nâng cao năng lực đội ngũ cán bộ...",
		description: "Ngày 3/3/2023, tại Sơn La, Công đoàn TT&TT Việt Nam tổ chức Hội nghị sơ kết phong trào thi ...",
		dateTime: "07/02/2023, 11:23",
		link: "https://canbotttt.edu.vn/2023/02/20/khai-giang-lop-dao-tao-nang-cao-nang-luc-doi-ngu-can-bo-lam-cong-tac-thong-tin-doi-ngoai-nam-2020-tai-dien-bien/",
	},
];
export const dataTinTuc = [
	{
		imageUrl: "/images/home/image-event.png",
		content: "Lễ ký kết chương trình phối hợp công tác giữa Trường Đào tạo,...",
		description: "Ngày 3/3/2023, tại Sơn La, Công đoàn TT&TT Việt Nam tổ chức Hội nghị sơ kết phong trào thi ...",
		dateTime: "12/02/2023, 11:23",
		link: "https://canbotttt.edu.vn/2023/02/27/le-ky-ket-chuong-trinh-phoi-hop-cong-tac-giua-truong-dao-tao-boi-duong-can-bo-quan-ly-thong-tin-va-truyen-thong-va-vien-chien-luoc-chuyen-doi-so/",
	},
	{
		imageUrl: "/images/home/image-event-2.png",
		content: "Khai giảng lớp “ Đào tạo nâng cao năng lực đội ngũ cán bộ...",
		description: "Ngày 3/3/2023, tại Sơn La, Công đoàn TT&TT Việt Nam tổ chức Hội nghị sơ kết phong trào thi ...",
		dateTime: "07/02/2023, 11:23",
		link: "https://canbotttt.edu.vn/2023/02/20/khai-giang-lop-dao-tao-nang-cao-nang-luc-doi-ngu-can-bo-lam-cong-tac-thong-tin-doi-ngoai-nam-2020-tai-dien-bien/",
	},
	{
		imageUrl: "/images/home/image-event-3.png",
		content: "Chi bộ Trường tổ chức Hội nghị tổng kết công tác Đảng năm 2022",
		description: "Ngày 3/3/2023, tại Sơn La, Công đoàn TT&TT Việt Nam tổ chức Hội nghị sơ kết phong trào thi ...",
		dateTime: "07/02/2023, 11:23",
		link: "https://canbotttt.edu.vn/2022/12/28/chi-bo-truong-to-chuc-hoi-nghi-tong-ket-cong-tac-dang-nam-2022-va-trien-khai-phuong-huong-nhiem-vu-nam-2023/",
	},
	{
		imageUrl: "/images/home/image-event-4.png",
		content: "Trường Đào tạo, Bồi dưỡng cán bộ quản lý Thông tin và Truyền thông",
		description: "Ngày 3/3/2023, tại Sơn La, Công đoàn TT&TT Việt Nam tổ chức Hội nghị sơ kết phong trào thi ...",
		dateTime: "07/02/2023, 11:23",
		link: "https://canbotttt.edu.vn/2022/11/29/truong-dao-tao-boi-duong-can-bo-quan-ly-thong-tin-va-truyen-thong-to-chuc-ky-niem-40-nam-ngay-nha-giao-viet-nam-20-11-1982-20-11-2022/",
	},
];
export const dataDaoTao = [
	{
		imageUrl: "/images/home/image-event.png",
		content: "Lễ ký kết chương trình phối hợp công tác giữa Trường Đào tạo,...",
		description: "Ngày 3/3/2023, tại Sơn La, Công đoàn TT&TT Việt Nam tổ chức Hội nghị sơ kết phong trào thi ...",
		dateTime: "12/02/2023, 11:23",
		link: "https://canbotttt.edu.vn/2023/02/27/le-ky-ket-chuong-trinh-phoi-hop-cong-tac-giua-truong-dao-tao-boi-duong-can-bo-quan-ly-thong-tin-va-truyen-thong-va-vien-chien-luoc-chuyen-doi-so/",
	},
	{
		imageUrl: "/images/home/image-event-2.png",
		content: "Khai giảng lớp “ Đào tạo nâng cao năng lực đội ngũ cán bộ...",
		description: "Ngày 3/3/2023, tại Sơn La, Công đoàn TT&TT Việt Nam tổ chức Hội nghị sơ kết phong trào thi ...",
		dateTime: "07/02/2023, 11:23",
		link: "https://canbotttt.edu.vn/2023/02/20/khai-giang-lop-dao-tao-nang-cao-nang-luc-doi-ngu-can-bo-lam-cong-tac-thong-tin-doi-ngoai-nam-2020-tai-dien-bien/",
	},
	{
		imageUrl: "/images/home/image-event-3.png",
		content: "Chi bộ Trường tổ chức Hội nghị tổng kết công tác Đảng năm 2022",
		description: "Ngày 3/3/2023, tại Sơn La, Công đoàn TT&TT Việt Nam tổ chức Hội nghị sơ kết phong trào thi ...",
		dateTime: "07/02/2023, 11:23",
		link: "https://canbotttt.edu.vn/2022/12/28/chi-bo-truong-to-chuc-hoi-nghi-tong-ket-cong-tac-dang-nam-2022-va-trien-khai-phuong-huong-nhiem-vu-nam-2023/",
	},
	{
		imageUrl: "/images/home/image-event-4.png",
		content: "Trường Đào tạo, Bồi dưỡng cán bộ quản lý Thông tin và Truyền thông",
		description: "Ngày 3/3/2023, tại Sơn La, Công đoàn TT&TT Việt Nam tổ chức Hội nghị sơ kết phong trào thi ...",
		dateTime: "07/02/2023, 11:23",
		link: "https://canbotttt.edu.vn/2022/11/29/truong-dao-tao-boi-duong-can-bo-quan-ly-thong-tin-va-truyen-thong-to-chuc-ky-niem-40-nam-ngay-nha-giao-viet-nam-20-11-1982-20-11-2022/",
	},
	{
		imageUrl: "/images/home/image-event-4.png",
		content: "Trường Đào tạo, Bồi dưỡng cán bộ quản lý Thông tin và Truyền thông",
		description: "Ngày 3/3/2023, tại Sơn La, Công đoàn TT&TT Việt Nam tổ chức Hội nghị sơ kết phong trào thi ...",
		dateTime: "07/02/2023, 11:23",
		link: "https://canbotttt.edu.vn/2022/11/29/truong-dao-tao-boi-duong-can-bo-quan-ly-thong-tin-va-truyen-thong-to-chuc-ky-niem-40-nam-ngay-nha-giao-viet-nam-20-11-1982-20-11-2022/",
	},
	{
		imageUrl: "/images/home/image-event-4.png",
		content: "Trường Đào tạo, Bồi dưỡng cán bộ quản lý Thông tin và Truyền thông",
		description: "Ngày 3/3/2023, tại Sơn La, Công đoàn TT&TT Việt Nam tổ chức Hội nghị sơ kết phong trào thi ...",
		dateTime: "07/02/2023, 11:23",
		link: "https://canbotttt.edu.vn/2022/11/29/truong-dao-tao-boi-duong-can-bo-quan-ly-thong-tin-va-truyen-thong-to-chuc-ky-niem-40-nam-ngay-nha-giao-viet-nam-20-11-1982-20-11-2022/",
	},
];
export const dataNews = [
	{
		imageUrl: "/images/home/edu.png",
		content: "Thông báo chiêu sinh khóa ” Bồi dưỡng chức danh Phóng viên...",
		dateTime: "07/02/2023, 11:23",
	},
	{
		imageUrl: "/images/home/edu.png",
		content: "Thông báo thông tin chiêu sinh đào tạo thạc sỹ đợt 1 năm 2020",
		dateTime: "07/02/2023, 11:23",
	},
	{
		imageUrl: "/images/home/edu.png",
		content: "Thông báo chiêu sinh khóa ” Bồi dưỡng chức danh Phóng viên",
		dateTime: "07/02/2023, 11:23",
	},
	{
		imageUrl: "/images/home/edu.png",
		content: "Thông báo chiêu sinh khóa ” Bồi dưỡng chức danh Phóng viên...",
		dateTime: "07/02/2023, 11:23",
	},
];

export const dataBanner = [
	{
		imageUrl: "/images/icons/icon-dky.svg",
		title: "Đăng ký tuyển sinh",
	},
	{
		imageUrl: "/images/icons/icon-dky2.svg",
		title: "Văn bản hướng dẫn",
	},
	{
		imageUrl: "/images/icons/icon-dky3.svg",
		title: "Đề thi tham khảo",
	},
	{
		imageUrl: "/images/icons/icon-dky4.svg",
		title: "Hỗ trợ học viên",
	},
];
export const dataGallery = [
	{
		id: "1",
		imageUrl: "/images/home/gallery.png",
		content:
			"Đại hội Đảng bộ Bộ TT&TT: Bí thư Chi bộ, Hiệu trưởng Trường Đào tạo, Bồi dưỡng cán bộ quản lý Thông tin và Truyền thông được bầu vào Ban chấp hành Đảng bộ nhiệm kỳ 2020 – 2025",
		description:
			"Trong 02 ngày 17 và 18/8, tại Hà Nội, Đảng bộ Bộ TT&TT đã tổ chức Đại hội Đại biểu lần thứ V (nhiệm kỳ 2020-2025) với chủ đề “Nâng cao năng lực, phẩm chất, giữ vững kỷ cương và trách nhiệm nêu gương; phát huy truyền thống...",
	},
	{
		id: "2",
		imageUrl: "/images/home/gallery.png",
		content:
			"Đại hội Đảng bộ Bộ TT&TT: Bí thư Chi bộ, Hiệu trưởng Trường Đào tạo, Bồi dưỡng cán bộ quản lý Thông tin và Truyền thông được bầu vào Ban chấp hành Đảng bộ nhiệm kỳ 2020 – 2025",
		description:
			"Trong 02 ngày 17 và 18/8, tại Hà Nội, Đảng bộ Bộ TT&TT đã tổ chức Đại hội Đại biểu lần thứ V (nhiệm kỳ 2020-2025) với chủ đề “Nâng cao năng lực, phẩm chất, giữ vững kỷ cương và trách nhiệm nêu gương; phát huy truyền thống...",
	},
	{
		id: "3",
		imageUrl: "/images/home/gallery.png",
		content:
			"Đại hội Đảng bộ Bộ TT&TT: Bí thư Chi bộ, Hiệu trưởng Trường Đào tạo, Bồi dưỡng cán bộ quản lý Thông tin và Truyền thông được bầu vào Ban chấp hành Đảng bộ nhiệm kỳ 2020 – 2025",
		description:
			"Trong 02 ngày 17 và 18/8, tại Hà Nội, Đảng bộ Bộ TT&TT đã tổ chức Đại hội Đại biểu lần thứ V (nhiệm kỳ 2020-2025) với chủ đề “Nâng cao năng lực, phẩm chất, giữ vững kỷ cương và trách nhiệm nêu gương; phát huy truyền thống...",
	},
];
export const dataNewEvent = [
	{
		id: "1",
		imageUrl: "/images/home/tin-tuc-1.png",
		content:
			"Lễ ký kết chương trình phối hợp công tác giữa Trường Đào tạo, Bồi dưỡng cán bộ quản lý Thông tin và Truyền thông và Viện chiến lược chuyển đổi số",
		description:
			"Sáng ngày 22/02/2023 tại trụ sở làm việc, Trường Đào tạo, Bồi dưỡng cán bộ quản lý Thông tin và Truyền thông(Trường) đã tổ chức lễ ký kết thoả thuận hợp tác trong lĩnh vực chuyển đổi số với Viện chiến lược chuyển đổi số Tới dự Lễ ký kết có ông Nguyễn Minh Hồng- Chủ tịch Hội Truyền thông số Việt Nam, ...",
		dateTime: "07/02/2023, 11:23",
	},
	{
		id: "2",
		imageUrl: "/images/home/tin-tuc-2.png",
		content:
			"Khai giảng lớp “ Đào tạo nâng cao năng lực đội ngũ cán bộ làm công tác thông tin đối ngoại” năm 2020 tại Điện Biên",
		description:
			"Sáng ngày 15/7/2020, tại  Điện Biên, Trường Đào tạo, Bồi dưỡng cán bộ quản lý Thông tin và Truyền thông (Trường) đã tổ chức Lễ khai giảng lớp “ Đào tạo nâng cao năng lực đội ngũ cán bộ làm công tác thông tin đối ngoại” cho  học viên là cán bộ làm công tác quản lý, đội ngũ biên tập viên, phóng viên của các cơ quan, hông tấn, báo chí Việt Nam có hợp tác với Lào; ...",
		dateTime: "07/02/2023, 11:23",
	},
	{
		id: "3",
		imageUrl: "/images/home/tin-tuc-3.png",
		content:
			"Chi bộ Trường tổ chức Hội nghị tổng kết công tác Đảng năm 2022 và triển khai phương hướng, nhiệm vụ năm 2023",
		description:
			"Chiều ngày 22/12/2022, Chi bộ Trường Đào tạo, Bồi dưỡng cán bộ quản lý Thông tin tổ chức Hội nghị trực tuyến Tổng kết công tác Đảng năm 2022 và triển khai phương hướng, nhiệm vụ năm 2023. Tham dự Hội nghị, về phía Đảng ủy Bộ Thông tin và Truyền thông có đồng chí  Mai Anh Chung, ủy viên BTV Đảng ủy, về phía Chi bộ Trường có đ/c Đinh Đức Thiện, ...",
		dateTime: "07/02/2023, 11:23",
	},
	{
		id: "4",
		imageUrl: "/images/home/tin-tuc-4.png",
		content:
			"Lorem Ipsum is simply dummy text of the printing and typesetting text of the printing and typesetting text of the printing and typesetting",
		description:
			"Lorem Ipsum is simply dummy text of the printing and typesetting text of the printing and typesetting text of the printing and typesetting Lorem Ipsum is simply dummy text of the printing and typesetting text of the printing and typesetting text of the printing and typesetting",
		dateTime: "07/02/2023, 11:23",
	},
	{
		id: "5",
		imageUrl: "/images/home/tin-tuc-5.png",
		content:
			"Lorem Ipsum is simply dummy text of the printing and typesetting text of the printing and typesetting text of the printing and typesetting",
		description:
			"Lorem Ipsum is simply dummy text of the printing and typesetting text of the printing and typesetting text of the printing and typesetting Lorem Ipsum is simply dummy text of the printing and typesetting text of the printing and typesetting text of the printing and typesetting",
		dateTime: "07/02/2023, 11:23",
	},
];

export const dataTable1 = [
	{
		content: "Bồi dưỡng ngạch chuyên viên cao cấp",
		object: ["– Lãnh đạo cấp phòng và tương đương", "- Cán bộ quy hoạch lãnh đạo cấp phòng và cấp tương đương"],
		time: "04 tuần",
		note: "Nghị định số 101/2017/NĐ-CP ngày 01/9/2017",
	},
	{
		content: "Bồi dưỡng ngạch chuyên viên chính",
		object: ["– Lãnh đạo các cơ quan báo chí", "- Cán bộ diện quy hoạch lãnh đạo"],
		time: "02 tuần",
		note: "Nghị định số 101/2017/NĐ-CP ngày 01/9/2017",
	},
];
export const dataTable2 = [
	{
		content: "Bồi dưỡng ngạch chuyên viên cao cấp",
		time: "04 tuần",
		access: 30,
		register: 25,
	},
	{
		content: "Bồi dưỡng ngạch chuyên viên chính",
		time: "02 tuần",
		access: 30,
		register: 25,
	},
];

export const dataLienQuan = [
	{
		id: "1",
		imageUrl: "/images/home/image-phong-su.png",
		content:
			"Chinh phục thị trường nước ngoài, làm rạng danh non sông, là sứ mệnh của doanh nghiệp công nghệ số Việt Nam",
		dateTime: "07/02/2023, 11:23",
	},
	{
		id: "2",
		imageUrl: "/images/home/image-phong-su-2.png",
		content:
			"Khai giảng lớp “ Đào tạo nâng cao năng lực đội ngũ cán bộ làm công tác thông tin đối ngoại” năm 2020 tại Điện Biên",
		dateTime: "07/02/2023, 11:23",
	},
	{
		id: "3",
		imageUrl: "/images/home/image-phong-su-3.png",
		content:
			"Lễ ký kết chương trình phối hợp công tác giữa Trường Đào tạo, Bồi dưỡng cán bộ quản lý Thông tin và Truyền thông và Viện chiến lược chuyển đổi số",
		dateTime: "07/02/2023, 11:23",
	},
	{
		id: "4",
		imageUrl: "/images/home/image-phong-su-4.png",
		content:
			"Lễ ký kết chương trình phối hợp công tác giữa Trường Đào tạo, Bồi dưỡng cán bộ quản lý Thông tin và Truyền thông và Viện chiến lược chuyển đổi số",
		dateTime: "07/02/2023, 11:23",
	},
];
export const dataComment = [
	{
		name: "Chinh Chu",
		imageUrl: "/images/home/lien-quan1.png",
		comment:
			"Chinh phục thị trường nước ngoài, làm rạng danh non sông, là sứ mệnh của doanh nghiệp công nghệ số Việt Nam",
		dateTime: "07/02/2023, 11:23",
	},
	{
		name: "Ngọc Nguyễn",
		imageUrl: undefined,
		comment:
			"Khai giảng lớp “ Đào tạo nâng cao năng lực đội ngũ cán bộ làm công tác thông tin đối ngoại” năm 2020 tại Điện Biên",
		dateTime: "07/02/2023, 11:23",
	},
	{
		name: "Hải Dớ",
		imageUrl: undefined,
		comment:
			"Lễ ký kết chương trình phối hợp công tác giữa Trường Đào tạo, Bồi dưỡng cán bộ quản lý Thông tin và Truyền thông và Viện chiến lược chuyển đổi số",
		dateTime: "07/02/2023, 11:23",
	},
];
export const DataImage=[
	{
		name:'Thư viện ảnh',
		code:'ALBUMIMAGE'
	},
	{
		name:'Thư viện video',
		code:'ALBUMVIDEO'
	},
	{
		name:'Thư viện audio',
		code:'AUDIO'
	},
]