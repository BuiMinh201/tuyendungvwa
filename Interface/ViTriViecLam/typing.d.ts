declare module ViTriViecLam {
  export interface IRecord {
    _id: "65aa2f745f27a1cb6a142028";
    dotTuyenDungId: "6599185f2f52e730ab302c81";
    donViId: "652e1ccdf1730e6b5d9000da";
    chucVuId: "652e0d31f1730e6b5d8ff8df";
    donViViTriId: "652e0d31f1730e6b5d8ff8df";

    soLuong: 2;
    trinhDoChuyenMon: "Tiến sĩ";
    trinhDoChuyenMonId: "652e1341f1730e6b5d8fffaf";
    tenViTri: null;
    yeuCau: string;
    tieuChuan: string;
    createdAt: "2024-01-19T08:14:44.134Z";
    updatedAt: "2024-01-19T08:14:44.134Z";
    dotTuyenDung: {
      hoSoDuTuyen: {
        _id: "65bb5243a3aa240e1c8c562b";
        dotTuyenDungId: "65bb4521aa8db9002f1f3aac";
        batBuoc: true;
        tenGiayTo: "Giấy khai sinh";
        ghiChuHoSoDuTuyen: null;
        huongDan: null;
        yeuCau: null;
        urlFileHuongDan: [];
        createdAt: "2024-02-01T08:11:47.527Z";
        updatedAt: "2024-02-01T08:11:54.580Z";
      }[];
      tenDotTuyenDung: "Tuyển dụng nhân sự năm 2024 (đợt 1)1";
      thoiGianBDNhanHoSo: "2024-01-04T04:58:00.018Z";
      thoiGianKTNhanHoSo: "2024-01-05T04:58:03.238Z";
      tieuChuanDieuKienChung: string;
    };
    chucVu: {
      ten: "Giảng viên cơ hữu";
      ma: "14";
    };
    donVi: {
      ten: "Bộ môn Công nghệ thông tin";
      maDonVi: "12.01";
    };
  }
}
