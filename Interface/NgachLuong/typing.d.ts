declare module NgachLuong {
  export interface IRecordHemis {
    _id: string;
    ma: string;
    ten: string;
    suDung: boolean;
    loaiNgachLuongId: string;
    bacCaoNhat: number;
    soThangNangBac: number;
    soThangDatPCVKLanDau: number;
    soThangDatPCVKLanSau: number;
    tyLeDatPCVKLanDau: number;
    tyLeDatPCVKLanSau: number;
    tinhPhuCapThamNien: boolean;
    ghiChu: string;
    thoiGianGiuBac?: number;
  }
}
