declare module NgoaiNgu {
  export interface IRecord {
    _id: string;
    ma: string;
    tenNgonNgu: string;
    moTa: string;
  }
}
