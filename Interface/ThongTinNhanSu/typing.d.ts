import { type ChucVu } from "@/services/DanhMuc/ChucVu/typing";
import { type DonViViTri } from "@/services/DanhMuc/DonViViTri/typing";
import { type ETrangThaiChinhSua } from "../Core/constant";
import { type ELoaiCanBoKhac } from "../../DanhMuc/constant";
import {
  type EHangChucDanhNgheNghiep,
  type ELoaiCanBoGiangVien,
} from "@/services/DanhMuc/constant";
import { type ETrangThaiHoSoUngTuyen } from "./constants";
import { type DienBienLuong } from "../DienBienLuong/typing";
import { type DotTuyenDung } from "@/services/TuyenDung/DotTuyenDung/typing";

declare module ThongTinNhanSu {
  export interface IRecord {
    conViTriChucDanhCoHieuLuc: true;
    tenDonViTuyenDung: string;
    tenDonViViTriTuyenDung: string;
    sinhMaHeThong: true;
    isBanGiamDoc: false;
    guiMail: false;
    guiMailTiepNhan: false;
    guiMailXetDuyet: false;
    dotTuyenDungId: "6598df7d2f52e730ab302c72";
    hoTen: "Chu Đức Bách";
    maDonViChinh: any;
    queQuanThanhPhoTen: any;
    queQuanQuanTen: any;
    queQuanXaTen: any;
    hoKhauThanhPhoTen: any;
    hoKhauQuanTen: any;
    hoKhauXaTen: any;
    ghiChu: any;
    fullName: any;

    dsKhenThuong?: KhenThuong.IRecord[];
    danhSachKhenThuong?: KhenThuong.IRecord[];

    dsSangKien?: SangKien.IRecord[];
    danhSachSangKien?: SangKien.IRecord[];

    dsDoiTuongChinhSach?: DoituongChinhSach.IRecord[];
    danhSachDoiTuongChinhSach?: DoituongChinhSach.IRecord[];

    dsKyLuat?: KyLuat.IRecord[];
    danhSachKyLuat?: KyLuat.IRecord[];

    dsQuaTrinhDaoTaoBoiDuong?: QuaTrinhDaoTaoBoiDuong.IRecord[];
    danhSachQuaTrinhDaoTaoBoiDuong?: QuaTrinhDaoTaoBoiDuong.IRecord[];

    dsQuaTrinhCongTac?: QuaTrinhCongTac.IRecord[];
    danhSachQuaTrinhCongTac?: QuaTrinhCongTac.IRecord[];

    dsQuanHeGiaDinhVeBanThan?: QuanHeGiaDinhVeBanThan.IRecord[];
    danhSachQuanHeGiaDinhVeBanThan?: QuanHeGiaDinhVeBanThan.IRecord[];

    dsQuanHeGiaDinhVeBenVoChong?: QuanHeGiaDinhVeBenVoChong.IRecord[];
    danhSachQuanHeGiaDinhVeBenVoChong?: QuanHeGiaDinhVeBenVoChong.IRecord[];

    danhSachDonViCanBoViTri?: DonViCanBoViTri.IRecord[];
    dsDonViCanBoViTri?: DonViCanBoViTri.IRecord[];

    danhSachDienBienLuong?: DienBienLuong.IRecord[];
    dsDienBienLuong?: DienBienLuong.IRecord[];

    danhSachDienBienPhuCap?: DienBienPhuCap.IRecord[];
    danhSachDienBienKhoan?: DienBienKhoan.IRecord[];
    dsDienBienPhuCap?: DienBienPhuCap.IRecord[];
    dsDienBienKhoan?: DienBienKhoan.IRecord[];

    danhSachThongTinTrinhDoDaoTao?: ThongTinTrinhDoDaoTao.IRecord[];
    dsThongTinTrinhDoDaoTao?: ThongTinTrinhDoDaoTao.IRecord[];

    danhSachHocHam?: HocHam.IRecord[];
    dsHocHam?: HocHam.IRecord[];

    danhSachThongTinTinHoc?: TinHocCuaNhanSu.IRecord[];
    dsThongTinTinHoc?: TinHocCuaNhanSu.IRecord[];

    danhSachThongTinNgoaiNgu?: NgoaiNguCuaNhanSu.IRecord[];
    dsThongTinNgoaiNgu?: NgoaiNguCuaNhanSu.IRecord[];

    danhSachThongTinTrinhDoLyLuanChinhTri?: TrinhDoLyLuanChinhTriCuaNhanSu.IRecord[];
    dsThongTinTrinhDoLyLuanChinhTri?: TrinhDoLyLuanChinhTriCuaNhanSu.IRecord[];

    danhSachThongTinTrinhDoQuanLyHanhChinh?: TrinhDoQuanLyHanhChinhCuaNhanSu.IRecord[];
    dsThongTinTrinhDoQuanLyHanhChinh?: TrinhDoQuanLyHanhChinhCuaNhanSu.IRecord[];

    danhSachThongTinTrinhDoQuanLyNhaNuoc?: TrinhDoQuanLyNhaNuocCuaNhanSu.IRecord[];
    dsThongTinTrinhDoQuanLyNhaNuoc?: TrinhDoQuanLyNhaNuocCuaNhanSu.IRecord[];

    danhSachBoiDuongQuocPhongAnNinh?: BoiDuongQPANCuaNhanSu.IRecord[];
    dsBoiDuongQuocPhongAnNinh?: BoiDuongQPANCuaNhanSu.IRecord[];
    ngayVaoNganhYTe: any;
    tinhTrangHonNhanId: any;
    thamNien: any;
    chungChiSuPhamGiangVien: any;
    noiVaoDang: any;
    soTheDang: any;
    chucVuDangId: any;
    ngayXuatNgu: any;
    donViQuanDoi: any;
    chucVuQuanDoi: any;
    ngayVaoDoan: any;
    noiVaoDoan: any;
    soTheDoan: any;
    chucVuDoanId: any;
    ngayThamGiaCongDoan: any;
    noiThamGiaCongDoan: any;
    soTheCongDoan: any;
    hinhThucTuyenDungId: any;
    soBaoDanh: any;
    ngayBatDauLamViec: any;
    ngayVaoNganh: any;
    emailCanBo: any;
    ngachTuyenDungId: any;
    donViViTriTuyenDungId: any;
    anhDaiDien: any;
    loaiHinhLamViec: any;
    coQuanChuQuan: any;
    soGiayPhepLaoDong: any;
    maQuocTich: any;
    chucVuCongTac: any;
    donViDangCongTac: any;
    soHieuVienChuc: any;
    linhVucDaoTaoId: any;
    linhVucDaoTaoMa: any;
    soHochieu: any;
    toChucCuDen: any;
    thoiGianBatDauCuDen: any;
    thoiGianKetThucCuden: any;
    linhVuc: any;
    chuyenNganh: any;
    linhVucDaoTaoTen: any;
    tenNghiaVuDanQuanTuVe: any;
    ngayKetNapDanQuanTuVe: any;
    ngayHoanThanhNghiaVu: any;
    chucDanhDanQuanTuVe: any;
    soQuyetDinhHoanThanh: any;
    nguoiKyQuyetDinh: any;
    maHeThong: any;
    chatLuongNhanSu: any;
    namKyHopDong: any;
    trinhDoDaoTao: any;
    maNoiBo: any;
    tenDonViTuyenDung: any;
    tenDonViViTriTuyenDung: any;
    banGoc: any;
    thoiGianGuiDuyet: any;
    ngayBatDauLamViecTaiTruong: any;
    urlFileMinhChung: any;
    nganh: any;
    tuNgay: any;
    diemTuyenDung: {
      _id: "65af9e95a2b74736c99648e3";
      ghiChuDiemTuyenDung: any;
      dotTuyenDungId: "659917b22f52e730ab302c80";
      diemThiGiang: any;
      diemThiViet: any;
      diemPhongVan: any;
      diemUuTien: any;
      tongDiem: any;
      diaDiemThi: string;
      hoSoUngTuyenId: "65ad350320cb954a10e7f350";
      urlFileMinhChung: any;
      viTriUngTuyenId: "65a80ec41058c5b824eb1898";
      cccdCMND: "099882334567";
      maHoSo: "UV202412.01147";
      hoTen: "Chu Đức Chính";
      sdtCaNhan: any;
      email: "7a3dqh@gmail.com";
      donVi: any;
      viTriViecLam: any;
      lichThiViet: "2024-01-09T11:10:21.003Z";
      lichThiGiang: "2024-01-17T11:10:24.034Z";
      lichThiPhongVan: "2024-01-31T11:10:26.420Z";
      guiMailDiem: false;
      guiMailThi: false;
      lichThiPhongVanFormat: "18:10 31/01/2024";
      lichThiGiangFormat: "18:10 17/01/2024";
      lichThiVietFormat: "18:10 09/01/2024";
      namSinh: any;
      queQuan: any;
      trinhDoChuyenMon: any;
      trungTuyen: any;
      lichSuSuaDiem: any;
      createdAt: "2024-01-23T11:10:13.343Z";
      updatedAt: "2024-01-23T11:10:28.936Z";
    };
    nganhId: any;

    // Dùng để filters
    idDotCapNhat: string;
    change?: boolean | Record<string, any> | null;
    _id: string;
    ssoId: string;
    maCanBo: string;
    hoDem: string;
    ten: string;
    tenGoiKhac: string;
    biDanh: string;
    email: string;
    gioiTinh: string;
    ngaySinh: string;

    noiOQuanMa: string; // '926';
    noiOQuanTen: string; // 'Huyện Phong Điền';
    noiOSoNha: string; // 'Số 9/C3, Tổ 9';
    noiOThanhPhoMa: string; // '92';
    noiOThanhPhoTen: string; // 'Thành phố Cần Thơ';
    noiOXaMa: string; // '31312';
    noiOXaTen: string; // 'Xã Mỹ Khánh';
    noiSinhQuanMa: string; // '925';
    noiSinhQuanTen: string; // 'Huyện Cờ Đỏ';
    noiSinhThanhPhoMa: string; // '92';
    noiSinhThanhPhoTen: string; // 'Thành phố Cần Thơ';
    noiSinhXaMa: string; // '31261';
    noiSinhXaTen: string; // 'Thị trấn Cờ Đỏ';

    queQuanSoNha: string;
    queQuanDuong: string;
    queQuanThanhPhoMa: string;
    queQuanQuanMa: string;
    queQuanXaMa: string;
    hoKhauSoNha: string;
    hoKhauDuong: string;
    hoKhauThanhPhoMa: string;
    hoKhauQuanMa: string;
    hoKhauXaMa: string;

    sdtCaNhan: string;
    sdtNhaRieng: string;
    sdtCoQuan: string;
    quocTichId: string;
    tenQuocTich: string;
    maQuoctich: string;
    danTocId: string;
    tenDanToc: string;
    maDanToc: string;
    tonGiaoId: string;
    tenTonGiao: string;
    maTonGiao: string;
    cccdCMND: string;
    ngayCap: string;
    noiCap: string;
    tinhTrangHonNhan: any;
    tenNganHang: string;
    chiNhanh: string;
    soTaiKhoan: string;
    soSoBHXH: string;
    noiCapBHXH: string;
    ngayCapBHXH: string;
    ngayThamGiaBHXH: string;
    ghiChuBHXH: string;
    maSoThue: string;
    ngayCapMaSoThue: string;
    donViQuanLy: string;
    chieuCao: number;
    canNang: number;
    nhomMau: string;

    laDangVien: boolean;
    ngayVaoDangDuBi: string;
    ngayChinhThuc: string;
    ngayKetNap: string;
    ngayNhapNgu: string;

    laDoanVien: boolean;
    thamGiaQuanDoi: boolean;
    tinhTrangSucKhoe: string;
    laGiangVienCoHuu: boolean;
    laChuyenVien: boolean;
    laGiangVienThinhGiang: boolean;
    trinhDoLyLuanChinhTri: Partial<TrinhDoLyLuanChinhTri.IRecord>;
    trinhDoQuanLyHanhChinhId: string;
    trinhDoQuanLyHanhChinh: Partial<TrinhDoQuanLyHanhChinh.IRecord>;
    trinhDoQuanLyNhaNuoc: Partial<TrinhDoQuanLyNhaNuoc.IRecord>;

    trinhDoTinHocId: string;
    danhHieuPhongTangId: string;
    kienThucANQP: string;
    thuongTat: boolean;
    phanTramThuongTat: string;
    ngoaiNguId: string;
    khungNangLucNgoaiNguId: string;
    trangThaiChinhSua: ETrangThaiChinhSua;
    donVi: Partial<DonVi.IRecord>;
    laGiangVienNuocNgoai: boolean;
    loaiHoSo: string;
    loaiHoSoKhac: ELoaiCanBoKhac;
    lichSuBanThanKhaiRo: string;
    lichSuBanThanThamGia: string;
    lichSuBanThanCoThanNhan: string;

    chucVuChinhId: string; // '64803914c0fb5270804566ac';
    maChucVuChinh: string; // '63';
    donViChinhId?: string | null;
    donViChinh: Partial<DonVi.IRecord>;
    donViViTri: DonViViTri.IRecord;

    createdAt: string;
    updatedAt: string;

    loaiCanBoGiangVien: ELoaiCanBoGiangVien;

    chucDanhNgheNghiep: string;
    chucDanhGiangVienId: string;
    chucDanhGiangVien?: Partial<ChucDanh.IRecord>;

    listFileUrl: string[];
    urlAnhDaiDien: string;
    trangThai: string;

    ngayTuyenDung: string;
    donViTuyenDung: Partial<DonVi.IRecord>;
    donViTuyenDungId: string;
    dotTuyenDung: any;
    hinhThucTuyenDung: any;

    hocHam: string;
    hocVi: string;

    trangThaiKhoa: string;

    // Form hồ sơ
    donViId: string;
    stt: number;
    email365: string;
    ngachLuongId: string;
    ngachLuong: NgachLuong.IRecord;
    maNgachLuong: string;
    hang: EHangChucDanhNgheNghiep;
    bacLuongId: string | undefined | null;
    heSo: number | undefined;
    ngayHuong: string;
    trinhDoGiaoDucPhoThongId: string;
    trinhDoDaoTaoId: string | undefined;
    maNganh: string | undefined;
    trinhDoLyLuanChinhTriId: string;
    trinhDoQuanLyNhaNuocId: string;
    danhHieu: string;
    namDuocPhong: number;
    soTruongCongTac: string;
    donViViTriId: string;
    dsKhenThuong?: {
      _id: string;
      hinhThucKhenThuongId: string;
      soQuyetDinh: string;
      coQuanQuyetDinh: string;
      nguoiKy: string;
      ngayQuyetDinh: string;
      ngayKy: string;
      noiDung: string;
      urlFileUpload: string;
    }[];
    danhSachKhenThuong?: {
      _id: string;
      hinhThucKhenThuongId: string;
      soQuyetDinh: string;
      coQuanQuyetDinh: string;
      nguoiKy: string;
      ngayQuyetDinh: string;
      ngayKy: string;
      noiDung: string;
      urlFileUpload: string;
    }[];
    dsKyLuat?: {
      _id: string;
      capKyLuatId: string;
      hinhThucKyLuatId: string;
      soQuyetDinh: string;
      coQuanQuyetDinh: string;
      nguoiKy: string;
      ngayQuyetDinh: string;
      ngayKy: string;
      noiDung: string;
      hinhThucKyLuat: any;
      urlFileUpload: string;
    }[];
    danhSachKyLuat?: {
      _id: string;
      capKyLuatId: string;
      hinhThucKyLuatId: string;
      soQuyetDinh: string;
      coQuanQuyetDinh: string;
      nguoiKy: string;
      ngayQuyetDinh: string;
      ngayKy: string;
      noiDung: string;
      hinhThucKyLuat: any;
      urlFileUpload: string;
    }[];
    dsQuaTrinhCuDiDaoTaoBoiDuong?: {
      _id: string;
      tenKhoaBoiDuongTapHuan: string;
      donViToChuc: string;
      loaiBoiDuongId: string;
      diaDiemToChuc: string;
      thoiGianBatDau: string;
      thoiGianKetThuc: string;
      nguoiKinhPhi: string;
      chungChi: string;
      ngayCap: string;
    }[];
    danhSachQuaTrinhCuDiDaoTaoBoiDuong?: {
      _id: string;
      tenKhoaBoiDuongTapHuan: string;
      donViToChuc: string;
      loaiBoiDuongId: string;
      loaiBoiDuong: any;
      diaDiemToChuc: string;
      thoiGianBatDau: string;
      thoiGianKetThuc: string;
      nguoiKinhPhi: string;
      chungChi: string;
      ngayCap: string;
    }[];
    dsQuaTrinhCongTac?: {
      _id: string;
      thongTinNhanSuId: string;
      idGoc: string;
      tuThagNam: string;
      denThangNam: string;
      donViCongTac: string;
      chucDanh: string;
      chucVu: string;
      noiDung: string;
    }[];
    danhSachQuaTrinhCongTac?: {
      _id: string;
      thongTinNhanSuId: string;
      idGoc: string;
      tuThagNam: string;
      denThangNam: string;
      donViCongTac: string;
      chucDanh: string;
      chucVu: string;
      noiDung: string;
    }[];
    dsQuanHeGiaDinhVeBanThan?: {
      _id: string;
      thongTinNhanSuId: string;
      idGoc: string;
      hoVaTen: string;
      namSinh: number;
      moiQuanHe: string;
      noiDung: string;
      nguoiPhuThuoc: true;
      ngheNghiep: string;
      noiCongTac: string;
      thangSinh: number;
      ngaySinh: number;
    }[];
    danhSachQuanHeGiaDinhVeBanThan?: {
      _id: string;
      thongTinNhanSuId: string;
      idGoc: string;
      hoVaTen: string;
      namSinh: number;
      moiQuanHe: string;
      noiDung: string;
      nguoiPhuThuoc: true;
      ngheNghiep: string;
      noiCongTac: string;
      thangSinh: number;
      ngaySinh: number;
    }[];
    dsQuanHeGiaDinhVeBenVoChong?: {
      _id: string;
      thongTinNhanSuId: string;
      idGoc: string;
      hoVaTen: string;
      namSinh: number;
      moiQuanHe: string;
      noiDung: string;
      nguoiPhuThuoc: true;
      ngheNghiep: string;
      noiCongTac: string;
      thangSinh: number;
      ngaySinh: number;
    }[];
    danhSachQuanHeGiaDinhVeBenVoChong?: {
      _id: string;
      thongTinNhanSuId: string;
      idGoc: string;
      hoVaTen: string;
      namSinh: number;
      moiQuanHe: string;
      noiDung: string;
      nguoiPhuThuoc: true;
      ngheNghiep: string;
      noiCongTac: string;
      thangSinh: number;
      ngaySinh: number;
    }[];
    danhSachDienBienLuong?: DienBienLuong.IRecord[];
    danhSachThongTinTrinhDoDaoTao?: ThongTinTrinhDoDaoTao.IRecord[];
    viTriUngTuyen?: string;
    khac?: string;
    taiLieuUngTuyen?: { file: string[] }[];
    trangThaiHoSoUngTuyen: ETrangThaiHoSoUngTuyen;
    thongTinUngTuyen?: {
      chucVu?: ChucVu.IRecord;
      chucVuId?: ChucVu.IRecord["_id"];
      donVi?: DonVi.IRecord;
      donViId?: DonVi.IRecord["_id"];
      dotTuyenDung?: DotTuyenDung.IRecordDotTuyenDung;
      dotTuyenDungId?: DotTuyenDung.IRecordDotTuyenDung["_id"];
    };
    maHoSo?: string;
  }
  export interface ThongTinTraCuu {
    buoc1: true;
    buoc2: true;
    buoc3: false;
    buoc4: false;
    hoSo: IRecord;
    thongTinThi: {
      _id: "65af9e95a2b74736c99648e3";
      ghiChuDiemTuyenDung: any;
      dotTuyenDungId: "659917b22f52e730ab302c80";
      diemThiGiang: any;
      diemThiViet: any;
      diemPhongVan: any;
      diemUuTien: any;
      tongDiem: any;
      hoSoUngTuyenId: "65ad350320cb954a10e7f350";
      urlFileMinhChung: any;
      viTriUngTuyenId: "65a80ec41058c5b824eb1898";
      cccdCMND: "099882334567";
      maHoSo: "UV202412.01147";
      hoTen: "Chu Đức Chính";
      sdtCaNhan: any;
      email: "7a3dqh@gmail.com";
      donVi: any;
      viTriViecLam: any;
      lichThiViet: "2024-01-09T11:10:21.003Z";
      lichThiGiang: "2024-01-17T11:10:24.034Z";
      lichThiPhongVan: "2024-01-31T11:10:26.420Z";
      guiMailDiem: false;
      guiMailThi: false;
      lichThiPhongVanFormat: "18:10 31/01/2024";
      lichThiGiangFormat: "18:10 17/01/2024";
      lichThiVietFormat: "18:10 09/01/2024";
      namSinh: any;
      queQuan: any;
      trinhDoChuyenMon: any;
      trungTuyen: any;
      lichSuSuaDiem: any;
      createdAt: "2024-01-23T11:10:13.343Z";
      updatedAt: "2024-01-23T11:10:28.936Z";
      hoSoUngTuyen: IRecord;
      viTriUngTuyen: {
        _id: "65a80ec41058c5b824eb1898";
        dotTuyenDungId: "659917b22f52e730ab302c80";
        donViId: "652e1ccdf1730e6b5d9000da";
        chucVuId: "652e0d31f1730e6b5d8ff8d6";
        donViViTriId: "652e0d31f1730e6b5d8ff8df";
        soLuong: 1;
        trinhDoChuyenMon: "Bác sĩ nội trú";
        trinhDoChuyenMonId: "652e1341f1730e6b5d8fffb3";
        tenViTri: "Phó Trưởng bộ môn và tương đương";
        yeuCau: "gg";
        createdAt: "2024-01-17T17:30:44.044Z";
        updatedAt: "2024-01-23T07:48:05.182Z";
      };
    };
    viTri: {
      _id: "65a80ec41058c5b824eb1898";
      dotTuyenDungId: "659917b22f52e730ab302c80";
      donViId: "652e1ccdf1730e6b5d9000da";
      chucVuId: "652e0d31f1730e6b5d8ff8d6";
      donViViTriId: "652e0d31f1730e6b5d8ff8df";

      soLuong: 1;
      trinhDoChuyenMon: "Bác sĩ nội trú";
      trinhDoChuyenMonId: "652e1341f1730e6b5d8fffb3";
      tenViTri: "Phó Trưởng bộ môn và tương đương";
      yeuCau: "gg";
      createdAt: "2024-01-17T17:30:44.044Z";
      updatedAt: "2024-01-23T07:48:05.182Z";
      dotTuyenDung: {
        tenDotTuyenDung: "Tuyển dụng nhân sự năm 2024 (đợt 2)";
        thoiGianBDNhanHoSo: "2024-01-19T04:58:00.018Z";
        thoiGianKTNhanHoSo: "2024-01-31T04:58:03.238Z";
      };
      chucVu: {
        ten: "Phó Trưởng bộ môn và tương đương";
        ma: "06";
      };
      donVi: {
        ten: "Bộ môn Công nghệ thông tin";
        maDonVi: "12.01";
      };
    };
  }
}
