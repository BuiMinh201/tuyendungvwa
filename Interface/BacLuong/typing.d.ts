declare module BacLuong {
  export interface IRecord {
    _id: string;
    suDung: boolean;
    ngachLuongId: string;
    ghiChu: string;
    bacLuong: number;
    heSo: number;
    namApDung: number;
    createdAt: string;
    updatedAt: string;
    ngachLuong: NgachLuong.IRecordHemis;
  }

  export interface IRecordHemis {
    _id: string;
    ten: string;
    ngachLuongId: string;
    ngachLuong?: NgachLuong.IRecordHemis;
    bacLuong: number;
    heSo: number;
    ma: string;
    namApDung: number;
    ghiChu: string;
    suDung: boolean;
    heSo: string;
  }
}
