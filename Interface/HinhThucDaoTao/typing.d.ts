declare module HinhThucDaoTao {
  export interface IRecord {
    _id: string;
    ma: string;
    ten: string;
    soThuTu: number;
    moTa: string;
    suDung: boolean;
    maHinhThucDaoTaoHemis: string;
    hinhThucDaoTaoHemis?: HinhThucDaoTao.IRecord;
  }
}
