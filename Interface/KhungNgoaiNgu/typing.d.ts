declare module KhungNangLucNgoaiNgu {
  export interface IRecord {
    _id: string;
    ma: string;
    tenNgonNgu: string;
    khungNangLuc: string;
    soThuTu: number;
    moTa: string;
    suDung: boolean;
  }
}
