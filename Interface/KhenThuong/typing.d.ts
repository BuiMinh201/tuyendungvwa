declare module KhenThuong {
  export interface HinhThucKhenThuong {
    _id: string;
    ma: string;
    ten: string;
    soThuTu: number;
    moTa: string;
    suDung: boolean;
    loaiKhenThuongId: string;
    loaiKhenThuong: LoaiKhenThuong;
    maHinhThucKhenThuongHemis: string;
    hinhThucKhenThuongHemis: HinhThucKhenThuong;
    capKhenThuongId: string;
    loaiKhenThuongHemisId: string;
    loaiKhenThuongHemis: LoaiKhenThuong;
  }
  export interface LoaiKhenThuong {
    _id: string;
    ma: string;
    ten: string;
    soThuTu: number;
    moTa: string;
    suDung: boolean;
    maLoaiKhenThuongHemis: string;
    loaiKhenThuongHemis: LoaiKhenThuong;
  }
  export interface PhuongThucKhenThuong {
    _id: string;
    ma: string;
    ten: string;
    soThuTu: number;
    moTa: string;
    suDung: boolean;
    maPhuongThucKhenThuongHemis: string;
    phuongThucKhenThuongHemis: PhuongThucKhenThuong;
  }
  export interface CapKhenThuong {
    _id: string;
    ma: string;
    ten: string;
    soThuTu: number;
    moTa: string;
    suDung: boolean;
    maCapKhenThuongHemis: string;
    capKhenThuongHemis: CapKhenThuong;
  }
}
