declare module TonGiao {
  export interface IRecord {
    _id: string;
    ma: string;
    tenTonGiao: string;
  }
}
