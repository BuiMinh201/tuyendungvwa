declare module TrinhDoDaoTao {
  export interface IRecord {
    _id: string;
    ma: string;
    ten: string;
    soThuTu: number;
    moTa: string;
    suDung: boolean;
    maTrinhDoDaoTaoHemis: string;
    trinhDoDaoTaoHemis?: TrinhDoDaoTao.IRecord;
  }
}
