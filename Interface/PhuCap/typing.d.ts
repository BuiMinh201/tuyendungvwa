import { type EHinhThucHuong } from '../constant';

declare module LoaiPhuCap {
  export interface IRecord {
    _id: string;
    ma: string;
    ten: string;
    soThuTu: number;
    moTa: string;
    heSoPhanTramHuong: number;
    suDung: boolean;
    coMucPhuCap: boolean;
    hinhThucHuong?: EHinhThucHuong;
    phuCapBHXH: boolean;
    thoiGianLenPhuCap?: number;
    isChinhSua?: boolean;
  }

  export interface IRecordMucPhuCap {
    _id: number;
    ten: number;
    mucPhuCap: number;
    loaiPhuCapId: string;
    trangThai: boolean;
    isChinhSua?: boolean;
  }
}
