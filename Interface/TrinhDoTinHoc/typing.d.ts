declare module TrinhDoTinHoc {
  export interface IRecord {
    _id: string;
    ma: string;
    trinhDoTinHoc: string;
    soThuTu: number;
    moTa: string;
    suDung: boolean;
  }
}
