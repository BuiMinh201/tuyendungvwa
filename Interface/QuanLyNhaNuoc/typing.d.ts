declare module TrinhDoQuanLyNhaNuoc {
  export interface IRecord {
    _id: string;
    ma: string;
    ten: string;
    soThuTu: number;
    moTa: string;
    suDung: boolean;
    maTrinhDoQuanLyNhaNuocHemis: string;
    trinhDoQuanLyNhaNuocHemis?: TrinhDoQuanLyNhaNuoc.IRecord;
  }
}
