declare module KyLuat {
  export interface CapKyLuat {
    _id: string;
    ma: string;
    ten: string;
    soThuTu: number;
    moTa: string;
    suDung: boolean;
    maCapKyLuatHemis: string;
    capKyLuatHemis: CapKyLuat;
  }
  export interface HinhThucKyLuat {
    _id: string;
    ma: string;
    ten: string;
    soThuTu: number;
    moTa: string;
    suDung: boolean;
    capKyLuatId: string;
    capKyLuat: CapKyLuat.IRecord;
    maHinhThucKyLuatHemis: string;
    hinhThucKyLuatHemis: HinhThucKyLuat;
    capKyLuatHemisId: string;
    capKyLuatHemis: CapKyLuat.IRecord;
  }
}
