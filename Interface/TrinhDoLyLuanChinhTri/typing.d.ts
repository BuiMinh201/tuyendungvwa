declare module TrinhDoLyLuanChinhTri {
  export interface IRecord {
    _id: string;
    ma: string;
    ten: string;
    soThuTu: number;
    moTa: string;
    suDung: boolean;
    maTrinhDoLyLuanChinhTriHemis: string;
    trinhDoLyLuanChinhTriHemis?: TrinhDoLyLuanChinhTri.IRecord;
  }
}
