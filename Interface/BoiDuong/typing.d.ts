declare module LoaiBoiDuong {
  export interface IRecord {
    _id: string;
    ma: string;
    ten: string;
    soThuTu: number;
    moTa: string;
    suDung: boolean;
    maLoaiBoiDuongHemis: string;
    loaiBoiDuongHemis: LoaiBoiDuong.IRecord;
  }
}
